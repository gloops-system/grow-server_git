﻿using System;
using System.Collections.Generic;
//using System.Configuration;
//using System.Diagnostics;
using System.IO;
//using System.Linq;
//using System.Reflection;
//using System.Threading;
//using System.Threading.Tasks;
//using Edge.Data.DataSet.ImageDataSetTableAdapters;

namespace Grow.Image.Batch
{
    public class ImageInfo
    {
        public string File;
        public DateTimeOffset Timestamp;
    }

    class Program
    {

        static void Main(string[] args)
        {
            //string appPath = Assembly.GetEntryAssembly().Location;
            //Console.WriteLine(appPath + "を実行します");
            ////config読み込み
            //string currentDir = Directory.GetCurrentDirectory();
            ////string imagePath = currentDir + "\\Image\\";
            ////string repository = "http://dev.mybooth.jp:9000/svn/Edge/trunk/Image/";
            ////string svnDotExe = @"C:\Program Files (x86)\VisualSVN\bin\svn.exe";
            //string imagePath = currentDir + ConfigurationManager.AppSettings["ImagePath"];
            //string repository = ConfigurationManager.AppSettings["Repository"];
            //string svnDotExe = ConfigurationManager.AppSettings["SvnDotExt"];

            ////svn.exeが存在するか
            //bool existSvnExe = File.Exists(svnDotExe);
            //if (!existSvnExe)
            //{
            //    Console.WriteLine(svnDotExe + "が存在しません。インストールしてください。");
            //    Console.ReadLine();
            //    return;
            //}

            ////.svnが存在位するかどうか
            //bool existDotSvn = File.Exists(imagePath);

            //ProcessStartInfo psInfo = new ProcessStartInfo();
            //psInfo.FileName = svnDotExe;
            //psInfo.CreateNoWindow = false;
            //psInfo.UseShellExecute = true;
            //psInfo.Arguments = existDotSvn ? "update " + repository :
            //    "checkout " + repository;
            //var p = Process.Start(psInfo);
            //p.WaitForExit();

            ////svn.exe正常終了
            //if (p.ExitCode == 0)
            //{
            //    var files = GetFileList(imagePath);

            //    var lastTimestamp = DateTimeOffset.MinValue;
            //    var imageRevision = 0;
            //    using (var imageRevisionAdapter = new ImageRevisionTableAdapter())
            //    {
            //        var row = imageRevisionAdapter.GetData().FirstOrDefault();
            //        if (row != null)
            //        {
            //            lastTimestamp = row.LastTimestamp;
            //            imageRevision = row.ImageRevision;
            //        }
            //    }

            //    Uri baseUri = new Uri(imagePath);
            //    var imageList = files.Where(x =>
            //    {
            //        //.svnの中は無視
            //        return !x.Contains(".svn");
            //    }).Select(x =>
            //    {
            //        //相対パスと更新日時を作成
            //        Uri imageUri = new Uri(x);
            //        string relativeUriString = baseUri.MakeRelativeUri(imageUri).ToString();
            //        DateTimeOffset dtUpdate = File.GetLastWriteTime(x);
            //        return new ImageInfo
            //            {
            //                File = relativeUriString,
            //                Timestamp = dtUpdate
            //            };
            //    }).Where(x =>
            //    {
            //        //最終更新日時以降のものだけピックアップ
            //        //ファイルタンプスタンプにはミリ秒まで含まれているのでDBのタイムスタンプは一秒足してから比較
            //        int result = x.Timestamp.CompareTo(lastTimestamp.AddMinutes(1));
            //        if(result > 0) Console.WriteLine(x.Timestamp.ToString("o") + ":" + x.File);
            //        return result > 0;
            //    }).ToList();

            //    //更新するべきものがなければ終了
            //    if (!imageList.Any())
            //    {
            //        Console.WriteLine("イメージの更新なし");
            //        Console.WriteLine("何かキーを押してください...");
            //        Console.ReadLine();
            //        return;
            //    }

            //    int total = imageList.Count();
            //    Console.WriteLine("総イメージ数: {0}", total);
            //    //ImageMasterアップデート
            //    int i = 0;
            //    int cursolposition = Console.CursorTop;
            //    Parallel.ForEach(imageList, x =>
            //        {
            //            using (var imageMasterAdapter = new ImageMasterTableAdapter())
            //            {
            //                int num = imageMasterAdapter.UpdateImage(x.File, x.Timestamp);
            //                if (num == 0)
            //                {
            //                    imageMasterAdapter.InsertImage(x.File, x.Timestamp);
            //                }

            //                int progress = Interlocked.Increment(ref i);
            //                if (progress % 10 == 0)
            //                {
            //                    Console.Write("タイムスタンプ更新中... " + progress + "/" + total);
            //                    Console.SetCursorPosition(0, cursolposition);
            //                }
            //            }
            //        });
            //    Console.SetCursorPosition(0, cursolposition);

            //    lastTimestamp = imageList.Max(x => x.Timestamp);

            //    //ImageRevisionのLastTimestampを更新する
            //    using(var imageRevisionAdapter = new ImageRevisionTableAdapter()){
            //        int num = imageRevisionAdapter.UpdateImageRevision(imageRevision, lastTimestamp);
            //        if (num == 0)
            //        {
            //            imageRevisionAdapter.InsertImageRevision(imageRevision, lastTimestamp);
            //        }
            //    }
            //    Console.WriteLine("イメージタイムスタンプが更新されました");
            //    Console.WriteLine("タイムスタンプはSubversionからアップデートした時間です");
            //    Console.WriteLine("何かキーを押してください...");
            //    Console.ReadLine();
            //}
            //else
            //{
            //    Console.WriteLine("svn.exe 実行エラー ");
            //    Console.WriteLine("何かキーを押してください...");
            //    Console.ReadLine();
            //}
        }

        static IEnumerable<string> GetFileList(string dir)
        {
            foreach (var sub in Directory.GetDirectories(dir))
            {
                foreach (var file in GetFileList(sub))
                {
                    yield return file;
                }
            }

            foreach (var file in Directory.GetFiles(dir))
            {
                yield return file;
            }
        }
    }
}
