﻿using Fango.Core.Data;
using Edge.Data.Gacha.DataSet.GachaDataSetTableAdapters;

namespace Edge.Data.Gacha.DataSet.Adapter.Gacha
{
    public class GachaUserInfoDataRepositoryAdapter
    {
        /// <summary>
        /// ガチャ実行回数の更新
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="gachaId"></param>
        /// <param name="point"></param>
        /// <param name="executeCount"></param>
        public void UpdateOrAddGacheUserInfo(int userId, int gachaId, int point, int executeCount)
        {
            new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var gachaUserInfoTableAdapter = new GachaUserInfoTableAdapter())
                    {
                        gachaUserInfoTableAdapter.UpdateOrAddGacheUserInfo(userId, gachaId, executeCount, point);
                    }
                });
        }

        private GachaUserInfoTableAdapter _gachaUserInfoTableAdapter;
        public GachaUserInfoTableAdapter GachaUserInfoTableAdapter
        {
            get { return _gachaUserInfoTableAdapter ?? (_gachaUserInfoTableAdapter = new GachaUserInfoTableAdapter()); }
        }

        private GachaDataSet.GachaUserInfoDataTable _gachaUserInfoDataTable;
        protected GachaDataSet.GachaUserInfoDataTable GachaUserInfoDataTable
        {
            get { return _gachaUserInfoDataTable ?? (_gachaUserInfoDataTable = new GachaDataSet.GachaUserInfoDataTable()); }
        }
    }
}