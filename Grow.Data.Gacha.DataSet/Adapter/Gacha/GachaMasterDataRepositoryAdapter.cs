﻿using System;
using Fango.Core.Data;
using Edge.Data.Gacha.DataSet.Caching;

namespace Edge.Data.Gacha.DataSet.Adapter.Gacha
{
    public class GachaMasterDataRepositoryAdapter
    {
        /// <summary>
        /// 開催中のガチャマスタを取得
        /// </summary>
        /// <returns></returns>
        public GachaDataSet.GachaMasterRow[] GetCurrentData(DateTimeOffset now)
        {
            var gachaMasterRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    var gachaMasterTableAdapter = GachaMasterCacheTableAdapter.GetInstance();
                    return gachaMasterTableAdapter.GetCurrentData(now);
                });
            return gachaMasterRows;
        }

        /// <summary>
        /// PKで取得
        /// </summary>
        /// <param name="gachaId"></param>
        /// <returns></returns>
        public GachaDataSet.GachaMasterRow GetDataByPK(int gachaId)
        {
            var gachaMasterRow = new DbAccessHandler().ExecuteAction(() =>
                {
                    var gachaMasterTableAdapter = GachaMasterCacheTableAdapter.GetInstance();
                    return gachaMasterTableAdapter.GetDataByPK(gachaId);
                });
            return gachaMasterRow;
        }
    }
}