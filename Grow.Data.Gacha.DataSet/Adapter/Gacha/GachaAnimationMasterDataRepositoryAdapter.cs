﻿using Fango.Core.Data;
using Edge.Data.Gacha.DataSet.Caching;

namespace Edge.Data.Gacha.DataSet.Adapter.Gacha
{
    public class GachaAnimationMasterDataRepositoryAdapter
    {
        public GachaDataSet.GachaAnimationMasterRow[] GetDataByGachaModeAndRank(int gachaMode, int rank)
        {
            var gachaAdjustPointMasterRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    var gachaAdjustPointMasterTableAdapter = GachaAnimationMasterCacheTableAdapter.GetInstance();
                    return gachaAdjustPointMasterTableAdapter.GetDataByGachaModeAndRank(gachaMode, rank);
                });
            return gachaAdjustPointMasterRows;
        }
    }
}