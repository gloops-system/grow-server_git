﻿using Fango.Core.Data;
using Edge.Data.Gacha.DataSet.Caching;

namespace Edge.Data.Gacha.DataSet.Adapter.Gacha
{
    public class GachaRankMasterDataRepositoryAdapter
    {
        /// <summary>
        /// 全取得
        /// </summary>
        /// <returns></returns>
        public GachaDataSet.GachaRankMasterRow[] GetData()
        {
            var gachaRankMasterRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    var gachaRankMasterCacheTableAdapter = GachaRankMasterCacheTableAdapter.GetInstance();
                    return gachaRankMasterCacheTableAdapter.GetData();
                });
            return gachaRankMasterRows;
        }

        /// <summary>
        /// GachaModeで取得
        /// </summary>
        /// <param name="gachaMode"></param>
        /// <returns></returns>
        public GachaDataSet.GachaRankMasterRow[] GetDataByGachaMode(int gachaMode)
        {
            var gachaRankMasterRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    var gachaRankMasterCacheTableAdapter = GachaRankMasterCacheTableAdapter.GetInstance();
                    return gachaRankMasterCacheTableAdapter.GetDataByGachaMode(gachaMode);
                });
            return gachaRankMasterRows;
        }
    }
}