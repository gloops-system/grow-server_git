﻿using Fango.Core.Data;
using Edge.Data.Gacha.DataSet.Caching;

namespace Edge.Data.Gacha.DataSet.Adapter.Gacha
{
    public class GachaAdjustPointMasterDataRepositoryAdapter
    {
        public GachaDataSet.GachaAdjustPointMasterRow GetDataByPK(int gachaModeId, int rank)
        {
            var gachaAdjustPointMasterRow = new DbAccessHandler().ExecuteAction(() =>
                {
                    var gachaAdjustPointMasterTableAdapter = GachaAdjustPointMasterCacheTableAdapter.GetInstance();
                    return gachaAdjustPointMasterTableAdapter.GetDataByPK(gachaModeId, rank);
                });
            return gachaAdjustPointMasterRow;
        }
    }
}