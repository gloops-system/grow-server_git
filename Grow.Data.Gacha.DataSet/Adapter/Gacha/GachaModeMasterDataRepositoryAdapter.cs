﻿using System.Linq;
using Fango.Core.Data;
using Edge.Data.Gacha.DataSet.Caching;

namespace Edge.Data.Gacha.DataSet.Adapter.Gacha
{
    public class GachaModeMasterDataRepositoryAdapter
    {
        /// <summary>
        /// 全取得
        /// </summary>
        /// <returns></returns>
        public GachaDataSet.GachaModeMasterRow[] GetDataMaster()
        {
            var gachaModeMasterRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    var gachaModeMasterCacheTableAdapter = GachaModeMasterCacheTableAdapter.GetInstance();
                    return gachaModeMasterCacheTableAdapter.GetData();
                });
            return gachaModeMasterRows;
        }

        /// <summary>
        /// GachaModeIdで取得
        /// </summary>
        /// <param name="gachaModeId"></param>
        /// <returns></returns>
        public GachaDataSet.GachaModeMasterRow[] GetDataByGachaModeId(int gachaModeId)
        {
            var gachaModeMasterRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    var gachaModeMasterCacheTableAdapter = GachaModeMasterCacheTableAdapter.GetInstance();
                    return gachaModeMasterCacheTableAdapter.GetDataByGachaModeId(gachaModeId);
                });
            return gachaModeMasterRows;
        }

        /// <summary>
        /// GachaModeとGachaModeIdで取得
        /// </summary>
        /// <param name="gachaMode"></param>
        /// <param name="gachaModeId"></param>
        /// <returns></returns>
        public GachaDataSet.GachaModeMasterRow GetGachaModeMasterByGachaModeAndGachaModeId(int gachaMode, int gachaModeId)
        {
            var gachaModeMasterRow = new DbAccessHandler().ExecuteAction(() =>
                {
                    var gachaModeMasterCacheTableAdapter = GachaModeMasterCacheTableAdapter.GetInstance();
                    return
                        gachaModeMasterCacheTableAdapter.GetDataByGachaModeId(gachaModeId)
                                                        .FirstOrDefault(x => x.GachaMode == gachaMode);
                });
            return gachaModeMasterRow;
        }
    }
}