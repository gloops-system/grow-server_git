﻿using Fango.Core.Data;
using Edge.Data.Gacha.DataSet.Caching;

namespace Edge.Data.Gacha.DataSet.Adapter.Gacha
{
    public class GachaItemMasterDataRepositoryAdapter
    {
        /// <summary>
        /// GachaModeとRankで取得
        /// </summary>
        /// <param name="gachaMode"></param>
        /// <param name="rank"></param>
        /// <returns></returns>
        public GachaDataSet.GachaItemMasterRow[] GetDataByGachaModeAndRank(int gachaMode, int rank)
        {
            var gachaItemMasterRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    var gachaItemMasterTableAdapter = GachaItemMasterCacheTableAdapter.GetInstance();
                    return gachaItemMasterTableAdapter.GetDataByGachaModeAndRank(gachaMode, rank);
                });
            return gachaItemMasterRows;
        }
    }
}