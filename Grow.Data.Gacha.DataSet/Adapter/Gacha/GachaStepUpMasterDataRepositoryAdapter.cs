﻿using Fango.Core.Data;
using Edge.Data.Gacha.DataSet.Caching;

namespace Edge.Data.Gacha.DataSet.Adapter.Gacha
{
    public class GachaStepUpMasterDataRepositoryAdapter
    {
        public GachaDataSet.GachaStepUpMasterRow[] GetDataByGachaId(int gachaId)
        {
            var gachaStepUpMasterRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    var gachaStepUpMasterTableAdapter = GachaStepUpMasterCacheTableAdapter.GetInstance();
                    return gachaStepUpMasterTableAdapter.GetDataByGachaId(gachaId);
                });
            return gachaStepUpMasterRows;
        }
    }
}