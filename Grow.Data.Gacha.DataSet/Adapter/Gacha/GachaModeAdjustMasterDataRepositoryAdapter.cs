﻿using Fango.Core.Data;
using Edge.Data.Gacha.DataSet.Caching;

namespace Edge.Data.Gacha.DataSet.Adapter.Gacha
{
    public class GachaModeAdjustMasterDataRepositoryAdapter
    {
        /// <summary>
        /// 全取得
        /// </summary>
        /// <returns></returns>
        public GachaDataSet.GachaModeAdjustMasterRow[] GetDataMaster()
        {
            var gachaModeAdjustMasterRows = new DbAccessHandler().ExecuteAction(() =>
            {
                var gachaModeAdjustMasterCacheTableAdapter = GachaModeAdjustMasterCacheTableAdapter.GetInstance();
                return gachaModeAdjustMasterCacheTableAdapter.GetData();
            });
            return gachaModeAdjustMasterRows;
        }

        /// <summary>
        /// GachaModeIdで取得
        /// </summary>
        /// <param name="gachaModeId"></param>
        /// <returns></returns>
        public GachaDataSet.GachaModeAdjustMasterRow[] GetDataByGachaModeId(int gachaModeId)
        {
            var gachaModeAdjustMasterRows = new DbAccessHandler().ExecuteAction(() =>
            {
                var gachaModeAdjustMasterCacheTableAdapter = GachaModeAdjustMasterCacheTableAdapter.GetInstance();
                return gachaModeAdjustMasterCacheTableAdapter.GetDataByGachaModeId(gachaModeId);
            });
            return gachaModeAdjustMasterRows;
        }
    }
}