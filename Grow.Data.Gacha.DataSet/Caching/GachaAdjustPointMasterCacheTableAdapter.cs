﻿using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Edge.Data.Gacha.DataSet.GachaDataSetTableAdapters;
using Edge.Extensions.Core;

namespace Edge.Data.Gacha.DataSet.Caching
{
    [Preload]
    internal class GachaAdjustPointMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static Dictionary<string, GachaDataSet.GachaAdjustPointMasterRow> _cacheGetDataByPK;

        private readonly static GachaAdjustPointMasterCacheTableAdapter Instance = new GachaAdjustPointMasterCacheTableAdapter();
        private GachaAdjustPointMasterCacheTableAdapter() { }

        public static GachaAdjustPointMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new GachaAdjustPointMasterTableAdapter())
            {
                _cacheGetDataByPK = adapter.GetData().ToDictionary(x => GenKey(x.GachaModeId, x.Rank));
            }
        }

        public GachaDataSet.GachaAdjustPointMasterRow GetDataByPK(int gachaModeId, int rank)
        {
            if (CACHE_ENABLED)
                return GetDataByPKFromCache(_cacheGetDataByPK, GenKey(gachaModeId, rank));

            using (var adapter = new GachaAdjustPointMasterTableAdapter())
            {
                return adapter.GetDataByPK(gachaModeId, rank).FirstOrDefault();
            }
        }
    }
}