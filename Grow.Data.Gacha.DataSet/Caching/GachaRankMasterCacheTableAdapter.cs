﻿using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Edge.Data.Gacha.DataSet.GachaDataSetTableAdapters;
using Edge.Extensions.Core;

namespace Edge.Data.Gacha.DataSet.Caching
{
    [Preload]
    internal class GachaRankMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static Dictionary<int, GachaDataSet.GachaRankMasterRow[]> _cacheGetDataByGachaMode;
        private GachaDataSet.GachaRankMasterRow[] _allData;

        private readonly static GachaRankMasterCacheTableAdapter Instance = new GachaRankMasterCacheTableAdapter();
        private GachaRankMasterCacheTableAdapter() { }

        public static GachaRankMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new GachaRankMasterTableAdapter())
            {
                _allData = adapter.GetData().ToArray();
                _cacheGetDataByGachaMode = _allData.GenerateCache(x => x.GachaMode);
            }
        }

        public GachaDataSet.GachaRankMasterRow[] GetData()
        {
            if (CACHE_ENABLED)
                return GetDataFromCache(_allData).ToArray();

            using (var adapter = new GachaRankMasterTableAdapter())
            {
                return adapter.GetData().ToArray();
            }
        }

        public GachaDataSet.GachaRankMasterRow[] GetDataByGachaMode(int gachaMode)
        {
            if (CACHE_ENABLED)
            {
                return GetDataByFromCache(_cacheGetDataByGachaMode, gachaMode);
            }

            using (var adapter = new GachaRankMasterTableAdapter())
            {
                return adapter.GetDataByGachaMode(gachaMode).ToArray();
            }
        }
    }
}