﻿using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Edge.Data.Gacha.DataSet.GachaDataSetTableAdapters;
using Edge.Extensions.Core;

namespace Edge.Data.Gacha.DataSet.Caching
{
    [Preload]
    internal class GachaModeAdjustMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static Dictionary<int, GachaDataSet.GachaModeAdjustMasterRow[]> _cacheGetDataByGachaModeId;
        private GachaDataSet.GachaModeAdjustMasterRow[] _allData;

        private readonly static GachaModeAdjustMasterCacheTableAdapter Instance = new GachaModeAdjustMasterCacheTableAdapter();
        private GachaModeAdjustMasterCacheTableAdapter() { }

        public static GachaModeAdjustMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new GachaModeAdjustMasterTableAdapter())
            {
                _allData = adapter.GetData().ToArray();
                _cacheGetDataByGachaModeId = _allData.GenerateCache(x => x.GachaModeId);
            }
        }

        public GachaDataSet.GachaModeAdjustMasterRow[] GetData()
        {
            if (CACHE_ENABLED)
                return GetDataFromCache(_allData).ToArray();
            
            using (var adapter = new GachaModeAdjustMasterTableAdapter())
            {
                return adapter.GetData().ToArray();
            }
        }

        public GachaDataSet.GachaModeAdjustMasterRow[] GetDataByGachaModeId(int gachaModeId)
        {
            if (CACHE_ENABLED)
                return GetDataByFromCache(_cacheGetDataByGachaModeId, gachaModeId);

            using (var adapter = new GachaModeAdjustMasterTableAdapter())
            {
                return adapter.GetDataByGachaModeId(gachaModeId).ToArray();
            }
        }
    }
}