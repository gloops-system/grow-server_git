﻿using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Edge.Data.Gacha.DataSet.GachaDataSetTableAdapters;
using Edge.Extensions.Core;

namespace Edge.Data.Gacha.DataSet.Caching
{
    [Preload]
    internal class GachaModeMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static Dictionary<int, GachaDataSet.GachaModeMasterRow[]> _cacheGetDataByGachaModeId;
        private GachaDataSet.GachaModeMasterRow[] _allData;

        private readonly static GachaModeMasterCacheTableAdapter Instance = new GachaModeMasterCacheTableAdapter();
        private GachaModeMasterCacheTableAdapter() { }

        public static GachaModeMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new GachaModeMasterTableAdapter())
            {
                _allData = adapter.GetData().ToArray();
                _cacheGetDataByGachaModeId = _allData.GenerateCache(x => x.GachaModeId);
            }
        }

        public GachaDataSet.GachaModeMasterRow[] GetData()
        {
            if (CACHE_ENABLED)
                return GetDataFromCache(_allData).ToArray();

            using (var adapter = new GachaModeMasterTableAdapter())
            {
                return adapter.GetData().ToArray();
            }
        }

        public GachaDataSet.GachaModeMasterRow[] GetDataByGachaModeId(int gachaModeId)
        {
            if (CACHE_ENABLED)
                return GetDataByFromCache(_cacheGetDataByGachaModeId, gachaModeId);

            using (var adapter = new GachaModeMasterTableAdapter())
            {
                return adapter.GetDataByGachaModeId(gachaModeId).ToArray();
            }
        }
    }
}