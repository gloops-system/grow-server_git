﻿using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Edge.Data.Gacha.DataSet.GachaDataSetTableAdapters;
using Edge.Extensions.Core;

namespace Edge.Data.Gacha.DataSet.Caching
{
    [Preload]
    internal class GachaItemMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static Dictionary<string, GachaDataSet.GachaItemMasterRow[]> _cacheGetDataByGachaModeAndRank;

        private readonly static GachaItemMasterCacheTableAdapter Instance = new GachaItemMasterCacheTableAdapter();
        private GachaItemMasterCacheTableAdapter() { }

        public static GachaItemMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new GachaItemMasterTableAdapter())
            {
                _cacheGetDataByGachaModeAndRank = adapter.GetData().GenerateCache(x => GenKey(x.GachaMode, x.Rank));
            }
        }

        public GachaDataSet.GachaItemMasterRow[] GetDataByGachaModeAndRank(int gachaMode, int rank)
        {
            if (CACHE_ENABLED)
                return GetDataByFromCache(_cacheGetDataByGachaModeAndRank, GenKey(gachaMode, rank));

            using (var adapter = new GachaItemMasterTableAdapter())
            {
                return adapter.GetDataByGachaModeAndRank(gachaMode, rank).ToArray();
            }
        }
    }
}