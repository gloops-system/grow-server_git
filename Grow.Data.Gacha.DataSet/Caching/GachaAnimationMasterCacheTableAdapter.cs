﻿using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Edge.Data.Gacha.DataSet.GachaDataSetTableAdapters;
using Edge.Extensions.Core;

namespace Edge.Data.Gacha.DataSet.Caching
{
    [Preload]
    internal class GachaAnimationMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static Dictionary<string, GachaDataSet.GachaAnimationMasterRow[]> _cacheGetDataByGachaModeAndRank;

        private readonly static GachaAnimationMasterCacheTableAdapter Instance = new GachaAnimationMasterCacheTableAdapter();
        private GachaAnimationMasterCacheTableAdapter() { }

        public static GachaAnimationMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new GachaAnimationMasterTableAdapter())
            {
                _cacheGetDataByGachaModeAndRank = adapter.GetData().GenerateCache(x => GenKey(x.GachaMode, x.Rank));
            }
        }

        public GachaDataSet.GachaAnimationMasterRow[] GetDataByGachaModeAndRank(int gachaMode, int rank)
        {
            if (CACHE_ENABLED)
                return GetDataByFromCache(_cacheGetDataByGachaModeAndRank, GenKey(gachaMode, rank)).ToArray();

            using (var adapter = new GachaAnimationMasterTableAdapter())
            {
                return adapter.GetDataByGachaModeAndRank(gachaMode, rank).ToArray();
            }
        }
    }
}