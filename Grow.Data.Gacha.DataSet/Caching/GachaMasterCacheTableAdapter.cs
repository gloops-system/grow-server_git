﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Edge.Data.Gacha.DataSet.GachaDataSetTableAdapters;
using Edge.Extensions.Core;

namespace Edge.Data.Gacha.DataSet.Caching
{
    [Preload]
    internal class GachaMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static Dictionary<int, GachaDataSet.GachaMasterRow> _cacheGetDataByPK;
        private GachaDataSet.GachaMasterRow[] _allData;

        private readonly static GachaMasterCacheTableAdapter Instance = new GachaMasterCacheTableAdapter();
        private GachaMasterCacheTableAdapter() { }

        public static GachaMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new GachaMasterTableAdapter())
            {
                _allData = adapter.GetData().ToArray();
                _cacheGetDataByPK = _allData.ToDictionary(x => x.GachaId);
            }
        }

        public GachaDataSet.GachaMasterRow[] GetCurrentData(DateTimeOffset now)
        {
            if (CACHE_ENABLED)
                GetDataFromCache(_allData).Where(x => now.InBetween(x.FromDate, x.ToDate)).ToArray();

            using (var adapter = new GachaMasterTableAdapter())
            {
                return adapter.GetCurrentData(now).ToArray();
            }
        }

        public GachaDataSet.GachaMasterRow GetDataByPK(int gachaId)
        {
            if (CACHE_ENABLED)
                GetDataByPKFromCache(_cacheGetDataByPK, gachaId);

            using (var adapter = new GachaMasterTableAdapter())
            {
                return adapter.GetDataByPK(gachaId).FirstOrDefault();
            }
        }
    }
}