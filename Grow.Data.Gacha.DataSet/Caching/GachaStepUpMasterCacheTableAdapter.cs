﻿using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Edge.Data.Gacha.DataSet.GachaDataSetTableAdapters;
using Edge.Extensions.Core;

namespace Edge.Data.Gacha.DataSet.Caching
{
    [Preload]
    internal class GachaStepUpMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static Dictionary<int, GachaDataSet.GachaStepUpMasterRow[]> _cacheGetDataByGachaId;

        private readonly static GachaStepUpMasterCacheTableAdapter Instance = new GachaStepUpMasterCacheTableAdapter();
        private GachaStepUpMasterCacheTableAdapter() { }

        public static GachaStepUpMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new GachaStepUpMasterTableAdapter())
            {
                _cacheGetDataByGachaId = adapter.GetData().GenerateCache(x => x.GachaId);
            }
        }

        public GachaDataSet.GachaStepUpMasterRow[] GetDataByGachaId(int gachaId)
        {
            if (CACHE_ENABLED)
                return GetDataByFromCache(_cacheGetDataByGachaId, gachaId).ToArray();

            using (var adapter = new GachaStepUpMasterTableAdapter())
            {
                return adapter.GetDataByGachaId(gachaId).ToArray();
            }
        }
    }
}