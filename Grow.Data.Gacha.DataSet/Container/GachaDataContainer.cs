﻿using System;
using Edge.Data.Gacha.DataSet.Adapter.Gacha;

namespace Edge.Data.Gacha.DataSet.Container
{
    public class GachaDataContainer
    {
        // Master
        private readonly Lazy<GachaMasterDataRepositoryAdapter> _gachaMaster;
        public GachaMasterDataRepositoryAdapter GachaMaster { get { return _gachaMaster.Value; } }

        private readonly Lazy<GachaModeMasterDataRepositoryAdapter> _gachaModeMaster;
        public GachaModeMasterDataRepositoryAdapter GachaModeMaster { get { return _gachaModeMaster.Value; } }

        private readonly Lazy<GachaModeAdjustMasterDataRepositoryAdapter> _gachaModeAdjustMaster;
        public GachaModeAdjustMasterDataRepositoryAdapter GachaModeAdjustMaster { get { return _gachaModeAdjustMaster.Value; } }

        private readonly Lazy<GachaRankMasterDataRepositoryAdapter> _gachaRankMaster;
        public GachaRankMasterDataRepositoryAdapter GachaRankMaster { get { return _gachaRankMaster.Value; } }

        private readonly Lazy<GachaItemMasterDataRepositoryAdapter> _gachaItemMaster;
        public GachaItemMasterDataRepositoryAdapter GachaItemMaster { get { return _gachaItemMaster.Value; } }

        private readonly Lazy<GachaAdjustPointMasterDataRepositoryAdapter> _gachaAdjustPointMaster;
        public GachaAdjustPointMasterDataRepositoryAdapter GachaAdjustPointMaster { get { return _gachaAdjustPointMaster.Value; } }

        private readonly Lazy<GachaStepUpMasterDataRepositoryAdapter> _gachaStepUpMaster;
        public GachaStepUpMasterDataRepositoryAdapter GachaStepUpMaster { get { return _gachaStepUpMaster.Value; } }

        private readonly Lazy<GachaAnimationMasterDataRepositoryAdapter> _gachaAnimationMaster;
        public GachaAnimationMasterDataRepositoryAdapter GachaAnimationMaster { get { return _gachaAnimationMaster.Value; } }

        // Transaction
        private readonly Lazy<GachaUserInfoDataRepositoryAdapter> _gachaUserInfo;
        public GachaUserInfoDataRepositoryAdapter GachaUserInfo { get { return _gachaUserInfo.Value; } }

        public GachaDataContainer()
        {
            _gachaMaster = new Lazy<GachaMasterDataRepositoryAdapter>(() => new GachaMasterDataRepositoryAdapter(), true);
            _gachaModeMaster = new Lazy<GachaModeMasterDataRepositoryAdapter>(() => new GachaModeMasterDataRepositoryAdapter(), true);
            _gachaModeAdjustMaster = new Lazy<GachaModeAdjustMasterDataRepositoryAdapter>(() => new GachaModeAdjustMasterDataRepositoryAdapter(), true);
            _gachaRankMaster = new Lazy<GachaRankMasterDataRepositoryAdapter>(() => new GachaRankMasterDataRepositoryAdapter(), true);
            _gachaItemMaster = new Lazy<GachaItemMasterDataRepositoryAdapter>(() => new GachaItemMasterDataRepositoryAdapter(), true);
            _gachaAdjustPointMaster = new Lazy<GachaAdjustPointMasterDataRepositoryAdapter>(() => new GachaAdjustPointMasterDataRepositoryAdapter(), true);
            _gachaStepUpMaster = new Lazy<GachaStepUpMasterDataRepositoryAdapter>(() => new GachaStepUpMasterDataRepositoryAdapter(), true);
            _gachaAnimationMaster = new Lazy<GachaAnimationMasterDataRepositoryAdapter>(() => new GachaAnimationMasterDataRepositoryAdapter(), true);
            _gachaUserInfo = new Lazy<GachaUserInfoDataRepositoryAdapter>(() => new GachaUserInfoDataRepositoryAdapter(), true);
        }
    }
}
