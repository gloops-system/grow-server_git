﻿using System;

public static class EdgeDoubleEx
{
    /// <summary>
    /// 引数のパーセンテージを掛けた数（小数点以下切り上げ）を返す
    /// </summary>
    public static double MultiplyByPercentage(this double source, int percentage)
    {
        return Math.Ceiling(source * percentage / 100.0);
    }

    /// <summary>
    /// 引数の数を掛けた数を返す
    /// </summary>
    public static double MultiplyBy(this double source, double multiplier)
    {
        return source * multiplier;
    }

    /// <summary>
    /// 引数のパーセンテージを掛けた数（小数点以下切り上げ）を足した数を返す
    /// </summary>
    public static double AddMultipliedByPercentage(this double source, int percentage)
    {
        return source + source.MultiplyByPercentage(percentage);
    }


    /// <summary>
    /// 引数のパーセンテージ分ゆらがせた数を返す
    /// </summary>
    public static double MultiplyByRandomPercentage(this double source, int randomRange)
    {
        return source.MultiplyByPercentage(RandomProvider.ThreadRandom.Next(100 - randomRange, 100 + randomRange));
    }

    /// <summary>
    /// 引数のパーセンテージ分ゆらがせた数を足した数を返す
    /// </summary>
    public static double AddMultipliedByRandomPercentage(this double source, int randomRange)
    {
        return source + source.MultiplyByRandomPercentage(randomRange);
    }

    /// <summary>
    /// 指定した桁数へ切り上げて返す
    /// </summary>
    public static double RoundUp(this double source, int digits)
    {
        var coefficient = Math.Pow(10, digits);

        return source > 0 ? Math.Ceiling(source * coefficient) / coefficient :
                    Math.Floor(source * coefficient) / coefficient;
    }
}
