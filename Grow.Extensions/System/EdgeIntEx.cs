﻿using System;

public static class EdgeIntEx
{
    /// <summary>
    /// 引数のパーセンテージを掛けた数（小数点以下切り上げ）を返す
    /// </summary>
    public static int MultiplyByPercentage(this int source, int percentage)
    {
        return (int)Math.Ceiling((double)source * percentage / 100.0);
    }

    /// <summary>
    /// 引数のパーセンテージを掛けた数（小数点以下切り上げ）を返す
    /// </summary>
    public static int MultiplyByPercentage(this int source, long percentage)
    {
        return (int)Math.Ceiling((double)source * percentage / 100.0);
    }

    /// <summary>
    /// 引数のパーセンテージを掛けた数（小数点以下切り上げ）を返す
    /// </summary>
    public static long MultiplyByPercentage(this long source, int percentage)
    {
        return (long)Math.Ceiling((double)source * percentage / 100.0);
    }

    /// <summary>
    /// 引数のパーセンテージを掛けた数（小数点以下切り上げ）を足した数を返す
    /// </summary>
    public static int AddMultipliedByPercentage(this int source, int percentage)
    {
        return source + source.MultiplyByPercentage(percentage);
    }

    /// <summary>
    /// 引数の分母でパーセンテージを計算して返す
    /// </summary>
    public static int CalcPercentage(this int source, int denominator)
    {
        return denominator == 0 ? 0 : (int)Math.Ceiling(source * 100.0 / denominator);
    }

    /// <summary>
    /// 引数の分母でパーセンテージを計算して返す
    /// </summary>
    public static long CalcPercentage(this long source, long denominator)
    {
        return denominator == 0 ? 0 : (long)Math.Ceiling(source * 100.0 / denominator);
    }

    /// <summary>
    /// 引数のパーセンテージ分ゆらがせた数を返す
    /// </summary>
    public static int MultiplyByRandomPercentage(this int source, int randomRange)
    {
        return source.MultiplyByPercentage(RandomProvider.ThreadRandom.Next(100 - randomRange, 100 + randomRange));
    }

    /// <summary>
    /// 引数のパーセンテージ分ゆらがせた数を足した数を返す
    /// </summary>
    public static int AddMultipliedByRandomPercentage(this int source, int randomRange)
    {
        return source + source.MultiplyByRandomPercentage(randomRange);
    }
}
