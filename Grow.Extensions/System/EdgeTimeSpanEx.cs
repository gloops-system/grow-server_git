﻿using System;

public static class EdgeTimeSpanEx
{
    /// <summary>
    /// 期間を表す文字列を返す
    /// </summary>
    public static string ToTimeSpanString(this TimeSpan span)
    {
        if (span.TotalSeconds <= 0)
            return "";

        if (span.TotalMinutes < 1)
            return string.Format("{0}秒", span.Seconds);

        if (span.TotalHours < 1)
            return string.Format("{0}分{1}", span.Minutes, span.Seconds > 0 ? string.Format("{0}秒", span.Seconds) : "");

        if (span.TotalDays < 1)
            return string.Format("{0}時間{1}", span.Hours, span.Minutes > 0 ? string.Format("{0}分", span.Minutes) : "");

        return string.Format("{0}日{1}{2}", span.Days, span.Hours > 0 ? string.Format("と{0}時間", span.Hours) : "", span.Minutes > 0 ? string.Format("{0}分", span.Minutes) : "");
    }
}

