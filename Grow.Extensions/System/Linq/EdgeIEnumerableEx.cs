﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

public static class EdgeIEnumerableEx
{
    /// <summary>
    /// ページング対象シーケンスの最大要素数
    /// </summary>
    const int PaginateMaxElementCount = 1000;

    /// <summary>
    /// シーケンスをページングして返す
    /// </summary>
    /// <param name="source">対象となるシーケンス</param>
    /// <param name="startIndex">1ページに表示する要素数</param>
    /// <param name="getCount">現在のページ数</param>
    /// <returns>ページングされたシーケンス</returns>
    public static IEnumerable<T> Paginate<T>(this IEnumerable<T> source, int startIndex, int getCount)
    {
        if (source == null)
            throw new ArgumentNullException("source");

        var enumerable = source as T[] ?? source.ToArray();
        if (enumerable.Count() > PaginateMaxElementCount)
            throw new ArgumentException(string.Format("ページング対象のシーケンスの要素数が{0}件を超えています。", PaginateMaxElementCount));
        
        return enumerable.Skip(startIndex).Take(getCount);
    }

    /// <summary>
    /// WeightedSampleの復元抽選最大回数
    /// </summary>
    private const int WeightedSampleMaxLoopCount = 1000;

    /// <summary>
    /// シーケンスからweightSelectorで指定された重み付けで復元抽選を実施し、当選した要素を返します。
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="source">抽選対象のシーケンス</param>
    /// <param name="weightSelector">シーケンスの各要素の重みを提供する関数</param>
    /// <returns></returns>
    public static IEnumerable<T> WeightedSample<T>(this IEnumerable<T> source, Func<T, int> weightSelector)
    {
        if (source == null) throw new ArgumentNullException("source");
        if (weightSelector == null) throw new ArgumentNullException("weightSelector");

        return WeightedSampleCore(source, weightSelector, RandomProvider.ThreadRandom);
    }

    /// <summary>
    /// シーケンスからweightSelectorで指定された重み付けで復元抽選を実施し、当選した要素を返します。
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="source">抽選対象のシーケンス</param>
    /// <param name="weightSelector">シーケンスの各要素の重みを提供する関数</param>
    /// <param name="random">抽選に使用するランダム生成子</param>
    /// <returns></returns>
    public static IEnumerable<T> WeightedSample<T>(this IEnumerable<T> source, Func<T, int> weightSelector, Random random)
    {
        if (source == null) throw new ArgumentNullException("source");
        if (weightSelector == null) throw new ArgumentNullException("weightSelector");
        if (random == null) throw new ArgumentNullException("random");

        return WeightedSampleCore(source, weightSelector, random);
    }

    static IEnumerable<T> WeightedSampleCore<T>(IEnumerable<T> source, Func<T, int> weightSelector, Random random)
    {
        var totalWeight = 0;
        var sortedArray = source
            .Select(x =>
            {
                var weight = weightSelector(x);
                if (weight <= 0) return null;

                checked { totalWeight += weight; }
                return new { Value = x, Bound = totalWeight };
            })
            .Where(x => x != null)
            .ToArray();

        if (!sortedArray.Any()) yield break;

        var loopCounter = 0;
        while (true)
        {
            if (++loopCounter > WeightedSampleMaxLoopCount)
                throw new InvalidOperationException(string.Format("[WeightedSample] 抽選回数が{0}回を超えています。", WeightedSampleMaxLoopCount));

            var draw = random.Next(1, totalWeight + 1);

            var lower = -1;
            var upper = sortedArray.Length;
            while (upper - lower > 1)
            {
                var index = (lower + upper) / 2;
                if (sortedArray[index].Bound >= draw)
                {
                    upper = index;
                }
                else
                {
                    lower = index;
                }
            }

            yield return sortedArray[upper].Value;
        }
    }

    /// <summary>
    /// シーケンスからcountに指定した要素数の配列を生成し、yield returnする
    /// </summary>
    public static IEnumerable<T[]> Buffer<T>(this IEnumerable<T> source, int count)
    {
        if (source == null) throw new ArgumentNullException("source");
        if (count <= 0) throw new ArgumentOutOfRangeException("count");

        return BufferCore(source, count);
    }

    static IEnumerable<T[]> BufferCore<T>(this IEnumerable<T> source, int count)
    {
        var buffer = new T[count];
        var index = 0;
        foreach (var item in source)
        {
            buffer[index++] = item;
            if (index != count)
                continue;

            yield return buffer;
            index = 0;
            buffer = new T[count];
        }

        if (index == 0)
            yield break;

        var dest = new T[index];
        Array.Copy(buffer, dest, index);
        yield return dest;
    }

    /// <summary>
    /// マスタのキャッシュ生成
    /// </summary>
    public static Dictionary<TKey, TRow[]> GenerateCache<TKey, TRow>(this IEnumerable<TRow> rows, Func<TRow, TKey> keyProvider)
    where TRow : DataRow
    {
        if (rows == null) throw new ArgumentNullException("rows");

        return GenerateCacheCore(rows, keyProvider);
    }

    static Dictionary<TKey, TRow[]> GenerateCacheCore<TKey, TRow>(this IEnumerable<TRow> rows, Func<TRow, TKey> keyProvider)
    where TRow : DataRow
    {
        return rows.GroupBy(keyProvider).ToDictionary(x => x.Key, x => x.ToArray());
    }

    /// <summary>
    /// マスタのキャッシュ生成
    /// </summary>
    public static Dictionary<TKey, TRow[]> GenerateOrderByCache<TKey, TOrderKey, TRow>(this IEnumerable<TRow> rows, Func<TRow, TKey> keyProvider, Func<TRow, TOrderKey> orderProvider)
    where TRow : DataRow
    {
        if (rows == null) throw new ArgumentNullException("rows");

        return GenerateOrderByCacheCore(rows, keyProvider, orderProvider);
    }

    static Dictionary<TKey, TRow[]> GenerateOrderByCacheCore<TKey, TOrderKey, TRow>(this IEnumerable<TRow> rows, Func<TRow, TKey> keyProvider, Func<TRow, TOrderKey> orderProvider)
    where TRow : DataRow
    {
        return rows.GroupBy(keyProvider).ToDictionary(x => x.Key, x => x.OrderBy(orderProvider).ToArray());
    }

    /// <summary>
    /// マスタのキャッシュ生成
    /// </summary>
    public static Dictionary<TKey, TRow[]> GenerateOrderByDescendingCache<TKey, TOrderKey, TRow>(this IEnumerable<TRow> rows, Func<TRow, TKey> keyProvider, Func<TRow, TOrderKey> orderProvider)
    where TRow : DataRow
    {
        if (rows == null) throw new ArgumentNullException("rows");

        return GenerateOrderByDescendingCacheCore(rows, keyProvider, orderProvider);
    }

    static Dictionary<TKey, TRow[]> GenerateOrderByDescendingCacheCore<TKey, TOrderKey, TRow>(this IEnumerable<TRow> rows, Func<TRow, TKey> keyProvider, Func<TRow, TOrderKey> orderProvider)
    where TRow : DataRow
    {
        return rows.GroupBy(keyProvider).ToDictionary(x => x.Key, x => x.OrderByDescending(orderProvider).ToArray());
    }

    /// <summary>
    /// ランキング対象シーケンスの最大要素数
    /// </summary>
    const int ToRankingMaxElementCount = 100;

    /// <summary>
    /// ランキング番号をKeyとしたKeyValuePairをyield returnする
    /// </summary>
    public static IEnumerable<KeyValuePair<int, T>> ToRankingDictionary<T>(this IEnumerable<T> source, Func<T, int> scoreProvider)
    {
        if (source == null) throw new ArgumentNullException("source");

        var enumerable = source as T[] ?? source.ToArray();
        if (enumerable.Count() > ToRankingMaxElementCount)
            throw new ArgumentException(string.Format("ランキング対象のシーケンスの要素数が{0}件を超えています。", ToRankingMaxElementCount));

        return ToRankingDictionaryCore(enumerable, scoreProvider);
    }

    static IEnumerable<KeyValuePair<int, T>> ToRankingDictionaryCore<T>(this IEnumerable<T> source, Func<T, int> scoreProvider)
    {
        int rankCounter = 1, lastRank = 1, lastScore = int.MinValue;
        foreach (var element in source.OrderByDescending(scoreProvider))
        {
            var rank = rankCounter++;
            var score = scoreProvider(element);

            if (lastScore == score)
                rank = lastRank;

            lastScore = score;
            lastRank = rank;

            yield return new KeyValuePair<int, T>(rank, element);
        }
    }

    /// <summary>ForEachのインデックス付きバージョン</summary>
    public static void ForEach<T>(this IEnumerable<T> source, Action<T, int> action)
    {
        var i = 0;
        foreach (var element in source)
            action(element, i++);
    }

    /// <summary>空かチェック</summary>
    public static bool IsBlank<T>(this IEnumerable<T> source)
    {
        return !source.Any();
    }
}
