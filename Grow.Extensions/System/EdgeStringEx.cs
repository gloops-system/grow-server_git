﻿using System.Text.RegularExpressions;

public static class EdgeStringEx
{
    private const string TagPattern = @"<.*?>";

    public static string RemoveTag(this string str)
    {
        return Regex.Replace(str, TagPattern, string.Empty);
    }
}
