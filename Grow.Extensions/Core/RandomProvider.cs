﻿using System;
using System.Security.Cryptography;
using System.Threading;

public static class RandomProvider
{
    private static readonly ThreadLocal<Random> RandomWrapper = new ThreadLocal<Random>(() =>
    {
        using (var rng = new RNGCryptoServiceProvider())
        {
            var buffer = new byte[sizeof(int)];
            rng.GetBytes(buffer);
            var seed = BitConverter.ToInt32(buffer, 0);
            return new Random(seed);
        }
    });

    public static Random ThreadRandom
    {
        get
        {
            return RandomWrapper.Value;
        }
    }
}
