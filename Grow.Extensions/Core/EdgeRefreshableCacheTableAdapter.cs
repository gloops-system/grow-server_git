﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using Fango.Data.Caching;

namespace Grow.Extensions.Core
{
    public abstract class EdgeRefreshableCacheTableAdapter : CacheTableAdapterBase, IRefreshableCache
    {
        protected ReaderWriterLock RwLock = new ReaderWriterLock();

        protected EdgeRefreshableCacheTableAdapter()
        {
            RefreshableCacheManager.Register(this);
        }

        protected IEnumerable<T> GetDataFromCache<T>(IEnumerable<T> cache)
        {
            try
            {
                RwLock.AcquireReaderLock(Timeout.Infinite);
                return cache;
            }
            finally
            {
                RwLock.ReleaseReaderLock();
            }
        }

        protected TRow GetDataByPKFromCache<TKey, TRow>(IDictionary<TKey, TRow> cache, TKey key) where TRow : DataRow
        {
            try
            {
                TRow row;
                RwLock.AcquireReaderLock(Timeout.Infinite);
                if (cache.TryGetValue(key, out row))
                    return row;
            }
            finally
            {
                RwLock.ReleaseReaderLock();
            }

            return default(TRow);
        }

        protected TRow[] GetDataByFromCache<TKey, TRow>(IDictionary<TKey, TRow[]> cache, TKey key)
        {
            try
            {
                TRow[] row;
                RwLock.AcquireReaderLock(Timeout.Infinite);
                if (cache.TryGetValue(key, out row))
                    return row;
            }
            finally
            {
                RwLock.ReleaseReaderLock();
            }

            return new TRow[0];
        }

        public void RefreshCache()
        {
            try
            {
                RwLock.AcquireWriterLock(Timeout.Infinite);
                InitializeCache();
            }
            catch(Exception ex)
            {
                var a = ex;
            }
            finally
            {
                RwLock.ReleaseWriterLock();
            }
        }

        protected abstract void InitializeCache();
    }
}
