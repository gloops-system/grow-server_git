﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks.Dataflow;

namespace Grow.Batch.Core.Battle
{
    public abstract class AbstractProducer<T> : IProducer<T>
    {
        protected ITargetBlock<T>  target;

        public abstract bool Post(T data);

        public void Complete()
        {
            this.target.Complete();
        }
    }
}
