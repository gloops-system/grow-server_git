﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks.Dataflow;

namespace Grow.Batch.Core.Battle
{
    public interface IProducer<T>
    {
        bool Post(T data);
        void Complete();
    }
}
