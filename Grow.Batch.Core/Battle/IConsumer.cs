﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace Grow.Batch.Core.Battle
{
    public interface IConsumer<T>
    {
        Task<List<T>> ConsumeAsync();
    }
}
