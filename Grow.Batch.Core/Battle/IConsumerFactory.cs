﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks.Dataflow;

namespace Grow.Batch.Core.Battle
{
    public interface IConsumerFactory<T>
    {
        IConsumer<T> GetConsumer(string name, IReceivableSourceBlock<T> source);
    }
}
