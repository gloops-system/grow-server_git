﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace Grow.Batch.Core.Battle
{
    public class ProducerConsumersRunner<T>
    {
        private BufferBlock<T> buffer;
        private IProducer<T> producer;
        private List<Task<List<T>>> taskList;

        public ProducerConsumersRunner(IProducerFactory<T> producerFactory, IConsumerFactory<T> consumerFactory, int numberOfConsumer)
        {
            taskList = new List<Task<List<T>>>();

            // Create a BufferBlock<byte[]> object. This object serves as the  
            // target block for the producer and the source block for the consumer. 
            buffer = new BufferBlock<T>();

            // create a producer
            producer = producerFactory.GetProducer(buffer);

            // create consumbers based on required number
            for (int i = 0; i < numberOfConsumer; i++)
            {
                // Start the consumer. The Consume method runs asynchronously.  
                IConsumer<T> consumer = consumerFactory.GetConsumer("#" + i, buffer);
                Task<List<T>> awaitableTask = consumer.ConsumeAsync();

                taskList.Add(awaitableTask);
            }
        }

        public List<Task<List<T>>> Execute(List<T> dataList)
        {
            dataList.ForEach(delegate(T data)
            {
                producer.Post(data);
            });

            // Set the target to the completed state to signal to the consumer 
            // that no more data will be available.
            producer.Complete();

            // Wait for all consumers to process all data.
            taskList.ForEach(delegate(Task<List<T>> task)
            {
                task.Wait();
            });

            return taskList;
        }
    }
}
