﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace Grow.Batch.Core.Battle
{
    public abstract class AbstractConsumer<T> : IConsumer<T>
    {
        protected String name = "";

        protected IReceivableSourceBlock<T> source;

        protected abstract void execute(T data);

        public async Task<List<T>> ConsumeAsync()
        {
            // Initialize a counter to track the number of uid that are processed. 
            List<T> errorList = new List<T>();

            // Read from the source buffer until the source buffer has no  
            // available output data.
            while (await this.source.OutputAvailableAsync())
            {
                T data;

                while (this.source.TryReceive(out data))
                {
                    try
                    {
                        execute(data);
                    } catch(Exception exception) {
                        Console.WriteLine(" Consumer {0}. Exception for data id {1}. Error: {2} ", this.name, data, exception);
                        errorList.Add(data);
                    }
                }
            }

            return errorList;
        }
    }
}
