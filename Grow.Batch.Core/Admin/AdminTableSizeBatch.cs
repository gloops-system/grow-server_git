﻿using Fango.Batch;
//using Edge.Batch.Core.Common;
//using Edge.Data.DataSet.AdminDataSetTableAdapters;
//using Edge.Data.Duty.DataSet.AdminDutyDataSetTableAdapters;
//using Edge.Data.Event.DataSet.AdminEventDataSetTableAdapters;
//using Edge.Data.Gacha.DataSet.AdminGachaDataSetTableAdapters;
//using Edge.Data.Game.DataSet.AdminGameDataSetTableAdapters;
//using Edge.Data.General.DataSet.AdminGeneralDataSetTableAdapters;
//using Edge.Data.History01.DataSet.AdminHistory01DataSetTableAdapters;
//using Edge.Data.News.DataSet.AdminNewsDataSetTableAdapters;
//using Edge.Data.Record.DataSet.AdminRecordDataSetTableAdapters;
//using Edge.Data.User.DataSet.AdminUserDataSetTableAdapters;
using System;
//using Edge.Data.Card.DataSet.AdminCardDataSetTableAdapters;

namespace Grow.Batch.Core.Admin
{
    public class AdminTableSizeBatch : IJob
    {
        public JobResultCode ExecuteJob(string[] args)
        {
            throw new NotImplementedException();
        }
        //public JobResultCode ExecuteJob(string[] args)
        //{
        //    try
        //    {
        //        // 各スキーマに定義したストアドプロシージャ(InsertAdminXXXTableSize)を実行します
        //        new AdminCardTableSizeTableAdapter().InsertAdminCardTableSize();
        //        new AdminTableSizeTableAdapter().InsertAdminTableSize();
        //        new AdminDutyTableSizeTableAdapter().InsertAdminDutyTableSize();
        //        new AdminEventTableSizeTableAdapter().InsertAdminEventTableSize();
        //        new AdminGachaTableSizeTableAdapter().InsertAdminGachaTableSize();
        //        new AdminGameTableSizeTableAdapter().InsertAdminGameTableSize();
        //        new AdminGeneralTableSizeTableAdapter().InsertAdminGeneralTableSize();
        //        new AdminUserTableSizeTableAdapter().InsertAdminUserTableSize();
        //        new AdminHistory01TableSizeTableAdapter().InsertAdminHistory01TableSize();
        //        new AdminNewsTableSizeTableAdapter().InsertAdminNewsTableSize();
        //        new AdminRecordTableSizeTableAdapter().InsertAdminRecordTableSize();

        //        return JobResultCode.Success;
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(@"Exception: " + e);   
        //        MailUtil.SendMail("Exception: " + e);

        //        return JobResultCode.Fail;
        //    }
        //}

        public Func<bool> ShouldStop
        {
            get;
            set;
        }

        public Guid BatchId
        {
            get;
            set;
        }
    }
}
