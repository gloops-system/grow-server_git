﻿using System;
using System.Linq;
using Fango.Batch;
using Fango.Core.Util;
using Grow.Data.DataSet.Adapter;
using Grow.Data.DataSet.Adapter.Purchase;
using Grow.Core.Application.Common;
using Grow.Data.Admin.DataSet.Adapter.Payment;
using Grow.Batch.Core.Common;

namespace Grow.Batch.Core.Admin
{
    public class AdminPaymentSummaryDailyBatch : IJob
    {

        /// <summary>
        /// コイン購入支払い集計。args[0]には集計対象の日付("2013-04-18")を入れる
        /// </summary>
        public JobResultCode ExecuteJob(string[] args)
        {
            try
            {
                //args[0]: 2013/04/18
                DateTimeOffset targetDate;
                string argument;
                if ( args == null || !args.Any() || string.IsNullOrEmpty(args[0]) )
                {
                    //argument = DateUtil.GetDateTimeOffsetNow().Date.AddDays(-1).ToString("o");
                    var now = DateUtil.GetDateTimeOffsetNow().AddHours(-24).ToOffset(new TimeSpan(9,0,0));
                    var year = now.Year;
                    var month = now.Month;
                    var day = now.Day;
                    argument = string.Format("{0}/{1}/{2} 0:00:00+09:00", year, month, day);

                }
                else
                {
                    argument = args[0] + " 0:00:00+09:00";
                }
                if (!DateTimeOffset.TryParse(argument, out targetDate))
                {
                    Console.WriteLine(@"Batch Failed");
                    return JobResultCode.Fail;
                }

                /*** iOS ***/
                //売上・PayedUser
                var coinMaster = new GemMasterDataRepositoryAdapter();
                var appPaymentAdapter = new AppPaymentDataRepositoryAdapter();
                Console.WriteLine(@"from: " + targetDate + @", to: " + targetDate.AddDays(1) + @", status: " +
                                  (int) AppCode.PaymentStatus.コイン付与完了);
                var paymentList = appPaymentAdapter.GetDataByFromTo(targetDate, targetDate.AddDays(1),
                                                                    (int)AppCode.PaymentStatus.コイン付与完了);
                long iosPrice = paymentList.Sum(payment => coinMaster.FindItem(payment.ProductId).Price);
                long iosUserNum = paymentList.Distinct(payment => payment.UserId).Count();

                /*** Android ***/
                //売上・PayedUser
                var coinForAndroidMaster = new AndroidGemMasterDataRepositoryAdapter();
                var appPaymentForAndroidAdapter = new AndroidAppPaymentDataRepositoryAdapter();
                Console.WriteLine(@"from: " + targetDate + @", to: " + targetDate.AddDays(1) + @", status: " +
                                  (int)AppCode.PaymentStatus.コイン付与完了);
                var paymentForAndroidList = appPaymentForAndroidAdapter.GetDataByFromTo(targetDate, targetDate.AddDays(1),
                                                                                        (int)AppCode.PaymentStatus.コイン付与完了);
                long androidPrice = paymentForAndroidList.Sum(payment => coinForAndroidMaster.FindItem(payment.ProductId).Price);
                long androidUserNum = paymentForAndroidList.Distinct(payment => payment.UserId).Count();


                ////ショップでの消費コイン
                //var shopBuyHistoryAdapter = new ShopBuyHistoryDataRepositoryAdapter();
                //var totalShopUseCoin = shopBuyHistoryAdapter.SumUseCoinByFromTo(targetDate, targetDate.AddHours(24));
                //var totalShopUserNum = shopBuyHistoryAdapter.SumUserByFromTo(targetDate, targetDate.AddHours(24));

                ////視察でのスカウト消費コイン
                //var scoutHistoryAdapter = new UserScoutHistoryDataRepositoryAdapter();
                //var totalScoutUseCoin = scoutHistoryAdapter.SumUseCoinByFromToAndUseCoin(targetDate, targetDate.AddHours(24));
                //var totalScoutUsedCoinUser = scoutHistoryAdapter.SumUserByFromToAndUseCoin(targetDate, targetDate.AddHours(24));


                ////ガチャでの消費コイン
                //var gachaHistoryAdapter = new GachaBuyHistoryDataRepositoryAdapter();
                //var gachaHistoryList = gachaHistoryAdapter.GetDataByFromTo(targetDate, targetDate.AddHours(24));
                //long totalGachaUseCoin = gachaHistoryList.Sum(row => row.UseCoin);
                //long totalGachaUserdCoinUser = gachaHistoryList.Distinct(row => row.UserId).Count();


                //Console.WriteLine(@"price: "+ totalPrice + @" userNum: "+ totalUserNum);
                // 売り上げ/日 をデータに入れる
                var paymentDataRepository = new AdminPaymentDataRepository();
                var totalPrice = iosPrice + androidPrice;
                var totalUserNum = iosUserNum + androidUserNum;
                paymentDataRepository.UpdateOrInsert(targetDate, iosPrice, androidPrice, (int)iosUserNum, (int)androidUserNum, 10,
                                                     10, 10, 10,
                                                     10, 10);
                //paymentDataRepository.UpdateOrInsert(targetDate, iosPrice, androidPrice, (int)iosUserNum, (int)androidUserNum, totalShopUseCoin,
                //                                     totalShopUserNum, totalScoutUseCoin, totalScoutUsedCoinUser,
                //                                     totalGachaUseCoin, totalGachaUserdCoinUser);

                return JobResultCode.Success;
            }
            catch (Exception e)
            {
                Console.WriteLine(@"Exception: " + e);
                MailUtil.SendMail("Exception: " + e);
                return JobResultCode.Fail;
            }
        }

        public Func<bool> ShouldStop { get; set; }
        public Guid BatchId { get; set; }
    }
}
