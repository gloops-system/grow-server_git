﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading;
using Fango.Batch;
using Fango.Core.Util;
using Grow.Data.DataSet.Adapter;
using Grow.Data.DataSet.Adapter.Purchase;
using Grow.Core.Application.Common;
using Grow.Data.Admin.DataSet.Adapter.Payment;
using Grow.Batch.Core.Common;

namespace Grow.Batch.Core.Admin
{
    public class AdminPaymentSummaryMonthlyBatch : IJob
    {
        /// <summary>
        /// コイン購入支払い集計。args[0]には集計対象の日付("2013-04-01")を入れる
        /// </summary>
        public JobResultCode ExecuteJob(string[] args)
        {
            try
            {
                //args[0]: 2013/04/01 0:00:00 +09:00
                Thread.CurrentThread.CurrentCulture = new CultureInfo("ja-JP");
                DateTimeOffset targetDate;
                string argument;
                if ( args == null || !args.Any() || string.IsNullOrEmpty(args[0]) )
                {
                    var now = DateUtil.GetDateTimeOffsetNow().AddMonths(-1);
                    var year = now.Year.ToString();

                    //前月の1日
                    var month = DateTimeOffset.Parse(year + "/" + now.Month + "/01 0:00:00+09:00").Month;
                    argument = string.Format("{0}/{1}/01 00:00:00+09:00", year, month);
                }
                else
                {
                    argument = args[0];
                }
                if (!DateTimeOffset.TryParse(argument, out targetDate))
                {
                    Console.WriteLine(@"Batch Failed");
                    return JobResultCode.Fail;
                }

                //売上・PayedUser
                // iOS
                var gemMaster = new GemMasterDataRepositoryAdapter().FindItems();
                var appPaymentAdapter = new AppPaymentDataRepositoryAdapter();
                Console.WriteLine(@"from: " + targetDate + @", to: " + targetDate.AddMonths(1) + @", status: " +
                                  (int) AppCode.PaymentStatus.コイン付与完了);
                var paymentList = appPaymentAdapter.GetDataByFromTo(targetDate, targetDate.AddMonths(1),
                                                                    (int) AppCode.PaymentStatus.コイン付与完了);

                // android
                var andGemMaster = new AndroidGemMasterDataRepositoryAdapter().FindItems();
                var andAppPaymentAdapter = new AndroidAppPaymentDataRepositoryAdapter();
                var androidPaymentList = andAppPaymentAdapter.GetDataByFromTo(targetDate, targetDate.AddMonths(1),
                    (int)AppCode.PaymentStatus.コイン付与完了);

                long totalPrice = paymentList.Sum(payment => payment.Gem)
                    + androidPaymentList.Sum(payment => payment.Gem);
                int totalUserNum = paymentList.Distinct(payment => payment.UserId).Count()
                    + androidPaymentList.Distinct(payment => payment.UserId).Count();

                Console.WriteLine(@"price: "+ totalPrice + @" userNum: "+ totalUserNum);

                // 売り上げ/月 をデータに入れる
                var paymentDataRepository = new AdminPaymentDataRepository();
                paymentDataRepository.UpdateOrInsertMonthlyData(targetDate, totalPrice, totalUserNum);

                return JobResultCode.Success;
            }
            catch (Exception e)
            {
                Console.WriteLine(@"Exception: " + e);
                MailUtil.SendMail("Exception: " + e);
                return JobResultCode.Fail;
            }
        }
        public Func<bool> ShouldStop { get; set; }
        public Guid BatchId { get; set; }
    }
}
