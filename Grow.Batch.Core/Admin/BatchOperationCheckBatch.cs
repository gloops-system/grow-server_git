﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fango.Batch;
using Fango.Core.Logging;

namespace Grow.Batch.Core.Admin
{
    /// <summary>
    /// バッチ動作確認用の空バッチ
    /// </summary>
    public class BatchOperationCheckBatch : IJob
    {
        public Fango.Batch.JobResultCode ExecuteJob(string[] args)
        {
            return JobResultCode.Success;
        }

        public Func<bool> ShouldStop
        {
            get;
            set;
        }

        public Guid BatchId
        {
            get;
            set;
        }
    }
}
