﻿using System;
//using System.Linq;
using Fango.Batch;
//using Fango.Core.Util;
//using Edge.Batch.Core.Common;
//using Edge.Data.General.DataSet.Adapter;
//using Edge.Data.General.DataSet.Adapter.Admin;

namespace Grow.Batch.Core.Admin
{
    public class AdminUniqueUserCurrencyDailyBatch : IJob
    {
        public JobResultCode ExecuteJob(string[] args)
        {
            throw new NotImplementedException();
        }
        ///// <summary>
        ///// 毎日のユーザの通貨支払い記録バッチ
        ///// </summary>
        //public JobResultCode ExecuteJob(string[] args)
        //{
        //    try
        //    {
        //        int datediff;

        //        //args[0]: 1(1日前)など
        //        datediff = !(args != null && args.Any()) ? 1 : int.Parse(args[0]);

        //        //プロシージャ実行
        //        var dataRepository = new AdminPaymentDataRepository();
        //        dataRepository.AdminUniqueUserCurrency(datediff);

        //        return JobResultCode.Success;
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(@"Exception: " + e);
        //        MailUtil.SendMail("Exception: " + e);
        //        return JobResultCode.Fail;
        //    }
        //}

        public Func<bool> ShouldStop
        {
            get;
            set;
        }

        public Guid BatchId
        {
            get;
            set;
        }

    }
}
