﻿using Fango.Batch;
//using Edge.Batch.Core.Common;
//using Edge.Data.DataSet.AdminDataSetTableAdapters;
//using Edge.Data.Duty.DataSet.AdminDutyDataSetTableAdapters;
//using Edge.Data.Event.DataSet.AdminEventDataSetTableAdapters;
//using Edge.Data.Gacha.DataSet.AdminGachaDataSetTableAdapters;
//using Edge.Data.Game.DataSet.AdminGameDataSetTableAdapters;
//using Edge.Data.General.DataSet.AdminGeneralDataSetTableAdapters;
//using Edge.Data.History01.DataSet.AdminHistory01DataSetTableAdapters;
//using Edge.Data.News.DataSet.AdminNewsDataSetTableAdapters;
//using Edge.Data.Record.DataSet.AdminRecordDataSetTableAdapters;
//using Edge.Data.User.DataSet.AdminUserDataSetTableAdapters;
using System;

namespace Grow.Batch.Core.Admin
{
    public class DeleteOldDataBatch : IJob
    {
        protected const int DeletePeriod = 30;

        public JobResultCode ExecuteJob(string[] args)
        {
            throw new NotImplementedException();
        }

        //public JobResultCode ExecuteJob(string[] args)
        //{
        //    try
        //    {
        //        // 各スキーマに定義したストアドプロシージャ(DeleteOldData)を実行します
        //        new AdminTableSizeTableAdapter().DeleteOldData(DeletePeriod);
        //        new AdminDutyTableSizeTableAdapter().DeleteOldData(DeletePeriod);
        //        new AdminEventTableSizeTableAdapter().DeleteOldData(DeletePeriod);
        //        new AdminGachaTableSizeTableAdapter().DeleteOldData(DeletePeriod);
        //        new AdminGameTableSizeTableAdapter().DeleteOldData(DeletePeriod);
        //        new AdminGeneralTableSizeTableAdapter().DeleteOldData(DeletePeriod);
        //        new AdminRecordTableSizeTableAdapter().DeleteOldData(DeletePeriod);
        //        new AdminNewsTableSizeTableAdapter().DeleteOldData(DeletePeriod);
        //        new AdminHistory01TableSizeTableAdapter().DeleteOldData(DeletePeriod);

        //        return JobResultCode.Success;
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(@"Exception: " + e);
        //        MailUtil.SendMail("Exception: " + e);

        //        return JobResultCode.Fail;

        //    }
        //}

        public Func<bool> ShouldStop
        {
            get;
            set;
        }

        public Guid BatchId
        {
            get;
            set;
        }
    }
}
