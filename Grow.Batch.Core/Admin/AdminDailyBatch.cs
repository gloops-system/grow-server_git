﻿using Fango.Batch;
//using Fango.Core.Logging;
//using Fango.Core.Util;
//using Edge.Batch.Core.Common;
//using Edge.Core.Application;
//using Edge.Core.Caching;
//using Edge.Core.Common;
//using Edge.Data.DataSet;
//using Edge.Data.DataSet.Adapter;
//using Edge.Data.DataSet.Adapter.Admin;
//using Edge.Data.DataSet.Adapter.Tutorial;
//using Edge.Data.DataSet.AdminDataSetTableAdapters;
//using Edge.Data.Duty.DataSet;
//using Edge.Data.Duty.DataSet.Adapter.Duty;
//using Edge.Data.Gacha.DataSet.Adapter;
//using Edge.Data.Game.DataSet.Adapter.Game;
//using Edge.Data.General.DataSet.Adapter;
//using Edge.Data.General.DataSet.Adapter.Challenger;
//using Edge.Data.User.DataSet.Adapter;
using System;
//using System.Collections.Generic;
//using System.Linq;
//using Edge.Data.User.DataSet.AccessDataSetTableAdapters;
//using Edge.Data.General.DataSet.AdminGeneralDataSetTableAdapters;

namespace Grow.Batch.Core.Admin
{
    /// <summary>
    /// 日次集計系バッチ
    /// </summary>
    public class AdminDailyBatch : IJob
    {
        public JobResultCode ExecuteJob(string[] args)
        {
            throw new NotImplementedException();
        }
        //private UserDutyDataRepositoryAdapter userDuty = new UserDutyDataRepositoryAdapter();
        //private LogData logData = null;

        //public JobResultCode ExecuteJob(string[] args)
        //{
        //    var startTime = DiveDateUtil.DatabaseDate;

        //    //集計対象日
        //    var aggregateDate = DiveDateUtil.DatabaseDate.AddDays(-1);

        //    logData = new LogData()
        //    {
        //        BatchName = "AdminDailyBatch",
        //        BatchArgs = args,
        //        ExceptionInfoList = new Dictionary<string, List<string>>()
        //                            {
        //                                {"MethodName", new List<string>()},
        //                            },
        //    };

        //    var userDataContainer = new UserDataContainer();
            
        //    var userCount = userDataContainer.UserBase.GetCount();

        //    #region ユーザ関係
        //    InsertUserCounts(aggregateDate, userCount, userDataContainer);
        //    UpdateHometownDistribution(userDataContainer);
        //    UpdateFavouriteClubDistribution();
        //    #endregion ユーザ関係

        //    #region ゲーム関係

        //    //購入関係
        //    var userAssetDataContainer = new UserAssetDataContainer();
        //    InsertCoinBuyHistory(aggregateDate);
        //    InsertHasCoinNum(aggregateDate, userAssetDataContainer);
        //    InsertShopBuyCount(aggregateDate, userAssetDataContainer);

        //    //ガチャ関係
        //    var gachaDataContainer = new GachaDataContainer();
        //    InsertGachaCount(aggregateDate, gachaDataContainer);
        //    InsertGachaCard(aggregateDate, gachaDataContainer);

        //    //アイテム関係
        //    InsertHasItemNum(aggregateDate, userAssetDataContainer);
        //    InsertUseItemNum(aggregateDate, userAssetDataContainer);

        //    //視察関係
        //    var dutyDataContainer = new DutyDataContainer();
        //    var dutyMasterRows = dutyDataContainer.DutyMaster.GetData();
        //    InsertDutyProgress(aggregateDate, dutyMasterRows);
        //    InsertDropCompleteStatus(aggregateDate, dutyMasterRows);
        //    InsertScoutSelect(aggregateDate, dutyDataContainer);

        //    //リーグ戦/親善試合関係
        //    var gameDataContainer = new GameDataContainer();
        //    InsertLeagueGroupDistribution(aggregateDate, gameDataContainer);
        //    InsertTacticsSetNum(aggregateDate, gameDataContainer);
        //    InsertChallengerPost(aggregateDate);
        //    InsertFriendlyMatchNum(aggregateDate, userCount, gameDataContainer);

        //    //フレンド招待の実行回数
        //    InsertInvite(aggregateDate);

        //    //チュートリアルの離脱ポイント
        //    InsertTutorialCheckPoint(aggregateDate, new TutorialDataContainer());

        //    #endregion

        //    MailUtil.WriteLogAndSendMail(logData);

        //    return JobResultCode.Success;
        //}

        //private void HandleExeption(Exception ex, string methodName)
        //{
        //    logData.ExceptionList.Add(ex);
        //    logData.ExceptionInfoList["MethodName"].Add(methodName);
        //}

        //#region ユーザ関係
        //private void InsertUserCounts(DateTimeOffset now, int totalCount, UserDataContainer userDataContainer)
        //{
        //    try
        //    {
        //        var to = new DateTimeOffset(now.Year, now.Month, now.Day, 0, 0, 0, now.Offset);
        //        var from = to.AddDays(-1);
        //        var installCount = userDataContainer.UserBase.GetInstallCount(from, to);
        //        var activeCount = new AdminUniqueUserTableAdapter().GetCountByDate(to);

        //        var adminUserCountTableAdapter = new AdminUserCountTableAdapter();
        //        adminUserCountTableAdapter.AddNew(now.Date, totalCount, installCount, activeCount.HasValue ? activeCount.Value : 0);
        //    }
        //    catch (Exception ex)
        //    {
        //        HandleExeption(ex, "InsertUserCounts");
        //    }
        //}

        ///// <summary>
        ///// ホームタウン分布更新
        ///// </summary>
        ///// <param name="userDataContainer"></param>
        //private void UpdateHometownDistribution(UserDataContainer userDataContainer)
        //{
        //    try
        //    {
        //        var adminUserHometownSummaryTableAdapter = new AdminUserHometownSummaryTableAdapter();
        //        //前回履歴をtruncate
        //        adminUserHometownSummaryTableAdapter.AdminTruncateUserHomeTown();
        //        var userHomeTownSummary = userDataContainer.UserBase.GetHometownSummaryForAdmin();
                
        //        foreach (var hometown in Enum.GetValues(typeof (AppCode.HomeTown)))
        //        {
        //            var target = userHomeTownSummary.FirstOrDefault(x => x.Hometown == (int) hometown);
        //            var count = target == null ? 0 : target.Expr1;
        //            adminUserHometownSummaryTableAdapter.AddNew((int) hometown, hometown.ToString(), count);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        HandleExeption(ex, "UpdateHometownDistribution");
        //    }
            
        //}

        ///// <summary>
        ///// お気に入り分布更新
        ///// </summary>
        //private void UpdateFavouriteClubDistribution()
        //{
        //    try
        //    {
        //        var userCommonDataContainer = new UserCommonDataContainer();
        //        var adminFavouriteClubSummaryTableAdapter = new AdminFavouriteClubSummaryTableAdapter();
        //        //前回履歴をtruncate
        //        adminFavouriteClubSummaryTableAdapter.AdminTruncateFavouriteClubSummary();
        //        var userFavouriteClubSummary = userCommonDataContainer.UserFavouriteClub.GetFavouriteClubSummary();

        //        foreach (var club in Enum.GetValues(typeof(AppCode.ClubType)))
        //        {
        //            var target = userFavouriteClubSummary.FirstOrDefault(x => x.ClubId == (int)club);
        //            var count = target == null ? 0 : target.Expr1;
        //            adminFavouriteClubSummaryTableAdapter.AddNew((int)club, club.ToString(), count);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        HandleExeption(ex, "UpdateFavouriteClubDistribution");
        //    }
        //}
        //#endregion

        //#region 購入関係

        ///// <summary>
        ///// コイン購入数Insert
        ///// </summary>
        //private void InsertCoinBuyHistory(DateTimeOffset now)
        //{
        //    try
        //    {
        //        var adminCoinBuyCount = new AdminCoinBuyCountTableAdapter();

        //        var appPaymentDataContainer = new AppPaymentDataContainer();
        //        var GemMasterRows = appPaymentDataContainer.GemMaster.FindItems();
        //        var to = new DateTimeOffset(now.Year, now.Month, now.Day, 0, 0, 0, now.Offset);
        //        var from = to.AddDays(-1);
        //        foreach (var GemMasterRow in GemMasterRows)
        //        {
        //            var count = appPaymentDataContainer.AppPayment.GetCountByProductId(GemMasterRow.ProductId, from, to);
        //            adminCoinBuyCount.AddNew(now.Date, GemMasterRow.ProductId, count);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        HandleExeption(ex, "InsertCoinBuyHistory");
        //    }
        //}

        ///// <summary>
        ///// 未消費コイン数Insert
        ///// </summary>
        //private void InsertHasCoinNum(DateTimeOffset now, UserAssetDataContainer userAssetDataContainer)
        //{
        //    try
        //    {
        //        //全体数しか取れない
        //        var hasNum = userAssetDataContainer.UserCoin.GetCoinSum();
        //        new AdminHasCoinNumTableAdapter().AddNew(now.Date, hasNum);
        //    }
        //    catch (Exception ex)
        //    {
        //        HandleExeption(ex, "InsertHasCoinNum");
        //    }
        //}

        ///// <summary>
        ///// 行動pt回復/試合pt回復/スポンサー購入数Insert
        ///// </summary>
        //private void InsertShopBuyCount(DateTimeOffset now, UserAssetDataContainer userAssetDataContainer)
        //{
        //    try
        //    {
        //        var to = new DateTimeOffset(now.Year, now.Month, now.Day, 0, 0, 0, now.Offset);
        //        var from = to.AddDays(-1);
        //        var adminShopBuyCount = new AdminShopBuyCountTableAdapter();
        //        var shopMasterRows = userAssetDataContainer.ShopMaster.GetShopItem();
        //        foreach (var shopMasterRow in shopMasterRows)
        //        {
        //            var count = userAssetDataContainer.ShopBuyHistory.GetSumBuyNumByShopItemId(shopMasterRow.ShopItemId, from, to);
        //            adminShopBuyCount.AddNew(now.Date, shopMasterRow.ShopItemId, count);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        HandleExeption(ex, "InsertShopBuyCount");
        //    }
        //}

        //#endregion

        //#region ガチャ関係

        ///// <summary>
        ///// 無料/有料/チケットガチャの実行回数Insert
        ///// </summary>
        //private void InsertGachaCount(DateTimeOffset now, GachaDataContainer gachaDataContainer)
        //{
        //    try
        //    {
        //        var to = new DateTimeOffset(now.Year, now.Month, now.Day, 0, 0, 0, now.Offset);
        //        var from = to.AddDays(-1);
        //        var adminGachaCountTA = new AdminGachaCountTableAdapter();
        //        var gachaMasterRows = gachaDataContainer.GachaMaster.GetGachaMasterInSession();
        //        var normalHistory = gachaDataContainer.NormalGachaHistory;
        //        var ticketHistory = gachaDataContainer.TicketGachaHistory;
        //        var gachaBuyHistory = gachaDataContainer.GachaBuyHistory;
        //        foreach (var gachaMasterRow in gachaMasterRows)
        //        {
        //            var count = 0;
        //            switch ((AppCode.GachaType) gachaMasterRow.GachaType)
        //            {
        //                case AppCode.GachaType.有料ガチャ:
        //                    count = gachaBuyHistory.GetCountByGachaId(gachaMasterRow.GachaId, from, to);
        //                    break;
        //                case AppCode.GachaType.無料ガチャ:
        //                    count = normalHistory.GetCountByGachaId(gachaMasterRow.GachaId, from, to);
        //                    break;
        //                case AppCode.GachaType.チケットガチャ:
        //                    count = ticketHistory.GetCurrencySummaryByGachaId(gachaMasterRow.GachaId, from, to);
        //                    //複数実行ありとのことなので、チケット数の総和を1回の必要チケット数で割る＝実行回数
        //                    if (count > 0)
        //                        count = count/gachaMasterRow.Currency;
        //                    break;
        //            }
        //            adminGachaCountTA.AddNew(now.Date, gachaMasterRow.GachaId, count);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        HandleExeption(ex, "InsertGachaCount");
        //    }
        //}

        ///// <summary>
        ///// ガチャで出たカードInsert
        ///// </summary>
        //private void InsertGachaCard(DateTimeOffset now, GachaDataContainer gachaDataContainer)
        //{
        //    try
        //    {
        //        var client = new EdgeApplicationModel().DefaultSequenceClient;
        //        var adminGachaCardDT = new AdminDataSet.AdminGachaCardDataTable();

        //        gachaDataContainer.UserGachaHistory.GetDataByFromDateAndToDate(now.Date, now.AddDays(1).Date).ForEach(x =>
        //        {
        //            var row = adminGachaCardDT.NewAdminGachaCardRow();
        //            row.Id = client.GetNextValue(CacheKey.AdminGachaCardId(), 1);
        //            row.Date = now.Date;
        //            row.GachaId = x.GachaId;
        //            row.CardId = x.CardId;
        //            row.UserId = x.UserId;
        //            adminGachaCardDT.AddAdminGachaCardRow(row);
        //        });

        //        gachaDataContainer.TicketGachaHistory.GetDataByFromDateAndToDate(now.Date, now.AddDays(1).Date).ForEach(x =>
        //        {
        //            var row = adminGachaCardDT.NewAdminGachaCardRow();
        //            row.Id = client.GetNextValue(CacheKey.AdminGachaCardId(), 1);
        //            row.Date = now.Date;
        //            row.GachaId = x.GachaId;
        //            row.CardId = x.CardId;
        //            row.UserId = x.UserId;
        //            adminGachaCardDT.AddAdminGachaCardRow(row);
        //        });

        //        gachaDataContainer.NormalGachaHistory.GetDataByFromDateAndToDate(now.Date, now.AddDays(1).Date).ForEach(x =>
        //        {
        //            var row = adminGachaCardDT.NewAdminGachaCardRow();
        //            row.Date = now.Date;
        //            row.GachaId = x.GachaId;
        //            row.CardId = x.CardId;
        //            row.UserId = x.UserId;
        //            row.Id = client.GetNextValue(CacheKey.AdminGachaCardId(), 1);
        //            adminGachaCardDT.AddAdminGachaCardRow(row);
        //        });

        //        if (adminGachaCardDT.Count > 0)
        //        {
        //            new AdminGachaCardDataRepositoryAdapter().BulkInsert(adminGachaCardDT);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        HandleExeption(ex, "InsertGachaCard");
        //    }
        //}

        //#endregion

        //#region アイテム関係

        ///// <summary>
        ///// 行動pt/試合pt/チケット所持数のInsert
        ///// </summary>
        //private void InsertHasItemNum(DateTimeOffset now, UserAssetDataContainer userAssetDataContainer)
        //{
        //    try
        //    {
        //        var adminHasItemNumTA = new AdminHasItemNumTableAdapter();

        //        //アイテム所持数
        //        foreach(var itemid in Enum.GetValues(typeof(AppCode.ItemIds)))
        //        {
        //            var count = userAssetDataContainer.UserItem.GetItemCountSumByItemId((int)itemid);
        //            adminHasItemNumTA.AddNew(now.Date, (int)itemid, (int)AppCode.ItemType.アイテム, count);
        //        }

        //        //チケット所持数
        //        var userDataContainer = new UserDataContainer();
        //        var ticketCount = userDataContainer.UserTicket.GetHasNumSum();
        //        adminHasItemNumTA.AddNew(now.Date, 1, (int)AppCode.ItemType.スカウトチケット, ticketCount);
        //    }
        //    catch (Exception ex)
        //    {
        //        HandleExeption(ex, "InsertHasItemNum");
        //    }
        //}

        ///// <summary>
        ///// 行動pt/試合pt消費数のInsert
        ///// </summary>
        //private void InsertUseItemNum(DateTimeOffset now, UserAssetDataContainer userAssetDataContainer)
        //{
        //    try
        //    {
        //        var to = new DateTimeOffset(now.Year, now.Month, now.Day, 0, 0, 0, now.Offset);
        //        var from = to.AddDays(-1);
        //        var adminUseItemNumTA = new AdminUseItemNumTableAdapter();

        //        //アイテム消費数
        //        foreach (var itemid in Enum.GetValues(typeof(AppCode.ItemIds)))
        //        {
        //            var count = userAssetDataContainer.UserItemUseHisotry.GetUseNumSummaryByItemId((int)itemid, from, to);
        //            adminUseItemNumTA.AddNew(now.Date, (int)itemid, (int)AppCode.ItemType.アイテム, count);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        HandleExeption(ex, "InsertUseItemNum");
        //    }
        //}

        //#endregion

        //#region 視察関係

        ///// <summary>
        ///// 視察の進捗状況Insert
        ///// (全体の累計）
        ///// </summary>
        //private void InsertDutyProgress(DateTimeOffset now, DutyDataSet.DutyMasterRow[] dutyMasterRows)
        //{
        //    try
        //    {
        //        var adminDutyProgressDT = new AdminDataSet.AdminDutyProgressDataTable();
        //        foreach (var dutyMasterRow in dutyMasterRows)
        //        {
        //            var row = adminDutyProgressDT.NewAdminDutyProgressRow();
        //            row.Date = now.Date;
        //            row.DutyId = dutyMasterRow.DutyId;
        //            row.UserCount = userDuty.GetNotClearCountByDutyId(dutyMasterRow.DutyId);
        //            adminDutyProgressDT.AddAdminDutyProgressRow(row);
        //        }

        //        new AdminDutyProgressDataRepositoryAdapter().BulkInsert(adminDutyProgressDT);
        //    }
        //    catch (Exception ex)
        //    {
        //        HandleExeption(ex, "InsertDutyProgress");
        //    }
        //}

        ///// <summary>
        ///// ドロップカードのコンプ状況Insert
        ///// (全体の累計）
        ///// </summary>
        //private void InsertDropCompleteStatus(DateTimeOffset now, DutyDataSet.DutyMasterRow[] dutyMasterRows)
        //{
        //    try
        //    {
        //        var adminDropCompleteStatusDT = new AdminDataSet.AdminDropCompleteStatusDataTable();
        //        foreach (var dutyMasterRow in dutyMasterRows)
        //        {
        //            var row = adminDropCompleteStatusDT.NewAdminDropCompleteStatusRow();
        //            row.Date = now.Date;
        //            row.DutyId = dutyMasterRow.DutyId;
        //            row.UserCount = userDuty.GetDropCompleteCountByDutyId(dutyMasterRow.DutyId);
        //            adminDropCompleteStatusDT.AddAdminDropCompleteStatusRow(row);
        //        }

        //        new AdminDropCompleteStatusDataRepositoryAdapter().BulkInsert(adminDropCompleteStatusDT);
        //    }
        //    catch (Exception ex)
        //    {
        //        HandleExeption(ex, "InsertDropCompleteStatus");
        //    }
        //}

        ///// <summary>
        ///// スカウトで選ばれている選択肢（コインor名声）Insert
        ///// </summary>
        ///// <param name="now"></param>
        //private void InsertScoutSelect(DateTimeOffset now, DutyDataContainer dutyDataContainer)
        //{
        //    try
        //    {
        //        var to = new DateTimeOffset(now.Year, now.Month, now.Day, 0, 0, 0, now.Offset);
        //        var from = to.AddDays(-1);
        //        var useCoinCount = dutyDataContainer.UserScoutHistory.GetUseCoinCount(from, to);
        //        var useFameCount = dutyDataContainer.UserScoutHistory.GetUseFameCount(from, to);

        //        new AdminScoutSelectTableAdapter().AddNew(now.Date, useFameCount, useCoinCount);
        //    }
        //    catch (Exception ex)
        //    {
        //        HandleExeption(ex, "InsertScoutSelect");
        //    }
        //}

        //#endregion

        //#region リーグ戦/親善試合関係

        ///// <summary>
        ///// リーグ戦の分布Insert
        ///// </summary>
        //private void InsertLeagueGroupDistribution(DateTimeOffset now, GameDataContainer gameDataContainer)
        //{
        //    try
        //    {
        //        var adminLeagueGroupDistributionTA = new AdminLeagueGroupDistributionTableAdapter();

        //        var userGameDataContainer = new UserGameDataContainer();
        //        var leagueGroup = userGameDataContainer.LeagueGroup;

        //        var leagueMasterRows = gameDataContainer.LeagueMaster.GetLeagueMaster();

        //        foreach (var leagueMasterRow in leagueMasterRows)
        //        {
        //            //終了済グループ数
        //            var closeCount = leagueGroup.GetUserCountByLeagueIdAndIsClose(leagueMasterRow.LeagueId, true);
        //            adminLeagueGroupDistributionTA.AddNew(now.Date, leagueMasterRow.LeagueId, true, closeCount);

        //            //開催中グループ数
        //            var openCount = leagueGroup.GetUserCountByLeagueIdAndIsClose(leagueMasterRow.LeagueId, false);
        //            adminLeagueGroupDistributionTA.AddNew(now.Date, leagueMasterRow.LeagueId, false, openCount);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        HandleExeption(ex, "InsertLeagueGroupDistribution");
        //    }
        //}

        ///// <summary>
        ///// 戦術カード設定数Insert 
        ///// </summary>
        //private void InsertTacticsSetNum(DateTimeOffset now, GameDataContainer gameDataContainer)
        //{
        //    try
        //    {
        //        var to = new DateTimeOffset(now.Year, now.Month, now.Day, 0, 0, 0, now.Offset);
        //        var from = to.AddDays(-1);
        //        var adminTacticsSetNumTA = new AdminTacticsSetNumTableAdapter();
        //        var gameResult = gameDataContainer.GameResult;

        //        foreach (var gameType in Enum.GetValues(typeof(AppCode.GameType)))
        //        {
        //            if ((int)gameType == (int)AppCode.GameType.リーグ戦 || (int)gameType == (int)AppCode.GameType.親善試合)
        //            {
        //                var homeSetCount = gameResult.GetHomeTacticsSetCountByGameType((int)gameType, from, to);
        //                var awaySetCount = gameResult.GetAwayTacticsSetCountByGameType((int)gameType, from, to);
        //                var gameCount = gameResult.GetCountByGameType((int)gameType, from, to);
        //                adminTacticsSetNumTA.AddNew(now.Date, (int)gameType, homeSetCount, awaySetCount, gameCount);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        HandleExeption(ex, "InsertTacticsSetNum");
        //    }
        //}

        ///// <summary>
        ///// 挑戦者募集実行回数Insert
        ///// </summary>
        //private void InsertChallengerPost(DateTimeOffset now)
        //{
        //    try
        //    {
        //        var to = new DateTimeOffset(now.Year, now.Month, now.Day, 0, 0, 0, now.Offset);
        //        var from = to.AddDays(-1);
        //        var challengerPostHistoryDataContainer = new ChallengerPostHistoryDataContainer();
        //        var adminChallengerPostTA = new AdminChallengerPostTableAdapter();
        //        foreach (var snsType in Enum.GetValues(typeof(AppCode.SNSType)))
        //        {
        //            var count = challengerPostHistoryDataContainer.ChallengerPostHistory.GetCountBySNSType((int)snsType, from, to);
        //            adminChallengerPostTA.AddNew(now.Date, (int)snsType, count);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        HandleExeption(ex, "InsertChallengerPost");
        //    }
        //}

        ///// <summary>
        ///// 親善試合数Insert
        ///// </summary>
        ///// <param name="now"></param>
        //private void InsertFriendlyMatchNum(DateTimeOffset now, int userCount, GameDataContainer gameDataContainer)
        //{
        //    try
        //    {
        //        var to = new DateTimeOffset(now.Year, now.Month, now.Day, 0, 0, 0, now.Offset);
        //        var from = to.AddDays(-1);
        //        var gameCount = gameDataContainer.GameResult.GetCountByGameType((int)AppCode.GameType.親善試合, from, to);
        //        var canScheduleCount = userCount * AppCode.FriendlyMatchGroupingSpanDays * 2;
        //        var setScheduleCount = gameDataContainer.UserFriendlyMatchSchedule.GetCount();
        //        new AdminFriendlyMatchNumTableAdapter().AddNew(now.Date, gameCount, canScheduleCount, setScheduleCount);
        //    }
        //    catch (Exception ex)
        //    {
        //        HandleExeption(ex, "InsertFriendlyMatchNum");
        //    }
        //}

        //#endregion

        //#region フレンド招待の実行回数

        //private void InsertInvite(DateTimeOffset now)
        //{
        //    try
        //    {
        //        var to = new DateTimeOffset(now.Year, now.Month, now.Day, 0, 0, 0, now.Offset);
        //        var from = to.AddDays(-1);
        //        var inviteDataContainer = new InviteDataContainer();
        //        var count = inviteDataContainer.InviteUser.GetCount(from, to);
        //        new AdminInviteTableAdapter().AddNew(now.Date, count);
        //    }
        //    catch (Exception ex)
        //    {
        //        HandleExeption(ex, "InsertInvite");
        //    }
        //}

        //#endregion

        //#region チュートリアルの離脱ポイント

        //private void InsertTutorialCheckPoint(DateTimeOffset now, TutorialDataContainer tutorialDataContainer)
        //{
        //    try
        //    {
        //        var adminTutorialCheckPoint = new AdminTutorialCheckPointTableAdapter();
        //        foreach (var checkPoint in Enum.GetValues(typeof(TutorialCheckPoint)))
        //        {
        //            var count = tutorialDataContainer.UserTutorialCheckPoint.GetCountByCheckPoint((int)checkPoint);
        //            adminTutorialCheckPoint.AddNew(now.Date, (int)checkPoint, count);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        HandleExeption(ex, "InsertTutorialCheckPoint");
        //    }
        //}

        
        //public enum TutorialCheckPoint
        //{
        //    #region ユーザ登録
        //    画面番号1 = 1,
        //    画面番号2 = 2,
        //    画面番号3 = 3,
        //    画面番号4 = 4,
        //    画面番号5 = 5,
        //    画面番号6 = 6,
        //    画面番号7 = 7,
        //    画面番号8 = 8,
        //    画面番号9 = 9,
        //    画面番号10 = 10,
        //    画面番号11 = 11,
        //    画面番号12 = 12,
        //    画面番号13 = 13,
        //    画面番号14 = 14,
        //    画面番号15 = 15,
        //    画面番号16 = 16,
        //    画面番号17 = 17,
        //    画面番号18 = 18,
        //    画面番号19 = 19,
        //    画面番号20 = 20,
        //    画面番号21 = 21,
        //    画面番号22 = 22,
        //    画面番号23 = 23,
        //    画面番号24 = 24,
        //    画面番号25 = 25,
        //    画面番号26 = 26,
        //    画面番号27 = 27,
        //    画面番号28 = 28,
        //    画面番号29 = 29,
        //    #endregion ユーザ登録

        //    #region 視察
        //    画面番号100 = 100,
        //    画面番号101 = 101,
        //    画面番号102 = 102,
        //    画面番号103 = 103,
        //    画面番号104 = 104,
        //    画面番号105 = 105,
        //    #endregion 視察

        //    #region 試合日程
        //    画面番号200 = 200,
        //    画面番号201 = 201,
        //    #endregion  試合日程

        //    #region フレンド
        //    画面番号300 = 300,
        //    画面番号301 = 301,
        //    #endregion フレンド

        //    #region 新着ボックス
        //    画面番号400 = 400,
        //    画面番号401 = 401,
        //    #endregion  新着ボックス

        //    #region ミッション
        //    画面番号500 = 500,
        //    画面番号501 = 501,
        //    #endregion ミッション

        //    #region 図鑑
        //    画面番号600 = 600,
        //    画面番号601 = 601,
        //    #endregion 図鑑

        //    #region チーム管理
        //    画面番号700 = 700,
        //    画面番号701 = 701,
        //    #endregion チーム管理
        //}

        
        //#endregion

        public Func<bool> ShouldStop
        {
            get;
            set;
        }

        public Guid BatchId
        {
            get;
            set;
        }
    }
}
