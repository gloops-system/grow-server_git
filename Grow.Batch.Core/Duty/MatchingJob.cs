﻿using Fango.Batch;
using System;

namespace Grow.Batch.Core.Duty
{
    public class MatchingJob: IJob
    {
        public JobResultCode ExecuteJob(string[] args)
        {
            Console.WriteLine("Test batch");
            var matchingLogic = new MatchingLogic();
            matchingLogic.CalculateUserStrength();
            matchingLogic.DoMatchingGroup();
            matchingLogic.OrganizeGroupMap();

            return JobResultCode.Success;
        }
        public Func<bool> ShouldStop { get; set; }
        public Guid BatchId { get; set; }
    }
}