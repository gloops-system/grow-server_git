﻿using Grow.Data.Duty.DataSet.Container;
using Grow.Data.User.DataSet;
using Grow.Data.User.DataSet.Container;
using Fango.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Batch.Core.Duty
{
    public class MatchingLogic
    {
        public void CalculateUserStrength()
        {
            var userDutyDataContainer = new UserLabyrinthDataContainer();
            userDutyDataContainer.MatchGroup.CalculateUserStrength();
        }

        public void DoMatchingGroup()
        {
            var userDutyDataContainer = new UserLabyrinthDataContainer();
            userDutyDataContainer.MatchGroup.DoMatchingGroup();
        }

        public void OrganizeGroupMap()
        {
            var listFloorId = new List<int>();
            var listMapId = new List<int>();
            var dutyDataContainer = new DutyDataContainer();
            var userDataContainer = new UserDataContainer();
            var userDutyDataContainer = new UserLabyrinthDataContainer();
            var floorMasterApater = dutyDataContainer.FloorMaster;
            var areaMasters = dutyDataContainer.AreaMaster.GetDataByEventId((int)Grow.Core.Application.Common.AppCode.QuestEventId.LABYRINTH);
            foreach (var areaMaster in areaMasters)
            {
                var floorMasters = floorMasterApater.GetDataByAreaId(areaMaster.AreaId);
                foreach (var floorMaster in floorMasters)
                {
                    listFloorId.Add(floorMaster.FloorId);
                }
            }
            var mapMasters = dutyDataContainer.MapMaster.GetEventMaps(1);
            foreach (var mapMaster in mapMasters)
            {
                listMapId.Add(mapMaster.MapId);
            }

            int numberFloor = listFloorId.Count;
            int numberMap = listMapId.Count;
            var matchGroupAdapter = userDutyDataContainer.MatchGroup;
            var matchGroupFloorMapAdapter = dutyDataContainer.MatchGroupFloorMap;
            long lastMatchGroupId = 0;
            var matchGroups = new List<UserLabyrinthDataSet.MatchGroupRow>();
            //TODO: Only work around now for haven't not enough master data
            if (numberMap < numberFloor)
            {
                for (var i = 0; i < numberFloor - numberMap; i++)
                {
                    var randomId = listMapId[numberMap - (numberFloor - numberMap) + i];
                    listMapId.Add(randomId);
                }
                numberMap = numberFloor;
            }
            //End work around
            var now = DateUtil.GetDateTimeOffsetNow();
            while (matchGroups.Count > 0 || lastMatchGroupId == 0)
            {
                matchGroups = matchGroupAdapter.GetListGroupPaging(10, lastMatchGroupId);
                if (matchGroups.Count > 0)
                {
                    foreach (var matchGroup in matchGroups)
                    {
                        var mapIndexs = getRandomIndex(numberFloor, numberMap);
                        var idx = 0;
                        foreach (var mapIndex in mapIndexs)
                        {
                            matchGroupFloorMapAdapter.AddNew(matchGroup.MatchGroupId, listFloorId[idx++], listMapId[mapIndex], now);
                        }
                        lastMatchGroupId = matchGroup.MatchGroupId;
                    }
                }
                else if (lastMatchGroupId == 0)
                {
                    lastMatchGroupId = 1;
                }
            }
        }

        private int[] getRandomIndex(int numItem, int totalItem)
        {
            int numberRemoveItem = totalItem - numItem;
            int[] removeItemIndexs = new int[numberRemoveItem];
            int[] randomIndexs = new int[numItem];
            int currentIndex = 0;
            Random rnd = new Random();
            while (numberRemoveItem > 0)
            {
                var removeIndex = rnd.Next(totalItem);
                if (Array.IndexOf(removeItemIndexs, removeIndex) == -1)
                {
                    removeItemIndexs[--numberRemoveItem] = removeIndex;
                }
            }
            for (var idx = 0; idx < totalItem; idx++)
            {
                if (Array.IndexOf(removeItemIndexs, idx) > -1)
                {
                    continue;
                }
                else
                {
                    randomIndexs[currentIndex++] = idx;
                }
            }
            return randomIndexs;
        }
    }
}
