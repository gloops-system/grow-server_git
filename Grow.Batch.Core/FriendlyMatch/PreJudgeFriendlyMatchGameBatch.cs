﻿using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
using Fango.Batch;
//using Fango.Core.Util;
//using Fango.Core.Logging;
//using Edge.Batch.Core.Common;
//using Edge.Core.Common;
//using Edge.Data.Game.DataSet;
//using Edge.Data.Game.DataSet.Adapter.Game;
//using Edge.Data.User.DataSet;
//using Edge.Data.User.DataSet.Adapter;
//using Edge.Data.Game.DataSet.Adapter.NPCFriendlyMatch;

namespace Grow.Batch.Core.FriendlyMatch
{
    public class PreJudgeFriendlyMatchGameBatch : IJob
    {
        public JobResultCode ExecuteJob(string[] args)
        {
            throw new NotImplementedException();
        }
//        /// <summary>
//        /// 親善試合結果判定の準備 1日2回
//        /// </summary>
//        /// <param name="args"></param>
//        /// <returns></returns>
//        public JobResultCode ExecuteJob(string[] args)
//        {
//            var now = DiveDateUtil.DatabaseDate;

//            var gameDataContainer = new GameDataContainer();

//#if !PROD
//            // NPC親善試合(開発環境のみ実行)
//            var npcFriendlyMatchDataContainer = new NPCFriendlyMatchDataContainer();
//            var npcFriendlyMatchMaster = npcFriendlyMatchDataContainer.NPCFriendlyMatchMaster.GetCurrentMasterRow(now);
//#endif

//            var logData = new LogData()
//            {
//                BatchName = "PreJudgeFriendlyMatchGameBatch",
//                BatchArgs = args,
//            };

//            try
//            {
//                //開始済みの親善試合スケジュール取得
//                var friendlyMatchIds = 
//                    gameDataContainer.UserFriendlyMatchSchedule
//                                     .GetAlreadyFriendlyMatchIdsStartScheduleByResult((int) AppCode.ResultStatus.未了).ToList();

//#if !PROD
//                // NPC親善試合(開発環境のみ実行)
//                //NPC親善試合の開始済みの試合スケジュールも含める
//                if ( npcFriendlyMatchMaster != null )
//                {
//                    friendlyMatchIds.AddRange(
//                        npcFriendlyMatchDataContainer.NPCFriendlyMatchSchedule
//                                                     .GetAlreadyFriendlyMatchIdsStartScheduleByResult(
//                                                        npcFriendlyMatchMaster.EventId
//                                                        , now
//                                                        , (int) AppCode.ResultStatus.未了)
//                                                     .Where(x => !friendlyMatchIds.Contains(x)));
//                }
//#endif

//                //既に判定準備済みのリスト
//                var alreadyPreJudgeList = gameDataContainer.FriendlyMatchExecuteHistory
//                                                            .GetUnJudgeData()
//                                                            .Select(x => x.FriendlyMatchId)
//                                                            .ToArray();

//                //EndDateをnullで突っ込んでおく
//                var friendlyMatchExecuteHistoryDataTable = new GameDataSet.FriendlyMatchExecuteHistoryDataTable();
//                friendlyMatchIds.Where(x => !alreadyPreJudgeList.Contains(x)).ForEach(friendlyMatchId =>
//                {
//                    var row = friendlyMatchExecuteHistoryDataTable.NewFriendlyMatchExecuteHistoryRow();
//                    row.FriendlyMatchId = friendlyMatchId;
//                    row.AddTime = now;
//                    row.UpdtTime = now;
//                    friendlyMatchExecuteHistoryDataTable.AddFriendlyMatchExecuteHistoryRow(row);
//                });
//                gameDataContainer.FriendlyMatchExecuteHistory.BulkInsert(friendlyMatchExecuteHistoryDataTable);
//            }
//            catch (Exception ex)
//            {
//                logData.ExceptionList.Add(ex);
//            }

//            MailUtil.WriteLogAndSendMail(logData);

//            return JobResultCode.Success;
//        }
        
        public Func<bool> ShouldStop
        {
            get;
            set;
        }

        public Guid BatchId
        {
            get;
            set;
        }
    }
}
