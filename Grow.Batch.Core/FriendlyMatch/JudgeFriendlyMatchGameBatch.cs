﻿using Fango.Batch;
//using Fango.Core.Logging;
//using Fango.Core.Util;
//using Edge.Batch.Core.Common;
//using Edge.Core.Common;
//using Edge.Core.Game;
//using Edge.Data.Game.DataSet.Adapter.Game;
//using Edge.Data.Game.DataSet;
using System;
//using System.Linq;
//using System.Collections.Generic;
//using Edge.Data.Game.DataSet.Adapter.NPCFriendlyMatch;

namespace Grow.Batch.Core.FriendlyMatch
{
    public class JudgeFriendlyMatchGameBatch : IJob
    {
        public JobResultCode ExecuteJob(string[] args)
        {
            throw new NotImplementedException();
        }
//        private const int FRIENDLY_MATCH_JUDGE_COUNT = 3000;
//        private GameDataContainer gameDataContainer = new GameDataContainer();
//        private NPCFriendlyMatchDataContainer npcFriendlyMatchDataContainer = new NPCFriendlyMatchDataContainer();
//        private NPCFriendlyMatchDataSet.NPCFriendlyMatchMasterRow npcFriendlyMatchMaster = null;

//        /// <summary>
//        /// 親善試合判定 1時間に1回
//        /// </summary>
//        /// <param name="args">[0]:FriendlyMatchId</param>
//        public JobResultCode ExecuteJob(string[] args)
//        {
//            var startTime = DiveDateUtil.DatabaseDate;

//#if !PROD
//            // NPC親善試合(開発環境のみ実行)
//            npcFriendlyMatchMaster = npcFriendlyMatchDataContainer.NPCFriendlyMatchMaster.GetCurrentMasterRow(startTime);
//#endif

//            var logData = new LogData()
//            {
//                BatchName = "JudgeFriendlyMatchGameBatch",
//                BatchArgs = args,
//                ExceptionInfoList = new Dictionary<string, List<string>>()
//                                    {
//                                        {"FriendlyMatchId", new List<string>()},
//                                        {"UserId", new List<string>()},
//                                        {"TargetUserId", new List<string>()}
//                                    },
//            };

//            //データがなくなるまでがんばる
//            var isExecute = false;
//            while (true)
//            {
//                //未判定の親善試合情報取得 通常1件
//                var unJudgeExecuteHistoryRows = gameDataContainer.FriendlyMatchExecuteHistory
//                                                .GetUnJudgeData();
//                //デバッグ用
//                if (args.Any() && args[0].Length > 0) unJudgeExecuteHistoryRows = unJudgeExecuteHistoryRows.Where(x => x.FriendlyMatchId == int.Parse(args[0])).ToArray();

//                //未判定の親善試合がなければ終了
//                if (!unJudgeExecuteHistoryRows.Any()) break;

//                isExecute = true;

//                //試合実行
//                ExecuteGame(unJudgeExecuteHistoryRows, logData);
//            }

//            //失敗履歴を残す
//            if (logData.ExceptionList.Any())
//            {
//                var addTime = DiveDateUtil.DatabaseDate;
//                var dataTable = new GameDataSet.FriendlyMatchFailHistoryDataTable();
//                for (int i = 0; i < logData.ExceptionList.Count; i++)
//                {
//                    var row = dataTable.NewFriendlyMatchFailHistoryRow();
//                    row.FriendlyMatchId = int.Parse(logData.ExceptionInfoList["FriendlyMatchId"][i]);
//                    row.UserId = int.Parse(logData.ExceptionInfoList["UserId"][i]);
//                    row.TargetUserId = int.Parse(logData.ExceptionInfoList["TargetUserId"][i]);
//                    row.AddTime = addTime;
//                    dataTable.AddFriendlyMatchFailHistoryRow(row);
//                }
//                new FriendlyMatchFailHistoryDataRepositoryAdapter().BulkInsert(dataTable);
//            }

//            //実行したらログとメール
//            if (isExecute)　MailUtil.WriteLogAndSendMail(logData);

//            return JobResultCode.Success;
//        }

//        private void ExecuteGame(GameDataSet.FriendlyMatchExecuteHistoryRow[] unJudgeExecuteHistoryRows, LogData logData)
//        {
//            foreach (var historyRow in unJudgeExecuteHistoryRows)
//            {
//                //データがなくなるまでがんばる
//                while (true)
//                {
//                    //未終了のスケジュール数取得
//                    var unJudgeScheduleCount = gameDataContainer.UserFriendlyMatchSchedule
//                                                .CountByFriendlyMatchIdAndResult(historyRow.FriendlyMatchId,
//                                                                                (int)AppCode.ResultStatus.未了);
//#if !PROD
//                    // NPC親善試合(開発環境のみ実行)
//                    //NPC親善試合の未終了のスケジュール数も含める
//                    if ( npcFriendlyMatchMaster != null )
//                        unJudgeScheduleCount += npcFriendlyMatchDataContainer.NPCFriendlyMatchSchedule
//                                                .CountByFriendlyMatchIdAndResult(npcFriendlyMatchMaster.EventId, historyRow.FriendlyMatchId,
//                                                                                (int) AppCode.ResultStatus.未了);
//#endif

//                    //判定対象がないから終了
//                    if (unJudgeScheduleCount == 0) break;

//                    //未終了のスケジュール取得
//                    var userScheduleRows = gameDataContainer.UserFriendlyMatchSchedule
//                                            .GetTopDataByFriendlyMatchId(FRIENDLY_MATCH_JUDGE_COUNT, historyRow.FriendlyMatchId, (int)AppCode.ResultStatus.未了)
//                                            .Where(x => x.IsHome)
//                                            .ToArray();

//                    foreach (var userScheduleRow in userScheduleRows)
//                    {
//                        var homeUserId = userScheduleRow.UserId;
//                        var awayUserId = userScheduleRow.TargetUserId;

//                        //再起動通知が来たら終了
//                        if (logData.ShoudStop = ShouldStop())
//                        {
//                            logData.ExecutingList.Add("FriendlyMatchId", userScheduleRow.FriendlyMatchId.ToString());
//                            logData.ExecutingList.Add("UserId", userScheduleRow.UserId.ToString());
//                            logData.ExecutingList.Add("TargetUserId", userScheduleRow.TargetUserId.ToString());
//                            return;
//                        }

//                        try
//                        {
//                            new FriendlyMatchGame(homeUserId, awayUserId).ExecuteGame();

//                        }
//                        catch (Exception e)
//                        {
//                            logData.ExceptionList.Add(e);
//                            logData.ExceptionInfoList["FriendlyMatchId"].Add(userScheduleRow.FriendlyMatchId.ToString());
//                            logData.ExceptionInfoList["UserId"].Add(userScheduleRow.UserId.ToString());
//                            logData.ExceptionInfoList["TargetUserId"].Add(userScheduleRow.TargetUserId.ToString());
//                        }
//                    }

//#if !PROD
//                    // NPC親善試合(開発環境のみ実行)
//                    //NPC親善試合の未終了スケジュール取得
//                    if ( npcFriendlyMatchMaster != null )
//                    {
//                        var npcScheduleRows = npcFriendlyMatchDataContainer.NPCFriendlyMatchSchedule
//                                                .GetTopDataByFriendlyMatchId(npcFriendlyMatchMaster.EventId, FRIENDLY_MATCH_JUDGE_COUNT
//                                                                            , historyRow.FriendlyMatchId, (int) AppCode.ResultStatus.未了)
//                                                .Where(x => !x.IsHome)
//                                                .ToArray();

//                        foreach ( var npcScheduleRow in npcScheduleRows )
//                        {
//                            var homeUserId = npcScheduleRow.TargetUserId;
//                            var awayUserId = npcScheduleRow.UserId;

//                            //再起動通知が来たら終了
//                            if ( logData.ShoudStop = ShouldStop() )
//                            {
//                                logData.ExecutingList.Add("FriendlyMatchId", npcScheduleRow.FriendlyMatchId.ToString());
//                                logData.ExecutingList.Add("UserId", npcScheduleRow.UserId.ToString());
//                                logData.ExecutingList.Add("TargetUserId", npcScheduleRow.TargetUserId.ToString());
//                                return;
//                            }

//                            try
//                            {
//                                new NPCFriendlyMatchGame(homeUserId, awayUserId, npcFriendlyMatchMaster.EventId).ExecuteGame();

//                            }
//                            catch ( Exception e )
//                            {
//                                logData.ExceptionList.Add(e);
//                                logData.ExceptionInfoList["FriendlyMatchId"].Add(npcScheduleRow.FriendlyMatchId.ToString());
//                                logData.ExceptionInfoList["UserId"].Add(npcScheduleRow.UserId.ToString());
//                                logData.ExceptionInfoList["TargetUserId"].Add(npcScheduleRow.TargetUserId.ToString());
//                            }
//                        }
//                    }
//#endif
//                }

//                //判定履歴更新
//                gameDataContainer.FriendlyMatchExecuteHistory.UpdateEndDateByPK(historyRow.FriendlyMatchId, DiveDateUtil.DatabaseDate);
//            }
//        }

        public Func<bool> ShouldStop
        {
            get;
            set;
        }

        public Guid BatchId
        {
            get;
            set;
        }
    }
}
