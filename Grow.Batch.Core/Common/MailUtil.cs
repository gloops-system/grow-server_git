﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fango.Core.Logging;
using Fango.Core.Util;
using Grow.Core.Application.Configuration;
using Grow.Core.Application.Util;

namespace Grow.Batch.Core.Common
{
    //　TODO System.Net.Mail に依存しているためプロジェクトを分割する
    public class MailUtil
    {
        private static readonly string From = MobileSettings.MailFrom;
        private static readonly string To = MobileSettings.MailTo;
        private static readonly string Subject = MobileSettings.MailSubject;
        private static readonly string Host = MobileSettings.MailSmtpHost;
        private static readonly string User = MobileSettings.MailSmtpUser;
        private static readonly string Password = MobileSettings.MailSmtpPass;
        private static readonly int Timeout = MobileSettings.MailSmtpTimeout;

        public static void SendMail(string message)
        {
            SendMailCore(message, string.Empty);
        }

        private static void SendMailCore(string message, string subject)
        {
            // 本番環境のみメール送信する
#if DEBUG
#else
            try
            {
                var msg = new MailMessage(
                    From,
                    To,
                    Subject + " " + subject,
                    message);

                var sc = new SmtpClient { Host = Host };
                if (!string.IsNullOrEmpty(User))
                    sc.Credentials = new NetworkCredential(User, Password);
                sc.EnableSsl = false;
                sc.Timeout = Timeout;
                sc.Send(msg);
                msg.Dispose();
            }
            catch (Exception ex)
            {
                LoggerManager.DefaultLogger.ErrorFormat(LogEntry.GetLogEntry(ex.Message), ex);
            }
#endif
        }

        public static void WriteLogAndSendMail(LogData logData)
        {
            var endTime = DateUtil.GetDateTimeOffsetNow();
            var message = string.Format("■StartTime:{0}\r\n\r\n■EndTime:{1}\r\n\r\n■TotalTime:{2}\r\n\r\n",
                                        logData.StartTime,
                                        endTime,
                                        endTime - logData.StartTime);

            var subject = logData.BatchName;
            if (logData.BatchArgs.Length > 0)
            {
                message += "■args:";
                logData.BatchArgs.ForEach(x =>
                    {
                        message += x;
                    });
                message += "\r\n\r\n";
            }

            message += " ■Result:";

            if (logData.ShoudStop)
            {
                subject += " 再起動通知";
                message += " 再起動通知により処理を中断しました。\r\n";

                if (logData.ExecutingList.Any())
                {
                    logData.ExecutingList.ForEach(x =>
                        {
                            message += string.Format(" {0}:{1}", x.Key, x.Value);
                        });
                    message += " 以降のバッチが未集計です\r\n";
                }
            }

            if (logData.ExceptionList.Any())
            {
                subject += " 例外検出";
                message += " 例外を検出しました\r\n";
                message += " ---------------------------------------------------------\r\n";

                var logMessage = string.Empty;
                for (var i = 0; i < logData.ExceptionList.Count; i++)
                {
                    logData.ExceptionInfoList.ForEach(x =>
                        {
                            if (x.Value.Any())
                            {
                                logMessage += "・実行情報:";
                                logMessage += string.Format("{0}:{1}\r\n", x.Key, x.Value[i]);
                            }
                        });

                    logMessage += string.Format(" {0}\r\n\r\n", logData.ExceptionList[i]);
                }
                LoggerManager.DefaultLogger.Error(logMessage);
                message += logMessage;
            }
            else if (!logData.ShoudStop)
            {
                subject += " 正常終了";
                message += " 正常終了";
            }

            LoggerManager.DefaultLogger.Info(message);
            SendMailCore(message, subject);
        }
    }

    public class LogData
    {
        public string BatchName { get; set; }
        public string[] BatchArgs { get; set; }
        public DateTimeOffset StartTime { get; set; }

        public List<Exception> ExceptionList { get; set; }
        public Dictionary<string, List<string>> ExceptionInfoList { get; set; }

        public bool ShoudStop { get; set; }
        public Dictionary<string, string> ExecutingList { get; set; }

        public LogData()
        {
            ExceptionList = new List<Exception>();
            ExceptionInfoList = new Dictionary<string, List<string>> {};
            ExecutingList = new Dictionary<string, string>();
            ShoudStop = false;
            StartTime = DateUtil.GetDateTimeOffsetNow();
        }
    }
}