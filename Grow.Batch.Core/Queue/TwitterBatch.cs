﻿using System;
using System.Linq;
using Fango.Batch;
using Fango.Core.Sequence;
using Fango.Core.Util;
using Fango.OpenSocial.Twitter;
using Grow.Core.Application.Caching;
using Grow.Core.Application.Util;
using Grow.Data.General.DataSet.Container;
using Grow.Data.History.DataSet.Container;

namespace Grow.Batch.Core.Queue
{
    public class TwitterBatch : IJob
    {
        public JobResultCode ExecuteJob(string[] args)
        {
            var queueContainer = new QueueDataContainer();
            var historyContainer = new HistoryDataContainer();

            var queueTwitterAdapter = queueContainer.QueueTwitter;
            var queueTwitterHistoryAdapter = historyContainer.QueueTwitterHistory;

            var now = DateUtil.GetDateTimeOffsetNow();

            const int topCount = 10;
            var queueTwitterList = queueTwitterAdapter.GetDataByTopAndDeliveryDate(topCount, now);
            if (queueTwitterList.Any())
                return JobResultCode.Success;

            try
            {
                var twitter = new TwitterManager(true);
                foreach (var queueTwitterRow in queueTwitterList)
                {
                    // Twitterへのツイート
                    twitter.SendTweet(queueTwitterRow.Message);

                    // 履歴追加
                    var historyId = SequenceClientManager.DefaultSequenceClient.GetNextValue(CacheKey.QueueTwitterId(), 1);
                    queueTwitterHistoryAdapter.AddNew(historyId, now, queueTwitterRow.DeliveryDate, queueTwitterRow.Message);

                    // キューの削除
                    queueTwitterAdapter.DeleteByPK(queueTwitterRow.TwitterId);

                    // タイムアウトの場合
                    if (ShouldStop())
                    {
                        // TODO : SEND MAIL
                        return JobResultCode.Timeout;
                    }
                }
            }
            catch (Exception)
            {
                // TODO : SEND MAIL
                return JobResultCode.Fail;
            }

            return JobResultCode.Success;
        }

        public Func<bool> ShouldStop
        {
            get;
            set;
        }

        public Guid BatchId
        {
            get;
            set;
        }
    }
}
