﻿using System;
using System.Linq;
using Fango.Batch;
using Fango.Core.Sequence;
using Fango.Core.Util;
using Fango.OpenSocial.Facebook;
using Grow.Core.Application.Caching;
using Grow.Core.Application.Util;
using Grow.Data.General.DataSet.Container;
using Grow.Data.History.DataSet.Container;

namespace Grow.Batch.Core.Queue
{
    public class FacebookBatch : IJob
    {
        public JobResultCode ExecuteJob(string[] args)
        {
            var queueContainer = new QueueDataContainer();
            var historyContainer = new HistoryDataContainer();

            var queueFacebookAdapter = queueContainer.QueueFacebook;
            var queueFacebookHistoryAdapter = historyContainer.QueueFacebookHistory;

            var now = DateUtil.GetDateTimeOffsetNow();

            const int topCount = 10;
            var queueFacebookList = queueFacebookAdapter.GetDataByTopAndDeliveryDate(topCount, now);
            if (queueFacebookList.Any())
                return JobResultCode.Success;

            try
            {
                var facebook = new FacebookManager();
                foreach (var queueFacebookRow in queueFacebookList)
                {
                    // Facebookへのポスト
                    facebook.Post(queueFacebookRow.Message,
                                  queueFacebookRow.Picture,
                                  queueFacebookRow.Link,
                                  queueFacebookRow.Name,
                                  queueFacebookRow.Caption,
                                  queueFacebookRow.Description);

                    // 履歴追加
                    var historyId = SequenceClientManager.DefaultSequenceClient.GetNextValue(CacheKey.QueueFacebookId(), 1);
                    queueFacebookHistoryAdapter.AddNew(historyId, now, queueFacebookRow.DeliveryDate,
                                                       queueFacebookRow.Message, queueFacebookRow.Link,
                                                       queueFacebookRow.Picture, queueFacebookRow.Name,
                                                       queueFacebookRow.Caption, queueFacebookRow.Description);

                    // キューの削除
                    queueFacebookAdapter.DeleteByPK(queueFacebookRow.FacebookId);

                    // タイムアウトの場合
                    if (ShouldStop())
                    {
                        // TODO : SEND MAIL
                        return JobResultCode.Timeout;
                    }
                }
            }
            catch (Exception)
            {
                // TODO : SEND MAIL
                return JobResultCode.Fail;
            }

            return JobResultCode.Success;
        }

        public Func<bool> ShouldStop
        {
            get;
            set;
        }

        public Guid BatchId
        {
            get;
            set;
        }
    }
}