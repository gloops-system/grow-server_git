﻿using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
using Fango.Batch;
//using Fango.Core.Sequence;
//using Fango.Core.Util;
//using Edge.Batch.Core.Common;
//using Edge.Core.Caching;
//using Edge.Core.Common;
//using Edge.Core.Common.PushNotice;
//using Edge.Data.General.DataSet.Adapter.Queue;
//using Edge.Data.User.DataSet.Adapter;

namespace Grow.Batch.Core.Queue
{
    public class NewsPushNoftificationBatch : IJob
    {
        public JobResultCode ExecuteJob(string[] args)
        {
            throw new NotImplementedException();
        }
        ///// <summary>
        ///// ニュース配信キューを登録する
        ///// </summary>
        ///// <param name="args"></param>
        ///// <returns></returns>
        //public JobResultCode ExecuteJob(string[] args)
        //{
        //    const int transaction = 10000;
        //    var queueDataContainer = new QueueDataContainer();
        //    var newsPushNotification = queueDataContainer.NewsPushNotification;
        //    var newsQueue = newsPushNotification.GetUnprocessedRows();
        //    if(!newsQueue.Any())
        //        return JobResultCode.Success;

        //    var userData = new UserCommonDataContainer().UserLastLogin.GetData();
        //    if (!userData.Any())
        //        return JobResultCode.Success;

        //    try
        //    {
        //        foreach (var row in newsQueue)
        //        {
        //            var index = 0;
        //            while (index <= userData.Count())
        //            {
        //                var userList = userData.Skip(index).Take(transaction);

        //                foreach (var userRow in userList)
        //                {
        //                    try
        //                    {
        //                        var queueId = SequenceClientManager.DefaultSequenceClient.GetNextValue(CacheKey.QueuePushNotificationsId(), 1);
        //                        var userDevice = new UserDevice(userRow.UserId);
        //                        if (userDevice.HaveSetting(AppCode.PushNotificationSettings.ニュース))
        //                            queueDataContainer.QueuePushNotifications.AddNew(queueId, userDevice.DeviceToken,
        //                                                                             (int)
        //                                                                             AppCode.PushNotificationType
        //                                                                                        .ニュース通知, row.Message,
        //                                                                             AppCode.PushDefaultSoundPath,
        //                                                                             userRow.UserId, null,
        //                                                                             row.QueueFromDate);
        //                    }
        //                    catch
        //                    {
        //                        //前回途中で終了した場合等はExceptionが出る
        //                        ;
        //                    }
        //                }

        //                // 時間制限チェック
        //                if (ShouldStop())
        //                {
        //                    MailUtil.SendMail("NewsPushNoftificationBatchがタイムアウトにより強制終了しました。");
        //                    return JobResultCode.Timeout;
        //                }
        //                index += transaction;
        //            }
        //            //全部おわった
        //            newsPushNotification.UpdateQueueSetDate(row.NewsNotificationId);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        MailUtil.SendMail(string.Format("NewsPushNoftificationBatchが異常終了しました。Exception:{0}", e.Message));
        //        return JobResultCode.Fail;
        //    }

        //    return JobResultCode.Success;
        //}

        public Func<bool> ShouldStop
        {
            get;
            set;
        }

        public Guid BatchId
        {
            get;
            set;
        }
    }
}
