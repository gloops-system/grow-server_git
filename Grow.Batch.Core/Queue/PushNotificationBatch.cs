﻿using System;
using System.IO;
using Fango.Batch;
using PushSharp;
using PushSharp.Android;
using PushSharp.Apple;

namespace Grow.Batch.Core.Queue
{
    // TODO : Fango.OpenSocial.OpenSocialあたりへ
    public class PushNotificationBatch : IJob
    {
        public Func<bool> ShouldStop { get; set; }
        public Guid BatchId { get; set; }

        public string AppleCertificatePath { get { return "hogehoge"; } }
        public string AppleCertificatePassword { get { return "mogemoge"; } }
        public string AndroidAuthorizationKey { get { return "komekome"; } }

        // TODO : どこかへ移動
        private enum TargetDevice
        {
            All = 0,
            iOS = 1,
            Android = 2,
        }

        public JobResultCode ExecuteJob(string[] args)
        {
            var result = JobResultCode.Fail;

            // 対象の端末判定
            var targetDevice = (TargetDevice)0; // TODO : 実装
            switch (targetDevice)
            {
                case TargetDevice.All:
                    result = ExecuteAll();
                    break;
                case TargetDevice.iOS:
                    result = ExecuteApple();
                    break;
                case TargetDevice.Android:
                    result = ExecuteAndroid();
                    break;
            }
            return result;
        }

        /// <summary>
        /// 送信対象かの判定処理
        /// </summary>
        /// <returns></returns>
        private bool Validate()
        {
            // TODO : 色々判定
            return true;
        }

        /// <summary>
        /// 全OS用の送信処理
        /// </summary>
        private JobResultCode ExecuteAll()
        {
            var resultApple = ExecuteApple();
            var resultAndroid = ExecuteAndroid();
            return resultApple == resultAndroid && resultApple == JobResultCode.Success
                       ? JobResultCode.Success
                       : JobResultCode.Fail;
        }

        /// <summary>
        /// iOS用の送信処理
        /// </summary>
        private JobResultCode ExecuteApple()
        {
            // TODO : QueueテーブルからQueueを取得
            // TODO : Queueが送る対象かをValidateでチェック
            if (Validate())
            {
                // Certificateの中身を取得
                var appleCertificate = File.ReadAllBytes(
                    Path.Combine(
                        AppDomain.CurrentDomain.BaseDirectory, AppleCertificatePath
                        ));

                var pushBroker = new PushBroker();
                pushBroker.RegisterAppleService(new ApplePushChannelSettings(false,
                                                                             appleCertificate,
                                                                             AppleCertificatePassword));

                // 送信
                pushBroker.QueueNotification(new AppleNotification()
                                                 .ForDeviceToken("togetoge")
                                                 .WithAlert("message sample")
                                                 .WithSound("default"));

                // Queue削除
            }
            return JobResultCode.Success;
        }

        /// <summary>
        /// Android用の送信処理
        /// </summary>
        private JobResultCode ExecuteAndroid()
        {
            // TODO : QueueテーブルからQueueを取得
            // TODO : Queueが送る対象かをValidateでチェック
            if (Validate())
            {
                var pushBroker = new PushBroker();
                pushBroker.RegisterGcmService(new GcmPushChannelSettings(AndroidAuthorizationKey));

                // 送信
                pushBroker.QueueNotification(new GcmNotification()
                                                 .ForDeviceRegistrationId("DEVICE REGISTRATION ID HERE")
                                                 .WithJson(@"{""alert"":""Hello World!""}"));

                // Queue削除
            }
            return JobResultCode.Success;
        }
    }
}