﻿using Grow.Data.Admin.DataSet.Adapter.Payment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Data.Admin.DataSet.Adapter
{
    public class AdminPaymentDataContainer
    {
        private Lazy<AdminPaymentDataRepository> _adminPaymentDataRepository;
        public AdminPaymentDataRepository AdminPaymentDataRepository { get { return _adminPaymentDataRepository.Value; } }
        private Lazy<AdminPaymentForecastDataRepositoryAdapter> _adminPaymentForecast;
        public AdminPaymentForecastDataRepositoryAdapter AdminPaymentForecast { get { return _adminPaymentForecast.Value; } }

        public AdminPaymentDataContainer()
        {
            _adminPaymentDataRepository = new Lazy<AdminPaymentDataRepository>(() => new AdminPaymentDataRepository(), true);
            _adminPaymentForecast = new Lazy<AdminPaymentForecastDataRepositoryAdapter>(() => new AdminPaymentForecastDataRepositoryAdapter(), true);
        }
    }
}
