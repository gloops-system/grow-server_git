﻿using Grow.Data.Admin.DataSet.AdminPaymentDataSetTableAdapters;
using Grow.Data.Admin.DataSet.Properties;
using Fango.Core.Data;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Data.Admin.DataSet.Adapter.Payment
{
    public class AdminPaymentForecastDataRepositoryAdapter
    {
        public AdminPaymentDataSet.AdminPaymentForecastRow[] AdminPaymentForecastDataRows()
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var tableAdapter = new AdminPaymentForecastTableAdapter())
                {
                    return tableAdapter.GetData();
                }
            });
            return result.ToArray();
        }

        public AdminPaymentDataSet.AdminPaymentForecastRow[] GetDataByDate(DateTime fromDate, DateTime toDate)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var tableAdapter = new AdminPaymentForecastTableAdapter())
                {
                    return tableAdapter.GetDataByDate(fromDate, toDate).ToArray();
                }
            });
            return result.ToArray();
        }

        public void DeleteByDate(DateTime fromDate, DateTime toDate)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var tableAdapter = new AdminPaymentForecastTableAdapter())
                {
                    tableAdapter.DeleteByDate(fromDate, toDate);
                }
            });
        }

        public void BulkInsert(AdminPaymentDataSet.AdminPaymentForecastDataTable adminPaymentForecastDataTable)
        {
            var inazumaUserConnstring = Settings.Default.edge_admin_devConnectionString;

            new DbAccessHandler().ExecuteAction(() => SqlHelper.BulkInsert(inazumaUserConnstring, adminPaymentForecastDataTable));
        }
    }
}
