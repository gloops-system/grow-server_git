﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Fango.Core.Data;
using Fango.Core.Caching;
using Fango.Core.Util;
using Grow.Data.Admin.DataSet.AdminPaymentDataSetTableAdapters;
//using Edge.Data.Admin.DataSet.AdminPaymentDataSetTableAdapters;
//using Edge.Data.Admin.DataSet.AndroidAdminPaymentDataSetTableAdapters;
using Fango.Redis.Caching;
using Grow.Data.DataSet.MasterDataSetTableAdapters;
using Grow.Data.DataSet.AppPaymentDataSetTableAdapters;

namespace Grow.Data.Admin.DataSet.Adapter.Payment
{
    public class MonthlyRow
    {
        public DateTimeOffset Date { get; set; }
        public long CurrencyTotal { get; set; }
        public int CurrencyUser { get; set; }
    }

    public class AdminPaymentDataRepository
    {
        public void UpdateOrInsert(DateTimeOffset date, long currencyIosTotal, long currencyAndroidTotal,
                                   int currencyIosUser, int currencyAndroidUser, long shopBuyUseCoin,
                                   long shopBuyUser, long scoutUseCoin, long scoutUsedCoinUser, long gachaUseCoin,
                                   long gachaUsedCoinUser)
        {
            
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var tableAdapter = new AdminPaymentSummaryDailyTableAdapter())
                {
                    int num = tableAdapter.UpdateDataByPK(date, currencyIosTotal, currencyAndroidTotal, currencyIosUser, currencyAndroidUser,
                                                          DateUtil.GetDateTimeOffsetNow(), shopBuyUseCoin, shopBuyUser, scoutUseCoin, scoutUsedCoinUser, gachaUseCoin, gachaUsedCoinUser);
                    if (num==0)
                    {
                        tableAdapter.AddNew(date, currencyIosTotal, currencyAndroidTotal,
                                            currencyIosUser, currencyAndroidUser,
                                            DateUtil.GetDateTimeOffsetNow(),
                                            shopBuyUseCoin, shopBuyUser, scoutUseCoin, scoutUsedCoinUser,
                                            gachaUseCoin, gachaUsedCoinUser);
                    }
                }
            });
        }

        public List<AdminPaymentDataSet.AdminPaymentSummaryDailyRow> GetDataByFromTo(DateTimeOffset from,
                                                                                     DateTimeOffset to)
        {
            var list = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var tableAdapter = new AdminPaymentSummaryDailyTableAdapter())
                {
                    var data = tableAdapter.GetDataByFromTo(from, to);
                    return data;
                }
            });
            return list.ToList();
        } 

        /// <summary>
        /// 毎日のユーザの通貨を記録する
        /// </summary>
        public void AdminUniqueUserCurrency(int datediff)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var tableAdapter = new AdminUniqueUserCurrencyTableAdapter())
                {
                    tableAdapter.AdminUniqueUserCurrencyUpdate(datediff);
                }
            });
        }

        /// <summary>
        /// 月毎データ（デバイス別）
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public AdminPaymentDataSet.AdminPaymentSummaryMonthlyCastRow[] GetMonthlyDataDetail(DateTimeOffset from, DateTimeOffset to)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var tableAdapter = new AdminPaymentSummaryMonthlyCastTableAdapter())
                {
                    return tableAdapter.GetData(from, to);
                }
            });
            return result.ToArray();
        }

        /// <summary>
        /// 月毎データ
        /// </summary>
        /// <returns></returns>
        public List<AdminPaymentDataSet.AdminPaymentSummaryMonthlyRow> GetMonthlyData()
        {
            var list = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var tableAdapter = new AdminPaymentSummaryMonthlyTableAdapter())
                    {
                        return tableAdapter.GetData();
                    }
                });
            return list.ToList();
        }

        public MonthlyRow GetCurrentIosMonthlyData()
        {
            MonthlyRow row;

            string year = DateUtil.GetDateTimeOffsetNow().Year.ToString(CultureInfo.InvariantCulture);
            string month = DateUtil.GetDateTimeOffsetNow().Month.ToString(CultureInfo.InvariantCulture);
            DateTimeOffset from = DateTimeOffset.Parse(year + "-" + month + "-01 0:00:00+09:00");
            DateTimeOffset to = DateUtil.GetDateTimeOffsetNow();

            string cacheKey = "MonthlyIosData_" + from.ToString("yyyy-MM");

            //キャッシュの確認
            var cacheClient = CacheClientFactory();
            var cacheData = cacheClient.Get<MonthlyRow>(cacheKey);
            if (cacheData!=null)
            {
                return cacheData;
            }

            row = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var masterAdapter = new GemMasterTableAdapter())
                    {
                        var masterData = masterAdapter.GetData();
                        using (var tableAdapter = new AppPaymentTableAdapter())
                        {
                            var data = tableAdapter.GetDataByFromTo(from, to.AddDays(1), 10 /*完了*/);
                            var currencyTotal = (from d in data
                                                 join m in masterData on d.ProductId equals m.ProductId
                                                 select new {price = d.Gem, userId = d.UserId}
                                                ).Sum(x=>x.price);
                            var currencyUser = data.Select(x => x.UserId).Distinct().Count();

                            var monthlyRow = new MonthlyRow
                                {
                                    Date = from,
                                    CurrencyTotal = currencyTotal,
                                    CurrencyUser = currencyUser
                                };

                            //5分間キャッシュにストア
                            cacheClient.Add(cacheKey, monthlyRow, TimeSpan.FromMinutes(5));

                            return monthlyRow;
                        }
                    }
                });
            return row;
        }

        public MonthlyRow GetCurrentAndroidMonthlyData()
        {
            MonthlyRow row;

            string year = DateUtil.GetDateTimeOffsetNow().Year.ToString(CultureInfo.InvariantCulture);
            string month = DateUtil.GetDateTimeOffsetNow().Month.ToString(CultureInfo.InvariantCulture);
            DateTimeOffset from = DateTimeOffset.Parse(year + "-" + month + "-01 0:00:00+09:00");
            DateTimeOffset to = DateUtil.GetDateTimeOffsetNow();

            string cacheKey = "MonthlyArdData_" + from.ToString("yyyy-MM");

            //キャッシュの確認
            var cacheClient = CacheClientFactory();
            var cacheData = cacheClient.Get<MonthlyRow>(cacheKey);
            if (cacheData != null)
            {
                return cacheData;
            }

            row = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var masterAdapter = new AndroidGemMasterTableAdapter())
                {
                    var masterData = masterAdapter.GetData();
                    using (var tableAdapter = new AndroidAppPaymentTableAdapter())
                    {
                        var data = tableAdapter.GetDataByFromTo(from, to.AddDays(1), 10 /*完了*/);
                        var currencyTotal = (from d in data
                                             join m in masterData on d.ProductId equals m.ProductId
                                             select new { price = d.Gem, userId = d.UserId }
                                            ).Sum(x => x.price);
                        var currencyUser = data.Select(x => x.UserId).Distinct().Count();

                        var monthlyRow = new MonthlyRow
                        {
                            Date = from,
                            CurrencyTotal = currencyTotal,
                            CurrencyUser = currencyUser
                        };

                        //5分間キャッシュにストア
                        cacheClient.Add(cacheKey, monthlyRow, TimeSpan.FromMinutes(5));

                        return monthlyRow;
                    }
                }
            });
            return row;
        }

        private RedisCacheClient CacheClientFactory()
        {
            var cacheClient = new RedisCacheClient();
            return cacheClient;
        }

        public void UpdateOrInsertMonthlyData(DateTimeOffset date, long currencyTotal, int currencyUser)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var tableAdapter = new AdminPaymentSummaryMonthlyTableAdapter())
                {
                    tableAdapter.AddOrUpdateAdminPaymentSummaryMonthly(date, currencyTotal, currencyUser);
                }
            });
        }

        /// <summary>
        /// 本日のデータを取得する。5分間キャッシュする。
        /// </summary>
        /// <returns></returns>
        public MonthlyRow GetTodayIosData()
        {
            DateTimeOffset now = DateUtil.GetDateTimeOffsetNow();
            string year = now.Year.ToString(CultureInfo.InvariantCulture);
            string month = now.Month.ToString(CultureInfo.InvariantCulture);
            string day = now.Day.ToString(CultureInfo.InvariantCulture);

            DateTimeOffset from = DateTimeOffset.Parse(string.Format("{0}-{1}-{2}T00:00:00+09:00", year, month, day));
            DateTimeOffset to = from.AddDays(1);

            string cacheKey = "TodayIosData_" + from.ToString("yyyy-MM-dd");
            
            //キャッシュの確認
            var cacheClient = CacheClientFactory();
            var cacheData = cacheClient.Get<MonthlyRow>(cacheKey);
            if (cacheData!=null)
            {
                return cacheData;
            }

            var row = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var masterAdapter = new GemMasterTableAdapter())
                    {
                        var masterData = masterAdapter.GetData();

                        using (var paymentAdapter = new AppPaymentTableAdapter())
                        {
                            var data = paymentAdapter.GetDataByFromTo(from, to, 10 /*完了*/);
                            var currencyTotal = (from d in data
                                                 join m in masterData on d.ProductId equals m.ProductId
                                                 select new { price = m.Price, userId = d.UserId }
                                                ).Sum(x => x.price);
                            var currencyUser = data.Select(x => x.UserId).Distinct().Count();

                            var monthlyRow = new MonthlyRow
                                {
                                    CurrencyTotal = currencyTotal,
                                    CurrencyUser = currencyUser,
                                    Date = from
                                };

                            //5分間キャッシュにストア
                            cacheClient.Add(cacheKey, monthlyRow, TimeSpan.FromMinutes(5));
                            return monthlyRow;
                        }
                    }
                });

            return row;
        }

        /// <summary>
        /// 本日のデータを取得する。5分間キャッシュする。
        /// </summary>
        /// <returns></returns>
        public MonthlyRow GetTodayAndroidData()
        {
            DateTimeOffset now = DateUtil.GetDateTimeOffsetNow();
            string year = now.Year.ToString(CultureInfo.InvariantCulture);
            string month = now.Month.ToString(CultureInfo.InvariantCulture);
            string day = now.Day.ToString(CultureInfo.InvariantCulture);

            DateTimeOffset from = DateTimeOffset.Parse(string.Format("{0}-{1}-{2}T00:00:00+09:00", year, month, day));
            DateTimeOffset to = from.AddDays(1);

            string cacheKey = "TodayArdData_" + from.ToString("yyyy-MM-dd");

            //キャッシュの確認
            var cacheClient = CacheClientFactory();
            var cacheData = cacheClient.Get<MonthlyRow>(cacheKey);
            if (cacheData != null)
            {
                return cacheData;
            }

            var row = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var masterAdapter = new AndroidGemMasterTableAdapter())
                {
                    var masterData = masterAdapter.GetData();

                    using (var paymentAdapter = new AndroidAppPaymentTableAdapter())
                    {
                        var data = paymentAdapter.GetDataByFromTo(from, to, 10 /*完了*/);
                        var currencyTotal = (from d in data
                                             join m in masterData on d.ProductId equals m.ProductId
                                             select new { price = m.Price, userId = d.UserId }
                                            ).Sum(x => x.price);
                        var currencyUser = data.Select(x => x.UserId).Distinct().Count();

                        var monthlyRow = new MonthlyRow
                        {
                            CurrencyTotal = currencyTotal,
                            CurrencyUser = currencyUser,
                            Date = from
                        };

                        //5分間キャッシュにストア
                        cacheClient.Add(cacheKey, monthlyRow, TimeSpan.FromMinutes(5));
                        return monthlyRow;
                    }
                }
            });

            return row;
        }
    }
}
