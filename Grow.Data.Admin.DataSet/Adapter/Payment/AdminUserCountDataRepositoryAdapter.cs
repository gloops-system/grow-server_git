﻿using Fango.Core.Data;
using Grow.Data.Admin.DataSet.AdminPaymentDataSetTableAdapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grow.Data.Admin.DataSet;

namespace Grow.Data.Admin.DataSet.Adapter.Payment
{
    public class AdminUserCountDataRepositoryAdapter
    {
        public AdminPaymentDataSet.AdminUserCountRow[] GetDataByDateRange(DateTime from, DateTime to)
        {
            var rows = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var tableAdapter = new AdminUserCountTableAdapter())
                {
                    return tableAdapter.GetDataByDateRange(from, to).ToArray();
                }
            });

            return rows;
        }
    }
}
