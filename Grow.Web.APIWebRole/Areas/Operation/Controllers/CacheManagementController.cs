﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Grow.Core.Application;
using Grow.Core.Application.Operation;

namespace Grow.Web.APIWebRole.Areas.Operation.Controllers
{
    public class CacheManagementController : ApiController
    {
        // GET api/operation/cachemanagement/GetRefreshLocalCache
        [ActionName("RefreshLocalCache")]
        public HttpResponseMessage GetRefreshLocalCache()
        {
            using (var applicationContext = new GrowApplicationModel())
            {
                var apiModel = new CacheManagementApiModel(applicationContext);
                try
                {
                    var responseContent = apiModel.RefreshLocalCache(null);
                    return Request.CreateResponse(HttpStatusCode.OK, responseContent);
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }
    }
}