﻿using System.Web.Mvc;

namespace Grow.Web.APIWebRole.Areas.WebView
{
    public class WebViewAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "WebView";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "WebView_default",
                "WebView/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
