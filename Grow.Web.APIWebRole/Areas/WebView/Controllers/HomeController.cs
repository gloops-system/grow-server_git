﻿using System.Web.Mvc;

namespace Grow.Web.APIWebRole.Areas.WebView.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /WebView/Home/

        public ActionResult Index()
        {
            return View();
        }

    }
}