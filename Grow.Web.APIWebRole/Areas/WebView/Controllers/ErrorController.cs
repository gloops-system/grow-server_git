﻿using System.Web.Mvc;
using Grow.Core.Application.Configuration;

namespace Grow.Web.APIWebRole.Areas.WebView.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.ImageRoot = EdgeConfigurationManager.GetImageRoot();
            ViewBag.Message = "<h2>サーバーエラー</h2> <p> 申し訳ございません。<br/> 予期せぬ問題が発生しました。<br/> 早急に解決できるようにいたしますので、<br/> 今しばらくお待ちください。</p>";
            return View("Index");
        }

        public ActionResult NotFound()
        {
            ViewBag.ImageRoot = EdgeConfigurationManager.GetImageRoot();
            ViewBag.Message = "<h2>ページがありません</h2><p>申し訳ございません。<br/>ただいま準備中です。</p>";
            //Response.StatusCode = (int) HttpStatusCode.NotFound;
            return View("Index");
        }

    }
}