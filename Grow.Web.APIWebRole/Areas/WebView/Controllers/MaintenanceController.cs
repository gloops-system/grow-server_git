﻿using System.Web.Mvc;

namespace Grow.Web.APIWebRole.Areas.WebView.Controllers
{
    public class MaintenanceViewModel
    {
        public int VisibleTime { set; get; }
        public string DateTime { set; get; }
        public string Message { set; get; }
    }

    public class MaintenanceController : Controller
    {

        public ActionResult Index()
        {
            //var tableAdapter = AdminMaintenanceMasterCacheTableAdapter.GetInstance();
            //var messageList =
            //    tableAdapter.GetData().Where(x => x.TimeVisible != 0).Select(x => new MaintenanceViewModel()
            //        {
            //            VisibleTime = x.TimeVisible,
            //            DateTime = x.UpdtTime.ToString("M月d日HH:mm更新"),
            //            Message = WebUtility.HtmlDecode(x.Message)
            //        });
            //ViewBag.Messages = messageList;
            return View();
        }

    }
}