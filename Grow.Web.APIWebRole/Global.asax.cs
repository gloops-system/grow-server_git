﻿using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Fango.Web.Core.Configuration;

namespace Grow.Web.APIWebRole
{
    // メモ: IIS6 または IIS7 のクラシック モードの詳細については、
    // http://go.microsoft.com/?LinkId=9394801 を参照してください

    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
#if DEBUG
            // デバッグ時のみappSettingsの内容をlocal.configファイルで上書きする
            LocalWebConfiguration.LoadSettings(HttpContext.Current.Server.MapPath("~/local.config"));
#endif

//            // ローカルストレージへのパスを環境変数に設定しWeb.Configの設定から使用できるようにする
//            var rootPath = RoleEnvironment.GetLocalResource("Logs").RootPath;
//            Environment.SetEnvironmentVariable("AzureLogs", rootPath);
//            // ログの出力情報を環境変数に設定しWeb.Configの設定から使用できるようにする
//            Environment.SetEnvironmentVariable("DeploymentId", RoleEnvironment.DeploymentId);
//            Environment.SetEnvironmentVariable("RoleInstance", RoleEnvironment.CurrentRoleInstance.Id);

            AreaRegistration.RegisterAllAreas();

//            // ApiControllerのセレクタを名前空間を使用するものに切り替える
//            GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpControllerSelector), new NamespaceControllerSelector(GlobalConfiguration.Configuration));

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

//            // TODO 変更の可能性あり
//            // Application スタート時にリソース情報を取得する
//            EdgeApplicationModel.Resources =
//                new ApplicationResources
//                {
//                    // BaseUrlをリクエストのUrlから取得するようにする。
//                    GetBaseUrl = () => HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority)
//                };

//            ShardingManager.Initialize(() =>
//            {
//                var manager = new Manager();
//                manager.LoadFromSection("ShardingSection");
//                return manager;
//            });

        }
    }
}