﻿using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using Grow.Web.Core.Filters;
using Fango.Web.Core.Filters;
using Fango.Web.Core.Handlers;

namespace Grow.Web.APIWebRole
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // 運用系API
            config.Routes.MapHttpRoute(
                name: "OperationApi",
                routeTemplate: "api/operation/{controller}/{action}",
                defaults: new { action = UrlParameter.Optional }
            );

            // クライアント用API
            config.Routes.MapHttpRoute("DefaultApi",
                routeTemplate: "api/{controller}/{action}",
                defaults: new { action = UrlParameter.Optional },
                constraints : null,
                handler: new Fango.Web.Core.Handlers.ClientApiHandler(config, () => false)
                //handler: new ClientApiHandler(config, () => MaintenanceManager.GetApiMode()==MaintenanceRow.Mode.MaintenanceMode)
            );

            // メディアタイプをjsonのみとし、xmlには対応しない
            var appXmlType = config.Formatters.XmlFormatter.SupportedMediaTypes.FirstOrDefault(t => t.MediaType == "application/xml");
            config.Formatters.XmlFormatter.SupportedMediaTypes.Remove(appXmlType);

            config.Filters.Add(new ApiExceptionFilterAttribute());

            //gzip圧縮
            //referd: http://stackoverflow.com/questions/10443588/mvc4-webapi-compress-get-method-response
            config.MessageHandlers.Add(new CompressHandler());
        }
    }
}
