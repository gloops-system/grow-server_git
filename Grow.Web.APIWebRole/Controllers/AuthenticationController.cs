﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Fango.Core.Role;
using Grow.Core.Application.Authentication;
using Grow.Core.Application.Common;
using Grow.Core.Application;
using Grow.Core.Application.Login;

namespace Edge.Web.APIWebRole.Controllers
{
    public class AuthenticationController : ApiController
    {
        /// <summary>
        /// /api/Authentication/Initialize
        /// </summary>
        /// <param name="postValue"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Initialize([FromBody]InitializePostValue postValue)
        {
            using (var applicationContext = new GrowApplicationModel())
            {
                var apiModel = new InitializeApiModel(applicationContext);
                var contentResponse = applicationContext.ExecuteHandledApi(
                    () => apiModel.Initialize(postValue.Time,
                                              postValue.Version,
                                              postValue.UUID,
                                              postValue.DeviceType,
                                              postValue.OSVersion));
                var statusCode = applicationContext.MapCodeToStatus(contentResponse);

                return Request.CreateResponse((HttpStatusCode) statusCode, contentResponse);
            }
        }

        public class InitializePostValue
        {
            public string Time { get; set; }
            public string Version { get; set; }
            public string UUID { get; set; }
            public AppCode.DeviceType DeviceType { get; set; }
            public string OSVersion { get; set; }
        }

        /// <summary>
        /// /api/Authentication/Login
        /// </summary>
        /// <param name="postValue"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Login([FromBody]LoginPostValue postValue)
        {
            Version version;
            if (!Version.TryParse(postValue.Version, out version))
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            using (var applicationContext = new GrowApplicationModel(postValue.UserId, false, version))
            {
                var apiModel = new Grow.Core.Application.Authentication.LoginApiModel(applicationContext);
                var contentResponse = applicationContext.ExecuteHandledApi(
                    () => apiModel.Login(postValue.UserId, version, postValue.UUID));
                var statusCode = applicationContext.MapCodeToStatus(contentResponse);

                return Request.CreateResponse((HttpStatusCode) statusCode, contentResponse);
            }
        }

        public class LoginPostValue
        {
            public int UserId { get; set; }
            public string Version { get; set; }
            public string UUID { get; set; }
        }
    }
}