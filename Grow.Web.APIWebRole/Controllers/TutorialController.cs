﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using Grow.Core.Application;
using Grow.Core.Application.User;
using Fango.Core.Role;
using Grow.Core.Application.Common;
using Grow.Web.Core.Controllers;
using Fango.Web.Core.ApiModel;

namespace Grow.Web.APIWebRole.Controllers
{
    public class TutorialController : ApiControllerBase
    {
        /// <summary>
        /// /api/Tutorial/RegistUser
        /// </summary>
        /// <param name="postValue"></param>
        /// <returns></returns>
        public HttpResponseMessage RegistUser([FromBody] RegistUserPostValue postValue)
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var contentResponse = new ContentResponse();
                if (postValue == null)
                {
                    contentResponse.Code = (int) AppCode.ApiResultCode.パラメータ不正;
                    contentResponse.Message = @"パラメータが不正です"; // TODO : ローカライズ対応
                }
                else
                {
                    var apiModel = new RegistUserApiModel(applicationContext);
                    contentResponse = applicationContext.ExecuteHandledApi(
                        () => apiModel.RegistUser(postValue.NickName));
                }

                var statusCode = applicationContext.MapCodeToStatus(contentResponse);
                if (contentResponse.Code == (int)AppCode.ApiResultCode.成功)
                {
                    var apiModel = new ProfileApiModel(applicationContext);
                    contentResponse = applicationContext.ExecuteHandledApi(apiModel.GetProfileInfo);
                    statusCode = applicationContext.MapCodeToStatus(contentResponse);
                }

                return Request.CreateResponse((HttpStatusCode) statusCode, contentResponse);
            }
        }

        public class RegistUserPostValue
        {
            public string NickName { get; set; }
        }
    }
}