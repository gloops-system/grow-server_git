﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using Grow.Core.Application;
using Grow.Core.Application.News;
using Fango.Core.Role;
using Grow.Core.Application.Common;
using Grow.Web.Core.Controllers;
using Fango.Web.Core.ApiModel;

namespace Grow.Web.APIWebRole.Controllers
{
    public class NewsController : ApiControllerBase
    {
        /// <summary>
        /// /api/News/Get
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Get()
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var apiModel = new NewsApiModel(applicationContext);
                var contentResponse = applicationContext.ExecuteHandledApi(apiModel.GetNewsInfo);
                var statusCode = applicationContext.MapCodeToStatus(contentResponse);

                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }

        /// <summary>
        /// /api/News/Open
        /// </summary>
        /// <param name="postValue"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Open([FromBody] NewsPostValue postValue)
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var contentResponse = new ContentResponse();
                if (postValue == null)
                {
                    contentResponse.Code = (int)AppCode.ApiResultCode.パラメータ不正;
                    contentResponse.Message = @"パラメータが不正です"; // TODO : ローカライズ対応
                }
                else
                {
                    var apiModel = new NewsApiModel(applicationContext);
                    contentResponse = applicationContext.ExecuteHandledApi(() => apiModel.OpenNews(postValue.NewsId));
                }

                var statusCode = applicationContext.MapCodeToStatus(contentResponse);

                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }

        public class NewsPostValue
        {
            public int NewsId { get; set; }
        }
    }
}