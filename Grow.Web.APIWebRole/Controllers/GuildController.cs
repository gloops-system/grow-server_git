﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Grow.Core.Application;
using Grow.Core.Application.User;
using Fango.Core.Role;
using Grow.Core.Application.Common;
using Grow.Web.Core.Controllers;
using Fango.Web.Core.ApiModel;

namespace Edge.Web.APIWebRole.Controllers
{
    public class GuildController : ApiControllerBase
    {
        [HttpPost]
        public HttpResponseMessage Get([FromBody] GuildIdPostValue postValue)
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var guildModel = new GuildApiModel(applicationContext);
                var contentResponse = applicationContext.ExecuteHandledApi(() => guildModel.GetGuild(postValue.GuildId));
                var statusCode = applicationContext.MapCodeToStatus(contentResponse);

                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }

        public class GuildIdPostValue
        {
            public int GuildId { get; set; }
        }

        public HttpResponseMessage CreateGuild([FromBody] CreateGuildPostValue postValue)
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var guildModel = new GuildApiModel(applicationContext);
                var contentResponse = applicationContext.ExecuteHandledApi(() => guildModel.CreateGuild(postValue.FounderID, postValue.Name, postValue.Symbol, postValue.Introduction, postValue.RecruitType, postValue.RequiredTrophy, DateTime.Now));
                var statusCode = applicationContext.MapCodeToStatus(contentResponse);

                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }

        public class CreateGuildPostValue
        {
            public int FounderID { get; set; }
            public string Name { get; set; }
            public string Symbol { get; set; }
            public string Introduction { get; set; }
            public int RecruitType { get; set; }
            public int RequiredTrophy { get; set; }
        }

        public HttpResponseMessage UpdateGuild([FromBody] UpdateGuildPostValue postValue)
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var guildModel = new GuildApiModel(applicationContext);
                var contentResponse = applicationContext.ExecuteHandledApi(() => guildModel.UpdateGuild(postValue.Introduction, postValue.RecruitType, postValue.RequiredTrophy, DateTime.Now, postValue.GuildId));
                var statusCode = applicationContext.MapCodeToStatus(contentResponse);

                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }

        public class UpdateGuildPostValue
        {
            public int GuildId { get; set; }
            public string Introduction { get; set; }
            public int RecruitType { get; set; }
            public int RequiredTrophy { get; set; }
        }

        public HttpResponseMessage DisbandGuild([FromBody] DisbandGuildPostValue postValue)
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var guildModel = new GuildApiModel(applicationContext);
                var contentResponse = applicationContext.ExecuteHandledApi(() => guildModel.DisbandGuild(postValue.GuildId));
                var statusCode = applicationContext.MapCodeToStatus(contentResponse);

                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }

        public class DisbandGuildPostValue
        {
            public int GuildId { get; set; }
        }


        public HttpResponseMessage LeaveGuild()
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var guildModel = new GuildApiModel(applicationContext);
                var contentResponse = applicationContext.ExecuteHandledApi(() => guildModel.LeaveGuild(userId));
                var statusCode = applicationContext.MapCodeToStatus(contentResponse);

                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }

    }
}