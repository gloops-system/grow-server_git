﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using Grow.Core.Application;
using Grow.Core.Application.User;
using Fango.Core.Role;
using Grow.Web.Core.Controllers;

namespace Grow.Web.APIWebRole.Controllers
{
    public class MonsterController : ApiControllerBase
    {
        /// <summary>
        /// /api/Monster/Revive
        /// </summary>
        /// <param name="postValue"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Revive([FromBody] RevivePostValue postValue)
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var apiModel = new MonsterApiModel(applicationContext);
                var contentResponse = applicationContext.ExecuteHandledApi(() => apiModel.ReviveMonster(postValue.UserMonsterId));
                var statusCode = applicationContext.MapCodeToStatus(contentResponse);

                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }

        public class RevivePostValue
        {
            public long UserMonsterId { get; set; }
        }
    }
}