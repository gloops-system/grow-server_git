﻿using System.ComponentModel.Composition;
using Grow.Core.Application;
using Grow.Core.Repositories.Users;
using Fango.Core.Sequence;
using Fango.Web.Core.ApiModel.Login;
using Fango.Web.Core.Controllers.Login;

namespace Grow.Web.APIWebRole.Controllers
{
    public class InitialController : InitialControllerBase
    {
        [Import(typeof (IInitialApiModel))] public IInitialApiModel Repository;
        public InitialController()
        {
            using (var applicationModel = new GrowApplicationModel())
            {
                ApiModel = new Grow.Core.Application.Login.InitialApiModel(applicationModel)
                    {
                        UserLoginDataRepository = new UserLoginDataRepository(),
                        SequenceClient = SequenceClientManager.DefaultSequenceClient
                    };
            }
        }
    }
}