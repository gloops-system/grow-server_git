﻿using Fango.Web.Core.Controllers.Login;

namespace Grow.Web.APIWebRole.Controllers
{
    public class LoginController : LoginControllerBase
    {
        public LoginController()
        {
            var repository = new Grow.Core.Repositories.Users.UserLoginDataRepository();
            loginApiModel.SetUserLoginInfoDataRepository(repository);
        }
    }
}