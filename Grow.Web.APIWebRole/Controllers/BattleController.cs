﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using Grow.Core.Application;
using Grow.Core.Application.User;
using Fango.Core.Role;
using Grow.Web.Core.Controllers;
using Fango.Web.Core.ApiModel;
using Grow.Core.Application.Common;

namespace Grow.Web.APIWebRole.Controllers
{
    public class BattleController : ApiControllerBase
    {
        /// <summary>
        /// /api/Battle/PvPStart
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage PvPStart()
        {
            var userId = UserId();

            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var apiModel = new BattleApiModel(applicationContext);
                
                var contentResponse = applicationContext.ExecuteHandledApi(apiModel.PvPStart);
                
                var statusCode = applicationContext.MapCodeToStatus(contentResponse);

                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }

        /// <summary>
        /// /api/Battle/PvpEnd
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage PvpEnd([FromBody] UpdateBattleHistory postValue)
        {
            var userId = UserId();

            return UpdateBattleResult(postValue.BattleHistoryId, userId, postValue.TargetUserId, postValue.TargetLootedGold, postValue.TargetLootedPlasma, (byte)AppCode.BattleStatus.END, postValue.BattleResult, postValue.ReplayData);
        }

        /// <summary>
        /// /api/Battle/PvpCancel
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage PvpCancel([FromBody] UpdateBattleHistory postValue)
        {
            var userId = UserId();

            return UpdateBattleResult(postValue.BattleHistoryId, userId, postValue.TargetUserId, 0, 0, (byte)AppCode.BattleStatus.CANCEL, (byte)AppCode.BattleResult.LOOSE, "");
        }

        private HttpResponseMessage UpdateBattleResult(int battleHistoryId, int userId, int targetUserId, int targetLootedGold, int targetLootedPlasma, byte battleStatus, byte battleResult, string replayData)
        {
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var apiModel = new BattleApiModel(applicationContext);

                var contentResponse = applicationContext.ExecuteHandledApi(() => apiModel.PvPEnd(battleHistoryId, userId, targetUserId, targetLootedGold, targetLootedPlasma, battleStatus, battleResult, replayData));

                var statusCode = applicationContext.MapCodeToStatus(contentResponse);

                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }

        public class UpdateBattleHistory
        {
            public int BattleHistoryId { get; set; }
            public int TargetUserId {get; set;}
            public int TargetLootedGold {get; set;}
            public int TargetLootedPlasma {get;set;}
            public byte BattleResult {get;set;}
            public string ReplayData {get;set;}
        }
    }
}