﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using Grow.Core.Application;
using Grow.Core.Application.Test;
using Fango.Core.Role;
using Grow.Web.Core.Controllers;
using Fango.Web.Core.ApiModel;

namespace Grow.Web.APIWebRole.Controllers
{
    public class TestController : ApiControllerBase
    {
        /// <summary>
        /// /api/Test/MonsterMaster
        /// </summary>
        /// <param name="postValue"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage GetMonsterMaster([FromBody] MonsterMasterPostValue postValue)
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var contentResponse = new ContentResponse();
                var apiModel = new MonsterMasterTestApiModel(applicationContext);
                contentResponse = applicationContext.ExecuteHandledApi(() => apiModel.GetMonsterMaster(postValue.MonsterId));
                var statusCode = applicationContext.MapCodeToStatus(contentResponse);
                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }

        public class MonsterMasterPostValue
        {
            public int MonsterId { get; set; }
        }
    }
}