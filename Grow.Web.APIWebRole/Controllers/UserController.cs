﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using Grow.Core.Application;
using Grow.Core.Application.User;
using Fango.Core.Role;
using Grow.Web.Core.Controllers;

namespace Grow.Web.APIWebRole.Controllers
{
    public class UserController : ApiControllerBase
    {
        /// <summary>
        /// /api/User/Pull
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Pull()
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var apiModel = new ProfileApiModel(applicationContext);
                var contentResponse = applicationContext.ExecuteHandledApi(apiModel.GetProfileInfo);
                var statusCode = applicationContext.MapCodeToStatus(contentResponse);

                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }

        /// <summary>
        /// /api/User/Push
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Push([FromBody] UpdateUserColonyPostValue postValue)
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var apiModel = new ProfileApiModel(applicationContext);
                var contentResponse = applicationContext.ExecuteHandledApi(() => apiModel.UpdateUserColony(postValue.Level, postValue.Gold, postValue.Plasma, postValue.ColonyData, postValue.ColonyHistoryIndex));
                var statusCode = applicationContext.MapCodeToStatus(contentResponse);

                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }

        public class UpdateUserColonyPostValue
        {
            public int Gold { get; set; }
            public int Plasma { get; set; }
            public short Level { get; set; }
            public string ColonyData { get; set; }
            public long ColonyHistoryIndex { get; set; }
        }


        /// <summary>
        /// /api/User/UpdateLastReadNotification
        /// </summary>
        /// <param name="postValue"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage UpdateLastReadNotification([FromBody] UpdateLastReadNotificationPostValue postValue)
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var apiModel = new ProfileApiModel(applicationContext);
                var contentResponse = applicationContext.ExecuteHandledApi(() => apiModel.UpdateLastReadNotification(postValue.LastNotificationId));
                var statusCode = applicationContext.MapCodeToStatus(contentResponse);

                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }

        public class UpdateLastReadNotificationPostValue
        {
            public long LastNotificationId { get; set; }
        }
    }
}