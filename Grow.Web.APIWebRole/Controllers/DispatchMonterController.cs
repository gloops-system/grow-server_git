﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using Grow.Core.Application;
using Grow.Core.Application.Duty;
using Fango.Core.Role;
using Grow.Web.Core.Controllers;

namespace Grow.Web.APIWebRole.Controllers
{
    public class DispatchMonterController : ApiControllerBase
    {
        /// <summary>
        /// /api/DispatchMonter/DispatchFriendList
        /// </summary>
        /// <param name="postValue"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage DispatchFriendList([FromBody] DispatchFriendListPostValue postValue)
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var apiModel = new DispatchMonterApiModel(applicationContext);
                var contentResponse = applicationContext.ExecuteHandledApi(() => apiModel.DispatchFriendList(postValue.UserMonsterId));
                var statusCode = applicationContext.MapCodeToStatus(contentResponse);

                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }

        public class DispatchFriendListPostValue
        {
            public long UserMonsterId { get; set; }
        }

        /// <summary>
        /// /api/DispatchMonter/DispatchExecute
        /// </summary>
        /// <param name="postValue"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage DispatchExecute([FromBody] DispatchExecutePostValue postValue)
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var apiModel = new DispatchMonterApiModel(applicationContext);
                var contentResponse = applicationContext.ExecuteHandledApi(() => apiModel.DispatchExecute(postValue.TargetUserId, postValue.UserMonsterId));
                var statusCode = applicationContext.MapCodeToStatus(contentResponse);

                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }

        public class DispatchExecutePostValue
        {
            public int TargetUserId { get; set; }
            public long UserMonsterId { get; set; }
        }


        /// <summary>
        /// /api/DispatchMonter/MeetMonsterList
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage MeetMonsterList()
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var apiModel = new DispatchMonterApiModel(applicationContext);
                var contentResponse = applicationContext.ExecuteHandledApi(apiModel.MeetMonsterList);
                var statusCode = applicationContext.MapCodeToStatus(contentResponse);

                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }
        
        
        /// <summary>
        /// /api/DispatchMonter/PullBack
        /// </summary>
        /// <param name="postValue"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage PullBack([FromBody] PullBackPostValue postValue)
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var apiModel = new DispatchMonterApiModel(applicationContext);
                var contentResponse = applicationContext.ExecuteHandledApi(() => apiModel.PullBack(postValue.UserMonsterId));
                var statusCode = applicationContext.MapCodeToStatus(contentResponse);

                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }

        public class PullBackPostValue
        {
            public long UserMonsterId { get; set; }
        }
    }
}