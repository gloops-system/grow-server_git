﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using Grow.Core.Application;
using Grow.Core.Application.Friend;
using Fango.Core.Role;
using Grow.Core.Application.Common;
using Grow.Web.Core.Controllers;
using Fango.Web.Core.ApiModel;

namespace Grow.Web.APIWebRole.Controllers
{
    public class FriendController : ApiControllerBase
    {
        /// <summary>
        /// /api/Friend/GetOwn
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage GetOwn()
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var apiModel = new FriendApiModel(applicationContext);
                var contentResponse = applicationContext.ExecuteHandledApi(apiModel.GetFriendInfo);
                var statusCode = applicationContext.MapCodeToStatus(contentResponse);

                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }

        /// <summary>
        /// /api/Friend/GetOthers
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage GetOthers()
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var apiModel = new FriendApiModel(applicationContext);
                var contentResponse = applicationContext.ExecuteHandledApi(apiModel.GetOthersInfo);
                var statusCode = applicationContext.MapCodeToStatus(contentResponse);

                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }

        /// <summary>
        /// /api/Friend/RequestFriend
        /// </summary>
        /// <param name="postValue"></param>
        /// <returns></returns>
        public HttpResponseMessage RequestFriend([FromBody] RequestPostValue postValue)
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var contentResponse = new ContentResponse();
                if (postValue == null)
                {
                    contentResponse.Code = (int)AppCode.ApiResultCode.パラメータ不正;
                    contentResponse.Message = @"パラメータが不正です"; // TODO : ローカライズ対応
                }
                else
                {
                    var apiModel = new FriendApiModel(applicationContext);
                    contentResponse = applicationContext.ExecuteHandledApi(
                        () => apiModel.ExecuteRequest(postValue.TargetUserId));
                }

                var statusCode = applicationContext.MapCodeToStatus(contentResponse);

                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }

        public class RequestPostValue
        {
            public int TargetUserId { get; set; }
        }

        /// <summary>
        /// /api/Friend/Answer
        /// </summary>
        /// <param name="postValue"></param>
        /// <returns></returns>
        public HttpResponseMessage Answer([FromBody] AnswerPostValue postValue)
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var contentResponse = new ContentResponse();
                if (postValue == null)
                {
                    contentResponse.Code = (int)AppCode.ApiResultCode.パラメータ不正;
                    contentResponse.Message = @"パラメータが不正です"; // TODO : ローカライズ対応
                }
                else
                {
                    var apiModel = new FriendApiModel(applicationContext);
                    contentResponse = applicationContext.ExecuteHandledApi(
                        () => apiModel.ExecuteAnswer(postValue.TargetUserId, postValue.Answer));
                }

                var statusCode = applicationContext.MapCodeToStatus(contentResponse);

                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }

        public class AnswerPostValue
        {
            public int TargetUserId { get; set; }
            public int Answer { get; set; }
        }

        /// <summary>
        /// /api/Friend/Delete
        /// </summary>
        /// <param name="postValue"></param>
        /// <returns></returns>
        public HttpResponseMessage Delete([FromBody] DeletePostValue postValue)
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var contentResponse = new ContentResponse();
                if (postValue == null)
                {
                    contentResponse.Code = (int)AppCode.ApiResultCode.パラメータ不正;
                    contentResponse.Message = @"パラメータが不正です"; // TODO : ローカライズ対応
                }
                else
                {
                    var apiModel = new FriendApiModel(applicationContext);
                    contentResponse = applicationContext.ExecuteHandledApi(
                        () => apiModel.ExecuteDelete(postValue.TargetUserId));
                }

                var statusCode = applicationContext.MapCodeToStatus(contentResponse);

                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }

        public class DeletePostValue
        {
            public int TargetUserId { get; set; }
        }
    }
}