﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using Grow.Core.Application;
using Grow.Core.Application.Gear;
using Fango.Core.Role;
using Grow.Web.Core.Controllers;

namespace Grow.Web.APIWebRole.Controllers
{
    public class GearController : ApiControllerBase
    {
        /// <summary>
        /// /api/gears
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage List()
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var apiModel = new UserGearApiModel(applicationContext);
                var contentResponse = applicationContext.ExecuteHandledApi(apiModel.GetUserGears);
                var statusCode = applicationContext.MapCodeToStatus(contentResponse);

                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }
    }
}