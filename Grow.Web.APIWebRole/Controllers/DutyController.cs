﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using Grow.Core.Application;
using Grow.Core.Application.Duty;
using Fango.Core.Role;
using Grow.Web.Core.Controllers;
using Grow.Core.InterfaceModel.Duty;

namespace Grow.Web.APIWebRole.Controllers
{
    public class DutyController : ApiControllerBase
    {
        /// <summary>
        /// /api/Duty/LabyrinthCamp
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage LabyrinthCamp()
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var apiModel = new LabyrinthApiModel(applicationContext);
                var contentResponse = applicationContext.ExecuteHandledApi(apiModel.LabyrinthCamp);
                var statusCode = applicationContext.MapCodeToStatus(contentResponse);
                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }


        /// <summary>
        /// /api/Duty/LabyrinthStart
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage LabyrinthStart([FromBody] LabyrinthStartRequestViewModel postValue)
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var apiModel = new LabyrinthApiModel(applicationContext);
                var contentResponse = applicationContext.ExecuteHandledApi(() => apiModel.LabyrinthStart(postValue));                
                var statusCode = applicationContext.MapCodeToStatus(contentResponse);
                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }

        /// <summary>
        /// /api/Duty/LabyrinthStart
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage LabyrinthStartField([FromBody] StartFieldPostValue postValue)
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var apiModel = new LabyrinthApiModel(applicationContext);
                var contentResponse = applicationContext.ExecuteHandledApi(() => apiModel.LabyrinthStartField(postValue.RequireStamina));
                var statusCode = applicationContext.MapCodeToStatus(contentResponse);
                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }

        public class StartFieldPostValue
        {
            public int RequireStamina { get; set; }
        }


        /// <summary>
        /// /api/Duty/LabyrinthFinishField
        /// </summary>
        /// <param name="postValue"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage LabyrinthFinishField([FromBody] LabyrinthFinishFieldRequestViewModel postValue)
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var apiModel = new LabyrinthApiModel(applicationContext);
                var contentResponse = applicationContext.ExecuteHandledApi(() => apiModel.LabyrinthFinishField(postValue));
                var statusCode = applicationContext.MapCodeToStatus(contentResponse);

                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }

        /// <summary>
        /// /api/Duty/LabyrinthFinishArea
        /// </summary>
        /// <param name="postValue"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage LabyrinthFinishArea([FromBody] LabyrinthFinishAreaRequestViewModel postValue)
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var apiModel = new LabyrinthApiModel(applicationContext);
                var contentResponse = applicationContext.ExecuteHandledApi(() => apiModel.LabyrinthFinishArea(postValue));
                var statusCode = applicationContext.MapCodeToStatus(contentResponse);

                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }

        /// <summary>
        /// /api/Duty/RecoveryStamina
        /// </summary>
        /// <param name="postValue"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage RecoveryStamina([FromBody] RecoveryStaminaPostValue postValue)
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var apiModel = new LabyrinthApiModel(applicationContext);
                var contentResponse = applicationContext.ExecuteHandledApi(() => apiModel.RecoveryStamina(postValue.RequireGem));
                var statusCode = applicationContext.MapCodeToStatus(contentResponse);

                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }

        public class RecoveryStaminaPostValue
        {
            public int RequireGem { get; set; }
        }

        /// <summary>
        /// /api/Duty/MeetDispatchMonster
        /// </summary>
        /// <param name="postValue"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage MeetDispatchMonster([FromBody] MeetDispatchMonsterPostValue postValue)
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var apiModel = new LabyrinthApiModel(applicationContext);
                var contentResponse = applicationContext.ExecuteHandledApi(() => apiModel.MeetDispatchMonster(postValue.UserMonsterIds));
                var statusCode = applicationContext.MapCodeToStatus(contentResponse);

                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }

        public class MeetDispatchMonsterPostValue
        {
            public long[] UserMonsterIds { get; set; }
        }

        [HttpPost]
        public HttpResponseMessage MatchGroup()
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var apiModel = new LabyrinthApiModel(applicationContext);
                var contentResponse = applicationContext.ExecuteHandledApi(apiModel.MatchGroup);
                var statusCode = applicationContext.MapCodeToStatus(contentResponse);

                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }

        [HttpPost]
        public HttpResponseMessage ActiveFeverTime()
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var apiModel = new LabyrinthApiModel(applicationContext);
                var contentResponse = applicationContext.ExecuteHandledApi(apiModel.ActiveFeverTime);
                var statusCode = applicationContext.MapCodeToStatus(contentResponse);

                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }

        [HttpPost]
        public HttpResponseMessage RemoveFeverTime()
        {
            var userId = UserId();
            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var apiModel = new LabyrinthApiModel(applicationContext);
                var contentResponse = applicationContext.ExecuteHandledApi(apiModel.RemoveFeverTime);
                var statusCode = applicationContext.MapCodeToStatus(contentResponse);

                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }
    }
}