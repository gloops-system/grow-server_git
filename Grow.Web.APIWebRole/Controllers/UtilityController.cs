﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using Grow.Core.Application;
using Grow.Core.Application.User;
using Fango.Core.Role;
using Grow.Core.Application.Common;
using Grow.Web.Core.Controllers;
using Fango.Web.Core.ApiModel;
using Grow.Core.Application.Utility;

namespace Grow.Web.APIWebRole.Controllers
{
    public class UtilityController : ApiControllerBase
    {
        /// <summary>
        /// /api/Utility/GetServerTime
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage GetServerTime()
        {
            var userId = UserId();

            using (var applicationContext = new GrowApplicationModel(userId))
            {
                var apiModel = new UtilityApiModel(applicationContext);
                var contentResponse = applicationContext.ExecuteHandledApi(apiModel.GetServerTime);
                var statusCode = applicationContext.MapCodeToStatus(contentResponse);
                return Request.CreateResponse((HttpStatusCode)statusCode, contentResponse);
            }
        }
    }
}