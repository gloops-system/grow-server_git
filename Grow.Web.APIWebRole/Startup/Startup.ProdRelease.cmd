@echo off 
:: PowerShell をファイルから起動できるよう実行ポリシーを変更する
powershell -command "Set-ExecutionPolicy Unrestricted" 2>> err.out  
:: IISのIPアドレス制限機能を設定する
::powershell .\Startup\SetupIPAddressRistriction.ps1 2>> err.out

:: Setup NSClient (Only Prod Release Environment)------------------------------
powershell .\Startup\DownloadStartupLibs.ps1 2>> err.out

:: Inatall NSClient++ (Only Production Environment)
msiexec /i Startup\NSCP-0.4.1.90-x64.msi /quiet /norestart
:: Copy NSClinet.ini file (Only Production Environment)
copy Startup\nsclient.ini "%ProgramFiles%\NSClient++\nsclient.ini"

:: Setup SNMP  Service (Only Production Environment)---------------------------
powershell .\Startup\SetupSNMP.ps1 2>> err.out
:: Restart NSClient++ Service
powershell -command "Restart-Service nscp"
