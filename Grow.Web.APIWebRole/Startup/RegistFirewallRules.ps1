$nsClientRule = Get-NetFirewallRule | Where-Object { $_.DisplayName -eq "NSClient++ Monitoring Agent" }

if ($nsClientRule -eq $null)
{
    New-NetFirewallRule -DisplayName "NSClient++ Monitoring Agent" -Direction Inbound  -Program %ProgramFiles%\NSClient++\nscp.exe -Action Allow;
}

$snmpRules =  Get-NetFirewallRule | Where-Object { $_.Group -eq "SNMP Service" }

if ($snmpRules -eq $null)
{
    New-NetFirewallRule -Name "SNMP-In-UDP"          -DisplayName "SNMP Service (UDP In)"  -Direction Inbound  -Program %SystemRoot%\system32\snmp.exe -Action Allow -Group "SNMP Service" -Service "SNMP" -Profile Private, Public -Protocol UDP -LocalPort 161 -Description "Inbound rule for the Simple Network Management Protocol (SNMP) Service to allow SNMP traffic. [UDP 161]";
    New-NetFirewallRule -Name "SNMP-In-UDP-NoScope"  -DisplayName "SNMP Service (UDP In)"  -Direction Inbound  -Program %SystemRoot%\system32\snmp.exe -Action Allow -Group "SNMP Service" -Service "SNMP" -Profile Domain          -Protocol UDP -LocalPort 161 -Description "Inbound rule for the Simple Network Management Protocol (SNMP) Service to allow SNMP traffic. [UDP 161]";
    New-NetFirewallRule -Name "SNMP-Out-UDP"         -DisplayName "SNMP Service (UDP Out)" -Direction Outbound -Program %SystemRoot%\system32\snmp.exe -Action Allow -Group "SNMP Service" -Service "SNMP" -Profile Private, Public -Protocol UDP -LocalPort 161 -Description "Outbound rule for the Simple Network Management Protocol (SNMP) Service to allow SNMP traffic. [UDP 161]" -RemoteAddress LocalSubnet;
    New-NetFirewallRule -Name "SNMP-Out-UDP-NoScope" -DisplayName "SNMP Service (UDP Out)" -Direction Outbound -Program %SystemRoot%\system32\snmp.exe -Action Allow -Group "SNMP Service" -Service "SNMP" -Profile Domain          -Protocol UDP -LocalPort 161 -Description "Outbound rule for the Simple Network Management Protocol (SNMP) Service to allow SNMP traffic. [UDP 161]";
}
