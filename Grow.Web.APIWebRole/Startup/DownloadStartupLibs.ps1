﻿$storageConnectionStringName = "AppStorageAccount";
$containerName               = "libs";

#Download NSCP-0.4.1.90-x64.msi------------------------------------------------
$blobName					 = "Startup/NSClient/NSCP-0.4.1.90-x64.msi";
$localPath					 = ".\Startup\NSCP-0.4.1.90-x64.msi";

$currentLocation = Get-Location;
$assemblyPath    = $currentLocation.ToString();

[System.Reflection.Assembly]::LoadFrom($assemblyPath + "\Fango.Azure.dll");
[System.Reflection.Assembly]::LoadWithPartialName("System.Threading.Tasks");

[Fango.Azure.Storage.StorageServiceManager]::DefaultManager.DownloadBlob($storageConnectionStringName, $containerName, $blobName, $localPath);

#Download nsclient.ini---------------------------------------------------------
$blobName					 = "Startup/NSClient/nsclient.ini";
$localPath					 = ".\Startup\nsclient.ini";

[Fango.Azure.Storage.StorageServiceManager]::DefaultManager.DownloadBlob($storageConnectionStringName, $containerName, $blobName, $localPath);
