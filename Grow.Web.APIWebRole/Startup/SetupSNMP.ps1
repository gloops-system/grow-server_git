Import-Module Servermanager
# SNMPサービスをインストールする 
$snmpService = Get-WindowsFeature | Where-Object {$_.Name -eq "SNMP-Service"};

If (-not $snmpService.Installed) {
    #Install/Enable SNMP Services
    Add-WindowsFeature SNMP-Service -IncludeManagementTools | Out-Null

	$snmpService = Get-WindowsFeature | Where-Object {$_.Name -eq "SNMP-Service"};

	If ($snmpService.Installed) {
		reg delete "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\services\SNMP\Parameters\PermittedManagers" /v 1 /f | Out-Null
		reg add "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\services\SNMP\Parameters\ValidCommunities" /v "gloops" /t REG_DWORD /d 4 /f | Out-Null
	}

	Restart-Service snmp;
}

# 別のサブネットにあるMonitorサーバーからアクセスできるようファイアウォールの設定を行う
Get-NetFirewallRule SNMP-In-UDP | Set-NetFirewallRule -RemoteAddress Any