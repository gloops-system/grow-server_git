@echo off 
:: PowerShell をファイルから起動できるよう実行ポリシーを変更する
powershell -command "Set-ExecutionPolicy Unrestricted" 2>> err.out  
:: IISのIPアドレス制限機能を設定する
powershell .\Startup\SetupIPAddressRistriction.ps1 2>> err.out