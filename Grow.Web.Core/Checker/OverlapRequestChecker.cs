﻿using System;
using System.Web;
using Fango.Core.Logging;
using Fango.Core.Util;
using Fango.Redis.Caching;
using Grow.Core.Application.Util;
using Grow.Data.General.DataSet.Container;

namespace Grow.Web.Core.Checker
{
    internal static class OverlapRequestChecker
    {
        /// <summary>
        /// 重複リクエストかどうか
        /// </summary>
        internal static bool IsOverlap(int userId)
        {
            var context = (HttpContextBase)(new HttpContextWrapper(HttpContext.Current));
            var routeData = System.Web.Routing.RouteTable.Routes.GetRouteData(context);
            if (routeData == null)
                return false;

            var controllerName = routeData.Values["controller"].ToString();
            var actionName = routeData.Values["action"].ToString();
            var key = controllerName + actionName;

            bool result;
            var cacheKey = string.Format("{0}-{1}", userId, key);
            var redis = new RedisCacheClient();
            if (redis.TryGet(cacheKey, out result))
                return true;

            redis.Add(cacheKey, true, TimeSpan.FromSeconds(1));

            // Databaseで検証
            var isOverlap = new OverlapRequestDataContainer().OverlapRequest.IsOverlap(userId);
            if (isOverlap)
            {
                LoggerManager.DefaultLogger.ErrorFormat("重複リクエスト {0} uid:{1} {2}",
                                                        DateUtil.GetDateTimeOffsetNow(),
                                                        userId,
                                                        context.Request.RawUrl);
                return true;
            }

            return false;
        }
    }
}