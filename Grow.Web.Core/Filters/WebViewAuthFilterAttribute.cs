﻿using Grow.Core.Application.Configuration;
using Fango.Web.Core.Handlers;
using System.Globalization;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Grow.Web.Core.Filters
{
    public class WebViewAuth : AuthorizeAttribute
    {
        private bool IsAuthorized(AuthorizationContext filterContext)
        {
            var authHeaders = filterContext.RequestContext.HttpContext.Request.Headers.GetValues("Authorization");
            if (authHeaders == null || !authHeaders.Any())
                return false;

            var bearerToken = authHeaders.ElementAt(0);
            var token = string.Empty;
            if (MobileSettings.AuthorizationKey == "Bearer-Debug")
                token = bearerToken.Substring(13);
            else if (MobileSettings.AuthorizationKey == "Bearer")
                token = bearerToken.Substring(7);

            if (string.IsNullOrWhiteSpace(token))
                return false;

            int userId;
            if (MobileSettings.AuthorizationKey == "Bearer-Debug")
            {
                // Debug
                // デバッグ時はtokenをそのままUserIdとする
                userId = int.Parse(token);
            }
            else
            {
                // Prod
                //暗号化トークンを復号化し、期限をチェックする。
                var tokenModel = ClientApiHandler.DecryptToken(token);

                userId = tokenModel.UserId;
                var expires = tokenModel.Expires;
                var resultExpires = ClientApiHandler.CheckExpires(expires);
                if (!resultExpires)
                {
                    return false;
                }
            }

            var identity = new GenericIdentity(userId.ToString(CultureInfo.InvariantCulture));
            var roles = new[] {"Users"};
            var principal = new GenericPrincipal(identity, roles);
            Thread.CurrentPrincipal = principal;
            HttpContext.Current.User = principal;

            return true;
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var isAuthorized = IsAuthorized(filterContext);
            if (!isAuthorized)
                filterContext.Result = new HttpUnauthorizedResult();
        }
    }
}