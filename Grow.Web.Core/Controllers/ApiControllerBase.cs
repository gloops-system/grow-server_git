﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using Grow.Core.Application;
using Grow.Core.Application.Common;
using Grow.Web.Core.Checker;
using Fango.Core.Logging;
using Fango.Web.Core.ApiModel;
using Fango.Web.Core.Filters;

namespace Grow.Web.Core.Controllers
{
    [Authorize]
    [ApiExceptionFilter]
    public class ApiControllerBase : ApiController
    {
        protected int UserId()
        {
            var principal = Thread.CurrentPrincipal;

            // NameはUserIdとVersionからなる
            // Name = "12345,1.0"
            var splitedName = principal.Identity.Name.Split(',');
            int userId;
            var canParse = int.TryParse(splitedName[0], out userId);
            if (!canParse)
            {
                LoggerManager.DefaultLogger.ErrorFormat("userIdにparse出来ませんでした。 principal.Identity.Name = ",
                                                        principal.Identity.Name);
                throw new Exception("Failed to parse Identity.Name");
            }
            return userId;
        }

        protected bool IsOverlap()
        {
            return OverlapRequestChecker.IsOverlap(UserId());
        }

        protected virtual HttpResponseMessage OverlapResponse(GrowApplicationModel applicationContext)
        {
            var errorContentResponse = new ContentResponse()
                {
                    Code = (int) AppCode.ApiResultCode.失敗,
                    Message = @"重複リクエストです。"
                };
            var errorStatusCode = applicationContext.MapCodeToStatus(errorContentResponse);
            return Request.CreateResponse((HttpStatusCode)errorStatusCode, errorContentResponse);
        }
    }
}