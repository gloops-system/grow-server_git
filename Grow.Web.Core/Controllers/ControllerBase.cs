﻿using System;
using System.Threading;
using System.Web.Mvc;

namespace Grow.Web.Core.Controllers
{
    public class ControllerBase : Controller
    {
        protected int UserId()
        {
            var principal = Thread.CurrentPrincipal;

            // FormatExceptionが出たら呼び出し元で[WebViewAuth]をつけること

            // NameはUserIdとVersionからなる
            // Name = "12345,1.0"
            var splitedName = principal.Identity.Name.Split(',');
            int userId;
            var canParse = int.TryParse(splitedName[0], out userId);
            if (!canParse)
            {
                //LoggerManager.DefaultLogger.Error("userIdにparse出来ませんでした。 principal.Identity.Name = " + principal.Identity.Name);
                throw new Exception("Failed to parse Identity.Name");
            }
            //LoggerManager.DefaultLogger.Debug(string.Format("@ApiControllerBase userId:{0} Principal:{1}", userId, principal));
            return userId;
        }
    }
}