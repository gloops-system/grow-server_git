﻿using Grow.Batch.Core.Battle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Test.Test
{
    public class Program
    {
        static void processOne()
        {
            // create 4 consumers and 1 producer pattern
            int numOfConsumer = 4;

            IProducerFactory<int> producerFactory = new IntProducerFactory();
            IConsumerFactory<int> consumerFactory = new IntConsumerFactory();

            ProducerConsumersRunner<int> runner = new ProducerConsumersRunner<int>(producerFactory, consumerFactory, numOfConsumer);

            // provide a list of data which need to be process concurrently
            List<int> uidList = new List<int>();
            for (int i = 0; i < 10; i++ )
            {
                uidList.Add(i);
            }

            // Producer will provide data to consumers to consum it
            List<Task<List<int>>> erorrTaskList = runner.Execute(uidList);

            // Find if there is any failed process
            erorrTaskList.ForEach(delegate(Task<List<int>> task)
            {
                List<int> errorIds = task.Result;

                errorIds.ForEach(delegate(int errorId)
                {
                    // Print the count of bytes processed to the console.
                    Console.WriteLine("Error id {0}.", errorId);
                });
            });

            Console.WriteLine("End Process 1.");
        }

        static void processTwo()
        {
            // create 4 consumers and 1 producer pattern
            int numOfConsumer = 5;

            IProducerFactory<TaskStruct> producerFactory = new StructProducerFactory();
            IConsumerFactory<TaskStruct> consumerFactory = new StructConsumerFactory();

            ProducerConsumersRunner<TaskStruct> runner = new ProducerConsumersRunner<TaskStruct>(producerFactory, consumerFactory, numOfConsumer);

            // provide a list of data which need to be process concurrently
            List<TaskStruct> uidList = new List<TaskStruct>();
            for (int i = 0; i < 10; i++)
            {
                TaskStruct task = new TaskStruct();
                task.id = i;

                uidList.Add(task);
            }

            // Producer will provide data to consumers to consum it
            List<Task<List<TaskStruct>>> erorrTaskList = runner.Execute(uidList);

            // Find if there is any failed process
            erorrTaskList.ForEach(delegate(Task<List<TaskStruct>> task)
            {
                List<TaskStruct> errors = task.Result;

                errors.ForEach(delegate(TaskStruct error)
                {
                    // Print the count of bytes processed to the console.
                    Console.WriteLine("Error id {0}.", error.id);
                });
            });

            Console.WriteLine("End Process 2.");
        }

        static void Main(string[] args)
        {
            // process in series
            processOne();

            processTwo();

            Console.WriteLine("End All Processes.");
        }
    }
}
