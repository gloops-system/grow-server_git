﻿using Grow.Batch.Core.Battle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks.Dataflow;

namespace Grow.Test.Test
{
    public class StructConsumerFactory : IConsumerFactory<TaskStruct>
    {
        public IConsumer<TaskStruct> GetConsumer(string name, IReceivableSourceBlock<TaskStruct> source)
        {
            return new StructConsumer(name, source);
        }
    }
}
