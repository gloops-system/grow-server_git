﻿using Grow.Batch.Core.Battle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks.Dataflow;

namespace Grow.Test.Test
{
    public class StructProducerFactory : IProducerFactory<TaskStruct>
    {
        public IProducer<TaskStruct> GetProducer(ITargetBlock<TaskStruct> target)
        {
            return new StructProducer(target);
        }
    }
}
