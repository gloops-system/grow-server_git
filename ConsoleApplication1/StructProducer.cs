﻿using Grow.Batch.Core.Battle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks.Dataflow;

namespace Grow.Test.Test
{
    public struct TaskStruct
    {
        public int id;
    }

    public class StructProducer : AbstractProducer<TaskStruct>
    {
        public StructProducer(ITargetBlock<TaskStruct> target)
        {
            this.target = target;
        }

        public override bool Post(TaskStruct data)
        {
            return target.Post(data);
        }
    }
}
