﻿using Grow.Batch.Core.Battle;
using System;
using System.Threading;
using System.Threading.Tasks.Dataflow;

namespace Grow.Test.Test
{
    public class IntConsumer : AbstractConsumer<int>
    {
        public IntConsumer(String name, IReceivableSourceBlock<int> source)
        {
            this.name = name;
            this.source = source;
        }

        protected override void execute(int data)
        {
            // TODO: actual implementation
            Thread.Sleep(2000);

            Console.WriteLine("Consumer: {0}. Process done for data {1}.", this.name, data);
        }
    }
}
