﻿using Grow.Batch.Core.Battle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks.Dataflow;

namespace Grow.Test.Test
{
    public class IntConsumerFactory : IConsumerFactory<int>
    {
        public IConsumer<int> GetConsumer(string name, IReceivableSourceBlock<int> source)
        {
            return new IntConsumer(name, source);
        }
    }
}
