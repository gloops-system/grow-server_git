﻿using Grow.Batch.Core.Battle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks.Dataflow;

namespace Grow.Test.Test
{
    public class IntProducerFactory : IProducerFactory<int>
    {
        public IProducer<int> GetProducer(ITargetBlock<int> target)
        {
            return new IntProducer(target);
        }
    }
}
