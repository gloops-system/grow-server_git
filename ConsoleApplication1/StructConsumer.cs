﻿using Grow.Batch.Core.Battle;
using System;
using System.Threading;
using System.Threading.Tasks.Dataflow;

namespace Grow.Test.Test
{
    public class StructConsumer : AbstractConsumer<TaskStruct>
    {
        public StructConsumer(String name, IReceivableSourceBlock<TaskStruct> source)
        {
            this.name = name;
            this.source = source;
        }

        protected override void execute(TaskStruct data)
        {
            // TODO: actual implementation
            Thread.Sleep(2000);

            Console.WriteLine("Consumer: {0}. Process done for data {1}.", this.name, data.id);
        }
    }
}
