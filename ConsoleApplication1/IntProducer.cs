﻿using Grow.Batch.Core.Battle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks.Dataflow;

namespace Grow.Test.Test
{
    public class IntProducer : AbstractProducer<int>
    {
        public IntProducer(ITargetBlock<int> target)
        {
            this.target = target;
        }

        public override bool Post(int data)
        {
            return target.Post(data);
        }
    }
}
