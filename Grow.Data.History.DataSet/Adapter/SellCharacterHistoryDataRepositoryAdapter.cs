﻿using System;
using Grow.Data.History.DataSet.HistoryDataSetTableAdapters;
using Fango.Core.Data;
using Grow.Data.History.DataSet.Properties;
using Microsoft.ApplicationBlocks.Data;

namespace Grow.Data.History.DataSet.Adapter
{
    public class SellCharacterHistoryDataRepositoryAdapter
    {
        /// <summary>
        /// 追加
        /// </summary>
        /// <param name="historyId"></param>
        /// <param name="logTime"></param>
        /// <param name="userId"></param>
        /// <param name="userCharacterId"></param>
        /// <param name="characterId"></param>
        /// <param name="level"></param>
        /// <param name="exp"></param>
        /// <param name="skillLevel"></param>
        /// <param name="skillExp"></param>
        /// <param name="hp"></param>
        /// <param name="attack"></param>
        /// <param name="defense"></param>
        /// <param name="speed"></param>
        public void AddNew(long historyId, DateTimeOffset logTime, int userId, long userCharacterId, int characterId,
                           int level, int exp, int skillLevel, int skillExp, int hp, int attack, int defense, int speed)
        {
            new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var sellCharacterHistoryTableAdapter = new SellCharacterHistoryTableAdapter())
                    {
                        return sellCharacterHistoryTableAdapter.AddNew(historyId, logTime, userId, userCharacterId,
                                                                       characterId, level, exp, skillLevel, skillExp, hp,
                                                                       attack, defense, speed);
                    }
                });
        }

        /// <summary>
        /// BulkInsert
        /// </summary>
        /// <param name="sellCharacterHistoryDataTable"></param>
        public void BulkInsert(HistoryDataSet.SellCharacterHistoryDataTable sellCharacterHistoryDataTable)
        {
            var historyConnStr = Settings.Default.messiah_history_devConnectionString;
            new DbAccessHandler().ExecuteAction(() => SqlHelper.BulkInsert(historyConnStr, sellCharacterHistoryDataTable));
        }
    }
}