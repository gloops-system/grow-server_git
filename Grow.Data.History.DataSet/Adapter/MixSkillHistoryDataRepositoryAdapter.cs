﻿using System;
using Fango.Core.Data;
using Grow.Data.History.DataSet.HistoryDataSetTableAdapters;

namespace Grow.Data.History.DataSet.Adapter
{
    public class MixSkillHistoryDataRepositoryAdapter
    {
        /// <summary>
        /// 追加
        /// </summary>
        /// <param name="historyId"></param>
        /// <param name="logTime"></param>
        /// <param name="userId"></param>
        /// <param name="userCharacterId"></param>
        /// <param name="characterId"></param>
        /// <param name="beforeSkillLevel"></param>
        /// <param name="beforeSkillExp"></param>
        /// <param name="afterSkillLevel"></param>
        /// <param name="afterSkillExp"></param>
        /// <param name="beforeGil"></param>
        /// <param name="afterGil"></param>
        /// <param name="mixSkillDetailHistoryId"></param>
        public void AddNew(long historyId, DateTimeOffset logTime, int userId, long userCharacterId, int characterId,
                           int beforeSkillLevel, int beforeSkillExp, int afterSkillLevel, int afterSkillExp,
                           int beforeGil, int afterGil, long mixSkillDetailHistoryId)
        {
            new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var mixSkillHistoryTableAdapter = new MixSkillHistoryTableAdapter())
                    {
                        return mixSkillHistoryTableAdapter.AddNew(historyId, logTime, userId, userCharacterId,
                                                                  characterId, beforeSkillExp, afterSkillExp, beforeGil,
                                                                  afterGil, mixSkillDetailHistoryId);
                    }
                });
        }
    }
}