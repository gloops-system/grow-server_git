﻿using System;
using Fango.Core.Data;
using Grow.Data.History.DataSet.HistoryDataSetTableAdapters;
using Grow.Data.History.DataSet.Properties;
using Microsoft.ApplicationBlocks.Data;

namespace Grow.Data.History.DataSet.Adapter
{
    public class QueueTwitterHistoryDataRepositoryAdapter
    {
        /// <summary>
        /// 追加
        /// </summary>
        /// <param name="historyId"></param>
        /// <param name="logTime"></param>
        /// <param name="deliveryDate"></param>
        /// <param name="message"></param>
        public void AddNew(long historyId, DateTimeOffset logTime, DateTimeOffset deliveryDate, string message)
        {
            new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var queueTwitterHistoryTableAdapter = new QueueTwitterHistoryTableAdapter())
                    {
                        return queueTwitterHistoryTableAdapter.AddNew(historyId, logTime, deliveryDate, message);
                    }
                });
        }

        /// <summary>
        /// BulkInsert
        /// </summary>
        /// <param name="queueTwitterHistoryDataTable"></param>
        public void BulkInsert(HistoryDataSet.QueueTwitterHistoryDataTable queueTwitterHistoryDataTable)
        {
            var historyConnStr = Settings.Default.messiah_history_devConnectionString;
            new DbAccessHandler().ExecuteAction(
                () => SqlHelper.BulkInsert(historyConnStr, queueTwitterHistoryDataTable));
        }
    }
}