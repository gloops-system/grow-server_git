﻿using System;
using Fango.Core.Data;
using Grow.Data.History.DataSet.HistoryDataSetTableAdapters;
using Grow.Data.History.DataSet.Properties;
using Microsoft.ApplicationBlocks.Data;

namespace Grow.Data.History.DataSet.Adapter
{
    public class QueueFacebookHistoryDataRepositoryAdapter
    {
        /// <summary>
        /// 追加
        /// </summary>
        /// <param name="historyId"></param>
        /// <param name="logTime"></param>
        /// <param name="deliveryDate"></param>
        /// <param name="message"></param>
        /// <param name="link"></param>
        /// <param name="picture"></param>
        /// <param name="name"></param>
        /// <param name="caption"></param>
        /// <param name="description"></param>
        public void AddNew(long historyId, DateTimeOffset logTime, DateTimeOffset deliveryDate, string message, string link, string picture, string name, string caption, string description)
        {
            new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var queueFacebookHistoryTableAdapter = new QueueFacebookHistoryTableAdapter())
                    {
                        return queueFacebookHistoryTableAdapter.AddNew(historyId, logTime, deliveryDate, message, link,
                                                                       picture, name, caption, description);
                    }
                });
        }

        /// <summary>
        /// BulkInsert
        /// </summary>
        /// <param name="queueFacebookHistoryDataTable"></param>
        public void BulkInsert(HistoryDataSet.QueueFacebookHistoryDataTable queueFacebookHistoryDataTable)
        {
            var historyConnStr = Settings.Default.messiah_history_devConnectionString;
            new DbAccessHandler().ExecuteAction(
                () => SqlHelper.BulkInsert(historyConnStr, queueFacebookHistoryDataTable));
        }
    }
}