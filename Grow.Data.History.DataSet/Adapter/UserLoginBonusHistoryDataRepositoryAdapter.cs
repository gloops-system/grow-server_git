﻿using System;
using Fango.Core.Data;
using Grow.Data.History.DataSet.HistoryDataSetTableAdapters;

namespace Grow.Data.History.DataSet.Adapter
{
    public class UserLoginBonusHistoryDataRepositoryAdapter
    {
        /// <summary>
        /// 追加
        /// </summary>
        /// <param name="historyId"></param>
        /// <param name="logTime"></param>
        /// <param name="userId"></param>
        /// <param name="loginBonusId"></param>
        /// <param name="sheetId"></param>
        /// <param name="loginNum"></param>
        /// <param name="category"></param>
        /// <param name="id"></param>
        public void AddNew(long historyId, DateTimeOffset logTime, int userId, int loginBonusId, int sheetId, int loginNum, int category, int id)
        {
            new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userLoginBonusHistoryTableAdapter = new UserLoginBonusHistoryTableAdapter())
                    {
                        return userLoginBonusHistoryTableAdapter.AddNew(historyId, logTime, userId, loginBonusId,
                                                                        sheetId, loginNum, category, id);
                    }
                });
        }
    }
}