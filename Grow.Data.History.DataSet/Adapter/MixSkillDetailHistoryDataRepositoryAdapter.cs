﻿using Fango.Core.Data;
using Grow.Data.History.DataSet.HistoryDataSetTableAdapters;
using Grow.Data.History.DataSet.Properties;
using Microsoft.ApplicationBlocks.Data;

namespace Grow.Data.History.DataSet.Adapter
{
    public class MixSkillDetailHistoryDataRepositoryAdapter
    {
        /// <summary>
        /// 追加
        /// </summary>
        /// <param name="mixSkillDetailHistoryId"></param>
        /// <param name="userCharacterId"></param>
        /// <param name="characterId"></param>
        /// <param name="level"></param>
        /// <param name="exp"></param>
        /// <param name="skillLevel"></param>
        /// <param name="skillExp"></param>
        /// <param name="hp"></param>
        /// <param name="attack"></param>
        /// <param name="defense"></param>
        /// <param name="speed"></param>
        public void AddNew(long mixSkillDetailHistoryId, long userCharacterId, int characterId, int level, int exp,
                           int skillLevel, int skillExp, int hp, int attack, int defense, int speed)
        {
            new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var mixSkillDetailHistoryTableAdapter = new MixSkillDetailHistoryTableAdapter())
                    {
                        return mixSkillDetailHistoryTableAdapter.AddNew(mixSkillDetailHistoryId, userCharacterId,
                                                                        characterId, level, exp, skillLevel, skillExp,
                                                                        hp, attack, defense, speed);
                    }
                });
        }

        /// <summary>
        /// BulkInsert
        /// </summary>
        /// <param name="mixSkillDetailHistoryDataTable"></param>
        public void BulkInsert(HistoryDataSet.MixSkillDetailHistoryDataTable mixSkillDetailHistoryDataTable)
        {
            var historyConnStr = Settings.Default.messiah_history_devConnectionString;
            new DbAccessHandler().ExecuteAction(
                () => SqlHelper.BulkInsert(historyConnStr, mixSkillDetailHistoryDataTable));
        }
    }
}