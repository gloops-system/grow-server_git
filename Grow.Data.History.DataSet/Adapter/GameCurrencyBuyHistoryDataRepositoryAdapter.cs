﻿using System;
using Fango.Core.Data;
using Grow.Data.History.DataSet.HistoryDataSetTableAdapters;
using Grow.Data.History.DataSet.Properties;
using Microsoft.ApplicationBlocks.Data;

namespace Grow.Data.History.DataSet.Adapter
{
    public class GameCurrencyBuyHistoryDataRepositoryAdapter
    {
        /// <summary>
        /// 追加
        /// </summary>
        /// <param name="historyId"></param>
        /// <param name="logTime"></param>
        /// <param name="userId"></param>
        /// <param name="gameCurrencyId"></param>
        /// <param name="price"></param>
        /// <param name="num"></param>
        public void AddNew(long historyId, DateTimeOffset logTime, int userId, int gameCurrencyId, int price, int num)
        {
            new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var gameCurrencyBuyHistoryTableAdapter = new GameCurrencyBuyHistoryTableAdapter())
                    {
                        return gameCurrencyBuyHistoryTableAdapter.AddNew(historyId, logTime, userId, gameCurrencyId, price, num);
                    }
                });
        }

        /// <summary>
        /// BulkInsert
        /// </summary>
        /// <param name="gameCurrencyBuyHistoryDataTable"></param>
        public void BulkInsert(HistoryDataSet.GameCurrencyBuyHistoryDataTable gameCurrencyBuyHistoryDataTable)
        {
            var historyConnStr = Settings.Default.messiah_history_devConnectionString;
            new DbAccessHandler().ExecuteAction(() => SqlHelper.BulkInsert(historyConnStr, gameCurrencyBuyHistoryDataTable));
        }
    }
}