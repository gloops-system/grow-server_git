﻿using System;
using System.Linq;
using Fango.Core.Data;
using Grow.Data.History.DataSet.HistoryDataSetTableAdapters;
using Grow.Data.History.DataSet.Properties;
using Microsoft.ApplicationBlocks.Data;

namespace Grow.Data.History.DataSet.Adapter
{
    public class GachaHistoryDataRepositoryAdapter
    {
        /// <summary>
        /// BulkInsert
        /// </summary>
        /// <param name="gachaHistoryDataTable"></param>
        public void BulkInsert(HistoryDataSet.GachaHistoryDataTable gachaHistoryDataTable)
        {
            var historyConnStr = Settings.Default.messiah_history_devConnectionString;
            new DbAccessHandler().ExecuteAction(() => SqlHelper.BulkInsert(historyConnStr, gachaHistoryDataTable));
        }
    }
}