﻿using System;
using System.Linq;
using Fango.Core.Data;
using Grow.Data.History.DataSet.HistoryDataSetTableAdapters;
using Grow.Data.History.DataSet.Properties;
using Microsoft.ApplicationBlocks.Data;

namespace Grow.Data.History.DataSet.Adapter
{
    public class ShopHistoryDataRepositoryAdapter
    {
        /// <summary>
        /// 追加
        /// </summary>
        /// <param name="historyId"></param>
        /// <param name="logTime"></param>
        /// <param name="userId"></param>
        /// <param name="shopId"></param>
        /// <param name="shopItemId"></param>
        /// <param name="price"></param>
        public void AddNew(DateTimeOffset logTime, int userId, int shopId, int shopItemId, int num)
        {
            new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var shopBuyHistoryTableAdapter = new ShopHistoryTableAdapter())
                    {
                        return shopBuyHistoryTableAdapter.AddNew(logTime, userId, shopId, shopItemId, num);
                    }
                });
        }
    }
}