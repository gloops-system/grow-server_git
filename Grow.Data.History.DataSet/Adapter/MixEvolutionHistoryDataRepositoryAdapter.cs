﻿using System;
using Fango.Core.Data;
using Grow.Data.History.DataSet.HistoryDataSetTableAdapters;

namespace Grow.Data.History.DataSet.Adapter
{
    public class MixEvolutionHistoryDataRepositoryAdapter
    {
        /// <summary>
        /// 追加
        /// </summary>
        /// <param name="historyId"></param>
        /// <param name="logTime"></param>
        /// <param name="userId"></param>
        /// <param name="userCharacterId"></param>
        /// <param name="beforeCharacterId"></param>
        /// <param name="afterCharacterId"></param>
        /// <param name="beforeGil"></param>
        /// <param name="afterGil"></param>
        /// <param name="mixEvolutionDetailHistoryId"></param>
        public void AddNew(long historyId, DateTimeOffset logTime, int userId, long userCharacterId,
                           int beforeCharacterId, int afterCharacterId, int beforeGil, int afterGil,
                           long mixEvolutionDetailHistoryId)
        {
            new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var mixEvolutionHistoryTableAdapter = new MixEvolutionHistoryTableAdapter())
                    {
                        return mixEvolutionHistoryTableAdapter.AddNew(historyId, logTime, userId, userCharacterId,
                                                                      beforeCharacterId, afterCharacterId, beforeGil,
                                                                      afterGil, mixEvolutionDetailHistoryId);
                    }
                });
        }
    }
}