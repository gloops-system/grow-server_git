﻿using System;
using Fango.Core.Data;
using Grow.Data.History.DataSet.HistoryDataSetTableAdapters;
using Grow.Data.History.DataSet.Properties;
using Microsoft.ApplicationBlocks.Data;

namespace Grow.Data.History.DataSet.Adapter
{
    public class QueueNotificationHistoryDataRepositoryAdapter
    {
        /// <summary>
        /// 追加
        /// </summary>
        /// <param name="logTime"></param>
        /// <param name="historyId"></param>
        /// <param name="notificationType"></param>
        /// <param name="deliveryDate"></param>
        /// <param name="fromUser"></param>
        /// <param name="toUser"></param>
        /// <param name="message"></param>
        /// <param name="optionBadge"></param>
        /// <param name="optionSound"></param>
        /// <param name="optionCollapseKey"></param>
        /// <param name="optionStyle"></param>
        /// <param name="optionIconUrl"></param>
        /// <param name="optionExtras"></param>
        /// <param name="targetDevice"></param>
        public void AddNew(long historyId, DateTimeOffset logTime, int notificationType, DateTimeOffset deliveryDate, int? fromUser, int? toUser,
                           string message, int? optionBadge, string optionSound, string optionCollapseKey,
                           string optionStyle, string optionIconUrl, string optionExtras, int targetDevice)
        {
            new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var queueNotificationsHistoryTableApdater = new QueueNotificationHistoryTableAdapter())
                    {
                        return queueNotificationsHistoryTableApdater.AddNew(
                                historyId, logTime, notificationType, deliveryDate, fromUser, toUser,
                                message, optionBadge, optionSound, optionCollapseKey, optionStyle,
                                optionIconUrl, optionExtras, targetDevice);
                    }
                });
        }

        /// <summary>
        /// BUlkInsert
        /// </summary>
        /// <param name="queueNotificationHistoryDataTable"></param>
        public void BulkInsert(HistoryDataSet.QueueNotificationHistoryDataTable queueNotificationHistoryDataTable)
        {
            var historyConnStr = Settings.Default.messiah_history_devConnectionString;
            new DbAccessHandler().ExecuteAction(
                () => SqlHelper.BulkInsert(historyConnStr, queueNotificationHistoryDataTable));
        }
    }
}