﻿using Fango.Core.Data;
using Grow.Data.History.DataSet.HistoryDataSetTableAdapters;
using Grow.Data.History.DataSet.Properties;
using Microsoft.ApplicationBlocks.Data;

namespace Grow.Data.History.DataSet.Adapter
{
    public class MixEvolutionDetailHistoryDataRepositoryAdapter
    {
        /// <summary>
        /// 追加
        /// </summary>
        /// <param name="mixEvolutionDetailHistoryId"></param>
        /// <param name="materialId"></param>
        /// <param name="beforeNum"></param>
        /// <param name="afterNum"></param>
        public void AddNew(long mixEvolutionDetailHistoryId, int materialId, int beforeNum, int afterNum)
        {
            new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var mixEvolutionDetailHistoryTableAdapter = new MixEvolutionDetailHistoryTableAdapter())
                    {
                        return mixEvolutionDetailHistoryTableAdapter.AddNew(mixEvolutionDetailHistoryId, materialId,
                                                                            beforeNum, afterNum);
                    }
                });
        }

        /// <summary>
        /// BulkInsert
        /// </summary>
        /// <param name="mixEvolutionDetailHistoryDataTable"></param>
        public void BulkInsert(HistoryDataSet.MixEvolutionDetailHistoryDataTable mixEvolutionDetailHistoryDataTable)
        {
            var historyConnStr = Settings.Default.messiah_history_devConnectionString;
            new DbAccessHandler().ExecuteAction(
                () => SqlHelper.BulkInsert(historyConnStr, mixEvolutionDetailHistoryDataTable));
        }
    }
}