﻿using System;
using Fango.Core.Data;
using Grow.Data.History.DataSet.HistoryDataSetTableAdapters;
using Grow.Data.History.DataSet.Properties;
using Microsoft.ApplicationBlocks.Data;

namespace Grow.Data.History.DataSet.Adapter
{
    public class UserGiftHistoryDataRepositoryAdapter
    {
        /// <summary>
        /// 追加
        /// </summary>
        /// <param name="historyId"></param>
        /// <param name="logTime"></param>
        /// <param name="userId"></param>
        /// <param name="giftType"></param>
        /// <param name="category"></param>
        /// <param name="id"></param>
        /// <param name="num"></param>
        /// <param name="getDate"></param>
        public void AddNew(long historyId, DateTimeOffset logTime, int userId, int giftType, int category, int id, int num, DateTimeOffset getDate)
        {
            new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userGiftHistoryTableAdapter = new UserGiftHistoryTableAdapter())
                    {
                        return userGiftHistoryTableAdapter.AddNew(historyId, logTime, userId, giftType, category, id, num, getDate);
                    }
                });
        }

        /// <summary>
        /// BulkInsert
        /// </summary>
        /// <param name="userGiftHistoryDataTable"></param>
        public void BulkInsert(HistoryDataSet.UserGiftHistoryDataTable userGiftHistoryDataTable)
        {
            var historyConnStr = Settings.Default.messiah_history_devConnectionString;
            new DbAccessHandler().ExecuteAction(
                () => SqlHelper.BulkInsert(historyConnStr, userGiftHistoryDataTable));
        }
    }
}