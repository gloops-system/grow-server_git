﻿using System;
using Grow.Data.History.DataSet.Adapter;

namespace Grow.Data.History.DataSet.Container
{
    public class HistoryDataContainer
    {
        // Gacha
        private readonly Lazy<GachaHistoryDataRepositoryAdapter> _gachaHistory;
        public GachaHistoryDataRepositoryAdapter GachaHistory { get { return _gachaHistory.Value; } }

        // Mix
        private readonly Lazy<MixEvolutionDetailHistoryDataRepositoryAdapter> _mixEvolutionDetailHistory;
        public MixEvolutionDetailHistoryDataRepositoryAdapter MixEvolutionDetailHistory { get { return _mixEvolutionDetailHistory.Value; } }

        private readonly Lazy<MixEvolutionHistoryDataRepositoryAdapter> _mixEvolutionHistory;
        public MixEvolutionHistoryDataRepositoryAdapter MixEvolutionHistory { get { return _mixEvolutionHistory.Value; } }

        private readonly Lazy<MixSkillHistoryDataRepositoryAdapter> _mixSkillHistory;
        public MixSkillHistoryDataRepositoryAdapter MixSkillHistory { get { return _mixSkillHistory.Value; } }

        private readonly Lazy<MixSkillDetailHistoryDataRepositoryAdapter> _mixSkillDetailHistory;
        public MixSkillDetailHistoryDataRepositoryAdapter MixSkillDetailHistory { get { return _mixSkillDetailHistory.Value; } }

        // Shop
        private readonly Lazy<ShopHistoryDataRepositoryAdapter> _shopHistory;
        public ShopHistoryDataRepositoryAdapter ShopHistory { get { return _shopHistory.Value; } }

        // GameCurrency
        private readonly Lazy<GameCurrencyBuyHistoryDataRepositoryAdapter> _gameCurrencyBuyHistory;
        public GameCurrencyBuyHistoryDataRepositoryAdapter GameCurrencyBuyHistory { get { return _gameCurrencyBuyHistory.Value; } }

        // Queue
        private readonly Lazy<QueueNotificationHistoryDataRepositoryAdapter> _queueNotificationsHistory;
        public QueueNotificationHistoryDataRepositoryAdapter QueueNotificationsHistory { get { return _queueNotificationsHistory.Value; } }

        private readonly Lazy<QueueTwitterHistoryDataRepositoryAdapter> _queueTwitterHistory;
        public QueueTwitterHistoryDataRepositoryAdapter QueueTwitterHistory { get { return _queueTwitterHistory.Value; } }

        private readonly Lazy<QueueFacebookHistoryDataRepositoryAdapter> _queueFacebookHistory;
        public QueueFacebookHistoryDataRepositoryAdapter QueueFacebookHistory { get { return _queueFacebookHistory.Value; } }

        // Gift
        private readonly Lazy<UserGiftHistoryDataRepositoryAdapter> _userGiftHistory;
        public UserGiftHistoryDataRepositoryAdapter UserGiftHistory { get { return _userGiftHistory.Value; } }

        // Sell
        private readonly Lazy<SellCharacterHistoryDataRepositoryAdapter> _sellCharacterHistory;
        public SellCharacterHistoryDataRepositoryAdapter SellCharacterHistory { get { return _sellCharacterHistory.Value; } }

        // LoginBonus
        private readonly Lazy<UserLoginBonusHistoryDataRepositoryAdapter> _userLoginBonusHistory;
        public UserLoginBonusHistoryDataRepositoryAdapter UserLoginBonusHistory { get { return _userLoginBonusHistory.Value; } }

        public HistoryDataContainer()
        {
            // Gacha
            _gachaHistory = new Lazy<GachaHistoryDataRepositoryAdapter>(() => new GachaHistoryDataRepositoryAdapter(), true);

            // Mix
            _mixEvolutionHistory = new Lazy<MixEvolutionHistoryDataRepositoryAdapter>(() => new MixEvolutionHistoryDataRepositoryAdapter(), true);
            _mixEvolutionDetailHistory = new Lazy<MixEvolutionDetailHistoryDataRepositoryAdapter>(() => new MixEvolutionDetailHistoryDataRepositoryAdapter(), true);
            _mixSkillHistory = new Lazy<MixSkillHistoryDataRepositoryAdapter>(() => new MixSkillHistoryDataRepositoryAdapter(), true);
            _mixSkillDetailHistory = new Lazy<MixSkillDetailHistoryDataRepositoryAdapter>(() => new MixSkillDetailHistoryDataRepositoryAdapter(), true);

            // Shop
            _shopHistory = new Lazy<ShopHistoryDataRepositoryAdapter>(() => new ShopHistoryDataRepositoryAdapter(), true);

            // GameCurrency
            _gameCurrencyBuyHistory = new Lazy<GameCurrencyBuyHistoryDataRepositoryAdapter>(() => new GameCurrencyBuyHistoryDataRepositoryAdapter(), true);

            // Queue
            _queueNotificationsHistory = new Lazy<QueueNotificationHistoryDataRepositoryAdapter>(() => new QueueNotificationHistoryDataRepositoryAdapter(), true);
            _queueTwitterHistory = new Lazy<QueueTwitterHistoryDataRepositoryAdapter>(() => new QueueTwitterHistoryDataRepositoryAdapter(), true);
            _queueFacebookHistory = new Lazy<QueueFacebookHistoryDataRepositoryAdapter>(() => new QueueFacebookHistoryDataRepositoryAdapter(), true);

            // Gift
            _userGiftHistory = new Lazy<UserGiftHistoryDataRepositoryAdapter>(() => new UserGiftHistoryDataRepositoryAdapter(), true);

            // Sell
            _sellCharacterHistory = new Lazy<SellCharacterHistoryDataRepositoryAdapter>(() => new SellCharacterHistoryDataRepositoryAdapter(), true);

            // LoginBonus
            _userLoginBonusHistory = new Lazy<UserLoginBonusHistoryDataRepositoryAdapter>(() => new UserLoginBonusHistoryDataRepositoryAdapter(), true);
        }
    }
}