@setlocal
@echo off

svn up

set PLATFORM=MBGA
set TARGET_FRAMEWORK_VERSION=v4.5
set EnableNuGetPackageRestore=true
set PATH=%PATH%;%WINDIR%\Microsoft.Net\Framework\v4.0.30319

for /f %%s in ('dir /b *.sln') do set SOLUTION=%%s

:option
if "%1"=="" goto option_end
if "%1"=="release" set RELEASE=1
if "%1"=="rebuild" set TARGET=/t:Rebuild
if "%1"=="clean"   set TARGET=/t:Clean
shift
goto option
:option_end

if "%RELEASE%" == "1" (set CONFIGURATION=Release) else (set CONFIGURATION=Debug)

REM 一応残しておく
set DEFINE_CONSTANTS=%PLATFORM%
if "%RELEASE%" == "1" (set DEFINE_CONSTANTS=%DEFINE_CONSTANTS%;RELEASE) else (set DEFINE_CONSTANTS=%DEFINE_CONSTANTS%;DEBUG)
if "%TARGET_FRAMEWORK_VERSION%" == "v4.5" set DEFINE_CONSTANTS=%DEFINE_CONSTANTS%;NET4

echo msbuild %SOLUTION% /p:Configuration=%CONFIGURATION% /p:TargetFrameworkVersion=%TARGET_FRAMEWORK_VERSION% /p:DefineConstants="%DEFINE_CONSTANTS%" /p:Platform="Any CPU" %TARGET%
msbuild %SOLUTION% /p:TargetFrameworkVersion=%TARGET_FRAMEWORK_VERSION% /p:DefineConstants="%DEFINE_CONSTANTS%" /p:Platform="Any CPU" %TARGET%

pause