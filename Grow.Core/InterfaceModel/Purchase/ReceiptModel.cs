﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.InterfaceModel.Purchase
{
    public class ReceiptModel
    {
        public long TransactionId { get; set; }
        public long OriginalTransactionId { get; set; }
        public int UserId { get; set; }
        public string Bid { get; set; }
        public string Bvrs { get; set; }
        public int ItemId { get; set; }
        public string OriginalPurchaseDate { get; set; }
        public string PurchaseDate { get; set; }
        public string ProductId { get; set; }
        public int Quantity { get; set; }
        public int ReceiptStatus { get; set; }
        public int Status { get; set; }
        public int Gem { get; set; }
    }
}
