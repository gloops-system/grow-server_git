﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.InterfaceModel.Purchase
{
     [DataContract]
    public class AndroidReceiptModel
    {
        [DataMember(Name = "nonce")]
        public string Nonce { get; set; }
        [DataMember(Name = "notificationId")]
        public string NotificationId { get; set; }
        [DataMember(Name = "orderId")]
        public string OrderId { get; set; }
        [DataMember(Name = "packageName")]
        public string PackageName { get; set; }
        [DataMember(Name = "productId")]
        public string ProductId { get; set; }
        [DataMember(Name = "purchaseTime")]
        public long PurchaseTime { get; set; }
        [DataMember(Name = "developerPayload")]
        public string DeveloperPayload { get; set; }
        [DataMember(Name = "purchaseState")]
        public int PurchaseState { get; set; }
        public int UserId { get; set; }
        public int Status { get; set; }
        public int Gem { get; set; }
    }
}
