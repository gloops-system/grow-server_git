﻿using Grow.Core.InterfaceModel.Avatar;
using Grow.Core.InterfaceModel.Gear;
using Grow.Core.InterfaceModel.Monster;
using Grow.Core.InterfaceModel.User;
using Fango.Core.Util;
using System.Collections.Generic;

namespace Grow.Core.InterfaceModel.Utility
{
    public class UtilityModel
    {
        public UtilityModel()
        {
        }

        public long TimeStamp { get; set; }
    }
}