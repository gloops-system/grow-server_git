﻿using System;

namespace Grow.Core.InterfaceModel.Authentication
{
    public class TokenModel
    {
        public int UserId;
        public string Version;
        public string UUID;
        public DateTimeOffset Expires;
    }
}