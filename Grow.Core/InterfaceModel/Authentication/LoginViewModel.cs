﻿namespace Grow.Core.InterfaceModel.Authentication
{
    public class LoginViewModel
    {
        public string SessionId { get; private set; }

        public LoginViewModel() { }
        public LoginViewModel(string sessionId)
        {
            SessionId = sessionId;
        }
    }
}