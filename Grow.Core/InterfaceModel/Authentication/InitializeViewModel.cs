﻿namespace Grow.Core.InterfaceModel.Authentication
{
    public class InitializeViewModel
    {
        public int UserId { get; private set; }
        public int UniqueId { get; private set; }
        public string Secret { get; private set; }

        public InitializeViewModel() { }
        public InitializeViewModel(int userId, int uniqueId, string secret)
        {
            UserId = userId;
            UniqueId = uniqueId;
            Secret = secret;
        }
    }
}