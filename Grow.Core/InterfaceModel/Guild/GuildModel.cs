﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.InterfaceModel.Guild
{
    public class GuildModel
    {
         public int GuildId { get; set; }
	     public int FounderID { get; set; }
         public string Name { get; set; }
         public string Symbol { get; set; }
	     public string Introduction { get; set; }
	     public int RecruitType { get; set; }
	     public int RequiredTrophy { get; set; }
	     public DateTimeOffset AddTime { get; set; }
	     public DateTimeOffset UpdtTime { get; set; }
    }
}
