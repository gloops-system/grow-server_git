﻿using Grow.Core.InterfaceModel.Monster;
using System.Collections.Generic;

namespace Grow.Core.InterfaceModel.Monster
{
    public class MeetDispatchMonsterViewModel
    {
        public List<DispatchMonsterModel> MonsterList { get; set; }
    }
}
