﻿using Grow.Core.InterfaceModel.Duty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.InterfaceModel.Monster
{
    public class DispatchFriendListViewModel
    {
        public List<FriendViewModel> FriendList { get; set; }
    }
}
