﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.InterfaceModel.Monster
{
    public class DispatchMonsterModel : UserMonsterModel 
    {
        public int UserId { get; set; }
        public string NickName { get; set; }
        public int AvatarId { get; set; }
        public int GearId { get; set; }
        public int AreaId { get; set; }
        public int FloorIndex { get; set; }
    }
}
