﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.InterfaceModel.Monster
{
    public class UserMonsterModel
    {
        public long UserMonsterId { get; set; }
        public int MonsterId { get; set; }
        public int Level { get; set; }
        public byte Status { get; set; }
        public int Exp { get; set; }
        public int Life { get; set; }
        public int Hp { get; set; }
        public int LastHp { get; set; }
        public long LastHpUpdtTime { get; set;}
    }

    public class UserMonsterSkillModel
    {
        public int MonsterSkillId { get; set; }
        public int Rarity { get; set; }
    }

}
