﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.InterfaceModel.Gear
{
    public class UserGearSkillModel
    {
	    public long UserGearId { get; set; }
	    public int GearSkillDetailId { get; set; }
        public string SkillName { get; set; }
	    public int Attack { get; set; }
	    public int Effect { get; set; }
    }
}
