﻿using Grow.Core.InterfaceModel.Gear;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.InterfaceModel.Gear
{
    public class UserGearsViewModel
    {
        public List<UserGearModel> UserGears { get; set; }
    }
}
