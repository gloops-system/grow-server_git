﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.InterfaceModel.Gear
{
    public class UserGearModel
    {
        public long UserGearId { get; set; }
	    public int UserId { get; set; }
	    public int GearId { get; set; }
	    public int GearType { get; set; }
	    public int Category { get; set; }
	    public int Property { get; set; }
	    public int Rarity { get; set; }
	    public int Attack { get; set; }
	    public int Defense { get; set; }
	    public int CutPercentage { get; set;}
	    public int Stun { get; set; }
        public int Point { get; set; }
        public List<UserGearSkillModel> Skills { get; set; }
    }
}
