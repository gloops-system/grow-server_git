﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.InterfaceModel.Duty
{
    public class LabyrinthMonsterRequestModel
    {
        public long UserMonsterId { get; set; }
        public int Hp { get; set; }
    }
}
