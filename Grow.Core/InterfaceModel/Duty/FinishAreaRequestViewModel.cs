﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.InterfaceModel.Duty
{
    public class FinishAreaRequestViewModel
    {
        public int Hp { get; set; }
        public int Exp { get; set; }
        public int Gold { get; set; }
        public FinishAreaItemRequestModel[] ItemList { get; set; }
        public int AreaId { get; set; }
        public FinishFieldRequestViewModel[] FloorList { get; set; }
        public FinishAreaAvatarRequestModel Avatar { get; set; }
    }

    public class FinishAreaItemRequestModel
    {
        public int ItemId { get; set; }        
        public int Number { get; set; }
    }

    public class FinishAreaAvatarRequestModel
    {
        public int AvatarUserId { get; set; }
        public int Exp { get; set; }
    }
}
