﻿using Grow.Core.InterfaceModel.Monster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.InterfaceModel.Duty
{
    public class LabyrinthFinishFieldViewModel
    {
        public UserMonsterModel[] UserMonsters { get; set; }
        public DispatchMonsterModel[] PlayerMonsters { get; set; }
        public LabyrinthFeverTimeViewModel FeverTime { get; set; }
    }
}
