﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.InterfaceModel.Duty
{
    public class FriendViewModel
    {
        public int UserId { get; set; }        
        public int RequiredGold { get; set; }
    }
}
