﻿using Grow.Core.InterfaceModel.Monster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.InterfaceModel.Duty
{
    public class LabyrinthFinishAreaViewModel : FinishAreaViewModel
    {
        public LabyrinthFinishAreaViewModel(FinishAreaViewModel finishAreaModel)
        {
            UserStatus = finishAreaModel.UserStatus;
            UserItem = finishAreaModel.UserItem;
            UserAvatar = finishAreaModel.UserAvatar;
        }
        public UserMonsterModel[] UserMonsters { get; set; }
        public DispatchMonsterModel[] PlayerMonsters { get; set; }
        public LabyrinthFeverTimeViewModel FeverTime { get; set; }
    }
}
