﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.InterfaceModel.Duty
{
    public class LabyrinthFinishAreaRequestViewModel : FinishAreaRequestViewModel
    {
        public int AreaIdx { get; set; }
        public LabyrinthMonsterRequestModel[] MonsterList { get; set; }
    }
}
