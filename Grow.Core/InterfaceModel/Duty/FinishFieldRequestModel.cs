﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.InterfaceModel.Duty
{
    public class FinishFieldRequestViewModel
    {
        public int FloorId { get; set; }
        public int Score { get; set; }
        public int RankScore { get; set; }
        public int RankTime { get; set; }
        public int RankMonster { get; set; }
    }    
}
