﻿using Grow.Core.Application.User;
using Grow.Core.InterfaceModel.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.InterfaceModel.Duty
{
    public class FinishAreaViewModel
    {
        public FinishAreaUserStatusModel UserStatus { get; set; }
        public List<UserItemModel> UserItem { get; set; }
        public FinishAreaAvatarModel UserAvatar { get; set; }
        //Reward......
    }

    public class FinishAreaUserStatusModel
    {
        public int Level { get; set; }
        public int Hp  { get; set; }
        public int Sp  { get; set; }
        public int Exp { get; set; }
        public int Gold { get; set; }
        public int Attack { get; set; }
        public int Defense { get; set; }
        public int StunValue { get; set; }
        public int StunResistance { get; set; }
        public int StunRecoveryStand { get; set; }
        public int StunRecoveryDown { get; set; }
        public int ScoreUp { get; set; }
        public int AvatarHpBonus { get; set; }
        public int AvatarSpBonus { get; set; }
        public int AvatarAttackBonus { get; set; }
        public int AvatarDefenseBonus { get; set; }
        public int AvatarScoreUpBonus { get; set; }
        public int MaxFriendsNum { get; set; }
        public int MaxMonsterBoxNum { get; set; }
        public int MaxItemBoxNum { get; set; }
        public int MaxGearBagNum { get; set; }
    }

    public class FinishAreaAvatarModel
    {
        public int Level { get; set; }
        public int Exp { get; set; }
        public int Hp { get; set; }
        public int Sp { get; set; }
        public int Attack { get; set; }
        public int Defense { get; set; }
    }

}
