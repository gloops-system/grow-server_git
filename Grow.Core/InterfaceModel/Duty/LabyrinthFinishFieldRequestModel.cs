﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.InterfaceModel.Duty
{
    public class LabyrinthFinishFieldRequestViewModel
    {
        public int AreaIdx { get; set; }
        public FinishFieldRequestViewModel Floor { get; set; }
        public LabyrinthMonsterRequestModel[] MonsterList { get; set; }
        public ItemRequestModel[] CollectItemList { get; set; }
        public ItemRequestModel[] UsedItemList { get; set; }
    }

    public class ItemRequestModel
    {
        public int ItemId { get; set; }
        public int Number { get; set; }
    }
}
