﻿using Grow.Core.InterfaceModel.Monster;
using Grow.Core.InterfaceModel.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.InterfaceModel.Duty
{
    public class RecoveryStaminaViewModel
    {
        public int Stamina { get; set; }
        public long StaminaLastHealTime { get; set; }
        public int Gem  { get; set; }        
    }    
}
