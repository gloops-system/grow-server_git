﻿using Grow.Core.InterfaceModel.Monster;
using Grow.Core.InterfaceModel.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.InterfaceModel.Duty
{
    public class LabyrinthStartFieldViewModel
    {
        public int Stamina { get; set; }
        public long StaminaLastHealTime { get; set; }
        public UserMonsterModel[] UserMonsters { get; set; }
        public DispatchMonsterModel[] PlayerMonsters { get; set; }
        public LabyrinthFeverTimeViewModel FeverTime { get; set; }
    }    
}
