﻿using Grow.Core.InterfaceModel.Monster;
using Grow.Core.InterfaceModel.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.InterfaceModel.Duty
{
    public class LabyrinthStartRequestViewModel
    {
        public UserEquipmentGearRequestModel[] Gears { get; set; }
        public UserEquipmentItemRequestModel[] Items { get; set; }
    }

    public class UserEquipmentGearRequestModel
    {
        public byte Type { get; set; } //Weapon(Main) = 1, Weapon secondary = 2, Weapon Rental = 3, Shield = 4, Accessory = 5 GearBag = 6, Consumable Items = 7
        public long UserGearId { get; set; }        
    }

    public class UserEquipmentItemRequestModel
    {
        public int ItemId { get; set; }
        public int Num { get; set; }
    }

}
