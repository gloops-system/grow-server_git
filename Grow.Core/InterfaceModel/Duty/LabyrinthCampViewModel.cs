﻿using Grow.Core.InterfaceModel.Monster;
using Grow.Core.InterfaceModel.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.InterfaceModel.Duty
{
    public class LabyrinthCampViewModel
    {
        public UserMonsterModel[] UserMonsters { get; set; }
        public DispatchMonsterModel[] PlayerMonsters { get; set; }
        public List<LeaderboardUserModel> GlobalRanking { get; set; }
        public List<LeaderboardUserModel> RivalsRanking { get; set; }
        public List<UserNotificationModel> UserNotifications { get; set; }
        public LabyrinthAreaViewModel CurrentArea { get; set; }
        public LabyrinthFeverTimeViewModel FeverTime { get; set; }
        public LabyrinthUserEquipmentViewModel Equipments { get; set; }
        public int CurrentScore { get; set; }

    }

    public class LeaderboardUserModel
    {
        public int UserId { get; set; }
        public int Level { get; set; }
        public string NickName { get; set; }
        public int AvatarId { get; set; }
        public int GearId { get; set; }
        public int Ranking { get; set; }
        public int Point { get; set; }
    }

    public class LabyrinthAreaViewModel
    {
        public int AreaIdx { get; set; }
        public List<LabyrinthFloorViewModel> Floors { get; set; }
        public int CurrentFloorIdx { get; set; }
        public bool IsCompleted { get; set; }
        public bool IsFinalArea { get; set; }
        public List<ItemRequestModel> CollectItemList { get; set; }
        public List<DefeatMonsterModel> MonsterList { get; set; }
    }

    public class LabyrinthFloorViewModel
    {
        public int FloorId { get; set; }
        public int Score { get; set; }
        public int RankScore { get; set; }
        public int RankTime { get; set; }
        public int RankMonster { get; set; }
    }

    public class UserRentEquipmentViewModel : UserEquipmentGearRequestModel
    {
        public int GearId { get; set; }
    }

    public class UserEquipmentOwnerViewModel
    {
        public List<UserEquipmentGearRequestModel> Gears { get; set; }
        public List<UserEquipmentItemRequestModel> Items { get; set; }
    }

    public class LabyrinthUserEquipmentViewModel
    {
        public UserEquipmentOwnerViewModel Owner { get; set; }
        public UserRentEquipmentViewModel Rent { get; set; }
    }

    public class DefeatMonsterModel
    {
        public long UserMonsterId { get; set; }
    }
}
