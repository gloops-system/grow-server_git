﻿using Grow.Core.InterfaceModel.Monster;
using Grow.Core.InterfaceModel.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.InterfaceModel.Duty
{
    public class LabyrinthFeverTimeViewModel
    {
        public long StartTime { get; set; }
        public long EndTime { get; set; }
        public int LessStamina { get; set; }
        public int AttackIncrease { get; set; }
        public int CriticalHit { get; set; }
        public int ObtainableGold { get; set; }
        public int MoreMana { get; set; }
        public int MoreTreasure { get; set; }
        public int DropMoreGold { get; set; }
        public int DropMoreGear { get; set; }
        public int DropMoreWeapon { get; set; }
        public int DropMoreShield { get; set; }
        public int DropMoreAccessory { get; set; }
        public int ChanceCapturingMonster { get; set; }
        public int ScoreIncrease { get; set; }
    }    
}
