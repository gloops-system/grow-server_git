﻿using System.Collections.Generic;

namespace Grow.Core.InterfaceModel.Duty
{
    public class LabyrinthAreaModel
    {
        public int AreaId { get; set; }
        public List<int> FloorIds { get; set; }
    }
}