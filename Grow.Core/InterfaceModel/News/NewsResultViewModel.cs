﻿namespace Grow.Core.InterfaceModel.News
{
    public class NewsResultViewModel
    {
        public bool HasImage { get; set; }
        public string ImageBase64 { get; set; }
    }
}