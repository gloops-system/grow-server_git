﻿using System.Collections.Generic;
using Grow.Core.InterfaceModel.News;

namespace Grow.Core.InterfaceModel.News
{
    public class NewsViewModel
    {
        public int EntryNum { get; set; }
        public virtual ICollection<NewsModel> News { get; set; }
    }
}