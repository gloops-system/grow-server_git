﻿namespace Grow.Core.InterfaceModel.News
{
    public class NewsModel
    {
        public int NewsId { get; set; }
        public string PostDate { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public int? Action { get; set; }
    }
}