﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Grow.Core.InterfaceModel.Common
{
    public class UserViewModel
    {
        public string TeamName { get; set; }
        public int UserId { get; set; }
        public string NickName { get; set; }
        public string League { get; set; }
        public int Rank { get; set; }
        public int Level { get; set; }
        public DateTimeOffset LastLogin { get; set; }
    }
}
