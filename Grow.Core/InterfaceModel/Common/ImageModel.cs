﻿namespace Grow.Core.InterfaceModel.Common
{
    public class ImageModel
    {
        public string ImagePath { get; set; }
        public string Revision { get; set; }
        public string Timestamp { get; set; }
    }
}