﻿using Grow.Core.InterfaceModel.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.InterfaceModel.Common
{
    public class CharacterModel
    {
        public long UserCharacterId { get; set; }
        public int CharacterId { get; set; }
        public string CharacterName { get; set; }

        public string Rank { get; set; }
        public int CostPoint { get; set; }
        public int Rarity { get; set; }

        public int Hp { get; set; }
        public int Attack { get; set; }
        public int Defence { get; set; }
        public int Speed { get; set; }

        

        public int Level { get; set; }
        public int MaxLevel { get; set; }
        public int BeforeExp { get; set; }
        public int Exp { get; set; }
        public int NextExp { get; set; }
   
        public virtual ImageModel CharacterImage { get; set; }
        public virtual ImageModel CharacterMediumImage { get; set; }
        public virtual ImageModel CharacterLargeImage { get; set; }
        public virtual ImageModel CharacterImageBackground { get; set; }
        public virtual ImageModel CharacterImageRear { get; set; }

        public int ViewType { get; set; }
       
    }
}
