﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inazuma.ViewModels.Common
{
    public class CaptainViewModel
    {
        public string Name;
        public int Level;
        public int MaxLevel;
        public string Image;
    }
}
