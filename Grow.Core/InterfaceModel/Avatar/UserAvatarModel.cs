﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.InterfaceModel.Avatar
{
    public class UserAvatarModel
    {
        public long UserAvatarId { get; set; }
        public int AvatarId { get; set; }
        public int Level { get; set; }
        public int Exp { get; set; }
        public int IsMain { get; set; }
        public int GradeId { get; set; }
        public int ColorId { get; set; }
    }
}
