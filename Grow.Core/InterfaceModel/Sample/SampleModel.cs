﻿namespace Grow.Core.InterfaceModel.Sample
{
    public class SampleModel
    {
        public int SampleInt { get; set; }
        public string SampleString { get; set; }
        public Sample2Model Sample2 { get; set; }
    }

    public class Sample2Model
    {
        public int Sample2Int { get; set; }
        public string Sample2String { get; set; }
    }
}