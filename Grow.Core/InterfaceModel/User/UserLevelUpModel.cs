﻿namespace Grow.Core.InterfaceModel.User
{
    public class UserLevelUpModel
    {
        public int Level { get; set; }
        public int AfterLevel { get; set; }
        public int BeforeExp { get; set; }//
        public int AfterExp { get; set; }//
        public int CurrentLevelExp { get; set; }
        public int NextLevelExp { get; set; }
        public int MaxDp { get; set; }
        public int AddMaxDp { get; set; }
        public int MaxCostPoint { get; set; }
        public int AddMaxCostPoint { get; set; }
        public int MaxFriendNum { get; set; }
        public int AddMaxFriendNum { get; set; }

    }
}
