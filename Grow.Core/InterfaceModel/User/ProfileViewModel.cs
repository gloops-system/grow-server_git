﻿using Grow.Core.InterfaceModel.User;

namespace Grow.Core.InterfaceModel.User
{
    public class ProfileViewModel
    {
        public ProfileModel Profile { get; set; }
    }
}