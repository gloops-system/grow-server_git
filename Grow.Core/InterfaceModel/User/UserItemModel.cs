﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.InterfaceModel.User
{
    public class UserItemModel
    {
        public int ItemId { get; set; }
        public int ItemCategory { get; set; }
        public int Num { get; set; }
    }
}
