﻿using Grow.Core.InterfaceModel.Avatar;
using Grow.Core.InterfaceModel.Gear;
using Grow.Core.InterfaceModel.Monster;
using Grow.Core.InterfaceModel.User;
using Fango.Core.Util;
using System.Collections.Generic;

namespace Grow.Core.InterfaceModel.User
{
    public class BattleProfileModel
    {
        public BattleProfileModel()
        {
        }

        public int BattleHistoryId { get; set; }
        public int TargetUserId { get; set; }
        public string TargetNickName { get; set; }
        public int TargetGold { get; set; }
        public int TargetPlasma { get; set; }
        public string TargetColonyData { get; set; }
    }
}