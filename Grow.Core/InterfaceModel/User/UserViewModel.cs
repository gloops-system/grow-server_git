﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.InterfaceModel.User
{
    public class UserBaseModel
    {
        public int UserId { get; set; }
        public string NickName { get; set; }
        public string FacebookId { get; set; }
        public int TutorialStatus { get; set; }
        public DateTimeOffset TutorialEndDate { get; set; }
        public DateTimeOffset AddTime { get; set; }
        public DateTimeOffset UpdtTime { get; set; }
    }

    public class UserViewModel : UserBaseModel
    {
        public short Level { get; set; }
        public int Gold { get; set; }
        public int Plasma { get; set; }
        public long ColonyHistoryIndex { get; set; }
        public string ColonyData { get; set; }
        public int LastSyncTimePassed { get; set; }
        public long UserAvatarId { get; set; }
        public long UserGearId { get; set; }
        public int Exp { get; set; }
        public int Life { get; set; }
        public DateTimeOffset LifeLastHealTime { get; set; }
        public int MaxLife { get; set; }
        public int Stamina { get; set; }
        public DateTimeOffset StaminaLastHealTime { get; set; }
        public int MaxStamina { get; set; }
        public int Hp { get; set; }
        public int Sp { get; set; }
        public int Attack { get; set; }
        public int Defense { get; set; }
        public int CutPercentage { get; set; }
        public int CriticalPercentage { get; set; }
        public int StunValue { get; set; }
        public int StunResistance { get; set; }
        public int StunRecoveryStand { get; set; }
        public int StunRecoveryDown { get; set; }
        public int ScoreUp { get; set; }
        public int LastQuestAreaId { get; set; }
        public int LastLabyrinthAreaId { get; set; }
        public short AvatarHpBonus { get; set; }
        public short AvatarSpBonus { get; set; }
        public short AvatarAttackBonus { get; set; }
        public short AvatarDefenseBonus { get; set; }
        public short AvatarScoreUpBonus { get; set; }
        public short MaxGearBoxNum { get; set; }
        public short MaxFriendsNum { get; set; }
        public short MaxMonsterBoxNum { get; set; }
        public short MaxItemBoxNum { get; set; }
        public short MaxGearBagNum { get; set; }
        public short MaxUsableMonstersNum { get; set; }
        public short MaxItemBagNum { get; set; }
    }
}
