﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.InterfaceModel.User
{
    public class UserNotificationModel
    {
        public long NotificationId { get; set; }
        public string Message { get; set; }
        public short Category { get; set; }
        public long AddTime { get; set; }
    }
}