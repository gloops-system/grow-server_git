﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.InterfaceModel.User
{
    public class UserLabyrinthModel
    {
        public int Point { get; set; }
        public int LastAreaId { get; set; }
    }
}