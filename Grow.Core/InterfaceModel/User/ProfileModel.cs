﻿using Grow.Core.InterfaceModel.Avatar;
using Grow.Core.InterfaceModel.Gear;
using Grow.Core.InterfaceModel.Monster;
using Grow.Core.InterfaceModel.User;
using Fango.Core.Util;
using System.Collections.Generic;
using System;

namespace Grow.Core.InterfaceModel.User
{
    public class ProfileModel
    {
        public ProfileModel(UserViewModel userModel)
        {
            UserId = userModel.UserId;
            NickName = userModel.NickName;
            //FacebookId = userModel.FacebookId;
            Level = userModel.Level;
            //Exp = userModel.Exp;
            Gold = userModel.Gold;
            Plasma = userModel.Plasma;
            ColonyData = userModel.ColonyData;
            ColonyHistoryIndex = userModel.ColonyHistoryIndex;
            LastSyncTimePassed = userModel.LastSyncTimePassed;
            CurrentServerTime = DateUtil.GetDateTimeOffsetNow().UtcDateTime.ToUnixTime() * 1000;
            //Stamina = userModel.Stamina;
            //StaminaLastHealTime = userModel.StaminaLastHealTime == null ? 0 : DateUtil.ToUnixTime(userModel.StaminaLastHealTime);
            //MaxStamina = userModel.MaxStamina;
            //Life = userModel.Life;
            //LifeLastHealTime = userModel.LifeLastHealTime == null ? 0 : DateUtil.ToUnixTime(userModel.LifeLastHealTime);
            //MaxLife = userModel.MaxLife;
            //Hp = userModel.Hp;
            //Sp = userModel.Sp;
            //Attack = userModel.Attack;
            //Defense = userModel.Defense;
            //CutPercentage = userModel.CutPercentage;
            //CriticalPercentage = userModel.CriticalPercentage;
            //StunValue = userModel.StunValue;
            //StunResistance = userModel.StunResistance;
            //StunRecoveryStand = userModel.StunRecoveryStand;
            //StunRecoveryDown = userModel.StunRecoveryDown;
            //ScoreUp = userModel.ScoreUp;
            //AvatarHpBonus = userModel.AvatarHpBonus;
            //AvatarSpBonus = userModel.AvatarSpBonus;
            //AvatarAttackBonus = userModel.AvatarAttackBonus;
            //AvatarDefenseBonus = userModel.AvatarDefenseBonus;
            //AvatarScoreUpBonus = userModel.AvatarScoreUpBonus;
            //MaxGearBoxNum = userModel.MaxGearBoxNum;
            //MaxFriendsNum = userModel.MaxFriendsNum;
            //MaxMonsterBoxNum = userModel.MaxMonsterBoxNum;
            //MaxItemBoxNum = userModel.MaxItemBoxNum;
            //MaxGearBagNum = userModel.MaxGearBagNum;
            //MaxUsableMonstersNum = userModel.MaxUsableMonstersNum;
            //MaxItemBagNum = userModel.MaxItemBagNum;
        }
        public int UserId { get; set; }
        public string NickName { get; set; }
        //public string FacebookId { get; set; }
        public int Gem { get; set; }
        public int Gold { get; set; }
        public int Level { get; set; }
        public int Plasma { get; set; }
        public int LastSyncTimePassed { get; set; }
        public string ColonyData { get; set; }
        public long ColonyHistoryIndex { get; set; }
        public long CurrentServerTime { get; set; }
        //public int Exp { get; set; }
        //public int Stamina { get; set; }
        //public long StaminaLastHealTime { get; set; }
        //public int MaxStamina { get; set; }        
        //public int Life { get; set; }
        //public long LifeLastHealTime { get; set; }
        //public int MaxLife { get; set; }
        //public int Hp { get; set; }
        //public int Sp { get; set; }
        //public int Attack { get; set; }
        //public int Defense { get; set; }
        //public int CutPercentage { get; set; }
        //public int CriticalPercentage { get; set; }
        //public int StunValue { get; set; }
        //public int StunResistance { get; set; }
        //public int StunRecoveryStand { get; set; }
        //public int StunRecoveryDown { get; set; }
        //public int ScoreUp { get; set; }
        //public short AvatarHpBonus { get; set; }
        //public short AvatarSpBonus { get; set; }
        //public short AvatarAttackBonus { get; set; }
        //public short AvatarDefenseBonus { get; set; }
        //public short AvatarScoreUpBonus { get; set; }
        //public short MaxGearBoxNum { get; set; }
        //public short MaxFriendsNum { get; set; }
        //public short MaxMonsterBoxNum { get; set; }
        //public short MaxItemBoxNum { get; set; }
        //public short MaxGearBagNum { get; set; }
        //public short MaxUsableMonstersNum { get; set; }
        //public short MaxItemBagNum { get; set; }
        //public List<UserAvatarModel> Avatar { get; set; }
        //public List<UserMonsterModel> Monster { get; set; } 
        //public List<UserGearModel> Gear { get; set; }
        //public List<UserItemModel> Item { get; set; }
        //public UserLabyrinthModel Labyrinth { get; set; }
    }
}