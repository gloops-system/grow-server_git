﻿namespace Grow.Core.InterfaceModel.User
{
    public class UserModel
    {
        public string NickName { get; set; }
        public int Level { get; set; }
        public long CurrentUserExp { get; set; }
        public long CurrentLevelExp { get; set; }
        public long NextLevelExp { get; set; }
        public int CurrentDungeonPoint { get; set; }
        public int MaxDungeonPoint { get; set; }
        public long Gil { get; set; }
        public int CurrentCharacterNum { get; set; }
        public int MaxCharacterNum { get; set; }
        public int CurrentCostPoint { get; set; }
        public int MaxCostPoint { get; set; }
        public int CurrentItemNum { get; set; }
        public int MaxItemNum { get; set; }
        public int CurrentItemEquipNum { get; set; }
        public int MaxItemEquipNum { get; set; }
        public int CurrentFriendNum { get; set; }
        public int MaxFriendNum { get; set; }
        public int GameCurrency { get; set; }
    }
}