﻿namespace Grow.Core.InterfaceModel.Friend
{
    public class FriendModel
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }
        public string Comment { get; set; }
        public int Status { get; set; }
        public string LastLogin { get; set; }

        public int CharacterId { get; set; }
        public string MobageAvatar { get; set; }
    }
}