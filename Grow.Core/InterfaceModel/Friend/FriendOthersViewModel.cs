﻿using System.Collections.Generic;
using Grow.Core.InterfaceModel.Friend;

namespace Grow.Core.InterfaceModel.Friend
{
    public class FriendOthersViewModel
    {
        public ICollection<FriendModel> NonFriendList { get; set; }
        public ICollection<FriendModel> MobageFriendList { get; set; }
    }
}