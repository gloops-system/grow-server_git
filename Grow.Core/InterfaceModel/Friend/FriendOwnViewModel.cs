﻿using System.Collections.Generic;
using Grow.Core.InterfaceModel.Friend;

namespace Grow.Core.InterfaceModel.Friend
{
    public class FriendOwnViewModel
    {
        public int CurrentFriendNum { get; set; }
        public int MaxFriendNum { get; set; }
        public int NoAnswerNum { get; set; }
        public ICollection<FriendModel> FriendList { get; set; }
        public ICollection<FriendModel> FriendListNoAnswer { get; set; }
    }
}