﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.InterfaceModel.Login
{
    public class LoginViewModel
    {
        public string SessionId;
        public DateTimeOffset Expires;
        public DateTimeOffset PatchUpdateTime;
        public bool FlgToUpgrade;
    }
}
