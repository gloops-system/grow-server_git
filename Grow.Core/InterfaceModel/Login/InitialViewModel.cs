﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.InterfaceModel.Login
{
    public class InitialViewModel
    {
        public int UserId;
        public string Secret;
        public string GameId;
        public bool FlgToUpgrade;

        public List<TutorialCheckPointModel> CheckPointList;
    }

    public class TutorialCheckPointModel
    {
        public int Category;
        public bool IsClear;
    }
}
