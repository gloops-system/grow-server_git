﻿using Grow.Core.Application.Test;
using Grow.Core.InterfaceModel.Test;
using Fango.Web.Core.ApiModel;

namespace Grow.Core.Application.Test
{
    public class MonsterMasterTestApiModel : GrowApiModel
    {
        public MonsterMasterTestApiModel(GrowApplicationModel applicationContext) : base(applicationContext) { }

        public ContentResponse<MonsterMasterTestViewModel> GetMonsterMaster(int monsterId)
        {
            var contentResponse = new ContentResponse<MonsterMasterTestViewModel>();
            var now = ApplicationContext.Now;
            contentResponse.AppData = new MonsterMasterTest(ApplicationContext).GetMonsterMaster(monsterId);
             
            return contentResponse;
        }
    }
}