﻿using Grow.Core.Application;
using Grow.Core.Common;
using Grow.Core.InterfaceModel.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.Application.Test
{
    public class MonsterMasterTest : ModelBase
    {
        public MonsterMasterTest(GrowApplicationModel applicationContext) : base(applicationContext) { }

        public MonsterMasterTestViewModel GetMonsterMaster(int monsterId)
        {
            var monsterMaster = new MonsterMasterTestViewModel();
            var monster = ApplicationContext.MasterDataContainer.MonsterMaster.GetDataByMonsterId(monsterId);
            if (monster != null)
            {
                monsterMaster.MonsterId = monster.MonsterId;
                monsterMaster.DefaultHp = monster.DefaultHp;
            }
            return monsterMaster;
        }
    }
}
