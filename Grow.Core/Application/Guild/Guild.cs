﻿using System;
using Grow.Core.Common;
using System.Collections.Generic;
using Grow.Core.InterfaceModel.Guild;
using Fango.Core.Util;
using Grow.Data.Guild.DataSet;

namespace Grow.Core.Application.Guild
{
    public class GuildInfo : GrowApiModel
    {
        public GuildInfo(GrowApplicationModel applicationContext) : base(applicationContext) { }

        public GuildModel GetGuild(int GuildId)
        {
            var guild = ApplicationContext.GuildDataContainer.GuildInfo.GetDataByGuildId(GuildId);
            if (guild == null)
                return null;
            var guildmodel = new GuildModel()
            {
                GuildId = guild.Id,
                FounderID = guild.FounderId,
                Name = guild.Name,
                Symbol = guild.Symbol,
                Introduction = guild.Introduction,
                RecruitType = guild.RecruitType,
                RequiredTrophy = guild.RequiredTrophy,
                AddTime = guild.AddTime,
                UpdtTime = guild.UpdtTime   
            };
            return guildmodel;
     
        }

        public int CreateGuild(int FounderId, string Name, string Symbol, string Introduction, int RecruitType, int RequiredTrophy, DateTimeOffset now)
        {
            var createdGuildId = ApplicationContext.GuildDataContainer.GuildInfo.CreateGuild(FounderId, Name, Symbol, Introduction, RecruitType, RequiredTrophy, now);

            return createdGuildId;

        }

        public int UpdateGuild(string Introduction, int RecruitType, int RequiredTrophy, DateTimeOffset now, int GuildId)
        {
            var result = ApplicationContext.GuildDataContainer.GuildInfo.UpdateGuild(Introduction, RecruitType, RequiredTrophy, now, GuildId);

            return result;

        }

        public int DisbandGuild(int GuildId)
        {
            var result = ApplicationContext.GuildDataContainer.GuildInfo.DisbandGuild(GuildId);

            return result;

        }

        public int LeaveGuild(int UserId)
        {
            var result = ApplicationContext.GuildDataContainer.GuildInfo.LeaveGuild(UserId);

            return result;

        }

    }
}
