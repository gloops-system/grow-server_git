﻿using System;
using System.Globalization;
using Fango.Core.Caching;
using Fango.Core.Common;
using Fango.Core.Logging;
using Fango.Core.Sequence;
using Fango.Core.Util;
using Grow.Core.Application.Common;
using Fango.Web.Core.ApiModel;
using Fango.Web.Core.ApiModel.Login;
using Fango.Web.Core.Repositories;
using Fango.Web.Core.ViewModels.Login;
using Grow.Core.Application.Configuration;

namespace Grow.Core.Application.Login
{
    public class InitialApiModel : GrowApiModel, IInitialApiModel
    {
        public IUserLoginDataRepository UserLoginDataRepository { get; set; }
        public ISequenceClient SequenceClient { get; set; }

        public InitialApiModel(GrowApplicationModel applicationContext) : base(applicationContext)
        {
        }

        ContentResponse<InitialViewModel> IInitialApiModel.GetInitializeInfo(string time, string version, string uuid, FangoCode.DeviceType deviceType)
        {
            var contentResponse = new ContentResponse<InitialViewModel>();

            // クライアントバージョンチェック
            if (CheckNeedUpgrade(version))
            {
                LoggerManager.DefaultLogger.Debug("need client upgrade.");
                contentResponse.Code = (int) AppCode.ApiResultCode.クライアント更新;
                contentResponse.Message = @"クライントの更新が必要です。"; // TODO : ErrorMessageから取得？

                return contentResponse;
            }

            // 復号化、時間チェック
            if (!CheckTime(time))
            {
                LoggerManager.DefaultLogger.Debug("deserialize parameter of time is failed.");
                contentResponse.Code = (int)AppCode.ApiResultCode.パラメータ不正;
                contentResponse.Message = @"パラメータが不正です。"; // TODO : ErrorMessageから取得？

                return contentResponse;
            }

            // UUIDチェック
            var guid = Guid.NewGuid();
            if (!Guid.TryParse(uuid, out guid))
            {
                LoggerManager.DefaultLogger.Debug("incorrect uuid.");
                contentResponse.Code = (int)AppCode.ApiResultCode.パラメータ不正;
                contentResponse.Message = @"パラメータが不正です。"; // TODO : ErrorMessageから取得？

                return contentResponse;
            }

            // DeviceTypeのチェック
            if (!Enum.IsDefined(typeof(AppCode.DeviceType), deviceType))
            {
                LoggerManager.DefaultLogger.Debug("DeviceTypeが不正");
                contentResponse.Code = (int)AppCode.ApiResultCode.パラメータ不正;
                contentResponse.Message = @"パラメータが不正です。"; // TODO : ErrorMessageから取得？

                return contentResponse;
            }

            // ユーザ用シークレット生成
            var secret = WebProtection.CreateSecret();

            // ゲームID生成
            var inGameId = CreateInGameId();

            // UserId 採番
            var serquenceClient = SequenceClient ?? SequenceClientManager.DefaultSequenceClient;
            var userId = (int) serquenceClient.GetNextValue(CacheKey.UserId(), 1);

            //ユーザ登録
            RegistUser(userId, uuid, secret, inGameId);

            var initial = new InitialViewModel()
                {
                    UserId = userId,
                    Secret = secret,
                    InGameId = inGameId,
                };

            contentResponse.AppData = initial;

            return contentResponse;
        }

        /// <summary>
        /// アップグレードの必要があるかチェックする。必要がある場合はtrueが返る
        /// </summary>
        public static bool CheckNeedUpgrade(string version)
        {
            var availableVersionString = MobileSettings.AvailableClientVersion;
            var availableVersion = Version.Parse(availableVersionString);
            var inputVersion = Version.Parse(version);
            return availableVersion.CompareTo(inputVersion) > 0;
        }

        /// <summary>
        /// 暗号化された時刻を確認
        /// </summary>
        private bool CheckTime(string time)
        {
            try
            {
                // 想定としては2013-05-24T18:16:48+09:00が送られるべきところ,西暦→和暦に設定を変えている人からは
                // 0025-05-24T18:16:48+09:00
                // が送られてくるので認証に失敗します。クライアント側で対応予定。現在はサーバ側で暫定対応。「午前」「午後」も同様
                var currentYear = DateUtil.GetDateTimeOffsetNow().Year.ToString(CultureInfo.InvariantCulture);
                var timeString = WebProtection.DecryptByCommonSecret(time).Substring(4 /*2013または0025を削る*/);

                timeString = currentYear + timeString; //2013を付け足す

                DateTimeOffset inputTime;
                try
                {
                    if (!DateTimeOffset.TryParse(timeString, out inputTime))
                    {

                        if (timeString.Contains("午後"))
                        {
                            var offsetPartbase = timeString.Substring(timeString.Length - 6, 6);
                            var offsetPart = offsetPartbase.Substring(0, 1) + offsetPartbase.Substring(2, 2) + ":" +
                                             offsetPartbase.Substring(4, 2);
                            var datetimePart = timeString.Substring(0, timeString.Length - 6).Replace("午後", "");

                            inputTime = timeString.Contains("午後12")
                                            ? DateTimeOffset.Parse(datetimePart + offsetPart)
                                            : DateTimeOffset.Parse(datetimePart + offsetPart).AddHours(12);
                        }
                        else if (timeString.Contains("午前"))
                        {
                            if (timeString.Contains("午前12"))
                            {
                                // 例:2013-05-28T午前12:00:58+:0900
                                var offsetPartbase = timeString.Substring(timeString.Length - 6, 6);
                                var offsetPart = offsetPartbase.Substring(0, 1) + offsetPartbase.Substring(2, 2) + ":" +
                                                 offsetPartbase.Substring(4, 2);
                                var datetimePart = timeString.Substring(0, timeString.Length - 6).Replace("午前12", "00");
                                inputTime = DateTimeOffset.Parse(datetimePart + offsetPart);
                            }
                            else
                            {
                                // 例:2013-05-28T午前07:25:49+:0900
                                var offsetPartbase = timeString.Substring(timeString.Length - 6, 6);
                                var offsetPart = offsetPartbase.Substring(0, 1) + offsetPartbase.Substring(2, 2) + ":" +
                                                 offsetPartbase.Substring(4, 2);
                                var datetimePart = timeString.Substring(0, timeString.Length - 6).Replace("午前", "");
                                inputTime = DateTimeOffset.Parse(datetimePart + offsetPart);
                            }

                        }
                    }
                }
                catch (FormatException formatException)
                {
                    LoggerManager.DefaultLogger.Error(string.Format("timeString:{0}", timeString), formatException);
                    throw;
                }

                var timeResult = WebTimeUtil.CheckInputTimeByLoginExpires(inputTime);
                return timeResult;
            }
            catch (Exception e)
            {
                LoggerManager.DefaultLogger.ErrorFormat("時刻の復号化に失敗. Exception:" + e);
                return false;
            }
        }

        /// <summary>
        /// ゲーム内ID生成
        /// </summary>
        private int CreateInGameId()
        {
            var inGameId = -1;

            // 10回トライ
            for (var i = 0; i < 10; i++)
            {
                var rnd = RandomUtil.UniqueRandom;
                var tryInGameId = rnd.Next(1, 999999999);
                var num = UserLoginDataRepository.CheckInGameId(tryInGameId);

                // 生成したInGameIdが被っていなければ終了
                if (num == 0)
                {
                    inGameId = tryInGameId;
                    break;
                }
            }

            if (inGameId == -1)
            {
                throw new Exception("inGameIdを生成できませんでした");
            }

            return inGameId;
        }

        /// <summary>
        /// ユーザ登録
        /// </summary>
        private void RegistUser(int userId, string UUID, string secret, int inGameId)
        {
            //var repositoryAdapter = ApplicationContext.LoginDataContainer.UserLoginInfoDataRepository;
            //repositoryAdapter.Add(userId, UUID, secret, inGameId);
        }
    }
}