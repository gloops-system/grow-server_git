﻿using System;
using System.IO;
using System.Runtime.Serialization.Json;
using Grow.Core.InterfaceModel.Login;
using Grow.Core.Application.Common;
using Fango.Core.Util;
using Fango.Web.Core.ApiModel;
using Fango.Web.Core.Models;

namespace Grow.Core.Application.Login
{
    public class LoginApiModel : GrowApiModel
    {
        public LoginApiModel(GrowApplicationModel applicationContext) : base(applicationContext)
        {

        }

        public ContentResponse<LoginViewModel> Get(int UserId, string UUID, string Version)
        {
            var userId = UserId;
            var resultContent = new ContentResponse<LoginViewModel>();
            string decryptedUUID;
            //バリデーション
            var needUpgrade = InitialApiModel.CheckNeedUpgrade(Version);
            var resultValidPair = IsValidPair(userId, UUID, out decryptedUUID);

            resultContent.AppData = new LoginViewModel
                {
                    FlgToUpgrade = needUpgrade
                };

            //エラーコード、エラーメッセージを設定
            resultContent.Code = resultValidPair;
            resultContent.Message = CreateMessage(resultValidPair);

            if (needUpgrade || resultValidPair!=C_VALID_PAIR)
            {
                return resultContent;
            }

            //トークン生成
            var expires = DateTime.Now.AddMinutes(AppCode.ログイン有効期間);
            var encryptedJson = CreateEncryptedToken(userId, Version, expires, decryptedUUID);

            //パッチのアップデート時刻
            //var generalContainer = ApplicationContext.GeneralDataContainer;
            //var lastUpdateTime = LastUpdateTime(generalContainer);

            //ViewModel生成
            //var login = new LoginViewModel()
            //{
            //    SessionId = encryptedJson,
            //    Expires = expires,
            //    PatchUpdateTime = lastUpdateTime,
            //    FlgToUpgrade = needUpgrade
            //};

            //resultContent.AppData = login;

            return resultContent;
        }

        /// <summary>
        /// Tipsとリーグ背景画像の最新の更新時間を比較して大きいDateTimeOffsetを返す
        /// </summary>
        //private static DateTimeOffset LastUpdateTime(GeneralDataContainer generalContainer)
        //{
        //    var tipMaster = generalContainer.Tips;
        //    var topImagesMaster = generalContainer.TopImages;
        //    var tutorialImagesMaster = generalContainer.TutorialImages;

        //    //Tipsの更新時間
        //    var tips = tipMaster.GetTips();
        //    var lastUpdateTimeOfTips = !tips.Any() ? DateTimeOffset.MinValue : tips.Max(x => x.UpdtTime);

        //    //トップ画像の更新時間
        //    var images = topImagesMaster.GetTopImages();
        //    var lastUpdateTimeOfTopImages = !images.Any() ? DateTimeOffset.MinValue : images.Max(x => x.UpdtTime);

        //    //チュートリアル画像の更新時間
        //    var tutorials = tutorialImagesMaster.GetTutorialImages();
        //    var lastUpdateTimeOfTutorialImages = !tutorials.Any()
        //                                             ? DateTimeOffset.MinValue
        //                                             : tutorials.Max(x => x.UpdtTime);

        //    var updateTimes = new List<DateTimeOffset>
        //        {
        //            lastUpdateTimeOfTips,
        //            lastUpdateTimeOfTopImages,
        //            lastUpdateTimeOfTutorialImages
        //        };

        //    //最も最新の時間を返す
        //    return updateTimes.Max();
        //}

        /// <summary>
        /// エラーメッセージを作成
        /// </summary>
        private static string CreateMessage(int resultValidPair)
        {
            if (resultValidPair == C_VALID_PAIR)
            { 
                return ""; 
            }

            switch (resultValidPair)
            {
                case C_DATA_TRANSFERED:
                    return ErrorMessage.InvalidUUID;
                case C_DECRYPT_ERROR:
                    return ErrorMessage.DecryptError;
                case C_EXPIRED:
                    return ErrorMessage.Expired;
                case C_NOT_ENOUGH_ELEMENT:
                    return ErrorMessage.NotEnough;
                default:
                    return "";
            }
        }

        /// <summary>
        /// 暗号化トークンを生成
        /// </summary>
        public static string CreateEncryptedToken(int userId, string clientVersion, DateTime expires, string uuid)
        {
            var token = new TokenModel()
                {
                    //UserId = userId,
                    UUID = uuid,
                    ClientVersion = clientVersion,
                    //Expires = expires
                };

            var tokenJson = new DataContractJsonSerializer(typeof(TokenModel));
            var stream = new MemoryStream();
            tokenJson.WriteObject(stream, token);
            stream.Position = 0;
            var reader = new StreamReader(stream);
            var tokenString = reader.ReadToEnd();

            var encryptedJson = WebProtection.EncryptByTokenSecret(tokenString);
            return encryptedJson;
        }

        public const int C_VALID_PAIR = 0;
        public const int C_DATA_TRANSFERED = -1;
        public const int C_DECRYPT_ERROR = -2;
        public const int C_NOT_ENOUGH_ELEMENT = -3;
        public const int C_EXPIRED = -4;
        public const string TESTING_UUID = "TestingUUID";

        /// <summary>
        /// UserIdとUUIDのペアが正しいか確認
        /// </summary>
        private int IsValidPair(int userId, string encryptedUUID, out string decryptedUUID)
        {
            decryptedUUID = null;
            //var adapter = ApplicationContext.LoginDataContainer.UserLoginInfoDataRepository;
            //var user = adapter.GetDataByUserId(userId);
            //string decryptedUUIDAndTime = "";

            ////ユーザ鍵で復号化
            //try
            //{
            //    decryptedUUIDAndTime = Protection.OpenSSLDecrypt(encryptedUUID, user.Secret);
            //}
            //catch
            //{
            //    //復号化失敗
            //    return C_DECRYPT_ERROR;
            //}
            ////"UUID,Datetime"分割
            //string[] stringArray = decryptedUUIDAndTime.Split(',');

            ////スプリットして要素が２つ以上あるか
            //if (stringArray.Count() < 2)
            //{
            //    return C_NOT_ENOUGH_ELEMENT;
            //}

            //string strdate = stringArray[1];
            //DateTimeOffset inputTime;

            //string currentYear = DiveDateUtil.DatabaseDate.Year.ToString(CultureInfo.InvariantCulture);
            ////string timeString = WebProtection.DecryptByCommonSecret(strdate);

            //if (strdate.StartsWith("0"))
            //{
            //    //想定としては2013-05-24T18:16:48+09:00が送られるべきところ,西暦→和暦に設定を変えている人からは
            //    //0025-05-24T18:16:48+09:00
            //    //が送られてくるので認証に失敗します。クライアント側で対応予定。現在はサーバ側で暫定対応。「午前」「午後」も同様
            //    strdate = currentYear + strdate.Substring(4 /*2013または0025を削る*/); //2013を付け足す
            //}

            //try
            //{
            //    if (!DateTimeOffset.TryParse(strdate, out inputTime))
            //    {

            //        if (strdate.Contains("午後"))
            //        {
            //            var offsetPartbase = strdate.Substring(strdate.Length - 6, 6);
            //            var offsetPart = offsetPartbase.Substring(0, 1) + offsetPartbase.Substring(2, 2) + ":" + offsetPartbase.Substring(4, 2);
            //            var datetimePart = strdate.Substring(0, strdate.Length - 6).Replace("午後", "");

            //            if (strdate.Contains("午後12"))
            //            {
            //                inputTime = DateTimeOffset.Parse(datetimePart + offsetPart);
            //            }
            //            else
            //            {
            //                inputTime = DateTimeOffset.Parse(datetimePart + offsetPart).AddHours(12);
            //            }
            //        }
            //        else if (strdate.Contains("午前"))
            //        {
            //            if (strdate.Contains("午前12"))
            //            {
            //                var offsetPartbase = strdate.Substring(strdate.Length - 6, 6);
            //                var offsetPart = offsetPartbase.Substring(0, 1) + offsetPartbase.Substring(2, 2) + ":" + offsetPartbase.Substring(4, 2);
            //                var datetimePart = strdate.Substring(0, strdate.Length - 6).Replace("午前12", "00");
            //                inputTime = DateTimeOffset.Parse(datetimePart + offsetPart);
            //            }
            //            else
            //            {
            //                var offsetPartbase = strdate.Substring(strdate.Length - 6, 6);
            //                var offsetPart = offsetPartbase.Substring(0, 1) + offsetPartbase.Substring(2, 2) + ":" + offsetPartbase.Substring(4, 2);
            //                var datetimePart = strdate.Substring(0, strdate.Length - 6).Replace("午前", "");
            //                inputTime = DateTimeOffset.Parse(datetimePart + offsetPart);
            //            }

            //        }
            //    }
            //}
            //catch (FormatException formatException)
            //{
            //    LoggerManager.DefaultLogger.Error(string.Format("decryptedUUIDAndTime:{0}", decryptedUUIDAndTime), formatException);
            //    throw;
            //}

            //bool timeResult = WebTimeUtil.CheckInputTimeByLoginExpires(inputTime);

            //if (!timeResult)
            //{
            //    return C_EXPIRED;
            //}

            ////UUIDのチェック
            //decryptedUUID = stringArray[0];
            //bool result = user.UUID == decryptedUUID;
            return 0;// result ? C_VALID_PAIR: C_DATA_TRANSFERED;
        }
    }
}