﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fango.OpenSocial.OAuth;
using Fango.OpenSocial.OpenSocial;
using Fango.OpenSocial.OpenSocial.Common;
using Fango.OpenSocial.OpenSocial.Data.MbgaApp;
using Fango.Redis.Caching;
using Grow.Core.Application;
using Grow.Core.Application.Caching;
using Grow.Core.Application.User;
using Grow.Core.Application.Util;
using Grow.Data.User.DataSet.Container;
using OAuthRequestModel = Fango.OpenSocial.OpenSocial.OAuthRequestModel;

namespace Grow.Core.Application.Platform
{
    public class PlatformManager
    {
        public GrowApplicationModel ApplicationContext { get; set; }
        public static readonly Dictionary<string, OpenSocialRequestParameter> OpenSocialRequestParameters =
            new Dictionary<string, OpenSocialRequestParameter>() { { "fields", new OpenSocialRequestParameter("id", "nickname", "birthday", "profileUrl", "thumbnailUrl", "gender", "bloodType", "hasApp", "isVerified", "addresses", "grade") } };
        public static int UpdatePlatformUserinfo(People people, PlatformDataContainer platformUserDataContainer)
        {
            var userId =  OpenSocialRestClientBase.GetId(people.Id);
            var platformUserInfo = platformUserDataContainer.PlatformUserInfo;
            var platformUserinfoRow = platformUserInfo.GetDataByPK(userId);
            var grade = (int?)people.Grade;
            
            // 新規登録
            if (platformUserinfoRow == null)
            {
                //platformUserInfo.AddNew(userId, OpenSocialRestClientMbgaApp.NormalizeString(people.DisplayName),
                //                            OpenSocialRestClientMbgaApp.PlatformProfileUrl(people),
                //                            people.ThumbnailUrl, people.Gender,
                //                            people.Birthday.HasValue ? people.Birthday.Value : new DateTime(9999, 12, 31),
                //                            people.BloodType,
                //                            people.IsVerified.HasValue && people.IsVerified.Value,
                //                            people.Addresses == null || people.Addresses.FirstOrDefault() == null ? null : people.Addresses.First().Formatted,
                //                            grade, DiveDateUtil.DatabaseDate);
            }
            // 更新
            else
            {
                ////招待報酬判定
                //if (people.IsVerified == true && (platformUserinfoRow.IsisVerifiedNull() || !platformUserinfoRow.isVerified))
                //{
                //    RemoveIsVerifiedCache(uid);
                //    InviteManager.SendInviteReward(uid);
                //}

                var isVerifiedDate = platformUserinfoRow.IsIsVerifiedDateNull() ? (DateTimeOffset?)null : platformUserinfoRow.IsVerifiedDate;

                var nickName = OpenSocialRestClientMbgaApp.NormalizeString(people.DisplayName);
                //platformUserInfo.UpdateByPK(userId,
                //                            nickName,
                //                            OpenSocialRestClientMbgaApp.PlatformProfileUrl(people),
                //                            people.ThumbnailUrl,
                //                            people.Gender,
                //                            people.Birthday.HasValue
                //                                ? people.Birthday.Value
                //                                : new DateTime(9999, 12, 31),
                //                            people.BloodType,
                //                            people.IsVerified.HasValue && people.IsVerified.Value,
                //                            isVerifiedDate,
                //                            people.Addresses == null || people.Addresses.FirstOrDefault() == null
                //                                ? null
                //                                : people.Addresses.First().Formatted,
                //                            grade,
                //                            DiveDateUtil.DatabaseDate
                //    );

                // grade更新時にhistory登録
                //if (!platformUserinfoRow.IsgradeNull() && grade.HasValue && platformUserinfoRow.grade != grade)
                //    new platform_userinfo_grade_up_historyTableAdapter().AddNew(uid, platformUserinfoRow.grade, grade.Value);
            }
            return userId;
        }

        public static int test(string sessionId)
        {
            var cacheClient = new RedisCacheClient();
            var session = cacheClient.Get<OAuthSession>(CacheKey.OAuthSession(sessionId));
            var OAuthAuthorization = session.ToDictionary();
            var osclient = new OpenSocialRestClientMbgaApp(OAuthAuthorization);
            osclient.ApplicationId = "1";// TODO : ApplicationId
            osclient.RequestModel = OAuthRequestModel.Trusted;
            var people = osclient.FetchPeople("39214", OpenSocialRequestParameters);
            //TODO:後々移行
            //PlatformUserInfoの作成
            var platformUserDataContainer = new PlatformDataContainer();
            var userId = UpdatePlatformUserinfo(people, platformUserDataContainer);
            var applicationContext = new GrowApplicationModel(userId);
            var user = applicationContext.CurrentUser;
            user.CreateInitialUserData(applicationContext, people.DisplayName);
            return userId;
        }
    }
}
