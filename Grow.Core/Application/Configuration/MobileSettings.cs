﻿using Fango.Core.Configuration;

namespace Grow.Core.Application.Configuration
{
    public class MobileSettings : CoreSettings
    {
        public static string AvailableClientVersion { get { return settings("available.client.version"); } }
        //public static string AuthorizationKey { get { return settings("authorization.key", "Bearer"); } }

        public static string ApiServerHost { get { return settings("image.server.host"); } }
        public static string ImageServerHost { get { return settings("image.server.host"); } }

        public static bool MockEnabled { get { return settingsAsBool("mock.enabled"); } }
        public static int MaxFriendCheerNum { get { return settingsAsInt("max.friend.num", 20); } }

        public static string CertificatePath { get { return settings("certificate.path"); } }
        public static string CertificatePassword { get { return settings("certificate.password", "0amuzani"); } }

        public static int TutorialNpcUserId { get { return settingsAsInt("tutorial.npc.userId"); } }

        //public static string MailFrom { get { return settings("mail.from", ""); } }
        //public static string MailTo { get { return settings("mail.to", ""); } }
        //public static string MailSubject { get { return settings("mail.subject", ""); } }
        //public static string MailSmtpHost { get { return settings("mail.smtp.host", ""); } }
        //public static string MailSmtpUser { get { return settings("mail.smtp.user", ""); } }
        //public static string MailSmtpPass { get { return settings("mail.smtp.pass", ""); } }
        //public static int MailSmtpTimeout { get { return settingsAsInt("mail.smtp.timeout", 10000); } }

        public static string GameCurrecyName { get { return settings("game.currency.name"); } }

        //DataBaseDate
        public static int DatabasedatetimeCache { get { return settingsAsInt("databasedatetime.cache", 60); } }
    }
}