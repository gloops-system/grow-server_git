﻿using System.Configuration;

namespace Grow.Core.Application.Configuration
{
    public class EdgeConfigurationManager
    {
        /// <summary>
        /// WebViewで使う
        /// </summary>
        /// <returns></returns>
        public static string GetImageRoot()
        {
            return ConfigurationManager.AppSettings["image.root"];
        }
    }
}
