﻿namespace Grow.Core.Application.Caching
{
    public class CacheKey
    {
        public static string TimeSpanFromDatabaseDateTimeIndividual(string hostName, string conStr)
        {
            return string.Format("TimeSpanFromDatabaseDateTime-{0}-{1}", hostName, conStr);
        }

        #region Session
        public static string UUID(int userId)
        {
            return string.Format("UUID-{0}", userId);
        }
        public static string OAuthSession(string sessionId)
        {
            return string.Format("OAuthSession-{0}", sessionId);
        }
        #endregion

        #region Sequence
        // UserId
        public static string UserId()
        {
            return @"UserId";
        }

        // Gacha
        public static string GachaUserHistory()
        {
            return @"GachaUserHistory";
        }
        public static string GachaUserBoxHistory()
        {
            return @"GachaUserBoxHistory";
        }
        public static string GachaUserMixHistory()
        {
            return @"GachaUserMixHistory";
        }

        // GameCurrency
        public static string GameCurrencyBuyHistory()
        {
            return "GameCurrencyBuyHistory";
        }

        // User
        public static string UserCharacterId()
        {
            return "UserCharacterId";
        }
        public static string UserGiftId()
        {
            return "UserGiftId";
        }

        // Mix
        public static string MixEvolutionHistory()
        {
            return "MixEvolutionHistory";
        }
        public static string MixEvolutionDetailHistory()
        {
            return "MixEvolutionDetailHistory";
        }
        public static string MixSkillHistory()
        {
            return "MixSkillHistory";
        }
        public static string MixSkillDetailHistory()
        {
            return "MixSkillDetailHistory";
        }

        // Shop
        public static string ShopBuyHistory()
        {
            return "ShopBuyHistory";
        }
        public static string ShopBuySaleHistory()
        {
            return "ShopBuySaleHistory";
        }
        public static string ShopBuySetHistory()
        {
            return "ShopBuySetHistory";
        }

        // Sell
        public static string SellCharacterHistory()
        {
            return "SellCharacterHistory";
        }

        // LoginBonus
        public static string UserLoginBonusHistory()
        {
            return "UserLoginBOnusHistory";
        }

        // QueueNotificationId
        public static string QueueNotificationId()
        {
            return "QueueNotificationId";
        }
        // QueueFacebookId
        public static string QueueFacebookId()
        {
            return "QueueFacebookId";
        }
        // QueueTwitterId
        public static string QueueTwitterId()
        {
            return "QueueTwitterId";
        }
        #endregion Sequence

        #region Duty

        /// <summary>
        /// DP計算用
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static string DutyPoint(int userId)
        {
            return string.Format("DutyPoints-{0}", userId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static string DungeonUserPartyModel(int userId)
        {
            return string.Format("DungeonUserPartyModel-{0}", userId);
        }



        #endregion

        #region Battle

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static string TargetBattleCharacterModel(int userId)
        {
            return string.Format("TargetBattleCharacterModel-{0}", userId);
        }

        #endregion

        #region Mix

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static string MixSkillDetailHistoryDataTable(int userId)
        {
            return string.Format("MixSkillDetailHistoryDataTable-{0}", userId);
        }

        public static string MixSkillHistoryModel(int userId)
        {
            return string.Format("MixSkillHistoryModel-{0}", userId);
        }

        #region Payment
        public static string AppPaymentHistoryId()
        {
            return "AppPaymentHistoryId";
        }
        #endregion


        #endregion
    }
}