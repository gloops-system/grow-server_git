﻿using System;
using Grow.Core.Application.Configuration;

namespace Grow.Core.Application.Caching
{
    public class CacheLimit
    {
        /// <summary> DataBaseDate </summary>
        public static readonly TimeSpan TIMESPAN_FROM_DATABASE_DATETIME = TimeSpan.FromSeconds(MobileSettings.DatabasedatetimeCache);

        /// <summary>
        /// UserBase用
        /// </summary>
        public static readonly TimeSpan UserBase = TimeSpan.FromMinutes(1);

        /// <summary>
        /// UserLastLogin用
        /// </summary>
        public static readonly TimeSpan UserLastLogin = TimeSpan.FromMinutes(3);

        /// <summary>
        /// メンテナンスモードをキャッシュする時間
        /// </summary>
        public static readonly TimeSpan MaintenanceMode = TimeSpan.FromSeconds(30);
    }
}