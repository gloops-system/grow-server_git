﻿using Fango.Web.Core.ApiModel;
using Fango.Web.Core.ViewModels.Login;

namespace Grow.Core.Application.Transfer
{

    public class DataTransferApiModel: GrowApiModel
    {
        public DataTransferApiModel(GrowApplicationModel applicationContext)
            : base(applicationContext)
        {

        }

        public ContentResponse Export(long gameId, string encryptedPassword)
        {
            var resultContent = new ContentResponse();
            //int userId = ApplicationContext.CurrentUserId;

            //var tableAdapter = ApplicationContext.LoginDataContainer.UserLoginInfoDataRepository;
            //var row = tableAdapter.GetDataByUserId(userId);
            //if (row == null)
            //{
            //    return CreateExportErrorResponse(-1, ErrorMessage.BadRequest);
            //}

            ////passwordの復号化
            //#region パスワードチェック
            //var secret = row.Secret;
            //string password;
            //try
            //{
            //    password = Protection.OpenSSLDecrypt(encryptedPassword, secret);
            //}
            //catch
            //{
            //    return CreateExportErrorResponse(-3, ErrorMessage.DecryptError);
            //}

            ////文字数チェック 半角英数字で、6文字以上12文字以下
            //bool resultPasswordValidation =
            //    ((Regex.Match(password, "^[a-zA-Z0-9]+$")).Success)
            //        && ((6 <= password.Length) && (password.Length <= 12));
            //if (!resultPasswordValidation)
            //{
            //    return CreateExportErrorResponse(-4, ErrorMessage.PasswordValidationError);
            //}
            //#endregion


            ////gameIdのチェック　-> gameId必要？
            //bool resultGameId = row.GameId == gameId;
            //if (!resultGameId)
            //{
            //    return CreateExportErrorResponse(-2, ErrorMessage.InvalidGameId);
            //}

            ////パスワードの設定。SHA-1化
            //byte[] sha1Password = Protection.SHA1Crypt(password);

            ////設定
            //const int status = (int)AppCode.LoginInfoStatus.移行設定中;
            //tableAdapter.UpdatePassword(sha1Password, status, userId);


            return resultContent;
        }

        private ContentResponse<object> CreateExportErrorResponse(int code, string message)
        {
            var response = new ContentResponse<object> { AppData = false, Code = code, Message = message};
            return response;
        }


        public ContentResponse<InitialViewModel> Import(int gameId, string uuid, string encryptedPassword, bool isConfirm)
        {
            var resultContent = new ContentResponse<InitialViewModel>();
            //int userId = ApplicationContext.CurrentUserId;
            //resultContent.AppData = new InitialViewModel();

            //gameIdからデータを検索
            //var tableAdapter = ApplicationContext.LoginDataContainer.UserLoginInfoDataRepository;
            //var userRow = tableAdapter.GetDataByUserId(userId);
            //var targetRow = tableAdapter.GetDataByGameId(gameId);
            //if (userRow == null)
            //{
            //    return CreateImportErrorResponse(-1, ErrorMessage.BadRequest);
            //}
            //if (targetRow == null)
            //{
            //    return CreateImportErrorResponse(-3, ErrorMessage.InvalidPassword);
            //}

            //#region Statusチェック

            ////移行元のステータスチェック
            //int userStatus = userRow.Status;
            //if (userStatus == (int)AppCode.LoginInfoStatus.移行設定中)
            //{
            //    return CreateImportErrorResponse(-7, ErrorMessage.TransferingUser);
            //}

            ////移行先のステータスチェック
            //int targetStatus = targetRow.Status;
            //switch ((AppCode.LoginInfoStatus)targetStatus)
            //{
            //    case AppCode.LoginInfoStatus.Ban:
            //    case AppCode.LoginInfoStatus.正常:
            //        return CreateImportErrorResponse(-6, ErrorMessage.InvalidUser);
            //    case AppCode.LoginInfoStatus.移行済なので使えない:
            //        return CreateImportErrorResponse(-8, ErrorMessage.InvalidUUID2);
            //    case AppCode.LoginInfoStatus.移行設定中:
            //        break;
            //    default:
            //        throw new ArgumentOutOfRangeException();
            //}

            //#endregion

            //#region passwordチェック

            //string password;
            //try
            //{
            //    password = Protection.OpenSSLDecrypt(encryptedPassword, userRow.Secret);
            //}
            //catch
            //{
            //    return CreateImportErrorResponse(-4, ErrorMessage.DecryptError);
            //}
            //var sha1Password = Protection.SHA1Crypt(password);

            //bool resultPassword = targetRow.Password.SequenceEqual(sha1Password);
            //if (!resultPassword)
            //{
            //    return CreateImportErrorResponse(-3, ErrorMessage.InvalidPassword);
            //}

            //#endregion

            //#region データ移行
            //var newSecret = targetRow.Secret;
            //var checkPointList = new List<TutorialCheckPointModel>();
            //if (!isConfirm)
            //{
            //    var targetUserId = targetRow.UserId;
            //    //UserSecretは変更しないようにした
            //    //var newSecret = WebProtection.CreateSecret();
            //    //移行先uuid,secret,statusの変更
            //    const int status = (int) AppCode.LoginInfoStatus.正常;
            //    int numOfUpdate = tableAdapter.UpdateUuidAndSecretAndStatusByUserId(uuid, status, newSecret,
            //                                                                        targetUserId);
            //    //移行元データを無効化.Statusを移行済1(LoginInfoStatus)に設定
            //    tableAdapter.VoidUser(userId);
            //    if (numOfUpdate != 1)
            //    {
            //        return CreateImportErrorResponse(-5, ErrorMessage.FailedToTransfer);
            //    }

            //    //無効なUUIDとして過去のUUIDを指定する
            //    string pastUUID = targetRow.UUID;
            //    InvalidUUIDManager.SetInvalidUUID(pastUUID);

            //    //チュート突破状況を取得
            //    var checkPointMaster = Enum.GetValues(typeof (AppCode.TutorialCategory));
            //    var tutorialCheckPointList =
            //        ApplicationContext.TutorialDataContainer.UserTutorialCheckPoint.FindItem(targetRow.UserId);

            //    checkPointList.AddRange(from object checkPoint in checkPointMaster
            //                            let check = tutorialCheckPointList.FirstOrDefault(x => x.Category == (int) checkPoint)
            //                            select new TutorialCheckPointModel
            //                                {
            //                                    Category = (int) checkPoint, IsClear = check != null,
            //                                });
            //}
            //resultContent.AppData = new InitialViewModel
            //    {
            //        UserId = targetRow.UserId,
            //        GameId = targetRow.GameId.ToString(CultureInfo.InvariantCulture),
            //        Secret = targetRow.Secret,
            //        FlgToUpgrade = false,
            //        CheckPointList = checkPointList,
            //    };

            //#endregion

            return resultContent;
        }

        private ContentResponse<InitialViewModel> CreateImportErrorResponse(int code, string message)
        {
            var response = new ContentResponse<InitialViewModel> {Code = code, Message = message};
            return response;
        }

    }
}
