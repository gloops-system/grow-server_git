﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;

namespace Grow.Core.Application.Operation
{
    public class CacheManagementApiModel : GrowApiModel
    {
        public CacheManagementApiModel(GrowApplicationModel applicationContext) : base(applicationContext)
        {
        }

        public IEnumerable<Type> GetAllLocalCache()
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            var refreshableCacheTypes =
                assemblies
                .SelectMany(assembly => assembly.GetTypes())
                .Where(type => type.GetInterface(typeof(IRefreshableCache).FullName) != null)
                ;

            return refreshableCacheTypes;
        }

        public IEnumerable<RefreshLocalCacheResult> RefreshLocalCache(string[] cacheNames)
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            var refreshableCacheTypes =
                assemblies
                    .SelectMany(assembly => assembly.GetTypes())
                    .Where(type => !type.IsAbstract && type.IsClass)
                    .Where(type => type.GetInterface(typeof (IRefreshableCache).FullName) != null);

            IEnumerable<Type> targetRefreshableCacheTypes;

            // 対象の指定がなければ全てのLocalCacheをRefreshする
            if (cacheNames == null || cacheNames.Length == 0)
            {
                targetRefreshableCacheTypes = refreshableCacheTypes;
            }
            else
            {
                targetRefreshableCacheTypes =
                    from refreshableCacheType in refreshableCacheTypes
                    join cacheName in cacheNames
                    on refreshableCacheType.AssemblyQualifiedName equals cacheName
                    select refreshableCacheType
                    ;
            }

            var refreshedCacheTypes = new List<RefreshLocalCacheResult>();

            foreach (var refreshableCacheType in targetRefreshableCacheTypes)
            {
                var result =
                    new RefreshLocalCacheResult
                    {
                        TypeName = refreshableCacheType.FullName
                    };

                refreshedCacheTypes.Add(result);

                try
                {
                    var getInstanceMethod = refreshableCacheType.GetMethod("GetInstance");

                    if (getInstanceMethod == null)
                    {
                        result.ErrorString = "GetInstanceメソッドが存在しません。";
                        result.Level = "Error";

                        continue;
                    }

                    dynamic refreshableCache = getInstanceMethod.Invoke(null, new object[] {});

                    if (refreshableCache != null)
                    {
                        refreshableCache.RefreshCache();
                        result.Level = "Success";
                    }
                    else
                    {
                        result.Level = "Warning";
                        result.ErrorString = "RefreshCacheメソッドがnullを返しました。";
                    }
                }
                catch(Exception exception)
                {
                    result.ErrorString = exception.Message;
                    result.Level       = "Error";
                }
                //catch
                //{
                //    var a = "aaa";
                //}
            }

            return refreshedCacheTypes;
        }

        // TODO 実装
        public IEnumerable<Type> PrefechLocalCache(string[] cacheNames)
        {
            throw new NotImplementedException();
        }
    }

    public class RefreshLocalCacheResult
    {
        public string TypeName    { get; set; }
        public string Level       { get; set; }
        public string ErrorString { get; set; }
    }
}