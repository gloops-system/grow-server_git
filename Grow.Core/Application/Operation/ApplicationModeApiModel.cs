﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inazuma.Core.ApplicationModel;

namespace Inazuma.Core.ApiModel.Operation
{
    public class ApplicationModeApiModel : InazumaApiModel
    {
        public ApplicationModeApiModel(InazumaApplicationModel applicationContext) : base(applicationContext)
        {
        }
    }
}
