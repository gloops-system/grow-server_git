﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using Fango.Core.Configuration;
using Fango.Core.Logging;
using Fango.OpenSocial.Configuration;
using Fango.OpenSocial.OAuth;
using Fango.OpenSocial.OpenSocial;
using Fango.Redis.Caching;
using Grow.Core.Application.Caching;

namespace Grow.Core.Application.OpenSocial
{
    public class AuthManager
    {
        private static readonly string TempCredentialUrl = OpenSocialSettings.OpenSocialApiAuthUrl + "/request_temporary_credential";
        private static readonly string TokenCredentialUrl = OpenSocialSettings.OpenSocialApiAuthUrl + "/request_token";

        public OAuthRequestModel RequestModel { get; private set; }
        public string CallBack { get; private set; }
        public string ConsumerKey { get; private set; }
        public string ConsumerSecret { get; private set; }
        public string RequestToken { get; private set; }
        public string RequestTokenSecret { get; private set; }
        public string Nonce { get; private set; }
        public string TimeStamp { get; private set; }
        public string Verifier { get; private set; }

        private Dictionary<string, string> GenerateParameters(bool isTemp)
        {
            var result = new Dictionary<string, string>
                {
                    {"oauth_callback", CallBack},
                    {"oauth_consumer_key", ConsumerKey},
                    {"oauth_consumer_secret", ConsumerSecret},
                    {"oauth_nonce", Nonce},
                    {"oauth_signature_method", "HMAC-SHA1"},
                    {"oauth_timestamp", TimeStamp},
                    {"oauth_version", "1.0"}
                };

            if (!isTemp)
            {
                if (!string.IsNullOrEmpty(RequestToken))
                    result.Add("oauth_token", RequestToken);
                if (!string.IsNullOrEmpty(RequestTokenSecret))
                    result.Add("oauth_token_secret", RequestTokenSecret);
                if (!string.IsNullOrEmpty((Verifier)))
                    result.Add("oauth_verifier", Verifier);

                // TODO : それぞれが取得できない場合はエラーに
            }
            return result;
        }

        // For TempCredential
        public AuthManager(string consumerKey, string consumerSecret)
        {
            var oAuthBase = new OAuthBase();

            ServicePointManager.Expect100Continue = false;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
            RequestModel = OAuthRequestModel.Proxy;
            CallBack = "oob";
            ConsumerKey = consumerKey;
            ConsumerSecret = consumerSecret;
            Nonce = oAuthBase.GenerateNonce();
            TimeStamp = oAuthBase.GenerateTimeStamp();
        }
        public Dictionary<string, string> GetTempCredential()
        {
            var parameters = GenerateParameters(true);
            var response = HttpRequest(parameters, OAuthRequestModel.Proxy, WebRequestMethods.Http.Post, TempCredentialUrl);
            return ToDictionary(response);
        }

        // For TokenCredential
        public AuthManager(string consumerKey, string consumerSecret, string requestToken, string requestTokenSecret, string verifier)
        {
            var oAuthBase = new OAuthBase();

            ServicePointManager.Expect100Continue = false;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
            RequestModel = OAuthRequestModel.Trusted;
            CallBack = string.Empty;
            ConsumerKey = consumerKey;
            ConsumerSecret = consumerSecret;
            RequestToken = requestToken;
            RequestTokenSecret = requestTokenSecret;
            Nonce = oAuthBase.GenerateNonce();
            TimeStamp = oAuthBase.GenerateTimeStamp();
            Verifier = verifier;
        }
        public Dictionary<string, string> GetTokenCredential()
        {
            var parameters = GenerateParameters(false);
            var response = HttpRequest(parameters, OAuthRequestModel.Proxy, WebRequestMethods.Http.Post, TokenCredentialUrl);
            return ToDictionary(response);
        }

        /// <summary>
        /// Do Request
        /// </summary>
        /// <param name="method"></param>
        /// <param name="url"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private static string HttpRequest(Dictionary<string, string> parameters, OAuthRequestModel model, string method, string url)
        {
            try
            {
                var uri = new Uri(url);
                var authorization = OAuthBase.CreateOAuthAuthorizationHeader(parameters, model, method, uri);
                var req = WebRequest.Create(url);
                req.Method = method.ToUpper();
                req.ContentType = "application/json";
                req.ContentLength = 0;
                req.Headers.Add("Authorization", authorization);

                using (var res = req.GetResponse())
                {
                    using (var reader = new StreamReader(res.GetResponseStream()))
                    {
                        var result = reader.ReadToEnd();
                        return result;
                    }
                }
            }
            catch (WebException ex)
            {
                LoggerManager.DefaultLogger.DebugFormat("{0}\r\n{1}", ex, ex.Status);
            }
            return string.Empty;
        }

        /// <summary>
        /// responseの文字列をDictionaryへ
        /// </summary>
        /// <param name="tokenString"></param>
        /// <returns></returns>
        private static Dictionary<string, string> ToDictionary(string tokenString)
        {
            return string.IsNullOrEmpty(tokenString)
                ? new Dictionary<string, string>()
                : tokenString.Split('&').Select(part => part.Split('=')).ToDictionary(smallArray => smallArray[0], smallArray => smallArray[1]);
        }

        /// <summary>
        /// OAuthSessionの作成
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="consumerKey"></param>
        /// <param name="consumerSecret"></param>
        /// <param name="oauthToken"></param>
        /// <param name="oauthTokenSecret"></param>
        /// <param name="oauth2Token"></param>
        /// <param name="sessionId"></param>
        /// <param name="uuid"></param>
        /// <returns></returns>
        public bool CreateOAuthSession(string userId, string consumerKey, string consumerSecret, string oauthToken, string oauthTokenSecret, string oauth2Token, string sessionId, string uuid)
        {
            bool result;
            try
            {
                var session = new OAuthSession
                {
                    OpenSocialAppId = OpenSocialSettings.OpenSocialAppId,
                    OpenSocialOwnerId = userId,
                    OAuthConsumerKey = consumerKey,
                    OAuthToken = oauthToken,
                    OAuthTokenSecret = oauthTokenSecret,
                    OAuth2Token = oauth2Token,
                    SessionId = sessionId
                };

                var redis = new RedisCacheClient();
                var expire = TimeSpan.FromMinutes(CoreSettings.ClaySessionTimeout);

                // OAuthSessionを登録
                var cacheKeySession = CacheKey.OAuthSession(sessionId);
                redis.Add(cacheKeySession, session, expire);

                // 複数端末対応のため、UUIDを登録
                var cacheKeyUUID = CacheKey.UUID(int.Parse(userId));
                redis.Add(cacheKeyUUID, uuid, expire);

                result = true;
            }
            catch
            {
                result = false;
            }
            return result;
        }
    }
}