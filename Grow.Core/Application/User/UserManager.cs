﻿using System;
using System.Linq;
using Grow.Core.Application.Common;
using Grow.Core.Application.Util;
using Grow.Core.InterfaceModel.User;
using Grow.Data.DataSet.Container;
using Grow.Data.User.DataSet.Container;

namespace Grow.Core.Application.User
{
    public class UserManager
    {
        public static int GetUserNextLevelExp(int userLevel)
        {
            int nextLevelExp = 0;
            var nextLevelMaster = new MasterDataContainer().UserLevelMaster.GetDataByPK((short)(userLevel + 1));
            if (nextLevelMaster != null)
            {
                nextLevelExp = nextLevelMaster.Exp;
            }
            return nextLevelExp;
        }
        
        /// <summary>
        /// 存在するチュートリアルステータスかチェック
        /// </summary>
        /// <param name="tutorialStatus"></param>
        /// <returns></returns>
        public static bool IsExistTutorialStatus(int tutorialStatus)
        {
            return Enum.IsDefined(typeof(AppCode.TutorialStatus), tutorialStatus);
        }

        /// <summary>
        /// 行動ptの全快予定時刻取得
        /// </summary>
        /// <param name="currentLife"></param>
        /// <param name="maxLife"></param>
        /// <param name="lastHealTime"></param>
        /// <returns></returns>
        public static DateTimeOffset GetLifeFullHealTime(int currentLife, int maxLife, DateTimeOffset lastHealTime)
        {
            return lastHealTime.AddSeconds((maxLife - currentLife) * AppCode.LifeHealInterval);
        }

        /// <summary>
        /// 試合ptの全快予定時刻取得
        /// </summary>
        /// <param name="currentStamina"></param>
        /// <param name="maxStamina"></param>
        /// <param name="lastHealTime"></param>
        /// <returns></returns>
        public static DateTimeOffset GetStaminaFullHealTime(int currentStamina, int maxStamina, DateTimeOffset lastHealTime)
        {
            return lastHealTime.AddSeconds((maxStamina - currentStamina) * AppCode.StaminaHealInterval);
        }

        /// <summary>
        /// ユーザーの最終ログインを整形
        /// </summary>
        /// <param name="now"></param>
        /// <param name="lastLogin"></param>
        /// <returns></returns>
        public static string GetLastLogin(DateTimeOffset now, DateTimeOffset lastLogin)
        {
            var diffTime = now - lastLogin;
            if (diffTime.TotalDays > 0) return diffTime.TotalDays.ToString("0日前");
            if (diffTime.TotalHours > 0) return diffTime.TotalHours.ToString("0時間前");
            if (diffTime.TotalMinutes > 0) return diffTime.TotalHours.ToString("0分前");

            return diffTime.TotalHours.ToString("ログイン中");
        }

        /// <summary>
        /// ユーザーレベルアップ
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="userLevel"></param>
        /// <param name="addExp"></param>
        /// <param name="userDataContainer"></param>
        public static void UserLevelUp(int userId, short userLevel, int addExp, UserDataContainer userDataContainer)
        {
            var nextLevelMaster = new MasterDataContainer().UserLevelMaster.GetDataByPK((short)(userLevel + 1));
            var userStatus = userDataContainer.UserStatus.GetDataByPK(userId);
            if (nextLevelMaster != null)
            {
                //commitActionViewModel.LevelUp = new LevelUpModel
                //{
                //Level = userCommitDutyAction.Level,
                //BeforeLevel = userBase.Level,
                //MaxActionPoint =
                //    userStatus.MaxActionPoint + nextLevelMaster.AddMaxActionPoint,
                //BeforeMaxActionPoint = userStatus.MaxActionPoint,
                //MaxGamePoint = userStatus.MaxGamePoint + nextLevelMaster.AddMaxGamePoint,
                //BeforeMaxGamePoint = userStatus.MaxGamePoint,
                //MaxFriendNum = userStatus.MaxFriendNum + nextLevelMaster.AddMaxFrinedNum,
                //BeforeMaxFriendNum = userStatus.MaxFriendNum
                //};

                //userDataContainer.UserStatus.UpdateLevelUpDataByPK(
                //    nextLevelMaster.Level,
                //    addExp,
                //    nextLevelMaster.AddMaxDp,
                //    nextLevelMaster.AddMaxCostPoint,
                //    nextLevelMaster.AddMaxFriendNum,
                //    userId,
                //    DiveDateUtil.DatabaseDate);
            }
        }



        /// <summary>
        /// UserLevelの更新処理
        /// </summary>
        /// <param name="user"></param>
        /// <param name="addExp"></param>
        /// <param name="userDataContainer"></param>
        /// <returns></returns>
        public static UserLevelUpModel UserLevelUp(GrowUser user, int addExp, UserDataContainer userDataContainer)
        {
            var userLevelUpModel = new UserLevelUpModel
            {
                    //BeforeExp = user.Exp,
                    //Level = user.Level,
                    //MaxDp = user.MaxDp,
                    //MaxCostPoint = user.MaxCostPoint,
                    //MaxFriendNum = user.MaxFriendNum,
            };
            /*
            var userLevelMaster =  new MasterDataContainer().UserLevelMaster;
            //if (userLevelMaster.GetDataByPK(user.Level + 1) != null)
            if (userLevelMaster.GetDataByPK(1) != null)
            {
                //var nextLevelMaster = userLevelMaster.GetData().OrderByDescending(x => x.Level).FirstOrDefault(x1 => x1.NeedExp < user.Exp + addExp);
                var nextLevelMaster = userLevelMaster.GetData().OrderByDescending(x => x.Level).FirstOrDefault(x1 => x1.NeedExp < addExp);
                if (nextLevelMaster != null)
                {
                        //if (nextLevelMaster.Level == user.Level)
                        if (nextLevelMaster.Level == 0)
                        {
                            //userDataContainer.UserStatus.UpdateExpByPK(addExp, user.UserId, DiveDateUtil.DatabaseDate);
                        }
                        else
                        {
                            //var dungeonPointUpdater = new DungeonPointUpdater(user.UserId, user.MaxDp);
                            //dungeonPointUpdater.LevelUpDp();
                                //userDataContainer.UserStatus.UpdateLevelUpDataByPK(
                                //    nextLevelMaster.Level,
                                //    addExp,
                                //    nextLevelMaster.AddMaxDp,
                                //    nextLevelMaster.AddMaxCostPoint,
                                //    nextLevelMaster.AddMaxFriendNum,
                                //    user.UserId,
                                //    DiveDateUtil.DatabaseDate);
                            
                            userLevelUpModel.AddMaxCostPoint = nextLevelMaster.AddMaxCostPoint;
                            userLevelUpModel.AddMaxDp = nextLevelMaster.AddMaxDp;
                            userLevelUpModel.AddMaxFriendNum = nextLevelMaster.AddMaxFriendNum;
                        }
                        //userLevelUpModel.CurrentLevelExp = userLevelMaster.GetDataByPK(user.Level).NeedExp;
                        //userLevelUpModel.NextLevelExp = userLevelMaster.GetDataByPK(user.Level + 1).NeedExp;
                        userLevelUpModel.AfterLevel = nextLevelMaster.Level;
                        //userLevelUpModel.AfterExp = user.Exp + addExp;
                }
            }
            */
            return userLevelUpModel;
        }
    }
}