﻿using Grow.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grow.Data;
using Grow.Core.Application;
using Fango.Core.Util;
using Grow.Core.InterfaceModel.User;

namespace Grow.Core.Application.User
{
    public class UserNotification : ModelBase
    {

        //public UserNotification(int userId) : base(userId) { }

        public UserNotification(GrowApplicationModel applicationContext) : base(applicationContext) { }
        
        public void AddNew(int userId, string message, short category, long threadId)
        {
            ApplicationContext.UserDataContainer.UserNotification.AddNew(userId, message, category, threadId);
        }

        public List<UserNotificationModel> GetUpdateNotification()
        {
            var userNotifications = ApplicationContext.UserDataContainer.UserNotification.GetDataByUserIdAndStatus(User.UserId, false);
            var listUserNotificationModel = new List<UserNotificationModel>();
            var listThreadId = new List<long>();
            foreach (var userNotification in userNotifications)
            {
                if (listThreadId.Contains(userNotification.ThreadId))
                {
                    continue;
                }
                else
                {
                    listThreadId.Add(userNotification.ThreadId);
                }
                listUserNotificationModel.Add(new UserNotificationModel()
                {
                    NotificationId = userNotification.UserNotificationId,
                    Message = userNotification.Message,
                    Category = userNotification.Category,
                    AddTime = DateUtil.ToUnixTime(userNotification.AddTime)
                });
            }
            return listUserNotificationModel;
        }

        public void UpdateLastReadNotification(long lastUserNotificationId)
        {
            ApplicationContext.UserDataContainer.UserNotification.UpdateLastReadNotification(lastUserNotificationId);
        }
    }
}
