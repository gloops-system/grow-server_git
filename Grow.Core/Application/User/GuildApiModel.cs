﻿using System;
using Grow.Core.Application;
using Grow.Core.Application.Guild;
using Grow.Core.InterfaceModel.Guild;
using Fango.Web.Core.ApiModel;

namespace Grow.Core.Application.User
{
    public class GuildApiModel : GrowApiModel
    {
        public GuildApiModel(GrowApplicationModel applicationContext) : base(applicationContext) { }
        public ContentResponse<GuildModel> GetGuild(int GuildId)
        {
            var contentResponse = new ContentResponse<GuildModel>();
            var resultData = new GuildInfo(ApplicationContext).GetGuild(GuildId);
            contentResponse.AppData = resultData;
            return contentResponse;
        }

        public ContentResponse<int> CreateGuild(int FounderId, string Name, string Symbol, string Introduction, int RecruitType, int RequiredTrophy, DateTimeOffset now)
        {
            var contentResponse = new ContentResponse<int>();
            var resultData = new GuildInfo(ApplicationContext).CreateGuild(FounderId, Name, Symbol, Introduction, RecruitType, RequiredTrophy, now);
            contentResponse.AppData = resultData;
            return contentResponse;
        }

        public ContentResponse<int> UpdateGuild(string Introduction, int RecruitType, int RequiredTrophy, DateTimeOffset now, int GuildId)
        {
            var contentResponse = new ContentResponse<int>();
            var resultData = new GuildInfo(ApplicationContext).UpdateGuild(Introduction, RecruitType, RequiredTrophy, now, GuildId);
            contentResponse.AppData = resultData;
            return contentResponse;
        }

        public ContentResponse<int> DisbandGuild(int GuildId)
        {
            var contentResponse = new ContentResponse<int>();
            var resultData = new GuildInfo(ApplicationContext).DisbandGuild(GuildId);
            contentResponse.AppData = resultData;
            return contentResponse;
        }

        public ContentResponse<int> LeaveGuild(int UserId)
        {
            var contentResponse = new ContentResponse<int>();
            var resultData = new GuildInfo(ApplicationContext).LeaveGuild(UserId);
            contentResponse.AppData = resultData;
            return contentResponse;
        }
    }
}
