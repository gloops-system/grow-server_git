﻿using System;
using System.Web;

namespace Grow.Core.Application.User
{
    public class UserDevice
    {
        private const string PhoneIDCookieKey = "pid";

        public static void UpdatePhoneId(HttpContext context)
        {
            try
            {
                var cookie = context.Request.Cookies[PhoneIDCookieKey];
                //phone_id生成
                var phoneID = cookie != null ? cookie.Value : Guid.NewGuid().ToString().Replace("-", "");
                //Cookieの発行
                SavePhoneId(context, phoneID);

                //var userId = int.Parse(!string.IsNullOrEmpty(OpenSocialUtil.OpenSocialOwnerId(context)) ? OpenSocialUtil.OpenSocialOwnerId(context) : "39214");
                //var udTA = new DeviceDataContainer().Device;
                //var userAgent = GetUserAgent();
                //if (udTA.GetDataByPK(userId, userAgent) == null)
                //    udTA.AddNew(userId, userAgent, DeviceUtil.GetCarrier().ToString(), phoneID);

                //udTA.UpdatePhoneIdByPK(phone_id, uid, userAgent);

                //logger.DebugFormat("UpdatePhoneId/uid:{0}/phone_id:{1}", uid, phone_id);
            }
            catch (Exception)
            {
                //LoggerManager.DefaultLogger.Warn(LogEntry.GetLogEntry("UpdatePhoneId失敗:" + ex.Message), ex);
            }
        }

        public static void SavePhoneId(HttpContext context, string phone_id)
        {
            var cookie = new HttpCookie(PhoneIDCookieKey, phone_id) {Expires = DateTime.Now.AddDays(30)};
            context.Response.Cookies.Add(cookie);
        }

        internal static string GetUserAgent()
        {
            throw new NotImplementedException();
            //var ctx = HttpContext.Current;
            //return StringUtil.Trim(ctx.Request.UserAgent, 200);
        }
    }
}
