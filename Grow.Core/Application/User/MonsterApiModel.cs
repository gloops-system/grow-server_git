﻿using Grow.Core.Application;
using Grow.Core.Application.Monster;
using Grow.Core.InterfaceModel.Monster;
using Fango.Web.Core.ApiModel;

namespace Grow.Core.Application.User
{
    public class MonsterApiModel : GrowApiModel
    {
        public MonsterApiModel(GrowApplicationModel applicationContext)
            : base(applicationContext) { }

        public ContentResponse<ReviveMonsterViewModel> ReviveMonster(long userMonsterId)
        {
            var contentResponse = new ContentResponse<ReviveMonsterViewModel>();
            var user = ApplicationContext.CurrentUser;
            var resultData = new MonsterUser(ApplicationContext).ReviveMonster(userMonsterId);
            contentResponse.AppData = resultData;
            return contentResponse;
        }
    }
}