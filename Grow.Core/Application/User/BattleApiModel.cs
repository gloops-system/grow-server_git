﻿using Grow.Core.InterfaceModel.User;
using Grow.Core.Application.Avatar;
using Grow.Core.Application.Monster;
using Grow.Core.Application.Gear;
using Grow.Core.Application.Duty;
using Fango.Web.Core.ApiModel;
using Fango.Core.Util;
using System.Collections.Generic;
using Fango.Redis.Caching;
using System;
using Grow.Core.Application.Util;
using Grow.Data.User.DataSet;
using Grow.Core.Application.Common;

namespace Grow.Core.Application.User
{
    public class BattleApiModel : GrowApiModel
    {
        public BattleApiModel(GrowApplicationModel applicationContext)
            : base(applicationContext) { }

        /// <summary>
        /// PvpStart
        /// </summary>
        /// <returns>Target user colony data</returns>
        public ContentResponse<BattleProfileModel> PvPStart()
        {
            var contentResponse = new ContentResponse<BattleProfileModel>();

            var user = ApplicationContext.CurrentUser;

            // Current temporary solution: random uid excluding uid from CurrentUserBattle redis key
            var currentUsersInBattle = GrowBattleRedisUtil.GetExcludedList();
            currentUsersInBattle.Add(user.UserId); // also exclude the attaker himself

            var offlineTimeInMinutes = 5;

            var availableTargetUsers = ApplicationContext.UserDataContainer.UserColony.GetDataExcludeFromList(offlineTimeInMinutes, currentUsersInBattle);

            var totalTargetUserCount = availableTargetUsers.Count;

            Random random = new Random();
            int rInt = random.Next(0, totalTargetUserCount);

            var targetUser = availableTargetUsers[rInt];

            int targetUserId = targetUser.UserId;

            // indicate that a battle is already reserved, a target user in this battle is no longer available
            GrowBattleRedisUtil.PutToExcludedList(user.UserId, targetUserId);

            // Collect target colony user
            var userColonyRow = ApplicationContext.UserDataContainer.UserColony.GetDataByPK(targetUserId);
            var userBaseRow = ApplicationContext.UserDataContainer.UserBase.GetDataByPK(targetUserId);

            // initialize battle
            var battleHistoryId = ApplicationContext.UserDataContainer.UserBattleHistory.AddNew(user.UserId, user.Userinfo.Gold, user.Userinfo.Plasma, targetUserId, userColonyRow.Gold, userColonyRow.Plasma, (byte)AppCode.BattleStatus.INIT, (byte)AppCode.BattleResult.WIN, GrowDateUtil.GetDateTimeOffsetNow(user.UserId));

            var profile = new BattleProfileModel();

            profile.BattleHistoryId = battleHistoryId;
            profile.TargetGold = userColonyRow.Gold;
            profile.TargetPlasma = userColonyRow.Plasma;
            profile.TargetUserId = userColonyRow.UserId;
            profile.TargetNickName = userBaseRow.NickName;
            profile.TargetColonyData = userColonyRow.ColonyData;

            contentResponse.AppData = profile;

            return contentResponse;
        }

        /// <summary>
        /// PvPEnd
        /// </summary>
        /// <returns></returns>
        public ContentResponse PvPEnd(int battleHistoryId, int userId, int targetUserId, int targetLootedGold, int targetLootedPlasma, byte battleStatus, byte battleResult, string battleReplayData)
        {
            var contentResponse = new ContentResponse();

            // Update datbase only if the key is successfully removed
            if (GrowBattleRedisUtil.RemoveFromExcludedList(userId, targetUserId))
            {
                var replayData = battleReplayData == null ? "" : battleReplayData;

                ApplicationContext.UserDataContainer.UserBattleHistory.UpdateBattle(battleHistoryId, userId, targetUserId, targetLootedGold, targetLootedPlasma, battleStatus, battleResult, replayData, GrowDateUtil.GetDateTimeOffsetNow(userId));
            }

            return contentResponse;
        }
    }
}
