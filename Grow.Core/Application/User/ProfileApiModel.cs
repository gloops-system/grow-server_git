﻿using Grow.Core.InterfaceModel.User;
using Grow.Core.Application.Avatar;
using Grow.Core.Application.Monster;
using Grow.Core.Application.Gear;
using Grow.Core.Application.Duty;
using Fango.Web.Core.ApiModel;
using Fango.Core.Util;

namespace Grow.Core.Application.User
{
    public class ProfileApiModel : GrowApiModel
    {
        public ProfileApiModel(GrowApplicationModel applicationContext)
            : base(applicationContext) { }

        /// <summary>
        /// プロフィール情報取得
        /// </summary>
        /// <returns></returns>
        public ContentResponse<ProfileModel> GetProfileInfo()
        {
            var contentResponse = new ContentResponse<ProfileModel>();
            // ユーザー情報取得
            var user = ApplicationContext.CurrentUser;
            //var userAvatars = new AvatarUser(ApplicationContext).GetUserAvatars();
            //var userMonsters = new MonsterUser(ApplicationContext).GetUserMonsters();
            //var userGears = new GearUser(ApplicationContext).GetGearList();
            //var userItems = new UserItem(ApplicationContext).LoadUserItem();
            //var userLabyrinth = new LabyrinthUser(ApplicationContext).LoadUserLabyrinthInfo();
            
            // TODO: Hardcode user avatar for now 
            //foreach (var userAvatar in userAvatars)
            //{
            //    userAvatar.GradeId = 1;
            //    userAvatar.ColorId = 0;
            //}            
            //End of hardcode            
            var profile = new ProfileModel(user.Userinfo)
            {
                Gem = user.GetUserGem()
                //Avatar = userAvatars,
                //Monster = userMonsters,
                //Gear = userGears,
                //Item = userItems,
                //Labyrinth = userLabyrinth
            };

            contentResponse.AppData = profile;
            return contentResponse;
        }

        /// <summary>
        /// コメントの登録、更新
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public ContentResponse UpdateLastReadNotification(long lastUserNotificationId)
        {         
            var contentResponse = new ContentResponse();
            var user = ApplicationContext.CurrentUser;
            new UserNotification(ApplicationContext).UpdateLastReadNotification(lastUserNotificationId);
            //contentResponse.AppData = resultData;
            return contentResponse;
        }

        public ContentResponse UpdateUserColony(short level, int gold, int plasma, string colonyData, long colonyHistoryIndex)
        {
            var contentResponse = new ContentResponse();

            var user = ApplicationContext.CurrentUser;

            ApplicationContext.UserDataContainer.UserColony.UpdateByPK(user.UserId, level, gold, plasma, colonyData, colonyHistoryIndex, DateUtil.GetDateTimeOffsetNow());

            return contentResponse;
        }
    }
}