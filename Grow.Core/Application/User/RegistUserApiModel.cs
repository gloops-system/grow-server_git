﻿using Grow.Core.Application;
using Grow.Core.Application.Duty;
using Fango.Web.Core.ApiModel;
using System.Collections.Generic;

namespace Grow.Core.Application.User
{
    public class RegistUserApiModel : GrowApiModel
    {
        public RegistUserApiModel(GrowApplicationModel applicationContext)
            : base(applicationContext) { }

        public ContentResponse RegistUser(string nickName)
        {
            var contentResponse = new ContentResponse();

            var user = ApplicationContext.CurrentUser;
            user.CreateInitialUserData(ApplicationContext, nickName);
            // TODO: Remove it, run once per hour by batch
            //var userIds = new List<int>();
            //userIds.Add(user.UserId);
            //new LabyrinthManager().RunMatchGroupNewUsers(userIds);
            return contentResponse;
        }
    }
}