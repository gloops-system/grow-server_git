﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grow.Core.Application.Common;
using Grow.Data.DataSet;
using Grow.Data.User.DataSet;

namespace Grow.Core.Application.User
{
    public partial class GrowUser
    {
        //private UserDataSet.UserShopCounterDataTable _userShopCounterDataTable;
        //public UserDataSet.UserShopCounterDataTable UserShopCounterDataTable
        //{
        //    get
        //    {
        //        return _userShopCounterDataTable ??
        //               (_userShopCounterDataTable = ApplicationContext.UserDataContainer.UserShopCounter.GetDataByUserId(_userId));
        //    }
        //}

        public int CanNum(ShopDataSet.ShopItemMasterRow shopItemMasterRow, DateTimeOffset now)
        {
            //var userShopCounterRow = UserShopCounterDataTable.FirstOrDefault(
            //            x => x.ShopItemId == shopItemMasterRow.ShopItemId);
            //var canNum = AppCode.ShopMaxBuyNum;

            //if (userShopCounterRow != null)
            //{
            //    switch ((AppCode.ShopLimitType)shopItemMasterRow.LimitType)
            //    {
            //        case AppCode.ShopLimitType.日制限:
            //            if (userShopCounterRow.DailyBuyCountTargetDate == now)
            //                canNum = shopItemMasterRow.LimitNum - userShopCounterRow.DailyBuyCount;
            //            break;
            //        case AppCode.ShopLimitType.無制限:
            //            canNum = shopItemMasterRow.LimitNum - userShopCounterRow.TotalBuyCount;
            //            break;
            //    }
            //}

            //switch ((AppCode.PaymentCurrencyType)shopItemMasterRow.PayType)
            //{
            //    case AppCode.PaymentCurrencyType.GameCurrency:
            //        canNum = GetCurrencyBuyNum(canNum, shopItemMasterRow.Price, GameCurrency);
            //        break;
            //    case AppCode.PaymentCurrencyType.Gil:
            //        canNum = GetCurrencyBuyNum(canNum, shopItemMasterRow.Price, Gil);
            //        break;
            //    default:
            //        canNum = 0; // ありえないので0で固定
            //        break;
            //}

            //if (canNum > AppCode.ShopMaxBuyNum)
            //    canNum = AppCode.ShopMaxBuyNum;

            //return canNum;
            return 0;
        }

        private int GetCurrencyBuyNum(int canNum, int price, int currency)
        {
            if (currency == 0)
                return 0;

            var currencyBuyNum = currency / price;
            if (currencyBuyNum > canNum)
                return canNum;

            return currencyBuyNum;
        }
    }
}
