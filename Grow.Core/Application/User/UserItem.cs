﻿using Grow.Core.Common;
using System.Collections.Generic;
using Grow.Core.InterfaceModel.User;

namespace Grow.Core.Application.User
{
    public class UserItem : ModelBase
    {
        public UserItem(GrowApplicationModel applicationContext) : base(applicationContext) { }
        
        public UserItemModel UseItem(int itemId, int number)
        {
            ApplicationContext.UserDataContainer.UserItem.DecrementUserItem(User.UserId, itemId, number, ApplicationContext.Now);
            return new UserItemModel()
                {
                    ItemId = itemId,
                    Num = GetUserItemNumByItemId(itemId)
                };
        }

        public UserItemModel GetItem(int itemId, int number)
        {
            ApplicationContext.UserDataContainer.UserItem.IncrementUserItem(User.UserId, itemId, number, ApplicationContext.Now);
            return new UserItemModel()
            {
                ItemId = itemId,
                Num = GetUserItemNumByItemId(itemId)
            };
        }

        private int GetUserItemNumByItemId(int itemId)
        {
            var userItem = ApplicationContext.UserDataContainer.UserItem.GetDataByPK(User.UserId, itemId);
            if (userItem != null)
            {
                return userItem.Num;
            }
            return 0;
        }

        public List<UserItemModel> LoadUserItem()
        {
            var userItems = ApplicationContext.UserDataContainer.UserItem.GetDataByUserId(User.UserId);
            var listUserItemModel = new List<UserItemModel>();
            foreach (var userItem in userItems)
            {
                if (userItem.Num > 0)
                {
                    listUserItemModel.Add(new UserItemModel()
                    {
                        ItemId = userItem.ItemId,
                        Num = userItem.Num
                    });
                }
            }
            return listUserItemModel;
        }
    }
}
