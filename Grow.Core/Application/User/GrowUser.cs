﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Web;
using Fango.Core.Util;
using Fango.OpenSocial.OpenSocial.Data.MbgaApp;
using Grow.Core.Application.Common;
using Grow.Core.Application.Util;
using Grow.Data;
using Grow.Data.DataSet.Container;
using Grow.Data.User.DataSet;
using Grow.Data.User.DataSet.Container;
using Grow.Data.User.DataSet.UserDataSetTableAdapters;
using Grow.Core.InterfaceModel.User;
using Grow.Core.Common;
using System.Collections.Generic;
using Grow.Core.Application.Monster;
using Grow.Core.Application.Avatar;
using Grow.Core.Application.Gear;
using Grow.Core.Application;
using Grow.Data.DataSet;

namespace Grow.Core.Application.User
{
    public partial class GrowUser
    {
        private readonly int _userId;
        private UserViewModel _userinfo;

        private GrowUser(int userId)
        {
            _userId = userId;
        }

        public static GrowUser GetInstance(int userId)
        {
            var context = HttpContext.Current;
            GrowUser user;
            if (context != null)
            {
                var cacheKey = "ContextGrowUser-" + userId;
                user = context.Items[cacheKey] as GrowUser;
                if (user == null)
                {
                    user = new GrowUser(userId);
                    context.Items[cacheKey] = user;
                }
            }
            else
            {
                user = new GrowUser(userId);
            }

            return user;
        }

        public int UserId
        {
            get { return _userId; }
        }

        public UserViewModel Userinfo
        {
            get { return _userinfo ?? (_userinfo = GetUserinfo(_userId)); }
        }        

        /// <summary>
        /// Check user exist
        /// </summary>
        public bool IsExistsUser
        {
            get { return Userinfo != null; }
        }

        public void CreateInitialUserData(GrowApplicationModel applicationContext, string nickName)
        {
            var now = DateUtil.GetDateTimeOffsetNow();

            try
            {
                var userBaseRow = applicationContext.UserDataContainer.UserBase.GetDataByPK(_userId, false);

                if (userBaseRow == null)
                {
                    // UserBase
                    applicationContext.UserDataContainer.UserBase.AddNew(_userId,
                                                      nickName,
                                                      "facebook" + _userId,
                                                      (int)AppCode.TutorialStatus.初期値,
                                                      GenerateInviteCode(),
                                                      now
                    );

                    // UserColony
                    applicationContext.UserDataContainer.UserColony.AddNew(_userId, 1, 0, 0, now);

                    // UserStatus
                    applicationContext.UserDataContainer.UserStatus.AddNew(_userId, 1, 0, 0, 0, AppCode.DefaultGold, AppCode.DefaultStamina, AppCode.DefaultStamina,
                                                                AppCode.DefaultHp, AppCode.DefaultSp, AppCode.DefaultAttack, AppCode.DefaultDefense,
                                                                AppCode.DefaultCutPercentage, AppCode.DefaultCriticalPercentage, AppCode.DefaultStunValue,
                                                                AppCode.DefaultStunResistance, AppCode.DefaultStunRecoveryStand, AppCode.DefaultStunRecoveryDown,
                                                                AppCode.DefaultScoreUp, 0, 0, AppCode.DefaultGearBoxNum, AppCode.DefaultFriendsNum, AppCode.DefaultMonsterBoxNum,
                                                                AppCode.DefaultItemBoxNum, AppCode.DefaultGearBagNum, AppCode.DefaultUsableMonstersNum, AppCode.DefaultItemBagNum, now);

                    // UserGem
                    applicationContext.UserDataContainer.UserGem.AddNew(_userId, AppCode.DefaultGem);
                    // User Monster
                    var monsterUser = new MonsterUser(applicationContext);
                    monsterUser.InsertBulkUserMonsters(AppCode.ListDefaultMonsterId);
                    // User Avatar
                    var avatarUser = new AvatarUser(applicationContext);
                    var avatarUserIds = avatarUser.AddUserAvatars(AppCode.ListDefaultAvatarId);
                    // User Gear
                    var gearUser = new GearUser(applicationContext);
                    var gearUserId = gearUser.AddNew(AppCode.ListDefaultGearId[0]);
                    gearUser.InsertBulkUserGear(AppCode.ListDefaultGearId.Skip(1).ToArray());
                    applicationContext.UserDataContainer.UserStatus.UpdateByPK(_userId, 1, 0, avatarUserIds[0], gearUserId, AppCode.DefaultGold,
                                                            AppCode.DefaultHp, AppCode.DefaultSp, AppCode.DefaultAttack, AppCode.DefaultDefense,
                                                            AppCode.DefaultCutPercentage, AppCode.DefaultCriticalPercentage, AppCode.DefaultStunValue,
                                                            AppCode.DefaultStunResistance, AppCode.DefaultStunRecoveryStand, AppCode.DefaultStunRecoveryDown,
                                                            AppCode.DefaultScoreUp, 0, 0, AppCode.DefaultGearBoxNum, AppCode.DefaultFriendsNum, AppCode.DefaultMonsterBoxNum,
                                                            AppCode.DefaultItemBoxNum, AppCode.DefaultGearBagNum, AppCode.DefaultUsableMonstersNum, AppCode.DefaultItemBagNum, now);
                    // User Item
                    foreach (KeyValuePair<int, int> itemAdd in AppCode.ListDefaultItemId)
                    {
                        applicationContext.UserDataContainer.UserItem.IncrementUserItem(_userId, itemAdd.Key, itemAdd.Value, now);
                    }
                }
            }
            catch (Exception e) {
                Console.Write(e.StackTrace); 
            }
        }

        public int UpdateIncreaseUserGold(UserDataContainer userDataContainer, int increaseGold, DateTimeOffset now)
        {            
            userDataContainer.UserStatus.IncrementGoldByPK(Userinfo.UserId, increaseGold, now);
            Userinfo.Gold = userDataContainer.UserStatus.GetDataByPK(Userinfo.UserId).Gold;
            return Userinfo.Gold;
        }

        public int UpdateDecremenUserGold(UserDataContainer userDataContainer, int decremenGold, DateTimeOffset now)
        {
            userDataContainer.UserStatus.DecrementGoldByPK(Userinfo.UserId, decremenGold, now);
            Userinfo.Gold = userDataContainer.UserStatus.GetDataByPK(Userinfo.UserId).Gold;
            return Userinfo.Gold;
        }

        public void UpdateAvatarBonus(UserDataContainer userDataContainer, DateTimeOffset now)
        {
            userDataContainer.UserStatus.UpdateAvatarBonus(Userinfo.UserId, Userinfo.AvatarHpBonus, Userinfo.AvatarSpBonus,
                                                                    Userinfo.AvatarAttackBonus, Userinfo.AvatarDefenseBonus, Userinfo.AvatarScoreUpBonus, now);
        }

        public bool UpdateUserStatusExp(int exp, UserDataContainer userDataContainer, MasterDataContainer masterDataContainer)
        {
            bool changeFlg = false;
            bool isLevelUp = false;
            if (Userinfo.Exp < exp)
            {
                Userinfo.Exp = exp;
                changeFlg = true;
            }
            var userLevelMasters = masterDataContainer.UserLevelMaster.GetData();
            var userLevelMasterUps = new List<MasterDataSet.UserLevelMasterRow>();
            foreach (var userLevelMaster in userLevelMasters)
            {
                if (Userinfo.Exp >= userLevelMaster.Exp)
                {
                    if (Userinfo.Level < userLevelMaster.Level)
                    {
                        userLevelMasterUps.Add(userLevelMaster);
                    }
                }
                else
                {
                    break;
                }
            }
            if (userLevelMasterUps.Count > 0)
            {
                foreach (var userLevelMasterUp in userLevelMasterUps)
                {
                    Userinfo.Stamina += userLevelMasterUp.StaminaUp;
                    Userinfo.Hp += userLevelMasterUp.HpUp;
                    Userinfo.Sp += userLevelMasterUp.SpUp;
                    Userinfo.Attack += userLevelMasterUp.AttackUp;
                    Userinfo.Defense += userLevelMasterUp.DefenseUp;
                    Userinfo.StunValue += userLevelMasterUp.StunValueUp;
                    Userinfo.StunResistance += userLevelMasterUp.StunResistanceUp;
                    Userinfo.StunRecoveryStand += userLevelMasterUp.StunRecoveryStandUp;
                    Userinfo.StunRecoveryDown += userLevelMasterUp.StunRecoveryDownUp;
                    Userinfo.ScoreUp += userLevelMasterUp.ScoreUpUp;
                    Userinfo.MaxFriendsNum += userLevelMasterUp.MaxFriendsNumUp;
                    Userinfo.MaxMonsterBoxNum += userLevelMasterUp.MaxMonsterBoxNumUp;
                    Userinfo.MaxItemBoxNum += userLevelMasterUp.MaxItemBoxNumUp;
                    Userinfo.MaxGearBagNum += userLevelMasterUp.MaxGearBagNumUp;
                    Userinfo.Level = userLevelMasterUp.Level;
                }
                isLevelUp = true;
            }
            if (changeFlg)
            {
                UpdateUserStatus(userDataContainer);
            }
            return isLevelUp;
        }

        public void UpdateUserStatus(UserDataContainer userDataContainer)
        {            
            userDataContainer.UserStatus.UpdateByPK(Userinfo.UserId, Userinfo.Level, Userinfo.Exp, Userinfo.UserAvatarId, Userinfo.UserGearId, Userinfo.Gold,
                                                    Userinfo.Hp, Userinfo.Sp,
                                                    Userinfo.Attack, Userinfo.Defense, Userinfo.CutPercentage, Userinfo.CriticalPercentage,
                                                    Userinfo.StunValue, Userinfo.StunResistance, Userinfo.StunRecoveryStand,
                                                    Userinfo.StunRecoveryDown, Userinfo.ScoreUp,
                                                    Userinfo.LastQuestAreaId, Userinfo.LastLabyrinthAreaId,
                                                    Userinfo.MaxGearBoxNum, Userinfo.MaxFriendsNum, Userinfo.MaxMonsterBoxNum, Userinfo.MaxItemBoxNum,
                                                    Userinfo.MaxGearBagNum, Userinfo.MaxUsableMonstersNum, Userinfo.MaxItemBagNum, DateUtil.GetDateTimeOffsetNow());
        }

        private UserViewModel GetUserinfo(int userId)
        {
            var userDataContainer = new UserDataContainer();
            var userBaseRow = userDataContainer.UserBase.GetDataByPK(userId);
            var userColonyRow = userDataContainer.UserColony.GetDataByPK(userId);

            if (userBaseRow == null || userColonyRow == null)
                return null;

            var userinfo = new UserViewModel()
            {
                UserId = userBaseRow.UserId,
                NickName = userBaseRow.NickName,
                FacebookId = userBaseRow.FacebookId,
                TutorialStatus = userBaseRow.TutorialStatus,
                Level = userColonyRow.Level,
                Gold = userColonyRow.Gold,
                Plasma = userColonyRow.Plasma,
                ColonyData = userColonyRow.ColonyData,
                ColonyHistoryIndex = userColonyRow.ColonyHistoryIndex,
                LastSyncTimePassed = (int)DateUtil.GetDateTimeOffsetNow().Subtract(userColonyRow.LastSyncTime).TotalSeconds,
            };

            return userinfo;
        }

        public int GetUserGem()
        {
            int gemNum = 0;
            var userDataContainer = new UserDataContainer();
            var userGem = userDataContainer.UserGem.FindUserGem(UserId);
            if (userGem != null)
            {
                gemNum = userGem.Num;
            }
            else
            {
                userDataContainer.UserGem.AddNew(UserId, 0);
            }
            return gemNum;
        }

        #region Register
        /*
        private void AddUserMonsters(UserDataContainer userDataContainer, int[] MonsterIds)
        {
            var masterDataContainer = new MasterDataContainer();
            var monsterMasterAdapter = masterDataContainer.MonsterMaster;
            foreach (var MonsterId in MonsterIds)
            {
                var monsterMaster = monsterMasterAdapter.GetDataByMonsterId(MonsterId);
                userDataContainer.UserMonster.AddNew(UserId, monsterMaster.MonsterId, (byte)UserMonsterStatus.AVAILABLE, (byte)monsterMaster.DefaultLife, monsterMaster.DefaultHp,
                                                      monsterMaster.DefaultHp, monsterMaster.DefaultHpRecoveryAmount, monsterMaster.DefaultStunResistance,
                                                      monsterMaster.DefaultStunRecoveryStand, monsterMaster.DefaultStunRecoveryDown, monsterMaster.DefaultAttack,
                                                      monsterMaster.DefaultDefense);
            }
        }

        private long[] AddUserAvatars(UserDataContainer userDataContainer, int[] AvatarIds)
        {
            var masterDataContainer = new MasterDataContainer();
            long[] resultAvatarIds = new long[AvatarIds.Length];
            var avatarMasterAdapter = masterDataContainer.AvatarMaster;
            int idx = 0;
            foreach (var AvatarId in AvatarIds)
            {
                var avatarMaster = avatarMasterAdapter.GetDataByAvatarId(AvatarId);
                var userAvatarId = userDataContainer.UserAvatar.AddNew(UserId, avatarMaster.AvatarId, avatarMaster.DefaultHp, avatarMaster.DefaultSp,
                                                                        avatarMaster.DefaultAttack, avatarMaster.DefaultDefense, avatarMaster.DefaultScore);
                resultAvatarIds[idx++] = userAvatarId;
            }
            return resultAvatarIds;
        }

        private List<long> AddUserGears(UserDataContainer userDataContainer, int[] GearIds)
        {
            var masterDataContainer = new MasterDataContainer();
            var gearMasterAdapter = masterDataContainer.GearMaster;
            var userGearIds = new List<long>();
            foreach (var GearId in GearIds)
            {
                var gearMaster = gearMasterAdapter.GetDataByGearId(GearId);
                userGearIds.Add(userDataContainer.UserGear.AddNew(UserId, gearMaster.GearId, gearMaster.GearType, gearMaster.Category,
                                                                gearMaster.Property, gearMaster.Rarity, gearMaster.Attack, gearMaster.Defense,
                                                                gearMaster.CutPercentage, gearMaster.Stun, gearMaster.Point));
            }
            return userGearIds;
        }

        private void Register(UserDataContainer userDataContainer, DateTimeOffset now)
        {
            // Insert into User Monster
            //var monsterUser = new MonsterUser(_userId);
            //monsterUser.AddUserMonsters(AppCode.ListDefaultMonsterId);
            AddUserMonsters(userDataContainer, AppCode.ListDefaultMonsterId);

            // Insert into User Avatar
            //var avatarUser = new AvatarUser(_userId);
            //var avatarUserIds = avatarUser.AddUserAvatars(AppCode.ListDefaultAvatarId);
            var avatarUserIds = AddUserAvatars(userDataContainer, AppCode.ListDefaultAvatarId);
            // Insert into User Gear
            //var gearUser = new GearUser(_userId);
            //var gearUserIds = gearUser.AddUserGears(AppCode.ListDefaultGearId);
            var gearUserIds = AddUserGears(userDataContainer, AppCode.ListDefaultGearId);

            userDataContainer.UserStatus.UpdateByPK(_userId, 1, 0, avatarUserIds[0], gearUserIds[0], AppCode.DefaultGold, AppCode.DefaultStamina, AppCode.DefaultStamina,
                                                    AppCode.DefaultHp, AppCode.DefaultSp, AppCode.DefaultAttack, AppCode.DefaultDefense,
                                                    AppCode.DefaultCutPercentage, AppCode.DefaultCriticalPercentage, AppCode.DefaultStunValue,
                                                    AppCode.DefaultStunResistance, AppCode.DefaultStunRecoveryStand, AppCode.DefaultStunRecoveryDown,
                                                    AppCode.DefaultScoreUp, 0, 0, AppCode.DefaultGearNum, AppCode.DefaultFriendNum, AppCode.DefaultMonsterNum,
                                                    AppCode.DefaultItemNum, AppCode.DefaultCarryingGearNum, now);
            // Insert User Item
            foreach (KeyValuePair<int, int> itemAdd in AppCode.ListDefaultItemId)
            {
                userDataContainer.UserItem.IncrementUserItem(_userId, itemAdd.Key, itemAdd.Value, now);
            }

        }
         */
        #endregion Register
        #region Invite Code
        private const string BaseString = "ABCDEFGHJKMNPQRSTUVWXYZabcdefghjkmnpqrstuvwxyz23456789";
        private const int CodeLength = 5;

        private static string GenerateInviteCode()
        {
            // Container
            var userContainer = new UserDataContainer();

            var inviteCode = GenerateCode();
            var cnt = 0;
            while (userContainer.UserBase.GetDataByInviteCode(inviteCode) != null)
            {
                inviteCode = GenerateCode();
                if (cnt >= 10)  //無限ループ防止
                    return null;
                cnt++;
            }
            return inviteCode;
        }

        /// <summary>
        /// ランダムな文字列を生成する
        /// </summary>
        /// <returns>生成された文字列</returns>
        private static string GenerateCode()
        {
            string code;
            do
            {
                code = "";
                //NGword検索
                for (var i = 0; i < CodeLength; i++)
                {
                    code += BaseString[RandomUtil.UniqueRandom.Next(BaseString.Length)];
                }
            }
            while (!NgWordManager.FetchNgWord(code).Valid);
            return code;
        }
        #endregion Invite Code
        #region Debug
        [Conditional("DEBUG")]
        public void DeleteUser()
        {
            var userBaseTA = new UserBaseTableAdapter();
            var userStatusTA = new UserStatusTableAdapter();
            var userBaseDT = userBaseTA.GetDataByPK(_userId);
            var userStatusDT = userStatusTA.GetDataByPK(_userId);

            userBaseDT.Rows[0].Delete();
            userStatusDT.Rows[0].Delete();

            userBaseTA.Update(userBaseDT);
            //userStatusTA.Update(userStatusDT);
        }
        #endregion Debug
    }
}