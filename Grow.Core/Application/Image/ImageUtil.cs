﻿using System;
using System.Net;
using Fango.Core.Logging;

namespace Grow.Core.Application.Image
{
    public class ImageUtil
    {
        public static string ToBase64(string imageUrl)
        {
            try
            {
                using (var webClient = new WebClient())
                {
                    var wcByte = webClient.DownloadData(imageUrl);
                    return wcByte.Length == 0 ? string.Empty : Convert.ToBase64String(wcByte);
                }
            }
            catch (WebException)
            {
                LoggerManager.DefaultLogger.ErrorFormat("URLが不正です。{0}", imageUrl);
            }

            return string.Empty;
        }
    }
}