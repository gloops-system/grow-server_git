﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fango.Core.Sequence;
using Fango.OpenSocial.OAuth;
using Fango.OpenSocial.OpenSocial;
using Grow.Core.Application.Caching;
using Grow.Core.Application.Common;
using Grow.Core.Application.Util;
using Grow.Data.General.DataSet.Container;

namespace Grow.Core.Application.Notification
{
    public class NotificationManager
    {
        private OAuthSession OAuthSession { get; set; }
        private int UserId { get; set; }
        public NotificationManager(OAuthSession oauthSession, int userId)
        {
            OAuthSession = oauthSession;
            UserId = userId;
        }

        public void SendNotification(string targetUserId, string message, int? badge, string sound, string collapseKey,
                                     string style, string iconUrl, Dictionary<string, string> extras, TargetDevice target)
        {
            var osClient = new OpenSocialRestClientMbgaApp(OAuthSession.ToDictionary())
                {
                    OpenSocialViewerId = UserId.ToString()
                };
            var result = osClient.CreateRemoteNotification(targetUserId, message, badge, sound, collapseKey, style, iconUrl, extras, target);
        }

        public bool CreateQueueNotification(AppCode.NotificationType notificationType, DateTimeOffset deliveryDate, int? fromUser, int? toUser, string message)
        {
            var notificationId = SequenceClientManager.DefaultSequenceClient.GetNextValue(CacheKey.QueueNotificationId(), 1);
            //switch (notificationType)
            //{
            //    // TODO : イベント以外だった場合はGetNotificationMessageでメッセージを作成
            //}
            message = GetNotificationMessage(notificationType, new[] {"Aさん"});
            //new QueueDataContainer().QueueNotification
            //                        .AddNew(notificationId, (int) notificationType, deliveryDate, fromUser, toUser,
            //                                message, null, null, null, null, null, null,
            //                                (int)AppCode.NotificationTargetDevice.All, DiveDateUtil.DatabaseDate);
            return true;
        }

        private static string GetNotificationMessage(AppCode.NotificationType notificationType, IEnumerable<object> param)
        {
            var message = string.Empty;
            //switch (notificationType)
            //{
            //    // TODO : どんな時に送信しないといけないのか決まり次第実装
            //}
            return param.Aggregate(message, string.Format);
        }
    }
}