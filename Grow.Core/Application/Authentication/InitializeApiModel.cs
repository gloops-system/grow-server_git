﻿using System;
using Fango.Core.Logging;
using Fango.Core.Sequence;
using Grow.Core.Application.Authentication;
using Grow.Core.Application.Caching;
using Grow.Core.Application.Common;
using Grow.Core.Application.Util;
using Grow.Core.InterfaceModel.Authentication;
using Fango.Web.Core.ApiModel;

namespace Grow.Core.Application.Authentication
{
    public class InitializeApiModel : GrowApiModel
    {
        public InitializeApiModel(GrowApplicationModel applicationContext)
            : base(applicationContext) { }

        /// <summary>
        /// ユーザー登録
        /// </summary>
        /// <returns></returns>
        public ContentResponse<InitializeViewModel> Initialize(string time, string version, string uuid,
                                                               AppCode.DeviceType deviceType, string osVersion)
        {
            var contentResponse = new ContentResponse<InitializeViewModel>();

            // Container
            var userDataContainer = ApplicationContext.UserDataContainer;

            // RepositoryAdapter
            var userLoginRepositoryAdapter = userDataContainer.UserLogin;

            // クライアントのバージョンチェック
            var needUpgrade = AuthenticationUtil.CheckVersion(version);
            if (needUpgrade)
            {
                contentResponse.Code = (int) AppCode.ApiResultCode.クライアント更新;
                contentResponse.Message = @"クライアントの更新が必要です。"; // TODO : ローカライズ対応

                return contentResponse;
            }

            // 復号化と時間のチェック
            if (!AuthenticationUtil.CheckTime(time, true))
            {
                LoggerManager.DefaultLogger.Debug("復号化と時間のチェックに失敗");
                contentResponse.Code = (int) AppCode.ApiResultCode.パラメータ不正;
                contentResponse.Message = @"パラメータが不正です。"; // TODO : ローカライズ対応
                return contentResponse;
            }

            // UUIDのチェック
            var guid = Guid.NewGuid();
            if (!Guid.TryParse(uuid, out guid))
            {
                LoggerManager.DefaultLogger.Debug("UUIDが不正");
                contentResponse.Code = (int) AppCode.ApiResultCode.パラメータ不正;
                contentResponse.Message = @"パラメータが不正です。"; // TODO : ローカライズ対応
                return contentResponse;
            }

            // DeviceTypeのチェック
            if (!Enum.IsDefined(typeof (AppCode.DeviceType), deviceType))
            {
                LoggerManager.DefaultLogger.Debug("DeviceTypeが不正");
                contentResponse.Code = (int)AppCode.ApiResultCode.パラメータ不正;
                contentResponse.Message = @"パラメータが不正です。"; // TODO : ローカライズ対応
                return contentResponse;
            }
            
            // UserIdの作成(表側には出ない)
            var userId = (int) SequenceClientManager.DefaultSequenceClient.GetNextValue(CacheKey.UserId(), 1);

            // 一意なIdの作成(表側に出るIdはこちら)
            var uniqueId = AuthenticationUtil.CreateUniqueId();

            // ユーザー用シークレットの作成
            var secret = AuthenticationUtil.CreateSecret();

            // ユーザー登録
            //var now = DiveDateUtil.DatabaseDate;            
            userLoginRepositoryAdapter.AddNew(userId, uniqueId, null, uuid, secret, true, ApplicationContext.Now, (int) deviceType,
                                              (int)AppCode.UserLoginStatus.正常, ApplicationContext.Now);
            contentResponse.AppData = new InitializeViewModel(userId, uniqueId, secret);

            return contentResponse;
        }
    }
}