﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using Fango.Core.Logging;
using Fango.Core.Util;
using Grow.Core.Application.Common;
using Grow.Core.Application.Configuration;
using Grow.Core.Application.Util;
using Grow.Core.InterfaceModel.Authentication;
using Grow.Data.User.DataSet.Container;

namespace Grow.Core.Application.Authentication
{
    public class AuthenticationUtil
    {
        private static readonly string CommonSecretKey = MobileSettings.CommonSecretKey;
        private static readonly string TokenSecretKey = MobileSettings.TokenSecretKey;

        /// <summary>
        /// 共通鍵で暗号化する
        /// </summary>
        public static string EncryptByCommonSecret(string content)
        {
            return Protection.OpenSSLEncrypt(content, CommonSecretKey);
        }

        /// <summary>
        /// 共通鍵で復号化する
        /// </summary>
        public static string DecryptByCommonSecret(string encryptedContent)
        {
            return Protection.OpenSSLDecrypt(encryptedContent, CommonSecretKey);
        }

        /// <summary>
        /// トークン鍵で暗号化する
        /// </summary>
        public static string EncryptByTokenSecret(string content)
        {
            return Protection.OpenSSLEncrypt(content, TokenSecretKey);
        }

        /// <summary>
        /// トークン鍵で復号化する
        /// </summary>
        public static string DecryptByTokenSecret(string encryptedContent)
        {
            return Protection.OpenSSLDecrypt(encryptedContent, TokenSecretKey);
        }

        /// <summary>
        /// 32文字のパスワード生成
        /// </summary>
        public static string CreateSecret()
        {
            return System.Web.Security.Membership.GeneratePassword(32, 0);
        }

        /// <summary>
        /// 一意ID生成
        /// </summary>
        public static int CreateUniqueId()
        {
            var rnd = RandomUtil.UniqueRandom;
            var uniqueId = rnd.Next(1, 999999999);

            return uniqueId;
        }

        /// <summary>
        /// 暗号化トークンを生成
        /// </summary>
        public static string CreateEncryptedToken(int userId, string version, string uuid, DateTimeOffset expires)
        {
            var token = new TokenModel
                {
                    UserId = userId,
                    Version = version,
                    UUID = uuid,
                    Expires = expires
                };

            var tokenJson = new DataContractJsonSerializer(typeof (TokenModel));
            var stream = new MemoryStream();
            tokenJson.WriteObject(stream, token);
            stream.Position = 0;
            var reader = new StreamReader(stream);
            var tokenString = reader.ReadToEnd();
            var encryptedJson = EncryptByTokenSecret(tokenString);

            return encryptedJson;
        }

        /// <summary>
        /// トークンを復号化する
        /// </summary>
        public static TokenModel DecryptToken(string tokenString)
        {
            var serializer = new DataContractJsonSerializer(typeof (TokenModel));
            var jsonString = DecryptByTokenSecret(tokenString);
            var bytes = Encoding.UTF8.GetBytes(jsonString);
            var stream = new MemoryStream(bytes);
            var token = (TokenModel) serializer.ReadObject(stream);

            return token;
        }

        /// <summary>
        /// UserIdとUUIDのペアのチェック
        /// </summary>
        public static AppCode.CheckPairStatus CheckPair(int userId, string encryptedUUID, out string decryptedUUID)
        {
            // Container
            var userDataContainer = new UserDataContainer();

            // RepositoryAdapter
            var userLoginRepositoryAdapter = userDataContainer.UserLogin;

            decryptedUUID = null;
            var user = userLoginRepositoryAdapter.GetDataByPK(userId);

            //ユーザ鍵で復号化
            string decryptedUUIDAndTime;
            try
            {
                decryptedUUIDAndTime = Protection.OpenSSLDecrypt(encryptedUUID, user.Secret);
            }
            catch
            {
                return AppCode.CheckPairStatus.復号化失敗;
            }

            // スプリットして要素が２つ以上あるか(UUID,DateTimeの形)
            var strArray = decryptedUUIDAndTime.Split(',');
            if (strArray.Count() < 2)
                return AppCode.CheckPairStatus.パラメータ不正;

            // 有効期限切れチェック
            var timeResult = CheckTime(strArray[1], false);
            if (!timeResult)
                return AppCode.CheckPairStatus.有効期限切れ;

            // UUIDのチェック
            decryptedUUID = strArray[0];
            return user.UUID == decryptedUUID
                       ? AppCode.CheckPairStatus.正常
                       : AppCode.CheckPairStatus.移行済;
        }

        /// <summary>
        /// 有効期限をチェックする
        /// </summary>
        public static bool CheckExpires(int userId, DateTimeOffset expires)
        {
            var currentTime = GrowDateUtil.GetDateTimeOffsetNow(userId);
            return currentTime.CompareTo(expires) < 0;
        }

        /// <summary>
        /// 時間チェック
        /// </summary>
        /// <param name="time"></param>
        /// <param name="isInitialize"></param>
        /// <returns></returns>
        public static bool CheckTime(string time, bool isInitialize)
        {
            try
            {
                // Initializeの時だけ必要
                var strTime = time;
                if (isInitialize)
                    strTime = DecryptByCommonSecret(time);

                if (time.StartsWith("0"))
                {
                    // 想定としては2013-05-24T18:16:48+09:00が送られるべきところ,西暦→和暦に設定を変えている人からは
                    // 0025-05-24T18:16:48+09:00
                    // が送られてくるので認証に失敗します。クライアント側で対応予定。現在はサーバ側で暫定対応。「午前」「午後」も同様
                    var currentYear = DateUtil.GetDateTimeOffsetNow().Year.ToString(CultureInfo.InvariantCulture);
                    strTime = currentYear + strTime.Substring(4/*2013または0025を削る*/); //2013を付け足す
                }

                DateTimeOffset inputTime;
                try
                {
                    if (!DateTimeOffset.TryParse(strTime, out inputTime))
                    {
                        if (strTime.Contains("午後"))
                        {
                            var offsetPartbase = strTime.Substring(strTime.Length - 6, 6);
                            var offsetPart = offsetPartbase.Substring(0, 1) + offsetPartbase.Substring(2, 2) + ":" + offsetPartbase.Substring(4, 2);
                            var datetimePart = strTime.Substring(0, strTime.Length - 6).Replace("午後", "");

                            inputTime = strTime.Contains("午後12")
                                            ? DateTimeOffset.Parse(datetimePart + offsetPart)
                                            : DateTimeOffset.Parse(datetimePart + offsetPart).AddHours(12);
                        }
                        else if (strTime.Contains("午前"))
                        {
                            if (strTime.Contains("午前12"))
                            {
                                // 例:2013-05-28T午前12:00:58+:0900
                                var offsetPartbase = strTime.Substring(strTime.Length - 6, 6);
                                var offsetPart = offsetPartbase.Substring(0, 1) + offsetPartbase.Substring(2, 2) + ":" + offsetPartbase.Substring(4, 2);
                                var datetimePart = strTime.Substring(0, strTime.Length - 6).Replace("午前12", "00");
                                inputTime = DateTimeOffset.Parse(datetimePart + offsetPart);
                            }
                            else
                            {
                                // 例:2013-05-28T午前07:25:49+:0900
                                var offsetPartbase = strTime.Substring(strTime.Length - 6, 6);
                                var offsetPart = offsetPartbase.Substring(0, 1) + offsetPartbase.Substring(2, 2) + ":" + offsetPartbase.Substring(4, 2);
                                var datetimePart = strTime.Substring(0, strTime.Length - 6).Replace("午前", "");
                                inputTime = DateTimeOffset.Parse(datetimePart + offsetPart);
                            }

                        }
                    }
                }
                catch (FormatException formatException)
                {
                    LoggerManager.DefaultLogger.Error(string.Format("strTime:{0}", strTime), formatException);
                    throw;
                }

                var timeResult = CheckInputTimeByLoginExpires(inputTime);
                return timeResult;
            }
            catch (Exception e)
            {
                LoggerManager.DefaultLogger.ErrorFormat("時刻の復号化に失敗. Exception:" + e);
                return false;
            }
        }

        /// <summary>
        /// クライアントのバージョンが最新かどうかをチェック
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        public static bool CheckVersion(string version)
        {
            var availableVersion = Version.Parse(MobileSettings.AvailableClientVersion);
            var currentVersion = Version.Parse(version);
            return availableVersion.CompareTo(currentVersion) > 0;
        }

        /// <summary>
        /// 対象のUUIDがInvalidUUIDキャッシュに入っていないか確認する
        /// </summary>
        public static bool CheckUUID(string uuid)
        {
            if (uuid == null)
                return false;

            // Debugのみ
            if (MobileSettings.AuthorizationKey == "Bearer-Debug")
                return uuid == AppCode.TestingUUID || InvalidUUIDManager.IsInvalidUUID(uuid);

            return InvalidUUIDManager.IsInvalidUUID(uuid);
        }

        /// <summary>
        /// 有効期限チェック
        /// </summary>
        /// <param name="inputTime"></param>
        /// <returns></returns>
        private static bool CheckInputTimeByLoginExpires(DateTimeOffset inputTime)
        {
            //入力時間のチェック
            //インデックスが配列の境界外です。
            var fromTime = DateUtil.GetDateTimeOffsetNow().AddMinutes(-AppCode.ログイン有効期間);
            var toTime = DateUtil.GetDateTimeOffsetNow().AddMinutes(AppCode.ログイン有効期間);
            return fromTime.CompareTo(inputTime) < 0 && 0 < toTime.CompareTo(inputTime);
        }

        /// <summary>
        /// エラーメッセージを作成
        /// </summary>
        public static string CreateMessage(AppCode.CheckPairStatus checkPairStatus)
        {
            // TODO : ローカライズ対応
            switch (checkPairStatus)
            {
                case AppCode.CheckPairStatus.正常:
                    return @"ゲームデータを他の端末へ移行済みです。アプリを再インストールしてください。";
                case AppCode.CheckPairStatus.復号化失敗:
                    return @"復号化に失敗しました。";
                case AppCode.CheckPairStatus.有効期限切れ:
                    return @"有効期限切れです。端末の時刻が合っていない可能性があります。";
                case AppCode.CheckPairStatus.パラメータ不正:
                    return @"パラメータが不正です。";
                default:
                    return string.Empty;
            }
        }
    }
}