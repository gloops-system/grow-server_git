﻿using System;
using Fango.Core.Util;
using Grow.Core.Application.Authentication;
using Grow.Core.Application.Common;
using Grow.Core.Application.Util;
using Grow.Core.InterfaceModel.Authentication;
using Fango.Web.Core.ApiModel;

namespace Grow.Core.Application.Authentication
{
    public class LoginApiModel : GrowApiModel
    {
        public LoginApiModel(GrowApplicationModel applicationContext)
            : base(applicationContext) { }

        public ContentResponse<LoginViewModel> Login(int userId, Version version, string uuid)
        {
            var contentResponse = new ContentResponse<LoginViewModel>();
            var strVersion = version.ToString();

            // クライアントのバージョンチェック
            var needUpgrade = AuthenticationUtil.CheckVersion(strVersion);
            if (needUpgrade)
            {
                contentResponse.Code = (int) AppCode.ApiResultCode.クライアント更新;
                contentResponse.Message = @"クライアントの更新が必要です。"; // TODO : ローカライズ対応

                return contentResponse;
            }

            // 復号化チェック
            string decryptedUUID;
            var checkPairStatus = AuthenticationUtil.CheckPair(userId, uuid, out decryptedUUID);
            if (checkPairStatus != AppCode.CheckPairStatus.正常)
            {
                contentResponse.Code = (int) AppCode.ApiResultCode.失敗;
                contentResponse.Message = AuthenticationUtil.CreateMessage(checkPairStatus);

                return contentResponse;
            }
            
            // トークン生成
            var expires = DateUtil.GetDateTimeOffsetNow().AddMinutes(AppCode.ログイン有効期間);
            var encryptedJson = AuthenticationUtil.CreateEncryptedToken(userId, strVersion, decryptedUUID, expires);

            // パッチ有無
            // TODO : 実装

            contentResponse.AppData = new LoginViewModel(encryptedJson);

            return contentResponse;
        }
    }
}