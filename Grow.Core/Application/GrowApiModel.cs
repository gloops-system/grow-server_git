﻿namespace Grow.Core.Application
{
    public class GrowApiModel : Fango.Core.Application.ApiModel
    {
        public new GrowApplicationModel ApplicationContext
        {
            get { return (GrowApplicationModel) base.ApplicationContext; }
        }

        public GrowApiModel(GrowApplicationModel applicationContext)
            : base(applicationContext)
        {
        }
    }
}
