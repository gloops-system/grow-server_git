﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Grow.Core.Common;
using Grow.Data.Duty.DataSet.Container;
using Grow.Data.User.DataSet.Container;
using Fango.Core.Util;
using Grow.Data.User.DataSet;

namespace Grow.Core.Application.Duty
{
    public class LabyrinthManager
    {

        public void RunMatchGroup()
        {
            CalculateUserStrength();
            DoMatchingGroup();
            OrganizeGroupMatch();
        }

        public void RunMatchGroupNewUsers(List<int> userIds)
        {
            var userDutyDataContainer = new UserLabyrinthDataContainer();
            var matchGroup = userDutyDataContainer.MatchGroup.GetLastestGroup();
            var matchGroupId = matchGroup.MatchGroupId;
            var matchGroupUserNum = matchGroup.UserNum;
            while (userIds.Count > 0)
            {
                var userId = userIds[0];
                var userMatchGroup = userDutyDataContainer.UserMatchGroup.GetUserGroupByUserId(userId);
                if (userMatchGroup == null)
                {
                    if (matchGroupUserNum >= 15)
                    {
                        matchGroupUserNum = 0;
                        matchGroupId = userDutyDataContainer.MatchGroup.AddNew(0, 1000);
                        OrganizeGroupMatch(matchGroupId);
                    }
                    userDutyDataContainer.UserMatchGroup.AddNew(userId, (int)matchGroupId);
                    matchGroupUserNum++;
                    userDutyDataContainer.MatchGroup.UpdateUserNum(matchGroupId, matchGroupUserNum);
                }
                userIds.RemoveAt(0);
            }
        }

        public void ActiveFeverTime(DutyDataContainer dutyDataContainer, DateTimeOffset now)
        {
            dutyDataContainer.FeverTime.DeleteAllData();
            var startTime = now.AddMinutes(10);
            var endTime = now.AddHours(1);
            dutyDataContainer.FeverTime.AddNew(startTime, endTime, 1, 50, 10, 10, 10, 2, 2, 10, 10, 10, 10, 10, 10, 10, now);
        }

        public void RemoveFeverTime(DutyDataContainer dutyDataContainer, DateTimeOffset now)
        {
            var feverTime = dutyDataContainer.FeverTime.GetActiveFeverByEventId(1, now);
            if (feverTime != null)
            {
                var endTime = now.AddMinutes(10);
                dutyDataContainer.FeverTime.DeleteAllData();
                dutyDataContainer.FeverTime.AddNew(feverTime.StartTime, endTime, feverTime.EventId, feverTime.LessStamina, feverTime.AttackIncrease, feverTime.CriticalHit,
                                    feverTime.ObtainableGold, feverTime.MoreMana, feverTime.MoreTreasure, feverTime.DropMoreGold, feverTime.DropMoreGear, feverTime.DropMoreWeapon,
                                    feverTime.DropMoreShield, feverTime.DropMoreAccessory, feverTime.ChanceCapturingMonster, feverTime.ScoreIncrease, now);
            }
        }

        private void CalculateUserStrength()
        {
            var userDutyDataContainer = new UserLabyrinthDataContainer();
            userDutyDataContainer.MatchGroup.CalculateUserStrength();
        }

        private void DoMatchingGroup()
        {
            var userDutyDataContainer = new UserLabyrinthDataContainer();
            userDutyDataContainer.MatchGroup.DoMatchingGroup();
        }

        private void OrganizeGroupMatch()
        {
            var listFloorId = new List<int>();
            var listMapId = new List<int>();
            var dutyDataContainer = new DutyDataContainer();
            var userDataContainer = new UserDataContainer();
            var userDutyDataContainer = new UserLabyrinthDataContainer();
            var floorMasterApater = dutyDataContainer.FloorMaster;
            var areaMasters = dutyDataContainer.AreaMaster.GetDataByEventId((int)QuestEventId.LABYRINTH);
            // Cleanup User Labyrinth data
            dutyDataContainer.UserArea.DeleteAllDataByEventId((int)QuestEventId.LABYRINTH);
            dutyDataContainer.UserAreaHistory.DeleteAllDataByEventId((int)QuestEventId.LABYRINTH);
            dutyDataContainer.UserFloor.DeleteAllDataByEventId((int)QuestEventId.LABYRINTH);            
            foreach (var areaMaster in areaMasters)
            {
                var floorMasters = floorMasterApater.GetDataByAreaId(areaMaster.AreaId);
                foreach (var floorMaster in floorMasters)
                {
                    listFloorId.Add(floorMaster.FloorId);
                }
            }
            var mapMasters = dutyDataContainer.MapMaster.GetEventMaps(1);
            foreach (var mapMaster in mapMasters)
            {
                listMapId.Add(mapMaster.MapId);
            }

            int numberFloor = listFloorId.Count;
            int numberMap = listMapId.Count;
            var matchGroupAdapter = userDutyDataContainer.MatchGroup;
            var matchGroupFloorMapAdapter = dutyDataContainer.MatchGroupFloorMap;
            long lastMatchGroupId = 0;
            var matchGroups = new List<UserLabyrinthDataSet.MatchGroupRow>();
            //TODO: Only work around now for haven't not enough master data
            if (numberMap < numberFloor)
            {
                for (var i = 0; i < numberFloor - numberMap; i++)
                {
                    var randomId = listMapId[numberMap - (numberFloor - numberMap) + i];
                    listMapId.Add(randomId);
                }
                numberMap = numberFloor;
            }
            //End work around
            var now = DateUtil.GetDateTimeOffsetNow();
            while (matchGroups.Count > 0 || lastMatchGroupId == 0)
            {
                matchGroups = matchGroupAdapter.GetListGroupPaging(10, lastMatchGroupId);
                if (matchGroups.Count > 0)
                {
                    foreach (var matchGroup in matchGroups)
                    {
                        var mapIndexs = getRandomIndex(numberFloor, numberMap);
                        var idx = 0;
                        foreach (var mapIndex in mapIndexs)
                        {
                            matchGroupFloorMapAdapter.AddNew(matchGroup.MatchGroupId, listFloorId[idx++], listMapId[mapIndex], now);
                        }
                        lastMatchGroupId = matchGroup.MatchGroupId;
                    }
                }
                else if (lastMatchGroupId == 0)
                {
                    lastMatchGroupId = 1;
                }
            }
        }


        private void OrganizeGroupMatch(long matchGroupId)
        {
            var listFloorId = new List<int>();
            var listMapId = new List<int>();
            var dutyDataContainer = new DutyDataContainer();
            var userDataContainer = new UserDataContainer();
            var userDutyDataContainer = new UserLabyrinthDataContainer();
            var floorMasterApater = dutyDataContainer.FloorMaster;
            var areaMasters = dutyDataContainer.AreaMaster.GetDataByEventId((int)QuestEventId.LABYRINTH);
            foreach (var areaMaster in areaMasters)
            {
                var floorMasters = floorMasterApater.GetDataByAreaId(areaMaster.AreaId);
                foreach (var floorMaster in floorMasters)
                {
                    listFloorId.Add(floorMaster.FloorId);
                }
            }
            var mapMasters = dutyDataContainer.MapMaster.GetEventMaps(1);
            foreach (var mapMaster in mapMasters)
            {
                listMapId.Add(mapMaster.MapId);
            }

            int numberFloor = listFloorId.Count;
            int numberMap = listMapId.Count;
            var matchGroupFloorMapAdapter = dutyDataContainer.MatchGroupFloorMap;
            //TODO: Only work around now for haven't not enough master data
            if (numberMap < numberFloor)
            {
                for (var i = 0; i < numberFloor - numberMap; i++)
                {
                    var randomId = listMapId[numberMap - (numberFloor - numberMap) + i];
                    listMapId.Add(randomId);
                }
                numberMap = numberFloor;
            }
            //End work around
            var now = DateUtil.GetDateTimeOffsetNow();
            var mapIndexs = getRandomIndex(numberFloor, numberMap);
            var idx = 0;
            foreach (var mapIndex in mapIndexs)
            {
                matchGroupFloorMapAdapter.AddNew(matchGroupId, listFloorId[idx++], listMapId[mapIndex], now);
            }
        }

        private int[] getRandomIndex(int numItem, int totalItem)
        {
            int numberRemoveItem = totalItem - numItem;
            int[] removeItemIndexs = new int[numberRemoveItem];
            int[] randomIndexs = new int[numItem];
            int currentIndex = 0;
            Random rnd = new Random();
            while (numberRemoveItem > 0)
            {
                var removeIndex = rnd.Next(totalItem);
                if (Array.IndexOf(removeItemIndexs, removeIndex) == -1)
                {
                    removeItemIndexs[--numberRemoveItem] = removeIndex;
                }
            }
            for (var idx = 0; idx < totalItem; idx++)
            {
                if (Array.IndexOf(removeItemIndexs, idx) > -1)
                {
                    continue;
                }
                else
                {
                    randomIndexs[currentIndex++] = idx;
                }
            }
            return randomIndexs;
        }
    }
}
