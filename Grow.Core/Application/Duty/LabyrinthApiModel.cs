﻿using Grow.Core.Application;
using Grow.Core.Common;
using Grow.Core.Application.Duty;
using Grow.Core.Application.Monster;
using Grow.Core.InterfaceModel.Duty;
using Grow.Core.InterfaceModel.Monster;
using Fango.Web.Core.ApiModel;

namespace Grow.Core.Application.Duty
{
    public class LabyrinthApiModel : GrowApiModel
    {
        public LabyrinthApiModel(GrowApplicationModel applicationContext)
            : base(applicationContext) { }

        public ContentResponse<LabyrinthCampViewModel> LabyrinthCamp()
        {
            var contentResponse = new ContentResponse<LabyrinthCampViewModel>();            
            var user = ApplicationContext.CurrentUser;
            var now = ApplicationContext.Now;
            var resultData = new LabyrinthUser(ApplicationContext).LabyrinthCamp();
            contentResponse.AppData = resultData;
            return contentResponse;
        }

        public ContentResponse<LabyrinthStartViewModel> LabyrinthStart(LabyrinthStartRequestViewModel requestModel)
        {
            var contentResponse = new ContentResponse<LabyrinthStartViewModel>();
            var user = ApplicationContext.CurrentUser;
            var now = ApplicationContext.Now;
            var resultData = new LabyrinthUser(ApplicationContext).LabyrinthStart(requestModel);
            contentResponse.AppData = resultData;
            return contentResponse;
        }

        public ContentResponse<LabyrinthStartFieldViewModel> LabyrinthStartField(int requireStamina)
        {
            var contentResponse = new ContentResponse<LabyrinthStartFieldViewModel>();
            var user = ApplicationContext.CurrentUser;
            var now = ApplicationContext.Now;
            var resultData = new LabyrinthUser(ApplicationContext).LabyrinthStartField(requireStamina);
            contentResponse.AppData = resultData;
            return contentResponse;
        }

        public ContentResponse<LabyrinthFinishFieldViewModel> LabyrinthFinishField(LabyrinthFinishFieldRequestViewModel requestModel)
        {
            var contentResponse = new ContentResponse<LabyrinthFinishFieldViewModel>();
            var user = ApplicationContext.CurrentUser;
            var now = ApplicationContext.Now;
            var resultData = new LabyrinthUser(ApplicationContext).LabyrinthFinishField(requestModel);
            contentResponse.AppData = resultData;
            return contentResponse;
        }

        public ContentResponse<LabyrinthFinishAreaViewModel> LabyrinthFinishArea(LabyrinthFinishAreaRequestViewModel requestModel)
        {
            var contentResponse = new ContentResponse<LabyrinthFinishAreaViewModel>();
            var user = ApplicationContext.CurrentUser;
            var now = ApplicationContext.Now;
            var resultData = new LabyrinthUser(ApplicationContext).LabyrinthFinishArea(requestModel);
            contentResponse.AppData = resultData;
            return contentResponse;
        }

        public ContentResponse<RecoveryStaminaViewModel> RecoveryStamina(int requireGem)
        {
            var contentResponse = new ContentResponse<RecoveryStaminaViewModel>();
            var user = ApplicationContext.CurrentUser;
            var now = ApplicationContext.Now;
            try
            {
                var resultData = new QuestUser(ApplicationContext).RecoveryStamina(requireGem);
                contentResponse.AppData = resultData;
            }
            catch (ApiException ex)
            {
                contentResponse.Code = (int)ex.Code;
                contentResponse.Message = ex.Message;

            }
            return contentResponse;
        }

        public ContentResponse MeetDispatchMonster(long[] userMonsterIds)
        {
            var contentResponse = new ContentResponse();
            var user = ApplicationContext.CurrentUser;
            var now = ApplicationContext.Now;
            new LabyrinthUser(ApplicationContext).MeetDispatchMonsters(userMonsterIds);            
            //contentResponse.AppData = resultData;
            return contentResponse;
        }

        public ContentResponse MatchGroup()
        {
            var contentResponse = new ContentResponse();
            var user = ApplicationContext.CurrentUser;
            new LabyrinthManager().RunMatchGroup();
            return contentResponse;
        }

        public ContentResponse ActiveFeverTime()
        {
            var contentResponse = new ContentResponse();
            var user = ApplicationContext.CurrentUser;
            new LabyrinthManager().ActiveFeverTime(ApplicationContext.DutyDataContainer, ApplicationContext.Now);
            return contentResponse;
        }

        public ContentResponse RemoveFeverTime()
        {
            var contentResponse = new ContentResponse();
            var user = ApplicationContext.CurrentUser;
            new LabyrinthManager().RemoveFeverTime(ApplicationContext.DutyDataContainer, ApplicationContext.Now);
            return contentResponse;
        }
    }
}