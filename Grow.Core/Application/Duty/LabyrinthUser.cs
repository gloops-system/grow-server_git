﻿using Grow.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grow.Data.User.DataSet;
using Fango.Core.Util;
using Grow.Data.Duty.DataSet;
using Grow.Core.InterfaceModel.Duty;
using Grow.Core.InterfaceModel.User;
using Grow.Core.Application.Avatar;
using Grow.Core.Application.Common;
using Grow.Core.InterfaceModel.Monster;
using Grow.Core.Application.Monster;
using Grow.Core.Application.User;

namespace Grow.Core.Application.Duty
{
    public class LabyrinthUser : QuestUser
    {
        private List<LabyrinthAreaModel> _listArea;

        public List<LabyrinthAreaModel> ListArea
        {
            get { return _listArea ?? (_listArea = GetListArea()); }
        }

        public UserLabyrinthDataSet.UserMatchGroupRow UserMatchGroup { get; set; }

        public LabyrinthUser(GrowApplicationModel applicationContext)
            : base(applicationContext)
        {
            EventId = (int)AppCode.QuestEventId.LABYRINTH;
            UserMatchGroup = ApplicationContext.UserLabyrinthDataContainer.UserMatchGroup.GetUserGroupByUserId(User.UserId);
        }

        public LabyrinthCampViewModel LabyrinthCamp()
        {
            var labyrinthCampModel = new LabyrinthCampViewModel();
            labyrinthCampModel.CurrentArea = GetCurrentArea();
            labyrinthCampModel.GlobalRanking = GetGlobalLeaderboard();
            labyrinthCampModel.RivalsRanking = GetRivalsLeaderboard();
            labyrinthCampModel.UserMonsters = GetListUserMonsterDispatched();
            labyrinthCampModel.PlayerMonsters = GetListMeetMonsterDispatched();
            labyrinthCampModel.UserNotifications = GetUpdateNotification();
            labyrinthCampModel.FeverTime = GetFeverTime();
            labyrinthCampModel.Equipments = GetUserEquipment();
            labyrinthCampModel.CurrentScore = ApplicationContext.DutyDataContainer.UserArea.GetLabyrinthUserTotalPoint(User.UserId);
            return labyrinthCampModel;
        }

        public LabyrinthStartViewModel LabyrinthStart(LabyrinthStartRequestViewModel requestModel)
        {
            // Update userEquipment
            if (requestModel.Gears != null && requestModel.Gears.Length > 0)
            {
                UpdateUserEquipment(requestModel);
            }
            var labyrinthStartModel = new LabyrinthStartViewModel();
            labyrinthStartModel.Life = User.Userinfo.Life - 1;
            labyrinthStartModel.LifeLastHealTime = DateUtil.ToUnixTime(User.Userinfo.LifeLastHealTime);
            ApplicationContext.UserDataContainer.UserStatus.UpdateLifeByPK(User.UserId, -1);
            var areaMasters = ApplicationContext.DutyDataContainer.AreaMaster.GetDataByEventId(EventId);
            var userAreaAdapter = ApplicationContext.DutyDataContainer.UserArea;
            var userArea = userAreaAdapter.GetDataByUserIdAndAreaId(User.UserId, areaMasters[0].AreaId);
            if (userArea == null)
            {
                userAreaAdapter.AddNew(User.UserId, areaMasters[0].AreaId, areaMasters[0].StageId, areaMasters[0].EventId, 0, (short)AppCode.AreaStatus.ACTIVE);
            }
            labyrinthStartModel.UserMonsters = GetListUserMonsterDispatched();
            labyrinthStartModel.PlayerMonsters = GetListMeetMonsterDispatched();
            labyrinthStartModel.FeverTime = GetFeverTime();
            return labyrinthStartModel;
        }

        public LabyrinthStartFieldViewModel LabyrinthStartField(int requireStamina)
        {
            var labyrinthStartFieldModel = new LabyrinthStartFieldViewModel();
            labyrinthStartFieldModel.Stamina = User.Userinfo.Stamina - requireStamina;
            labyrinthStartFieldModel.StaminaLastHealTime = DateUtil.ToUnixTime(User.Userinfo.StaminaLastHealTime);
            ApplicationContext.UserDataContainer.UserStatus.UpdateStaminaByPK(User.UserId, -1 * requireStamina);
            labyrinthStartFieldModel.UserMonsters = GetListUserMonsterDispatched();
            labyrinthStartFieldModel.PlayerMonsters = GetListMeetMonsterDispatched();
            labyrinthStartFieldModel.FeverTime = GetFeverTime();
            return labyrinthStartFieldModel;
        }

        public LabyrinthFinishFieldViewModel LabyrinthFinishField(LabyrinthFinishFieldRequestViewModel requestModel)
        {
            var labyrinthFinishField = new LabyrinthFinishFieldViewModel();
            UpdateFloor(requestModel);
            if (requestModel.MonsterList != null && requestModel.MonsterList.Length > 0)
            {
                UpdatePlayerDispatchMonsterStatus(requestModel.MonsterList);
            }
            labyrinthFinishField.UserMonsters = GetListUserMonsterDispatched();
            labyrinthFinishField.PlayerMonsters = GetListMeetMonsterDispatched();
            labyrinthFinishField.FeverTime = GetFeverTime();
            return labyrinthFinishField;
        }

        public LabyrinthFinishAreaViewModel LabyrinthFinishArea(LabyrinthFinishAreaRequestViewModel requestModel)
        {
            if (requestModel.MonsterList != null && requestModel.MonsterList.Length > 0)
            {
                UpdatePlayerDispatchMonsterStatus(requestModel.MonsterList);
            }
            var finishAreaModel = FinishArea(requestModel);
            var labyrinthFinishArea = new LabyrinthFinishAreaViewModel(finishAreaModel);
            labyrinthFinishArea.UserMonsters = GetListUserMonsterDispatched();
            labyrinthFinishArea.PlayerMonsters = GetListMeetMonsterDispatched();
            labyrinthFinishArea.FeverTime = GetFeverTime();
            return labyrinthFinishArea;
        }

        public void MeetDispatchMonsters(long[] userMonsterIds)
        {
            foreach (var userMonsterId in userMonsterIds)
            {
                var userMonster = ApplicationContext.UserDataContainer.UserMonster.GetDataByPK(userMonsterId);
                if (userMonster != null)
                {
                    var userNotification = new UserNotification(ApplicationContext);
                    userNotification.AddNew(User.UserId, User.Userinfo.NickName + " meet your monster in Labyrinth",
                                            (short)AppCode.NotificationCategory.DISPATCH_MONSTER,
                                            userMonster.UserMonsterId);
                }
            }
        }

        public UserLabyrinthModel LoadUserLabyrinthInfo()
        {
            var labyrinthModel = new UserLabyrinthModel();
            if (UserMatchGroup != null)
            {
                labyrinthModel.Point = UserMatchGroup.Point;
            }
            labyrinthModel.LastAreaId = ApplicationContext.CurrentUser.Userinfo.LastLabyrinthAreaId;
            if (labyrinthModel.LastAreaId == 0)
            {
                labyrinthModel.LastAreaId = ApplicationContext.DutyDataContainer.AreaMaster.GetDataByEventId((int)AppCode.QuestEventId.LABYRINTH)[0].AreaId;
            }
            return labyrinthModel;
        }

        public FinishAreaViewModel FinishArea(LabyrinthFinishAreaRequestViewModel requestModel)
        {
            var finishAreaModel = new FinishAreaViewModel();
            var isFirstSuccess = UpdateArea(requestModel);
            finishAreaModel.UserItem = UseItems(requestModel.ItemList);
            //Update Avatar status
            var avatarUser = new AvatarUser(ApplicationContext);
            if (requestModel.Avatar != null)
            {
                finishAreaModel.UserAvatar = avatarUser.UpdateAvatarExp(requestModel.Avatar.AvatarUserId, requestModel.Avatar.Exp);
            }
            if (isFirstSuccess == false)
            {
                requestModel.Exp = 0;
            }
            finishAreaModel.UserStatus = UpdateUseStatus(requestModel);
            return finishAreaModel;
        }

        private void UpdateFloor(LabyrinthFinishFieldRequestViewModel requestModel)
        {
            var currentArea = ListArea[requestModel.AreaIdx];
            if (currentArea.FloorIds.Exists(element => element == requestModel.Floor.FloorId))
            {
                var finishFieldModel = new FinishFieldModel()
                {
                    AreaId = currentArea.AreaId,
                    FloorId = requestModel.Floor.FloorId,
                    Score = requestModel.Floor.Score,
                    RankScore = requestModel.Floor.RankScore,
                    RankTime = requestModel.Floor.RankTime,
                    RankMonster = requestModel.Floor.RankMonster
                };
                UpdateFinishFloor(finishFieldModel);
            }
            var userCollectItemApdater = ApplicationContext.DutyDataContainer.UserCollectItem;
            userCollectItemApdater.DeleteDataByUserIdAreaIdAndFloorId(User.UserId, currentArea.AreaId, requestModel.Floor.FloorId);
            if (requestModel.CollectItemList != null && requestModel.CollectItemList.Length > 0)
            {
                var userCollectItemDataTable = new DutyDataSet.UserCollectItemDataTable();
                foreach (var collectItem in requestModel.CollectItemList)
                {
                    var userCollectItemRow = userCollectItemDataTable.NewUserCollectItemRow();
                    userCollectItemRow.UserId = User.UserId;
                    userCollectItemRow.AreaId = currentArea.AreaId;
                    userCollectItemRow.FloorId = requestModel.Floor.FloorId;
                    userCollectItemRow.ItemId = collectItem.ItemId;
                    userCollectItemRow.Num = collectItem.Number;
                    userCollectItemRow.AddTime = ApplicationContext.Now;
                    userCollectItemDataTable.AddUserCollectItemRow(userCollectItemRow);
                }
                userCollectItemApdater.BulkInsert(userCollectItemDataTable);
            }
        }

        private bool UpdateArea(LabyrinthFinishAreaRequestViewModel requestModel)
        {
            var isFirstSuccess = false;
            var currentArea = ListArea[requestModel.AreaIdx];
            var currentAreaId = currentArea.AreaId;
            if (requestModel.FloorList != null && requestModel.FloorList.Length > 0)
            {
                foreach (var floor in requestModel.FloorList)
                {
                    var floorFinishModel = new LabyrinthFinishFieldRequestViewModel();
                    floorFinishModel.Floor = floor;
                    floorFinishModel.AreaIdx = requestModel.AreaIdx;
                    UpdateFloor(floorFinishModel);
                }
            }
            var userAreaAdapter = ApplicationContext.DutyDataContainer.UserArea;
            var userArea = userAreaAdapter.GetDataByUserIdAndAreaId(User.UserId, currentAreaId);
            //var areaMaster = ApplicationContext.DutyDataContainer.AreaMaster.GetDataByAreaId(currentAreaId);
            var userFloorAdapter = ApplicationContext.DutyDataContainer.UserFloor;
            var userFloors = userFloorAdapter.GetDataByUserIdAndAreaId(User.UserId, currentAreaId);
            var totalScore = 0;
            foreach (var userFloor in userFloors)
            {
                totalScore += userFloor.Point;
            }
            // Finish area
            if (userFloors.Length == currentArea.FloorIds.Count)
            {
                if (userArea.Status != (short)AppCode.AreaStatus.CLEARED)
                {
                    isFirstSuccess = true;
                }
                userAreaAdapter.UpdateByKey(userArea.UserAreaId, totalScore, (short)AppCode.AreaStatus.CLEARED, ApplicationContext.Now);
                // Check whether last area or not?
                if (currentAreaId == ListArea[ListArea.Count - 1].AreaId)
                {
                    // It is last floor
                    ResetLabyrinthPath();
                }
                else
                {
                    var nextArea = ListArea.FirstOrDefault(element => element.AreaId > currentAreaId);
                    var nextAreaMaster = ApplicationContext.DutyDataContainer.AreaMaster.GetDataByAreaId(nextArea.AreaId);
                    userAreaAdapter.AddNew(User.UserId, nextAreaMaster.AreaId, nextAreaMaster.StageId, nextAreaMaster.EventId, 0, (short)AppCode.AreaStatus.ACTIVE);
                    User.Userinfo.LastLabyrinthAreaId = nextAreaMaster.AreaId;
                }
            }
            // Give up
            else
            {
                if (userArea != null && totalScore > 0)
                {
                    userAreaAdapter.UpdateByKey(userArea.UserAreaId, totalScore, (short)AppCode.AreaStatus.ACTIVE, ApplicationContext.Now);
                }
                ResetLabyrinthPath();
            }
            // Check again is first time success from history
            if (isFirstSuccess)
            {
                var numCompleted = ApplicationContext.DutyDataContainer.UserAreaHistory.CountUserIdAndAreaId(User.UserId, currentAreaId, (short)AppCode.AreaStatus.CLEARED);
                if (numCompleted > 0)
                {
                    isFirstSuccess = false;
                }
            }
            return isFirstSuccess;
        }

        private void ResetLabyrinthPath()
        {
            //Update group score
            UpdateUserPoint();
            var userAreaAdapter = ApplicationContext.DutyDataContainer.UserArea;
            var userFloorAdapter = ApplicationContext.DutyDataContainer.UserFloor;
            // TODO: Save data to UserAreaHistory and UserFloorHistory
            var userAreaHistoryDataTable = new DutyDataSet.UserAreaHistoryDataTable();
            var userAreas = userAreaAdapter.GetDataByUserIdAndEventId(User.UserId, EventId);
            foreach (var userArea in userAreas)
            {
                var userAreaHistoryRow = userAreaHistoryDataTable.NewUserAreaHistoryRow();
                userAreaHistoryRow.UserId = User.UserId;
                userAreaHistoryRow.AreaId = userArea.AreaId;
                userAreaHistoryRow.StageId = userArea.StageId;
                userAreaHistoryRow.EventId = userArea.EventId;
                userAreaHistoryRow.Point = userArea.Point;
                userAreaHistoryRow.Status = userArea.Status;
                userAreaHistoryRow.ClearDate = userArea.ClearDate;
                userAreaHistoryRow.AddTime = ApplicationContext.Now;
                userAreaHistoryDataTable.AddUserAreaHistoryRow(userAreaHistoryRow);
            }
            ApplicationContext.DutyDataContainer.UserAreaHistory.BulkInsert(userAreaHistoryDataTable);

            // Delete all area and floor
            userAreaAdapter.DeleteByUserIdAndEventId(User.UserId, EventId);
            userFloorAdapter.DeleteByUserIdAndEventId(User.UserId, EventId);
        }

        private LabyrinthAreaViewModel GetCurrentArea()
        {
            var userAreaAdapter = ApplicationContext.DutyDataContainer.UserArea;
            var userAreas = userAreaAdapter.GetDataByUserIdAndEventId(User.UserId, (int)AppCode.QuestEventId.LABYRINTH);
            LabyrinthAreaModel currentArea = null;
            LabyrinthAreaViewModel areaViewModel = new LabyrinthAreaViewModel();
            var listArea = ListArea;
            // TODO: Handle in case user not matching group
            if (listArea == null)
            {
                areaViewModel.AreaIdx = -1;
                return areaViewModel;
            }
            var lastAreaId = 0;
            var currentAreaIdx = 0;
            var currentFloorIdx = -1;
            Grow.Data.Duty.DataSet.DutyDataSet.UserFloorRow[] userFloors = null;
            var userAreasLen = 0;
            var userFloorLen = 0;
            if (userAreas != null && userAreas.Length > 0)
            {
                userAreasLen = userAreas.Length;
                lastAreaId = userAreas[userAreasLen - 1].AreaId;
                foreach (var areaModel in listArea)
                {
                    if (lastAreaId == areaModel.AreaId)
                    {
                        currentArea = areaModel;
                        break;
                    }
                    currentAreaIdx++;
                }
                userFloors = ApplicationContext.DutyDataContainer.UserFloor.GetDataByUserIdAndAreaId(User.UserId, lastAreaId);
                // Work around in case there is an error that couldn't insert UserArea
                if (userFloors.Length == currentArea.FloorIds.Count && currentAreaIdx != listArea.Count - 1)
                {
                    var notFinishField = userFloors.First(element => element.Status == (short)AppCode.FieldStatus.ACTIVE);
                    if (notFinishField == null)
                    {
                        currentAreaIdx++;
                        currentArea = listArea[currentAreaIdx];
                        lastAreaId = currentArea.AreaId;
                        userFloors = null;
                        var areaMaster = ApplicationContext.DutyDataContainer.AreaMaster.GetDataByAreaId(lastAreaId);
                        userAreaAdapter.AddNew(User.UserId, areaMaster.AreaId, areaMaster.StageId, areaMaster.EventId, 0, (short)AppCode.AreaStatus.ACTIVE);
                    }
                }
            }
            else
            {
                currentArea = listArea[0];
            }
            var listFloorModel = new List<LabyrinthFloorViewModel>();
            areaViewModel.AreaIdx = currentAreaIdx;
            foreach (var floorId in currentArea.FloorIds)
            {
                var floorModel = new LabyrinthFloorViewModel();
                floorModel.FloorId = floorId;
                floorModel.Score = 0;
                if (userFloors != null && userFloors.Length > 0)
                {
                    userFloorLen = userFloors.Length;
                    foreach (var userFloor in userFloors)
                    {
                        if (userFloor.FloorId == floorId)
                        {
                            floorModel.Score = userFloor.Point;
                            floorModel.RankTime = userFloor.RankTime;
                            floorModel.RankScore = userFloor.RankScore;
                            floorModel.RankMonster = userFloor.RankMonster;
                            break;
                        }
                    }
                }
                listFloorModel.Add(floorModel);
            }
            areaViewModel.Floors = listFloorModel;
            if (userAreasLen > 0)
            {
                currentFloorIdx = userFloorLen;
            }
            areaViewModel.CurrentFloorIdx = currentFloorIdx;
            if (currentAreaIdx == listArea.Count - 1)
            {
                areaViewModel.IsFinalArea = true;
            }
            else
            {
                areaViewModel.IsFinalArea = false;
            }
            var numCompleted = ApplicationContext.DutyDataContainer.UserAreaHistory.CountUserIdAndAreaId(User.UserId, currentArea.AreaId, (short)AppCode.AreaStatus.CLEARED);
            if (numCompleted > 0)
            {
                areaViewModel.IsCompleted = true;
            }
            else
            {
                areaViewModel.IsCompleted = false;
            }
            // Get collect item list
            var collectItemList = new List<ItemRequestModel>();
            var items = ApplicationContext.DutyDataContainer.UserCollectItem.GetDataByUserIdAndAreaId(User.UserId, currentArea.AreaId);
            foreach (var item in items)
            {
                var collectItem = collectItemList.FirstOrDefault(i => i.ItemId == item.ItemId);
                if (collectItem != null)
                {
                    collectItem.Number += item.Num;
                }
                else
                {
                    collectItemList.Add(new ItemRequestModel()
                    {
                        ItemId = item.ItemId,
                        Number = item.Num
                    });
                }
            }
            areaViewModel.CollectItemList = collectItemList;
            areaViewModel.MonsterList = new List<DefeatMonsterModel>();
            return areaViewModel;
        }

        private List<LabyrinthAreaModel> GetListArea()
        {
            var listAreas = new List<LabyrinthAreaModel>();
            if (UserMatchGroup == null)
            {
                return null;
            }
            var areaMasters = ApplicationContext.DutyDataContainer.AreaMaster.GetDataByEventId(EventId);
            var floorMasterAdapter = ApplicationContext.DutyDataContainer.FloorMaster;
            var floorMapMasters = ApplicationContext.DutyDataContainer.MatchGroupFloorMap.GetDataByMatchGroupId(UserMatchGroup.MatchGroupId);
            foreach (var areaMaster in areaMasters)
            {
                var areaModel = new LabyrinthAreaModel();
                areaModel.AreaId = areaMaster.AreaId;
                areaModel.FloorIds = new List<int>();
                var floorMasters = floorMasterAdapter.GetDataByAreaId(areaMaster.AreaId);
                foreach (var floorMaster in floorMasters)
                {
                    var mapId =
                        from f in floorMapMasters
                        where f.FloorId == floorMaster.FloorId
                        select f.MapId;
                    areaModel.FloorIds.Add(mapId.FirstOrDefault());
                }
                listAreas.Add(areaModel);
            }
            return listAreas;
        }

        private UserMonsterModel[] GetListUserMonsterDispatched()
        {
            var monsterUser = new MonsterUser(ApplicationContext);
            return monsterUser.GetUserMonsters().ToArray();
        }

        private DispatchMonsterModel[] GetListMeetMonsterDispatched()
        {
            var dispatchMonsterUser = new DispatchMonsterUser(ApplicationContext);
            var dispatchMonsterModels = dispatchMonsterUser.GetMeetMonsterList().ToArray();
            return dispatchMonsterModels;
        }

        private List<UserNotificationModel> GetUpdateNotification()
        {
            var userNotification = new UserNotification(ApplicationContext);
            return userNotification.GetUpdateNotification();
        }

        private List<LeaderboardUserModel> GetGlobalLeaderboard()
        {
            var adapter = ApplicationContext.UserLabyrinthDataContainer.UserMatchGroupLeaderboard;
            var userMatchGroupLeaderboards = adapter.GetGlobalLeaderboard(10);
            return ParseToLeaderboardUserModel(userMatchGroupLeaderboards);
        }

        private List<LeaderboardUserModel> GetRivalsLeaderboard()
        {
            var adapter = ApplicationContext.UserLabyrinthDataContainer.UserMatchGroupLeaderboard;
            var userMatchGroupLeaderboards = adapter.GetRivalsLeaderboard(User.UserId);
            return ParseToLeaderboardUserModel(userMatchGroupLeaderboards);
        }

        private List<LeaderboardUserModel> ParseToLeaderboardUserModel(UserLabyrinthDataSet.UserMatchGroupLeaderboardRow[] userMatchGroupLeaderboards)
        {
            var listUserLeaderboard = new List<LeaderboardUserModel>();
            var ranking = 1;
            foreach (var userMatchGroupLeaderboard in userMatchGroupLeaderboards)
            {
                var leaderboardUserModel = new LeaderboardUserModel()
                {
                    UserId = userMatchGroupLeaderboard.UserId,
                    Level = userMatchGroupLeaderboard.Level,
                    NickName = userMatchGroupLeaderboard.NickName,
                    AvatarId = userMatchGroupLeaderboard.AvatarId,
                    GearId = userMatchGroupLeaderboard.GearId,
                    Point = userMatchGroupLeaderboard.Point,
                    Ranking = ranking++,
                };
                listUserLeaderboard.Add(leaderboardUserModel);
            }
            return listUserLeaderboard;
        }

        private int UpdateUserPoint()
        {
            var totalPoint = ApplicationContext.DutyDataContainer.UserArea.GetLabyrinthUserTotalPoint(User.UserId);
            if (UserMatchGroup != null && UserMatchGroup.Point < totalPoint)
            {
                ApplicationContext.UserLabyrinthDataContainer.UserMatchGroup.UpdateUserPoint(User.UserId, totalPoint);
            }
            return totalPoint;
        }

        private void UpdatePlayerDispatchMonsterStatus(LabyrinthMonsterRequestModel[] dispatchMonsters)
        {
            var userMonsterAdapter = ApplicationContext.UserDataContainer.UserMonster;
            foreach (var dispatchMonster in dispatchMonsters)
            {
                //TODO: Check whether user be dispatched or not?
                if (dispatchMonster.Hp > 0)
                {
                    userMonsterAdapter.RecoverUserMonster(dispatchMonster.UserMonsterId, dispatchMonster.Hp);
                }
                else
                {
                    var userMonster = userMonsterAdapter.GetDataByPK(dispatchMonster.UserMonsterId);
                    if (userMonster.Life == 1)
                    {
                        userMonsterAdapter.DeleteByUserMonster(dispatchMonster.UserMonsterId);
                    }
                    else
                    {
                        userMonsterAdapter.PullBackUserMonster(dispatchMonster.UserMonsterId);
                        ApplicationContext.UserLabyrinthDataContainer.UserDispatchMonster.DeleteByUserMonsterId(dispatchMonster.UserMonsterId);
                    }
                    var userNotification = new UserNotification(ApplicationContext);
                    userNotification.AddNew(userMonster.UserId, User.Userinfo.NickName + " defeat your monster in Labyrinth",
                                            (short)AppCode.NotificationCategory.DISPATCH_MONSTER,
                                            dispatchMonster.UserMonsterId);
                }
            }
        }

        private LabyrinthFeverTimeViewModel GetFeverTime()
        {
            var feverTime = ApplicationContext.DutyDataContainer.FeverTime.GetActiveFeverByEventId(EventId, ApplicationContext.Now);
            if (feverTime != null)
            {
                var feverTimeModel = new LabyrinthFeverTimeViewModel()
                {
                    StartTime = DateUtil.ToUnixTime(feverTime.StartTime),
                    EndTime = DateUtil.ToUnixTime(feverTime.EndTime),
                    LessStamina = feverTime.LessStamina,
                    AttackIncrease = feverTime.AttackIncrease,
                    CriticalHit = feverTime.CriticalHit,
                    ObtainableGold = feverTime.ObtainableGold,
                    MoreMana = feverTime.MoreMana,
                    MoreTreasure = feverTime.MoreTreasure,
                    DropMoreGold = feverTime.DropMoreGold,
                    DropMoreGear = feverTime.DropMoreGear,
                    DropMoreWeapon = feverTime.DropMoreWeapon,
                    DropMoreShield = feverTime.DropMoreShield,
                    DropMoreAccessory = feverTime.DropMoreAccessory,
                    ChanceCapturingMonster = feverTime.ChanceCapturingMonster,
                    ScoreIncrease = feverTime.ScoreIncrease
                };
                return feverTimeModel;
            }
            else
            {
                return null;
            }
        }

        private void UpdateUserEquipment(LabyrinthStartRequestViewModel requestModel)
        {
            var userEquipmentAdapter = ApplicationContext.UserLabyrinthDataContainer.UserEquipment;
            userEquipmentAdapter.DeleteDataByUserId(User.UserId);
            var userEquipmentDataTable = new UserLabyrinthDataSet.UserEquipmentDataTable();
            var userWeaponMainId = User.Userinfo.UserGearId;
            foreach (var equipment in requestModel.Gears)
            {
                var userEquipmentRow = userEquipmentDataTable.NewUserEquipmentRow();
                userEquipmentRow.UserId = User.UserId;
                userEquipmentRow.Type = equipment.Type;
                userEquipmentRow.UserEquipId = equipment.UserGearId;
                userEquipmentRow.Num = 1;
                if (userEquipmentRow.Type == (byte)AppCode.EquipmentCategory.WEAPON_MAIN)
                {
                    userWeaponMainId = userEquipmentRow.UserEquipId;
                }
                userEquipmentRow.AddTime = ApplicationContext.Now;
                userEquipmentRow.UpdtTime = ApplicationContext.Now;
                userEquipmentDataTable.AddUserEquipmentRow(userEquipmentRow);
            }
            if (requestModel.Items != null && requestModel.Items.Length > 0)
            {
                foreach (var equipment in requestModel.Items)
                {
                    var userEquipmentRow = userEquipmentDataTable.NewUserEquipmentRow();
                    userEquipmentRow.UserId = User.UserId;
                    userEquipmentRow.Type = (byte)AppCode.EquipmentCategory.CONSUMABLE_ITEMS;
                    userEquipmentRow.UserEquipId = equipment.ItemId;
                    userEquipmentRow.Num = equipment.Num;
                    userEquipmentRow.AddTime = ApplicationContext.Now;
                    userEquipmentRow.UpdtTime = ApplicationContext.Now;
                    userEquipmentDataTable.AddUserEquipmentRow(userEquipmentRow);
                }
            }
            userEquipmentAdapter.BulkInsert(userEquipmentDataTable);
            // Check if changing main weapon 
            if (userWeaponMainId != User.Userinfo.UserGearId && userWeaponMainId != 0)
            {
                User.Userinfo.UserGearId = userWeaponMainId;
                User.UpdateUserStatus(ApplicationContext.UserDataContainer);
            }
        }

        private LabyrinthUserEquipmentViewModel GetUserEquipment()
        {
            var labyrinthUserEquipment = new LabyrinthUserEquipmentViewModel();
            UserRentEquipmentViewModel rentEquipment = null;
            UserEquipmentOwnerViewModel ownerEquipment = new UserEquipmentOwnerViewModel();
            var gearEquipments = new List<UserEquipmentGearRequestModel>();
            var itemEquipments = new List<UserEquipmentItemRequestModel>();
            var userEquipmentAdapter = ApplicationContext.UserLabyrinthDataContainer.UserEquipment;
            var equipments = userEquipmentAdapter.GetDataByUserId(User.UserId);
            foreach (var equipment in equipments)
            {
                if (equipment.Type != (byte)AppCode.EquipmentCategory.WEAPON_RENTAL)
                {
                    if (equipment.Type != (byte)AppCode.EquipmentCategory.CONSUMABLE_ITEMS)
                    {
                        gearEquipments.Add(new UserEquipmentGearRequestModel()
                        {
                            Type = equipment.Type,
                            UserGearId = equipment.UserEquipId
                        });
                    }
                    else
                    {
                        itemEquipments.Add(new UserEquipmentItemRequestModel()
                        {
                            ItemId = (int)equipment.UserEquipId,
                            Num = equipment.Num
                        });
                    }
                }
                else
                {
                    rentEquipment = new UserRentEquipmentViewModel()
                    {
                        Type = equipment.Type,
                        UserGearId = equipment.UserEquipId,
                        GearId = (int)equipment.UserEquipId //TODO: Hardcode for now
                    };
                }
            }
            ownerEquipment.Gears = gearEquipments;
            ownerEquipment.Items = itemEquipments;
            labyrinthUserEquipment.Owner = ownerEquipment;
            labyrinthUserEquipment.Rent = rentEquipment;
            return labyrinthUserEquipment;
        }
    }
}
