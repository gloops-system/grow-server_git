﻿using Grow.Core.Common;
using System.Collections.Generic;
using Grow.Core.Application.User;
using Grow.Core.InterfaceModel.User;
using Fango.Core.Util;
using Grow.Core.InterfaceModel.Duty;
using Grow.Core.Application.Common;
using Fango.Web.Core.ApiModel;

namespace Grow.Core.Application.Duty
{
    public class QuestUser : ModelBase
    {
        public int EventId { get; set; }
        
        public QuestUser(GrowApplicationModel applicationContext) : base(applicationContext) {
            EventId = (int)AppCode.QuestEventId.NORMAL_QUEST;
        }

        public RecoveryStaminaViewModel RecoveryStamina(int requireGem)
        {
            var recoveryStaminaModel = new RecoveryStaminaViewModel();
            var userGemAdapter = ApplicationContext.UserDataContainer.UserGem;
            var userStatusAdapter = ApplicationContext.UserDataContainer.UserStatus;
            var userGem = userGemAdapter.FindUserGem(User.UserId);
            if (userGem.Num >= requireGem)
            {
                userGemAdapter.DecrementUserGem(User.UserId, requireGem);
                userStatusAdapter.RecoveryStaminaByPK(User.UserId, User.Userinfo.MaxStamina, ApplicationContext.Now);
                recoveryStaminaModel.Gem = userGem.Num - requireGem;
                recoveryStaminaModel.Stamina = User.Userinfo.MaxStamina;
                recoveryStaminaModel.StaminaLastHealTime = DateUtil.ToUnixTime(ApplicationContext.Now);
            }
            else
            {
                var ex = new ApiException();
                ex.Code = (int) AppCode.ApiResultCode.Not_Enough_Gem;
                //ex.Message = "Not engouh gem";
                throw ex;
            }

            return recoveryStaminaModel;
        }

        /*
        public FinishAreaViewModel FinishArea(FinishAreaRequestViewModel requestModel)
        {
            var finishAreaModel = new FinishAreaViewModel();
            UpdateArea(requestModel);
            finishAreaModel.UserItem = UseItems(requestModel.ItemList);
            //Update Avatar status
            var avatarUser = new AvatarUser(ApplicationContext);
            if (requestModel.Avatar != null)
            {
                finishAreaModel.UserAvatar = avatarUser.UpdateAvatarExp(requestModel.Avatar.AvatarUserId, requestModel.Avatar.Exp);
            }
            finishAreaModel.UserStatus = UpdateUseStatus(requestModel);
            return finishAreaModel;
        }
        */
        protected void UpdateFinishFloor(FinishFieldModel requestModel)
        {
            var dutyDataContainer = ApplicationContext.DutyDataContainer;
            var userAreaAdapter = ApplicationContext.DutyDataContainer.UserArea;
            var areaMaster = dutyDataContainer.AreaMaster.GetDataByAreaId(requestModel.AreaId);
            dutyDataContainer.UserFloor.UpdateUserFloorData(User.UserId, requestModel.FloorId, requestModel.AreaId, 
                                                            areaMaster.StageId, areaMaster.EventId, requestModel.FloorId, requestModel.Score,
                                                            0, requestModel.RankScore, requestModel.RankTime, requestModel.RankMonster, (short)AppCode.FieldStatus.CLEARED, ApplicationContext.Now);
            var userArea = userAreaAdapter.GetDataByUserIdAndAreaId(User.UserId, requestModel.AreaId);
            if (userArea == null)
            {
                userAreaAdapter.AddNew(User.UserId, requestModel.AreaId, areaMaster.StageId, EventId, 0, (short)AppCode.AreaStatus.ACTIVE);
            }

        }
        /*
        protected void UpdateArea(FinishAreaRequestViewModel requestModel)
        {
            
            var userAreaAdapter = ApplicationContext.DutyDataContainer.UserArea;
            var userArea = userAreaAdapter.GetDataByUserIdAndAreaId(User.UserId, requestModel.AreaId);
            var areaPoint = 0;
            long userAreaId = 0;
            var areaMaster = ApplicationContext.DutyDataContainer.AreaMaster.GetDataByAreaId(requestModel.AreaId);
            var floorMasters = ApplicationContext.DutyDataContainer.FloorMaster.GetDataByAreaId(requestModel.AreaId);
            short areaStatus = (short)AppCode.AreaStatus.OPEN;
            bool isFirstCleared = false;
            foreach (var floor in requestModel.FloorList)
            {
                areaPoint += floor.Score;
            }
            if (requestModel.FloorList.Count() == floorMasters.Count())
            {
                areaStatus = (short)AppCode.AreaStatus.CLEARED;
            }            
            if (userArea == null)
            {
                userAreaId = userAreaAdapter.AddNew(User.UserId, requestModel.AreaId, areaMaster.StageId, EventId, areaPoint, areaStatus);
                if (areaStatus == (short)AppCode.AreaStatus.CLEARED)
                {
                    isFirstCleared = true;
                }
            }
            else
            {
                userAreaId = userArea.UserAreaId;
                if (areaPoint > userArea.Point || userArea.Status < areaStatus)
                {
                    if (userArea.Status < areaStatus)
                    {
                        userArea.Status = areaStatus;
                        if (areaStatus == (short)AppCode.AreaStatus.CLEARED)
                        {
                            isFirstCleared = true;
                        }
                    }
                    if (areaPoint > userArea.Point)
                    {
                        userArea.Point = areaPoint;
                    }
                    userAreaAdapter.UpdateByKey(userAreaId, userArea.Point, userArea.Status, ApplicationContext.Now);
                }
            }
            //Insert new area into UserArea incase first clear
            if (isFirstCleared)
            {
                var nextAreaMaster = ApplicationContext.DutyDataContainer.AreaMaster.GetNextAreaId(areaMaster);
                if (nextAreaMaster != null)
                {
                    if (userAreaAdapter.GetDataByUserIdAndAreaId(User.UserId, nextAreaMaster.AreaId) == null)
                    {
                        userAreaAdapter.AddNew(User.UserId, nextAreaMaster.AreaId, nextAreaMaster.StageId, EventId, 0, (short)AppCode.AreaStatus.OPEN);
                    }
                    if (EventId == (int)AppCode.QuestEventId.NORMAL_QUEST)
                    {
                        User.Userinfo.LastQuestAreaId = nextAreaMaster.AreaId;
                    }
                    else
                    {
                        User.Userinfo.LastLabyrinthAreaId = nextAreaMaster.AreaId;
                    }
                    User.UpdateUserStatus(ApplicationContext.UserDataContainer);
                }
            }

        }
        */
        protected List<UserItemModel> UseItems(FinishAreaItemRequestModel[] itemList)
        {
            var userItem = new UserItem(ApplicationContext);
            var userItemModels = new List<UserItemModel>();
            foreach (var areaItem in itemList)
            {
                userItemModels.Add(userItem.UseItem(areaItem.ItemId, areaItem.Number));
            }
            return userItemModels;
        }

        protected FinishAreaUserStatusModel UpdateUseStatus(FinishAreaRequestViewModel requestModel)
        {
            if (requestModel.Exp > 0)
            {
                User.UpdateUserStatusExp(User.Userinfo.Exp + requestModel.Exp, ApplicationContext.UserDataContainer, ApplicationContext.MasterDataContainer);
            }
            return new FinishAreaUserStatusModel()
            {
                Level = User.Userinfo.Level,
                Hp = User.Userinfo.Hp,
                Sp = User.Userinfo.Sp,
                Exp = User.Userinfo.Exp,
                Gold = User.Userinfo.Gold,
                Attack = User.Userinfo.Attack,
                Defense = User.Userinfo.Defense,
                StunValue = User.Userinfo.StunValue,
                StunResistance = User.Userinfo.StunResistance,
                StunRecoveryStand = User.Userinfo.StunRecoveryStand,
                StunRecoveryDown = User.Userinfo.StunRecoveryDown,
                ScoreUp = User.Userinfo.ScoreUp,
                AvatarHpBonus = User.Userinfo.AvatarHpBonus,
                AvatarSpBonus = User.Userinfo.AvatarSpBonus,
                AvatarAttackBonus = User.Userinfo.AvatarAttackBonus,
                AvatarDefenseBonus = User.Userinfo.AvatarDefenseBonus,
                AvatarScoreUpBonus = User.Userinfo.AvatarScoreUpBonus,
                MaxFriendsNum = User.Userinfo.MaxFriendsNum,
                MaxMonsterBoxNum = User.Userinfo.MaxMonsterBoxNum,
                MaxItemBoxNum = User.Userinfo.MaxItemBoxNum,
                MaxGearBagNum = User.Userinfo.MaxGearBagNum
            };
        }
    }
}
