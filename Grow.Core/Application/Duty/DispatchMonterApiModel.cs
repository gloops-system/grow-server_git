﻿using Grow.Core.Application;
using Grow.Core.Application.Duty;
using Grow.Core.Application.Monster;
using Grow.Core.InterfaceModel.Duty;
using Grow.Core.InterfaceModel.Monster;
using Fango.Web.Core.ApiModel;

namespace Grow.Core.Application.Duty
{
    public class DispatchMonterApiModel : GrowApiModel
    {
        public DispatchMonterApiModel(GrowApplicationModel applicationContext)
            : base(applicationContext) { }

        public ContentResponse<DispatchFriendListViewModel> DispatchFriendList(long userMonsterId)
        {
            var contentResponse = new ContentResponse<DispatchFriendListViewModel>();
            var user = ApplicationContext.CurrentUser;
            var friendList = new DispatchMonsterUser(ApplicationContext).GetFriendList(userMonsterId);
            contentResponse.AppData = new DispatchFriendListViewModel()
            {
                FriendList = friendList
            };
            return contentResponse;
        }

        public ContentResponse<DispatchMonsterExecuteViewModel> DispatchExecute(int targetUserId, long userMonsterId)
        {
            var contentResponse = new ContentResponse<DispatchMonsterExecuteViewModel>();
            var user = ApplicationContext.CurrentUser;
            var resultData = new DispatchMonsterUser(ApplicationContext).ExecuteDispatch(targetUserId, userMonsterId);
            contentResponse.AppData = resultData;
            return contentResponse;
        }

        public ContentResponse<MeetDispatchMonsterViewModel> MeetMonsterList()
        {
            var contentResponse = new ContentResponse<MeetDispatchMonsterViewModel>();
            var user = ApplicationContext.CurrentUser;
            var resultData = new DispatchMonsterUser(ApplicationContext).GetMeetMonsterList();
            contentResponse.AppData = new MeetDispatchMonsterViewModel()
            {
                MonsterList = resultData
            };
            return contentResponse;
        }

        public ContentResponse<PullBackMonsterViewModel> PullBack(long userMonsterId)
        {
            var contentResponse = new ContentResponse<PullBackMonsterViewModel>();
            var user = ApplicationContext.CurrentUser;
            var resultData = new DispatchMonsterUser(ApplicationContext).PullBack(userMonsterId);
            contentResponse.AppData = resultData;
            return contentResponse;
        }
    }
}