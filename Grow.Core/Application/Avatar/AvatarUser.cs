﻿using Grow.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grow.Core.Application.User;
using Grow.Data;
using Grow.Core.InterfaceModel.Duty;
using Grow.Core.InterfaceModel.User;
using Grow.Core.InterfaceModel.Avatar;
using Grow.Data.User.DataSet.Container;
using Grow.Core.Application;
using Grow.Data.DataSet;

namespace Grow.Core.Application.Avatar
{
    public class AvatarUser : ModelBase
    {
        public AvatarUser(GrowApplicationModel applicationContext) : base(applicationContext) {}
        
        public List<UserAvatarModel> GetUserAvatars()
        {
            List<UserAvatarModel> avatarUserList = new List<UserAvatarModel>();
            var userDataContainer = ApplicationContext.UserDataContainer;
            var userAvatars = userDataContainer.UserAvatar.GetDataByUserId(User.UserId);
            foreach (var userAvatar in userAvatars)
            {
                var userAvatarModel = new UserAvatarModel();
                userAvatarModel.UserAvatarId = userAvatar.UserAvatarId;
                userAvatarModel.AvatarId = userAvatar.AvatarId;
                userAvatarModel.Level = userAvatar.Level;
                userAvatarModel.Exp = userAvatar.Exp;
                userAvatarModel.IsMain = (User.Userinfo.UserAvatarId == userAvatar.UserAvatarId? 1 : 0);
                avatarUserList.Add(userAvatarModel);
            }
            return avatarUserList;
        }

        public long[] AddUserAvatars(int[] AvatarIds)
        {
            long[] resultAvatarIds = new long[AvatarIds.Length];
            var avatarMasterAdapter = ApplicationContext.MasterDataContainer.AvatarMaster;
            int idx = 0;
            foreach (var AvatarId in AvatarIds)
            {
                var avatarMaster = avatarMasterAdapter.GetDataByAvatarId(AvatarId);
                var userAvatarId = ApplicationContext.UserDataContainer.UserAvatar.AddNew(User.UserId, avatarMaster.AvatarId, avatarMaster.DefaultHp, avatarMaster.DefaultSp,
                                                                        avatarMaster.DefaultAttack, avatarMaster.DefaultDefense, avatarMaster.DefaultScore);
                resultAvatarIds[idx++] = userAvatarId;
            }
            return resultAvatarIds;
        }

        public FinishAreaAvatarModel UpdateAvatarExp(long userAvatarId, int exp)
        {
            var userAvatar = ApplicationContext.UserDataContainer.UserAvatar.GetDataByUserAvatarId(userAvatarId);
            if (userAvatar == null || userAvatar.UserId != User.UserId)
            {
                return null;
            }
            bool changeFlg = false;
            if (userAvatar.Exp < exp)
            {
                userAvatar.Exp = exp;
                changeFlg = true;
            }
            var avatarLevelMasters = ApplicationContext.MasterDataContainer.AvatarLevelMaster.GetDataByAvatarId(userAvatar.AvatarId);
            var avatarLevelMasterUps = new List<MasterDataSet.AvatarLevelMasterRow>();
            foreach (var avatarLevelMaster in avatarLevelMasters)
            {
                if (userAvatar.Exp >= avatarLevelMaster.Exp)
                {
                    if (userAvatar.Level < avatarLevelMaster.Level)
                    {
                        avatarLevelMasterUps.Add(avatarLevelMaster);
                    }
                }
                else
                {
                    break;
                }
            }
            if (avatarLevelMasterUps.Count > 0)
            {
                foreach (var avatarLevelMasterUp in avatarLevelMasterUps)
                {
                    userAvatar.Hp += avatarLevelMasterUp.HpUp;
                    userAvatar.Sp += avatarLevelMasterUp.SpUp;
                    userAvatar.Attack += avatarLevelMasterUp.AttackUp;
                    userAvatar.Defense += avatarLevelMasterUp.DefenseUp;
                    userAvatar.Level = (short) avatarLevelMasterUp.Level;
                }
                var avatarBonusMaster = ApplicationContext.MasterDataContainer.AvatarBonusMaster.GetDataByAvatarId(userAvatar.AvatarId);
                if (avatarBonusMaster != null)
                {
                    if (avatarBonusMaster.MaxLevel == userAvatar.Level)
                    {
                        User.Userinfo.AvatarHpBonus += avatarBonusMaster.Hp;
                        User.Userinfo.AvatarSpBonus += avatarBonusMaster.Sp;
                        User.Userinfo.AvatarAttackBonus += avatarBonusMaster.Attack;
                        User.Userinfo.AvatarDefenseBonus += avatarBonusMaster.Defense;
                        User.Userinfo.AvatarScoreUpBonus += avatarBonusMaster.Score;
                        User.UpdateAvatarBonus(ApplicationContext.UserDataContainer, ApplicationContext.Now);
                    }
                }
            }
            if (changeFlg)
            {
                ApplicationContext.UserDataContainer.UserAvatar.UpdateAvatar(userAvatar);
            }

            return new FinishAreaAvatarModel()
            {
                Level = userAvatar.Level,
                Exp = userAvatar.Exp,
                Hp = userAvatar.Hp,
                Sp = userAvatar.Sp,
                Attack = userAvatar.Attack,
                Defense = userAvatar.Defense
            };
        }
    }
}
