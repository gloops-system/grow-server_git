﻿using Grow.Core.Common;
using System.Collections.Generic;
using System.Linq;
using Grow.Core.InterfaceModel.Duty;
using Grow.Core.InterfaceModel.Monster;
using Grow.Core.Application.Common;

namespace Grow.Core.Application.Monster
{
    public class DispatchMonsterUser : ModelBase
    {
        public DispatchMonsterUser(GrowApplicationModel applicationContext): base(applicationContext){}
        
        public List<FriendViewModel> GetFriendList(long userMonsterId)
        {
            List<FriendViewModel> friendList = new List<FriendViewModel>();
            var monster = new MonsterUser(ApplicationContext);
            var userMonsterModel = monster.GetMonster(userMonsterId);
            if (userMonsterModel == null)
            {
                throw new AppException(AppResponseCode.InvalidUserMonster, "The dispatch monster is invalid");
            }
            var userDutyDataContainer = ApplicationContext.UserLabyrinthDataContainer;
            var rows = userDutyDataContainer.UserMatchGroupLeaderboard.GetRivalsLeaderboard(User.UserId);
            var numberUsers = rows.Count();
            if (numberUsers > 1)
            {
                var userRanking = 0;
                foreach (var row in rows)
                {
                    if (row.UserId == User.UserId)
                    {
                        break;
                    }
                    userRanking++;
                }
                var firstPlayPosition = userRanking - 2;
                if (firstPlayPosition < 0)
                {
                    firstPlayPosition = 0;
                }
                else
                {
                    while (firstPlayPosition + 4 >= numberUsers )
                    {
                        firstPlayPosition--;
                    }
                }
                int idx = 0;
                foreach (var row in rows)
                {
                    if (idx != userRanking && idx >= firstPlayPosition && idx <= firstPlayPosition + 4)
                    {
                        friendList.Add(new FriendViewModel() { UserId = row.UserId, RequiredGold = CalculateGold(userMonsterModel)});
                    }
                    idx++;
                }
            }
            return friendList;
        }

        public List<DispatchMonsterModel> GetMeetMonsterList()
        {
            var userDutyDataContainer = ApplicationContext.UserLabyrinthDataContainer;
            List<DispatchMonsterModel> meetMonsterModelList = new List<DispatchMonsterModel>();
            var rows = userDutyDataContainer.UserMeetDispatchMonster.GetMeetMonsterList(User.UserId);
            DispatchMonsterModel meetMonsterModel = null;
            var currentFloorId = 0;
            foreach (var row in rows)
            {
                meetMonsterModel = new DispatchMonsterModel()
                {
                    UserId = row.UserId,
                    UserMonsterId = row.UserMonsterId,
                    NickName = row.NickName,
                    AvatarId = row.AvatarId,
                    GearId = row.GearId,
                    MonsterId = row.MonsterId,
                    Level = row.Level,
                    Hp = row.LastHp,
                    AreaId = row.TargetPosition,
                    FloorIndex = currentFloorId

                };
                meetMonsterModelList.Add(meetMonsterModel);
                //TODO: Hardcode for now
                currentFloorId++;
                if (currentFloorId > 3)
                {
                    currentFloorId = 0;
                }
            }
            return meetMonsterModelList;
        }

        public DispatchMonsterExecuteViewModel ExecuteDispatch(int targetUserId, long userMonsterId)
        {
            var dispatchMonsterResult = new DispatchMonsterExecuteViewModel() { ConsumeGold = 0 };
            var userDutyDataContainer = ApplicationContext.UserLabyrinthDataContainer;
            var monsterUser = new MonsterUser(ApplicationContext);
            var userMonsterModel = monsterUser.GetMonster(userMonsterId);
            if (userMonsterModel != null && userMonsterModel.Status == (byte)AppCode.UserMonsterStatus.AVAILABLE && userMonsterModel.LastHpUpdtTime == 0)
            {
                var adapter = userDutyDataContainer.UserDispatchMonster;
                var dispatchMonster = adapter.GetDispatchMonster(targetUserId, userMonsterId);
                if (dispatchMonster == null)
                {
                    var targetUserStatus = ApplicationContext.UserDataContainer.UserStatus.GetDataByPK(targetUserId);
                    if (targetUserStatus != null)
                    {
                        if (targetUserStatus.LastLabyrinthAreaId == 0)
                        {
                            targetUserStatus.LastLabyrinthAreaId = ApplicationContext.DutyDataContainer.AreaMaster.GetDataByEventId((int) AppCode.QuestEventId.LABYRINTH)[0].AreaId;
                        }
                        var targerUserDispatchArea = ApplicationContext.DutyDataContainer.AreaMaster.GetNextAreaId(targetUserStatus.LastLabyrinthAreaId);
                        if (targerUserDispatchArea != null)
                        {
                            adapter.AddNew(User.UserId, targetUserId, userMonsterId, targerUserDispatchArea.AreaId);
                        }
                        else
                        {
                            throw new AppException(AppResponseCode.InvalidDispatch);
                        }
                    }
                    else
                    {
                        throw new AppException(AppResponseCode.InvalidDispatch);
                    }
                }
                ApplicationContext.UserDataContainer.UserMonster.UpdateUserMonsterStatus(userMonsterId, (byte)AppCode.UserMonsterStatus.DISPATCHED);
                var consumeGold = CalculateGold(userMonsterModel);
                if (consumeGold > 0)
                {
                    var remainGold = User.Userinfo.Gold - consumeGold;
                    if (remainGold < 0)
                        remainGold = 0;

                    ApplicationContext.UserDataContainer.UserStatus.UpdateGoldByPK(User.UserId, remainGold, ApplicationContext.Now);
                    dispatchMonsterResult.ConsumeGold = consumeGold;
                }
            }
            else
            {
                throw new AppException(AppResponseCode.InvalidDispatch);
            }
            return dispatchMonsterResult;
        }

        private int CalculateGold(UserMonsterModel userMonsterModel)
        {
            return userMonsterModel.Level * 10 + 10;
        }

        public PullBackMonsterViewModel PullBack(long userMonsterId)
        {
            var resultModel = new PullBackMonsterViewModel();
            var monsterUser = new MonsterUser(ApplicationContext);
            var userMonsterAdapter = ApplicationContext.UserDataContainer.UserMonster;
            var userMonster = userMonsterAdapter.GetDataByPK(userMonsterId);
            var userDutyDataContainer = ApplicationContext.UserLabyrinthDataContainer;
            if (userMonster != null && userMonster.Status == (byte)AppCode.UserMonsterStatus.DISPATCHED)
            {
                if (userMonster.Life == 1)
                {
                    userMonsterAdapter.DeleteByUserMonster(userMonsterId);
                    resultModel.Status = (int)AppCode.PullBackResultStatus.DIE;
                }
                else
                {
                    userMonsterAdapter.PullBackUserMonster(userMonsterId);
                    resultModel.Status = (int)AppCode.PullBackResultStatus.ALIVE;
                }
                userDutyDataContainer.UserDispatchMonster.DeleteByUserMonsterId(userMonsterId);
            }
            else
            {
                throw new AppException(AppResponseCode.InvalidPullBack);
            }
            if (resultModel.Status == (int)AppCode.PullBackResultStatus.ALIVE)
            {
                resultModel.UserMonster = monsterUser.GetMonster(userMonsterId);
            }
            return resultModel;
        }
    }
}
