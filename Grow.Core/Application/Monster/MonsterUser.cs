﻿using Grow.Core.Common;
using System.Collections.Generic;
using Grow.Core.InterfaceModel.Monster;
using Fango.Core.Util;
using Grow.Data.User.DataSet;

namespace Grow.Core.Application.Monster
{
    public class MonsterUser : ModelBase
    {
        //public MonsterUser(int userId) : base(userId) {}

        public MonsterUser(GrowApplicationModel applicationContext) : base(applicationContext) {}

        public UserMonsterModel GetMonster(long userMonsterId)
        {
            var userMonster = ApplicationContext.UserDataContainer.UserMonster.GetDataByPK(userMonsterId);
            if (userMonster == null || userMonster.UserId != User.UserId)
                return null;
            var lastHpUpdtTime = AutoRecover(userMonster);
            var userMonsterModel = new UserMonsterModel()
            {
                UserMonsterId = userMonster.UserMonsterId,
                MonsterId = userMonster.MonsterId,
                Level = userMonster.Level,
                Exp = userMonster.Exp,
                Life = userMonster.Life,
                Hp = userMonster.LastHp,
                LastHp = userMonster.Hp,
                LastHpUpdtTime = lastHpUpdtTime,
                Status = userMonster.Status
            };            
            return userMonsterModel;
        }

        public List<UserMonsterModel> GetUserMonsters()
        {
            var userMonsters = ApplicationContext.UserDataContainer.UserMonster.GetDataByUserId(User.UserId);
            if (userMonsters.Length == 0)
                return null;
            var userMonsterModels = new List<UserMonsterModel>();
            foreach (var userMonster in userMonsters)
            {
                var lastHpUpdtTime = AutoRecover(userMonster);
                var userMonsterModel = new UserMonsterModel()
                {
                    UserMonsterId = userMonster.UserMonsterId,
                    MonsterId = userMonster.MonsterId,
                    Level = userMonster.Level,
                    Status = userMonster.Status,
                    Exp = userMonster.Exp,
                    Life = userMonster.Life,
                    Hp = userMonster.LastHp,                    
                    LastHpUpdtTime = lastHpUpdtTime,                    
                };
                userMonsterModels.Add(userMonsterModel);
            }
            return userMonsterModels;
        }

        public ReviveMonsterViewModel ReviveMonster(long userMonsterId)
        {
            var userMonster = GetMonster(userMonsterId);
            var remainingGold = User.Userinfo.Gold;
            if (userMonster == null)
            {
                throw new AppException(AppResponseCode.InvalidUserMonster);
            }
            else if (userMonster.LastHpUpdtTime > 0)
            {
                var userDataContainer = ApplicationContext.UserDataContainer;
                userDataContainer.UserMonster.RecoverUserMonster(userMonsterId, userMonster.LastHp);
                var numberHp = userMonster.LastHp - userMonster.Hp;
                if (User.Userinfo.Gold < numberHp)
                {
                    throw new AppException(AppResponseCode.NotEnoughGold, "Not enough gold");
                }
                User.Userinfo.Gold -= numberHp;
                userDataContainer.UserStatus.DecrementGoldByPK(User.UserId, numberHp, ApplicationContext.Now);
            }
            else if (userMonster.Status == (byte)UserMonsterStatus.DISPATCHED)
            {
                throw new AppException(AppResponseCode.InvalidReviveMonsterStatus, "Invalid monster status to revive");
            }
            var reviveMonsterModel = new ReviveMonsterViewModel()
            {
                Gold = User.Userinfo.Gold
            };
            return reviveMonsterModel;
        }

        public void AddUserMonsters(int[] MonsterIds)
        {
            var monsterMasterAdapter = ApplicationContext.MasterDataContainer.MonsterMaster;
            foreach (var MonsterId in MonsterIds)
            {
                var monsterMaster = monsterMasterAdapter.GetDataByMonsterId(MonsterId);
                ApplicationContext.UserDataContainer.UserMonster.AddNew(User.UserId, monsterMaster.MonsterId, (byte)UserMonsterStatus.AVAILABLE, (byte)monsterMaster.DefaultLife, monsterMaster.DefaultHp,
                                                      monsterMaster.DefaultHp, monsterMaster.DefaultHpRecoveryAmount, monsterMaster.DefaultStunResistance,
                                                      monsterMaster.DefaultStunRecoveryStand, monsterMaster.DefaultStunRecoveryDown, monsterMaster.DefaultAttack,
                                                      monsterMaster.DefaultDefense);
            }
        }

        public void InsertBulkUserMonsters(int[] MonsterIds)
        {
            var userMonsterDataTable = new UserDataSet.UserMonsterDataTable();
            var monsterMasterAdapter = ApplicationContext.MasterDataContainer.MonsterMaster;
            foreach (var MonsterId in MonsterIds)
            {
                var userMonsterRow = userMonsterDataTable.NewUserMonsterRow();
                var monsterMaster = monsterMasterAdapter.GetDataByMonsterId(MonsterId);
                userMonsterRow.UserId = User.UserId;
                userMonsterRow.MonsterId = monsterMaster.MonsterId;
                userMonsterRow.Status = (byte)UserMonsterStatus.AVAILABLE;
                userMonsterRow.Life = (byte)monsterMaster.DefaultLife;
                userMonsterRow.Level = 1;
                userMonsterRow.Exp = 0;
                userMonsterRow.Hp = monsterMaster.DefaultHp;
                userMonsterRow.LastHp = monsterMaster.DefaultHp;
                userMonsterRow.LastHpUpdtTime = ApplicationContext.Now;
                userMonsterRow.HpRecoveryAmount = monsterMaster.DefaultHpRecoveryAmount;
                userMonsterRow.StunResistance = monsterMaster.DefaultStunResistance;
                userMonsterRow.StunRecoveryStand = monsterMaster.DefaultStunRecoveryStand;
                userMonsterRow.StunRecoveryDown = monsterMaster.DefaultStunRecoveryDown;
                userMonsterRow.Attack = monsterMaster.DefaultAttack;
                userMonsterRow.Defense = monsterMaster.DefaultDefense;
                userMonsterRow.CaptureAreaId = 0;
                userMonsterRow.AddTime = ApplicationContext.Now;
                userMonsterRow.UpdtTime = ApplicationContext.Now;
                userMonsterDataTable.AddUserMonsterRow(userMonsterRow);
            }
            ApplicationContext.UserDataContainer.UserMonster.BulkInsert(userMonsterDataTable);
        }

        private long AutoRecover(Grow.Data.User.DataSet.UserDataSet.UserMonsterRow userMonster)
        {
            long lastUpdtTime = 0;
            if (userMonster.Status == (byte)UserMonsterStatus.AVAILABLE && userMonster.Hp > userMonster.LastHp)
            {
                var now = DateUtil.GetDateTimeOffsetNow();
                var timeSpan = now.Subtract(userMonster.LastHpUpdtTime);
                var lastHpUpdate = userMonster.LastHp + (int)timeSpan.TotalMinutes * userMonster.HpRecoveryAmount;
                if (lastHpUpdate >= userMonster.Hp)
                {
                    ApplicationContext.UserDataContainer.UserMonster.RecoverUserMonster(userMonster.UserMonsterId, userMonster.Hp);
                    userMonster.LastHp = userMonster.Hp;
                }
                else
                {
                    lastUpdtTime = DateUtil.ToUnixTime(userMonster.LastHpUpdtTime);
                }
            }
            return lastUpdtTime;
        }
    }
}
