﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.Common
{
    public class AppMessage
    {
        public static readonly AppMessage Instance = new AppMessage();
        private Dictionary<AppResponseCode, string> messageList = new Dictionary<AppResponseCode, string>();

        private AppMessage()
        {
            messageList.Add(AppResponseCode.InvalidUser, "Invalid User");
            messageList.Add(AppResponseCode.InvalidDispatch, "Dupplicated Dispatching");
        }

        public string GetMessage(AppResponseCode responseCode)
        {
            var message = "";
            try
            {
                message = messageList[responseCode];
            }
            catch (Exception)
            {
            }
            return message;
        }
    }
}
