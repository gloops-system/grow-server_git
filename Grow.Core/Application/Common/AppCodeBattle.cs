﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.Application.Common
{
    public partial class AppCode
    {
        #region BattleStatus
        /// <summary>
        /// Battle Status
        /// </summary>
        public enum BattleStatus
        {
            INIT = 1,
            CANCEL = 2,
            START = 3,
            END = 4
        }

        /// <summary>
        /// Battle Result
        /// </summary>
        public enum BattleResult
        {
            WIN = 1,
            LOOSE = 2,
        }
        #endregion
    }
}
