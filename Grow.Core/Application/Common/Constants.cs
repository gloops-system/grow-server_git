﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.Common
{
    public class Constants
    {
        #region User Init Data

        public const int DefaultGem = 100;
        public const int DefaultGold = 50000;
        public const int DefaultStamina = 10;
        public const int DefaultHp = 100;
        public const int DefaultSp = 100;
        public const int DefaultAttack = 10;
        public const int DefaultDefense = 0;
        public const int DefaultCutPercentage = 10;
        public const int DefaultCriticalPercentage = 10;
        public const int DefaultStunValue = 10;
        public const int DefaultStunResistance = 10;
        public const int DefaultStunRecoveryStand = 10;
        public const int DefaultStunRecoveryDown = 10;
        public const int DefaultScoreUp = 10;
        public const int FirstQuestAreaId = 0;
        public const int FirstLabyrinthAreaId = 0;
        public const int DefaultGearNum = 10;
        public const int DefaultFriendNum = 10;
        public const int DefaultMonsterNum = 10;
        public const int DefaultItemNum = 10;
        public const int DefaultCarryingGearNum = 10;

        public static readonly int[] ListDefaultAvatarId = { 1 };
        public static readonly int[] ListDefaultMonsterId = { 1, 2, 3, 4, 5 };
        public static readonly int[] ListDefaultGearId = { 101, 102, 103, 107, 108, 109, 110, 111, 201, 202, 301, 302 };
        public static readonly Dictionary<int, int> ListDefaultItemId = new Dictionary<int, int> { 
            {1, 5}, {2,5}, {3, 5}, {4,5}, {5,5}, {6,5}
        };

        #endregion
    }
}
