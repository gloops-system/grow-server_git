﻿using System;
using System.Runtime.Serialization;

namespace Grow.Core.Application.Common
{
    public partial class AppCode
    {
        /// <summary>
        /// アイテムカテゴリ
        /// </summary>
        public enum Category
        {
            キャラクター = 0,
            通常アイテム = 1,
            宝箱 = 2,
            強化アイテム = 4,
            モンスター = 5,
            Npcキャラクター = 6,
            アンロックステージ = 7,
            ギル = 8,
            経験値 = 9,
            Bp = 10,
            Qp = 11,
            Gp = 12,

            ゲーム内通貨 = 100,

            キャラクター枠拡張 = 200,
            Dp全回復 = 201,
        }

        /// <summary>
        /// 通常アイテムカテゴリ
        /// </summary>
        public enum ItemType
        {
            ハーブ = 1,
            フラワー = 2,
            秘薬 = 3,
            オーブ = 4,
            エリクシャー = 5,
        }

        /// <summary>
        /// 通常アイテムのエフェクト
        /// </summary>
        public enum EffectType
        {
            全体 = 1,
            単体 = 2,
        }

        // TODO : NotificationTargetDeviceをどうするか
        public enum DeviceType
        {
            iOS = 1,
            Android = 2,
        }

        #region PUSH通知
        /// <summary>
        /// 通知タイプ
        /// </summary>
        public enum NotificationType
        {
            イベント予告 = 1,
            イベント開始 = 2,
        }

        /// <summary>
        /// 通知ターゲットデバイス
        /// </summary>
        public enum NotificationTargetDevice
        {
            All = 0,
            iOS = 1,
            Android = 2,
        }
        #endregion PUSH通知

        #region 招待
        /// <summary>
        /// 招待タイプ
        /// </summary>
        public enum InviteType
        {
            Facebook = 1,
            Twitter = 2,
            Line = 3,
            Mail = 4,
            Sms = 5,
        }
        #endregion

        /// <summary>
        /// ガチャタイプ
        /// </summary>
        public enum GachaType
        {
            Normal = 0,
            Rare = 1,
            Box = 2,
            StepUp = 3,
            Panel = 4,
            Crystal = 5,
        }

        /// <summary>
        /// 結晶タイプ
        /// </summary>
        public enum CrystalType
        {
            //TODO:仮
            Bronze = 1,
            Silver = 2,
            Gold = 3,
        }

        /// <summary>
        /// 飛空艇のフロア
        /// </summary>
        public enum AirshipFloor
        {
            甲板 = 0,
            メイン = 1,
            居住 = 2,
        }

        /// <summary>
        /// 飛空艇の場所
        /// </summary>
        public enum AirshipPlace
        {
            船長 = 1,
            ショップ = 2,
            インゴットショップ = 3,
            Pub = 4,
            合成 = 5,
        }

        /// <summary>
        /// Npcタイプ
        /// </summary>
        public enum NpcType
        {
            船長 = 1,
            ショップ店員 = 2,
            インゴットショップ店員 = 3,
            Pub店員 = 4,
            合成店員 = 5,
        }

        /// <summary>
        /// ダンジョンのオブジェクトタイプ
        /// </summary>
        [DataContract]
        public enum ObjectType
        {
            なし = 0,
            モンスター = 1,
            宝箱 = 2,
            罠 = 3,

            Npc = 4,
            Npc挙動 = 10,

            障害物 = 5,
            PvP = 6,
            特殊合成商人 = 7,
            特殊宿屋商人 = 8,
            要スキル床 = 9,
            ボススタート地点 = 900,
            ボス = 901,
            階段 = 999
        }

        /// <summary>
        /// ルームタイプ
        /// </summary>
        public enum RoomType
        {
            通常 = 0,
            宿屋 = 1,
            高等合成所 = 2,
            Goodモンスターハウス = 3,
            Badモンスターハウス = 4,
            スキル解除部屋 = 5,
            ボス = 100,
        }

        /// <summary>
        /// トラップの種類
        /// </summary>
        public enum TrapType
        {
            ダメージ = 1,
            移動制御 =2,
            回復 =3,
        }


        /// <summary>
        /// 形状の名前
        /// </summary>
        public enum ShapeName
        {
            Box = 0,
            Cross = 1,
            Lshape = 2,
            LshapeInverse = 3,
            LshapeUid = 4,
            LshapeUd = 5,
            SshapeDu = 6,
            SshapeUd = 7,
            SshapeDmu = 8,
            SshapeUmd = 9,
            TshapeUp = 10,
            TshapeDown = 11,
            TshapeRight = 12,
            TshapeLeft = 13,
            CshapeUp = 14,
            CshapeDown = 15,
            CshapeRight = 16,
            CshapeLeft = 17,
        }

        /// <summary>
        /// ルームサイズ
        /// </summary>
        public enum RoomSize
        {
            サイズ9 = 0,
            サイズ12 = 1,
            サイズ15 = 2,
            サイズ18 = 3,
            サイズ21 = 4,
            サイズ24 = 5,
        }

        public enum RoomDirection
        {
             上方向 = 0,
             右方向 = 1,
             下方向 = 2,
             左方向 = 3,
        }


        public enum ClearType
        {
            通常クリア = 1,
            諦める = 2,
            アイテム使用 = 3,
        }
        

        /// <summary>
        /// キャラクタータイプ
        /// </summary>
        public enum CharacterType
        {
            斬撃 = 1,
            刺突 = 2,
            打撃 = 3,
            魔法 = 4,
        }

        public enum SlotId
        {
            前衛左 = 0,
            前衛左中 = 1,
            前衛右中 = 2,
            前衛右 = 3,
            後衛左 = 4,
            後衛左中 = 5,
            後衛右中 = 6,
            後衛右 = 7,
        }

        /// <summary>
        /// スキンデータ
        /// </summary>
        public enum SkinType
        {
            Floor = 0,
            TopWall = 1,
            BottomWall = 2,
            LeftWall = 3,
            RightWall = 4,
            TopLeftWallCorner = 5,
            BottomLeftWallCorner = 6,
            TopRightWallCorner = 7,
            BottomRightWallCorner = 8,
            BottomLeftToTopWallCorner = 9,
            BottomRightToTopWallCorner = 10,
            TopLeftToBottomWallCorner = 11,
            TopRightToBottomWallCorner = 12,
            SpecialReserved1 = 13,
            SpecialReserved2 = 14,
            SpecialReserved3 = 15,
        }

        /// <summary>
        /// ギフト対応
        /// </summary>
        /// TODO:Edge用の修正
        public enum GiftType
        {
            ミッションクリア報酬 = 1,
            招待コード入力報酬 = 2,
            招待報酬 = 3,
            カムバックボーナス = 4,
            ログインボーナス = 5,
            ガチャ = 6,
            ガチャおまけ = 12,

            仕様変更補填 = 99997,
            メンテナンス補填 = 99998,
            不具合補填 = 99999
        }

        /// <summary>
        /// バナーカテゴリ
        /// </summary>
        public enum BannerCategory : int
        {
            ピクチャ = 0,
            テキスト = 1,
            FLASH = 2,
        }

        /// <summary>
        /// API結果コード
        /// </summary>
        public enum ApiResultCode
        {
            [EnumValue("実行エラー")]
            失敗 = -1,
            [EnumValue("成功")]
            成功 = 0,
            [EnumValue("クライアント更新")]
            クライアント更新 = 1,
            [EnumValue("メンテナンス中")]
            メンテナンス中 = 2,
            [EnumValue("要再認証")]
            要再認証 = 3,
            [EnumValue("パラメータ不正")]
            パラメータ不正 = 4,

            [EnumValue("Not_Enough_Gem")]
            Not_Enough_Gem = 2000,

        }

        #region チュートリアル
        /// <summary>
        /// チュートリアルステータス
        /// </summary>
        /// TODO:要修正
        public enum TutorialStatus
        {
            初期値 = 0,
            チュートリアル完了 = 9999,
        }
        #endregion チュートリアル

        /// <summary>
        /// 購入カテゴリ
        /// </summary>
        public enum PurchaseCategory
        {
            [EnumValue("GameCurrency")]
            GameCurrency = 0,
        }

        #region 友だち関連
        /// <summary>
        /// フレンドタイプ
        /// </summary>
        public enum FriendType
        {
            無関係 = 0,
            申請中 = 1,
            フレンド = 2,
        }

        /// <summary>
        /// フレンドの申請回答
        /// </summary>
        public enum FriendRequestAnswer
        {
            拒否 = 0,
            承認 = 1,
        }
        #endregion 友だち関連

        /// <summary>
        /// ショップタイプ
        /// </summary>
        public enum ShopType
        {
            通常 = 1,
            新人 = 2,
            カムバック = 3,
            インゴット = 9999,
        }
        public enum ShopLimitType
        {
            無制限 = 0,
            日制限 = 1,
            合計制限 = 2,
        }

        /// <summary>
        /// コメントタイプ
        /// </summary>
        public enum CommentType
        {
            User = 0,
            Gacha = 1,
            Npc = 2,
            System = 99,
        }

        #region 画像タイプ

        /// <summary>
        /// 画像タイプ
        /// </summary>
        /// TODO:リファクタリング必須
        public enum ImageType
        {
            #region areaGame

            [EnumValue("areagame")]
            地区別対抗戦,

            #endregion

            #region banner

            [EnumValue("banner")]
            バナー,

            #endregion

            #region card

            [EnumValue("card/85_85")]
            選手カード_85_85,

            [EnumValue("card/90_120")]
            選手カード_90_120,

            [EnumValue("card/120_160")]
            アニメーション用選手カード_120_160,

            [EnumValue("card/150_200")]
            選手カード_150_200,

            [EnumValue("card/420_560")]
            選手カード_420_560,

            [EnumValue("card/612_816")]
            選手カード_612_816,

            [EnumValue("card/home")]
            選手カード_home,

            #endregion

            #region collection

            [EnumValue("collection/background")]
            クラブ背景,

            [EnumValue("collection/detail")]
            クラブ詳細,

            [EnumValue("collection/header")]
            クラブホーム,

            [EnumValue("collection/rarity")]
            レアリティアイコン,

            [EnumValue("collection/title")]
            クラブタイトル,

            #endregion

            #region club

            [EnumValue("club/60_60")]
            クラブロゴ_60_60,

            [EnumValue("club/110_110")]
            クラブロゴ_110_110,

            #endregion

            #region coin

            [EnumValue("coin/80_80")]
            仮想通貨_80_80,

            [EnumValue("coin/120_120")]
            仮想通貨_120_120,

            [EnumValue("coin/150_150")]
            仮想通貨_150_150,

            [EnumValue("coin/event_150_150")]
            仮想通貨_イベント用_150_150,

            [EnumValue("coin/event_560_150")]
            仮想通貨_イベント用_560_150,

            #endregion

            #region common

            [EnumValue("common")]
            選手カード_裏,

            #endregion

            #region duty

            [EnumValue("duty/background")]
            視察アニメーション背景,

            [EnumValue("duty/list")]
            視察一覧,

            [EnumValue("duty/home")]
            視察ホーム,

            [EnumValue("duty/drop")]
            視察ドロップコンプカード,

            #endregion

            #region dutyEvent

            [EnumValue("dutyEvent/header")]
            視察イベントトップヘッダー,

            [EnumValue("dutyEvent/button/common")]
            視察イベント共通ボタン,

            [EnumValue("dutyEvent/button/duty")]
            視察イベント視察ボタン,

            [EnumValue("dutyEvent/background/top")]
            視察イベントトップ背景,

            [EnumValue("dutyEvent/background/common")]
            視察イベント共通背景,

            [EnumValue("dutyEvent/background/duty")]
            視察イベント視察画面背景,

            [EnumValue("dutyEvent/list/black")]
            視察イベント視察リスト黒画像,

            [EnumValue("dutyEvent/lock")]
            視察イベントロック画像,

            [EnumValue("dutyEvent/list")]
            視察イベント視察リスト画像,

            [EnumValue("dutyEvent/require")]
            視察イベント参加条件,

            [EnumValue("dutyEvent/dutyBackground")]
            視察イベント視察アニメーション,

            [EnumValue("dutyEvent/rewardHeader")]
            視察イベント報酬ページヘッダー,

            [EnumValue("dutyEvent/situation")]
            視察イベント現在の視察先背景,

            #endregion

            #region formation

            [EnumValue("formation/60_60")]
            フォーメーション_60_60,

            [EnumValue("formation/90_120")]
            フォーメーション_90_120,

            [EnumValue("formation/150_200")]
            フォーメーション_150_200,

            #endregion

            #region gacha

            [EnumValue("gacha/display")]
            ガチャ表示カード,

            #endregion

            #region home

            [EnumValue("home/league")]
            昇格降格アイコン,

            [EnumValue("home/next/league")]
            次の試合のメッセージ_リーグ,

            [EnumValue("home/next/friendlyMatch")]
            次の試合のメッセージ_親善試合,

            #endregion

            #region icon

            [EnumValue("icon/")]
            ゲームアイコン,

            #endregion

            #region invite

            [EnumValue("invite")]
            招待報酬,

            #endregion

            #region item

            [EnumValue("item/80_80")]
            アイテム_80_80,

            [EnumValue("item/120_120")]
            アイテム_120_120,

            [EnumValue("item/150_150")]
            アイテム_150_150,

            #endregion

            #region fame
            [EnumValue("fame/80_80")]
            名声pt_80_80,

            [EnumValue("fame/120_120")]
            名声pt_120_120,

            [EnumValue("fame/150_150")]
            名声pt_150_150,

            #endregion

            #region league

            [EnumValue("game/emblem/533_541")]
            試合エンブレム_533_541,

            [EnumValue("game/emblem/80_80")]
            試合エンブレム_小,

            #endregion

            #region navi

            [EnumValue("navi")]
            ナビキャラ,

            #endregion

            #region notice
            [EnumValue("webview/rarity")]
            WebViewレアリティアイコン,
            #endregion

            #region player

            [EnumValue("player/60_60")]
            選手カード_60_60,

            [EnumValue("player/140_170")]
            選手カード_140_170,

            [EnumValue("player/280_340")]
            選手カード_280_340,

            #endregion

            #region sponsor

            [EnumValue("sponsor/80_80")]
            スポンサー_80_80,

            [EnumValue("sponsor/150_150")]
            スポンサー_150_150,

            #endregion

            #region supporter

            [EnumValue("supporter/80_80")]
            サポーター_80_80,

            [EnumValue("supporter/120_120")]
            サポーター_120_120,

            [EnumValue("supporter/150_150")]
            サポーター_150_150,

            #endregion

            #region tactics

            [EnumValue("tactics/60_60")]
            戦術カード_60_60,

            [EnumValue("tactics/82_78")]
            戦術カード_82_78,

            [EnumValue("tactics/90_120")]
            戦術カード_90_120,

            [EnumValue("tactics/150_200")]
            戦術カード_150_200,

            [EnumValue("tactics/240_320")]
            戦術カード_240_320,

            #endregion

            #region ticket

            [EnumValue("ticket/80_80")]
            チケット_80_80,

            [EnumValue("ticket/120_120")]
            チケット_120_120,

            [EnumValue("ticket/150_150")]
            チケット_150_150,

            #endregion

            #region top

            [EnumValue("top")]
            TOP画像,

            #endregion

            #region tutorial
            [EnumValue("tutorial")]
            チュートリアル,
            #endregion

            #region TODO 画像パス未定の人たち

            //これはとりあえず使ってないやつだからスルー
            [EnumValue("league/background")]
            所属リーグ背景,

            [EnumValue("help")]
            ヘルプ用画像,

            #endregion
        }
        #endregion 画像タイプ

        #region 画像拡張子

        /// <summary>
        /// 画像拡張用
        /// </summary>
        public enum ImageExtension
        {
            [EnumValue(".png")]
            PNG = 1,
            [EnumValue(".jpg")]
            JPG = 2,
        }

        #endregion

        /// <summary>
        /// 支払いの通貨タイプ
        /// </summary>
        public enum PaymentCurrencyType
        {
            GameCurrency = 1, // Ingot
            Gil = 2,
            Crystal = 3,
        }

        /// <summary>
        /// ギフトカテゴリ
        /// </summary>
        public enum GiftCategory
        {

        }

        /// <summary>
        /// 行動カテゴリ
        /// </summary>
        public enum ActivityCategory
        {

        }

        /// <summary>
        /// メッセージカテゴリ
        /// </summary>
        public enum MessageCategory
        {

        }

        /// <summary>
        /// バナーのターゲットデバイス
        /// </summary>
        [Flags]
        public enum BannerTargetDevices : int
        {
            Mobile = 1,
            iOS = 2,
            Android = 4,
            iOS_Hybrid = 8,
        }

        /// <summary>
        /// アイテム付与時のエラー
        /// </summary>
        public enum AddItemResult
        {
            付与完了 = 0,
            付与アイテムなし = 1,
            所持上限 = 2,
            付与済み = 3,
            効果適応 = 100,
            付与失敗 = 999,
        }

        /// <summary>
        /// UserLoginのStatus
        /// </summary>
        public enum UserLoginStatus
        {
            
            正常 = 0,
            移行中 = 1,
            移行済 = 2,
            Ban = 99,
        }

        /// <summary>
        /// UserIdとUUIDのペアが正しいか
        /// </summary>
        public enum CheckPairStatus
        {
            正常 = 0,
            移行済 = 1,
            パラメータ不正 = 2,
            復号化失敗 = 3,
            有効期限切れ = 4,
        }

        public enum Attribute
        {
            無属性 = 0x00000000,
            火属性 = 0x00000001,
            風属性 = 0x00000002,
            土属性 = 0x00000004,
            水属性 = 0x00000008,
            光属性 = 0x00000010,
            闇属性 = 0x00000020,
        }

        public enum SphereEffectType
        {
            None = 0,
            AccessorySlot,
            EquipmentBoost,
            Skill,
            AttackUp,
            DefenseUp,
            MagicAtkUp,
            MagicDefUp,
            LuckUp,
            HitPointUp,
            MoveOthreRankBoard,
            MoveSelectGrid,
            ChangeEquipmentBoost,
        }

        #region 課金
        public enum ReceiptStatus
        {
            正常 = 0,
            初期値 = 1,
        }

        public enum PurchaseState
        {
            購入 = 0,
            キャンセル = 1,
            払い戻し = 2,
        }

        public enum PaymentStatus
        {
            未検証 = 0,
            検証_OK = 1,
            検証_NG = 2,
            ユーザーキャンセル = 3,
            コイン付与完了 = 10,
        }
        #endregion 課金

        #region Labyrinth
        public enum UserMonsterStatus
        {
            AVAILABLE = 0,
            DISPATCHED = 1
        }

        public enum PullBackResultStatus
        {
            DIE = 0,
            ALIVE = 1
        }

        public enum QuestEventId
        {
            NORMAL_QUEST = 0,
            LABYRINTH = 1
        }

        public enum NotificationCategory
        {
            DISPATCH_MONSTER = 1
        }

        public enum FieldStatus
        {
            ACTIVE = 1,
            CLEARED = 2
        }

        public enum AreaStatus
        {
            LOCKED = 0,
            JOINED = 1,
            OPEN = 2,
            ACTIVE = 3,
            CLEARED = 4
        }

        public enum EquipmentCategory
        {
            WEAPON_MAIN = 1,
            WEAPON_SECONDARY = 2,
            WEAPON_RENTAL = 3,
            SHIELD = 4,
            ACCESSORY = 5,
            GEAR_BAG = 6,
            CONSUMABLE_ITEMS = 7
        }


        #endregion
    }
}