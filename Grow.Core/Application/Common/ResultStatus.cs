﻿namespace Grow.Core.Application.Common
{
    public class ResultStatus
    {
        public int Code { get; set; }
        public string Message { get; set; }
    }
}