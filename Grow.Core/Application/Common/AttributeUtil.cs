﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.Application.Common
{
    public class AttributeUtil
    {
        public static IEnumerable<AppCode.Attribute> GetAttributes(int x)
        {
            if (x == (int)AppCode.Attribute.無属性)
                yield return AppCode.Attribute.無属性;
            if ((x & (int)AppCode.Attribute.火属性) != 0)
                yield return AppCode.Attribute.火属性;
            if ((x & (int)AppCode.Attribute.風属性) != 0)
                yield return AppCode.Attribute.風属性;
            if ((x & (int)AppCode.Attribute.土属性) != 0)
                yield return AppCode.Attribute.土属性;
            if ((x & (int)AppCode.Attribute.水属性) != 0)
                yield return AppCode.Attribute.水属性;
            if ((x & (int)AppCode.Attribute.光属性) != 0)
                yield return AppCode.Attribute.光属性;
            if ((x & (int)AppCode.Attribute.闇属性) != 0)
                yield return AppCode.Attribute.闇属性;
        }

        /// <summary>
        /// 有効属性取得
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static IEnumerable<AppCode.Attribute> GetEffectiveAttributes(int x)
        {
            // 無属性は対向属性がない
            //if (x == (int)AppCode.Attribute.無属性)
                //yield return AppCode.Attribute.無属性;
            if ((x & (int)AppCode.Attribute.火属性) != 0)
                yield return AppCode.Attribute.風属性;
            if ((x & (int)AppCode.Attribute.風属性) != 0)
                yield return AppCode.Attribute.土属性;
            if ((x & (int)AppCode.Attribute.土属性) != 0)
                yield return AppCode.Attribute.水属性;
            if ((x & (int)AppCode.Attribute.水属性) != 0)
                yield return AppCode.Attribute.火属性;
            if ((x & (int)AppCode.Attribute.光属性) != 0)
                yield return AppCode.Attribute.闇属性;
            if ((x & (int)AppCode.Attribute.闇属性) != 0)
                yield return AppCode.Attribute.光属性;
        }
    }
}
