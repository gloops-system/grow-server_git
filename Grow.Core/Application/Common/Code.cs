﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.Common
{
    public enum AppResponseCode
    {
        // >= 850 for application
        InvalidUser = 850,
        InvalidDispatch = 851,
        InvalidPullBack = 852,
        InvalidUserMonster = 853,
        InvalidReviveMonsterStatus = 854,
        NotEnoughGold = 1000,
        NotEnoughGem = 2000
    }

    public enum DispatchMonsterStatus
    {
        ALIVE = 0,
        PULLBACKED = 1,
        DIED = 2,
        TIMEOUT = 3
    }

    public enum UserMonsterStatus
    {
        AVAILABLE = 0,
        DISPATCHED = 1        
    }

    public enum PullBackResultStatus
    {
        DIE = 0,
        ALIVE = 1
    }

    public enum QuestEventId
    {
        NORMAL_QUEST = 0,
        LABYRINTH = 1
    }

    public enum NotificationCategory
    {
        DISPATCH_MONSTER = 1
    }

    public enum AreaStatus
    {
        LOCKED = 0,
        JOINED = 1,
        OPEN = 2,
        ACTIVE = 3,
        CLEARED = 4
    }
}
