﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.Common
{
    public class AppException : Exception
    {
        private AppResponseCode code;
        public AppException(AppResponseCode code, string message = null)
            : base(!String.IsNullOrEmpty(message) ? message : AppMessage.Instance.GetMessage(code)) { this.code = code; }

        public AppResponseCode Code
        {
            get { return this.code; }
        }
    }
}
