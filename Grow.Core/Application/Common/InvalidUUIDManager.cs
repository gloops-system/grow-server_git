﻿using System;
using Fango.Redis.Caching;

namespace Grow.Core.Application.Common
{
    public class InvalidUUIDManager
    {
        private const string CacheKeyPrefix = "InvalidUUID-";

        /// <summary>
        /// キャッシュが含まれているか確認する
        /// </summary>
        public static bool IsInvalidUUID(string uuid)
        {
            //キャッシュの確認
            var cacheKey = CacheKeyPrefix + uuid;
            var cacheClient = new RedisCacheClient();
            var hasCache = cacheClient.Get<bool?>(cacheKey);

            return hasCache == null;
        }

        /// <summary>
        /// 無効なUUIDの指定。30分キャッシュ
        /// </summary>
        public static void SetInvalidUUID(string uuid)
        {
            var cacheKey = CacheKeyPrefix + uuid;
            var cacheClient = new RedisCacheClient();
            cacheClient.Add(cacheKey, true, TimeSpan.FromMinutes(30));
        }
    }
}