﻿using System.Collections.Generic;

namespace Grow.Core.Application.Common
{
    public partial class AppCode
    {
        #region ログイン周り
        public static readonly string TestingUUID = @"TestingUUID";
        #endregion

        #region ユーザー関連

        /// <summary>
        /// ユーザーがひとつのアイテムで持てる最大所持数
        /// </summary>
        public static readonly int ItemMaxNum = 99;

        #endregion

        #region フレンド関連

        /// <summary>
        /// 非フレンドリストで表示する人数
        /// </summary>
        public static readonly int ShowNonFriendNum = 10;

        /// <summary>
        /// 非フレンドリストで表示する条件(最終ログイン)
        /// </summary>
        public static readonly int ShowNonFriendBeforeDay = -7;

        /// <summary>
        /// 非フレンドリストで表示する条件(調整値)
        /// 小さい数字にするほど、表示されやすい
        /// </summary>
        public static readonly int ShowNonFriendAdjustNum = 1;

        #endregion

        #region キャラクター関連

        /// <summary>
        /// ユーザーが所持するキャラクター最小値
        /// </summary>
        public static readonly int MinUserCharacterNum = 1;

        /// <summary>
        /// パーティー数
        /// </summary>
        public static readonly int PartyMaxNum = 5;

        /// <summary>
        /// パーティーに設定できるキャラクター数
        /// </summary>
        public static readonly int PartyMaxCharacterNum = 8;

        #endregion

        #region ガチャ関連

        /// <summary>
        /// ガチャで1度に購入できる最大値
        /// </summary>
        public static readonly int GachaMaxNum = 10;

        #endregion

        #region ショップ関連

        public static readonly int ShopMaxBuyNum = 10;

        #endregion

        #region 招待関連

        /// <summary>
        /// ユーザーが招待コードを発行後、招待が成功した時に報酬が貰える回数
        /// </summary>
        public static readonly int MaxInviteNum = 10;

        #endregion

        #region 不要？

        public static int GamePointRate = 100;
        public static readonly int ActionPointHealInterval = 60;
        public static readonly int GamePointHealInterval = 60;
        public static readonly int BestPositionPowerRate = 100;
        public static readonly int BetterPositionPowerRate = 80;
        public static readonly int OtherPositionPowerRate = 50;        

        public static readonly int ログイン有効期間 = 120;

        public static readonly int LifeHealInterval = 60 * 3; //10800; // 3 hours
        public static readonly int StaminaHealInterval = 60 * 3;//300; // 5 minutes

        #endregion

        #region User Init Data

        public const int DefaultGem = 100;
        public const int DefaultGold = 50000;
        public const int DefaultStamina = 10;
        public const int DefaultHp = 100;
        public const int DefaultSp = 100;
        public const int DefaultAttack = 10;
        public const int DefaultDefense = 0;
        public const int DefaultCutPercentage = 10;
        public const int DefaultCriticalPercentage = 10;
        public const int DefaultStunValue = 10;
        public const int DefaultStunResistance = 10;
        public const int DefaultStunRecoveryStand = 10;
        public const int DefaultStunRecoveryDown = 10;
        public const int DefaultScoreUp = 10;
        public const int FirstQuestAreaId = 0;
        public const int FirstLabyrinthAreaId = 0;
        public const int DefaultGearBoxNum = 10;
        public const int DefaultFriendsNum = 10;
        public const int DefaultMonsterBoxNum = 10;
        public const int DefaultItemBoxNum = 10;
        public const int DefaultGearBagNum = 5;
        public const int DefaultUsableMonstersNum = 5;
        public const int DefaultItemBagNum = 5;

        public static readonly int[] ListDefaultAvatarId = { 1 };
        public static readonly int[] ListDefaultMonsterId = { 1, 2, 3, 4, 5 };
        public static readonly int[] ListDefaultGearId = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 107, 111, 301, 302 };
        public static readonly Dictionary<int, int> ListDefaultItemId = new Dictionary<int, int> { 
            {1, 5}, {2,5}, {3, 5}, {4,5}, {5,5}, {6,5}
        };

        #endregion
    }
}