﻿using Grow.Core.Application;
using Grow.Core.Application.User;
using Grow.Data.User.DataSet.Container;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.Common
{
    public class ModelBase
    {
        protected GrowApplicationModel ApplicationContext { get; private set; }
        protected GrowUser User { get; private set; }

        protected ModelBase() { }
        protected ModelBase(GrowApplicationModel applicationContext)
        {
            ApplicationContext = applicationContext;
            User = ApplicationContext.CurrentUser;
        }
        //public int UserId { get; set; }

        //public UserViewModel Userinfo { get; set; }

        //protected readonly UserDataContainer ApplicationContext.UserDataContainer;

        //public ModelBase(int userId)
        //{
        //    UserId = userId;
        //    ApplicationContext.UserDataContainer = new UserDataContainer();
        //}

        //public ModelBase(UserViewModel userinfo)
        //{
        //    UserId = userinfo.UserId;
        //    Userinfo = userinfo;
        //    ApplicationContext.UserDataContainer = new UserDataContainer();
        //}
    }
}
