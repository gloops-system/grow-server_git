﻿namespace Grow.Core.Application.Common
{
    public class ErrorMessage
    {
        public const string BadRequest = "無効なリクエストです";

        public const string InvalidUUID = "ゲームデータを他の端末へ移行済みです。アプリを再インストールしてください。";
        public const string InvalidUUID2 = "新しい端末に移行済みです。再びゲームを始める場合は、一度アプリを削除して、インストールし直して下さい。";

        public const string DecryptError = "復号化に失敗しました。";

        public const string Expired = "有効期限切れです。端末の時刻が合っていない可能性があります。";

        public const string NotEnough = "要素が足りません";

        public const string FailedToTransfer = "データ移行に失敗しました";

        public const string InvalidPassword = "入力したゲームIDとパスワードが見つかりません";

        public const string InvalidGameId = "ゲームIDが正しくありません";

        public const string InvalidUser = "無効なユーザです";

        public const string TransferingUser = "移行設定中なのでデータの取り込みはできません";

        public const string PasswordValidationError = "パスワードは半角英数字6文字以上12文字以下で入力してください";

        public const string InMaintenanceMode = "現在メンテナンス中です";
    }
}
