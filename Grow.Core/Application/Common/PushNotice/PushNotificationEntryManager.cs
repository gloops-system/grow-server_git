﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using Fango.Core.Sequence;
//using Edge.Core.Caching;
//using Edge.Core.League;
//using Edge.Data.General.DataSet.Adapter.Queue;

namespace Grow.Core.Application.Common.PushNotice
{
    public class PushNotificationEntryManager
    {
        //private readonly UserDevice _userDevice;

        //public PushNotificationEntryManager(int targetUserId)
        //{
        //    _userDevice = new UserDevice(targetUserId);
        //}

        //#region 各種プッシュ通知のキュー登録
        ///// <summary>
        ///// 親善試合申し込みプッシュ通知
        ///// </summary>
        ///// <param name="userId"></param>
        ///// <param name="selfNick"></param>
        ///// <returns></returns>
        //public bool QueueEntryNotificateGameOffer(int userId, string selfNick)
        //{
        //    if (!_userDevice.HaveSetting(AppCode.PushNotificationSettings.親善試合))
        //        return false;
            
        //    var queueId = SequenceClientManager.DefaultSequenceClient.GetNextValue(CacheKey.QueuePushNotificationsId(), 1);
        //    new QueueDataContainer().QueuePushNotifications.AddNew(queueId,
        //                                                           _userDevice.DeviceToken,
        //                                                           (int)AppCode.PushNotificationType.親善試合申し込み,
        //                                                           GetNortificateMessage(AppCode.PushNotificationType.親善試合申し込み, new object[] {selfNick, "親善試合"}),
        //                                                           GetNortificateSoundPath(AppCode.PushNotificationType.親善試合申し込み),
        //                                                           _userDevice.UserId,
        //                                                           userId);

        //    return true;
        //}

        ///// <summary>
        ///// 親善試合受諾プッシュ通知
        ///// </summary>
        ///// <param name="userId"></param>
        ///// <param name="selfNick"></param>
        ///// <returns></returns>
        //public bool QueueEntryNotificateGameOfferAccept(int userId, string selfNick)
        //{
        //    if (!_userDevice.HaveSetting(AppCode.PushNotificationSettings.親善試合))
        //        return false;

        //    var queueId = SequenceClientManager.DefaultSequenceClient.GetNextValue(CacheKey.QueuePushNotificationsId(), 1);
        //    new QueueDataContainer().QueuePushNotifications.AddNew(queueId,
        //                                                           _userDevice.DeviceToken,
        //                                                           (int)AppCode.PushNotificationType.親善試合受諾,
        //                                                           GetNortificateMessage(AppCode.PushNotificationType.親善試合受諾, new object[] { selfNick, "親善試合" }),
        //                                                           GetNortificateSoundPath(AppCode.PushNotificationType.親善試合受諾),
        //                                                           _userDevice.UserId,
        //                                                           userId);

        //    return true;
        //}

        ///// <summary>
        ///// リーグ戦各試合終了時通知
        ///// </summary>
        ///// <param name="userId"></param>
        ///// <param name="gameNum"></param>
        ///// <param name="distributionTime"></param>
        ///// <returns></returns>
        //public bool QueueEntryNotificateGameJudge(int userId, int gameNum, DateTimeOffset distributionTime)
        //{
        //    if (!_userDevice.HaveSetting(AppCode.PushNotificationSettings.リーグ戦))
        //        return false;

        //    var queueId = SequenceClientManager.DefaultSequenceClient.GetNextValue(CacheKey.QueuePushNotificationsId(), 1);
        //    new QueueDataContainer().QueuePushNotifications.AddNew(queueId,
        //                                                           _userDevice.DeviceToken,
        //                                                           (int)AppCode.PushNotificationType.リーグ戦試合終了,
        //                                                           GetNortificateMessage(AppCode.PushNotificationType.リーグ戦試合終了, new object[] { gameNum }),
        //                                                           GetNortificateSoundPath(AppCode.PushNotificationType.リーグ戦試合終了),
        //                                                           _userDevice.UserId,
        //                                                           userId,
        //                                                           distributionTime);

        //    return true;
        //}

        ///// <summary>
        ///// リーグ戦終了通知
        ///// </summary>
        ///// <param name="userId"></param>
        ///// <param name="leagueId"></param>
        ///// <param name="distributionTime"></param>
        ///// <returns></returns>
        //public bool QueueEntryNotificationLeagueJudge(int userId, int leagueId, DateTimeOffset distributionTime)
        //{
        //    if (!_userDevice.HaveSetting(AppCode.PushNotificationSettings.リーグ戦))
        //        return false;

        //    var queueId = SequenceClientManager.DefaultSequenceClient.GetNextValue(CacheKey.QueuePushNotificationsId(), 1);
        //    new QueueDataContainer().QueuePushNotifications.AddNew(queueId,
        //                                                           _userDevice.DeviceToken,
        //                                                           (int)AppCode.PushNotificationType.リーグ戦終了,
        //                                                           GetNortificateMessage(AppCode.PushNotificationType.リーグ戦終了, new object[] { LeagueManager.GetLeagueName(leagueId) }),
        //                                                           GetNortificateSoundPath(AppCode.PushNotificationType.リーグ戦終了),
        //                                                           _userDevice.UserId,
        //                                                           userId,
        //                                                           distributionTime);

        //    return true;
        //}

        ///// <summary>
        ///// ニュース通知
        ///// </summary>
        ///// <param name="userId"></param>
        ///// <param name="message"></param>
        ///// <param name="distributionTime"></param>
        ///// <returns></returns>
        //public bool QueueEntryNotificationNews(int userId, string message, DateTimeOffset distributionTime)
        //{
        //    if (!_userDevice.HaveSetting(AppCode.PushNotificationSettings.ニュース))
        //        return false;

        //    var queueId = SequenceClientManager.DefaultSequenceClient.GetNextValue(CacheKey.QueuePushNotificationsId(), 1);
        //    new QueueDataContainer().QueuePushNotifications.AddNew(queueId,
        //                                                           _userDevice.DeviceToken,
        //                                                           (int)AppCode.PushNotificationType.ニュース通知,
        //                                                           message,
        //                                                           GetNortificateSoundPath(AppCode.PushNotificationType.リーグ戦終了),
        //                                                           _userDevice.UserId,
        //                                                           userId,
        //                                                           distributionTime);

        //    return true;
        //}
        //#endregion

        ///// <summary>
        ///// メッセージ内容を取得
        ///// </summary>
        ///// <param name="type"></param>
        ///// <param name="param"></param>
        ///// <returns></returns>
        //private static string GetNortificateMessage(AppCode.PushNotificationType type, IEnumerable<object> param)
        //{
        //    string　message = string.Empty;
        //    switch (type)
        //    {
        //        case AppCode.PushNotificationType.親善試合申し込み:
        //            message = "{0}さんから{{0}}の申し込みです";
        //            break;
        //        case AppCode.PushNotificationType.親善試合受諾:
        //            message = "{0}さんが{{0}}を受諾しました";
        //            break;
        //        case AppCode.PushNotificationType.リーグ戦試合終了:
        //            message = "リーグ戦第{0}試合が終了しました";
        //            break;
        //        case AppCode.PushNotificationType.リーグ戦終了:
        //            message = "{0}の結果が出ました!";
        //            break;
        //    }
        //    return param.Aggregate(message, string.Format);
        //}

        ///// <summary>
        ///// サウンドパスを取得
        ///// </summary>
        ///// <param name="type"></param>
        ///// <returns></returns>
        //private static string GetNortificateSoundPath(AppCode.PushNotificationType type)
        //{
        //    string soundPath;
        //    switch (type)
        //    {
        //        default:
        //            soundPath = AppCode.PushDefaultSoundPath;
        //            break;
        //    }
        //    return soundPath;
        //}
    }
}
