﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Core.Application.Common
{
    public partial class AppCode
    {
        public enum AICategory
        {
            プレイヤー戦士 = 10,
            プレイヤー武道家 = 20,
            プレイヤー魔法使い = 30,
            狼 = 1000,
            スライム = 2000,
            ゴブリン = 3000,
        }

        public enum AITarget
        {
            自分自身 = 1,
            フレンド1 = 2,
            フレンド2 = 3,
            フレンド3 = 4,
            フレンド全員 = 5,


            近い敵 = 10001,
            遠い敵 = 10002,
            戦っている敵 = 10003,
        }

        public enum AIParamater
        {
            HP = 1,
            MP = 2,
            ATK = 3,
            DEF = 4,
            SPD = 5,
        }

        public enum AICondition
        {
            同じ = 1,
            以上 = 2,
            以下 = 3,
            超える = 4,
            未満 = 5,
        }

        public enum AIAction
        {
            行動 = 1,
            スキル = 2,
            アニメーション = 3,
        }

        public enum AIActionDetailAttack
        {
            攻撃 = 1,
            防御 = 2,
        }

        public enum AIActionDetailSkill
        {
            //TODO:スキルマスターから取得？
            Hoge = 1,
            Foo = 2,
        }

        public enum AIActionDetailAnimation
        {
            無表情 = 1,
            笑う = 2,
            泣く = 3,
            怒る = 4,
        }
    }
}
