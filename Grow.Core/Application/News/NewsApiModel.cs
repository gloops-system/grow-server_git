﻿using System.Linq;
using System.Collections.Generic;
using Grow.Core.Application;
using Grow.Core.Application.Util;
using Grow.Core.InterfaceModel.News;
using Fango.Web.Core.ApiModel;
using Grow.Core.Application.Image;

namespace Grow.Core.Application.News
{
    public class NewsApiModel : GrowApiModel
    {
        public NewsApiModel(GrowApplicationModel applicationContext)
            : base(applicationContext) { }

        public ContentResponse<NewsViewModel> GetNewsInfo()
        {
            var contentResponse = new ContentResponse<NewsViewModel>();

            // Container
            var newsDataContainer = ApplicationContext.NewsDataContainer;

            // RepositoryAdapter
            var newsMasterDataRepositoryAdapter = newsDataContainer.NewsMaster;

            var newsList = new List<NewsModel>();
            var newsMaster = newsMasterDataRepositoryAdapter.GetDataInSession(ApplicationContext.Now)
                                                        .OrderBy(x => x.DipsOrder)
                                                        .ThenByDescending(y => y.FromDate);
            foreach (var newsMasterRow in newsMaster)
            {
                newsList.Add(new NewsModel
                    {
                        NewsId = newsMasterRow.NewsId,
                        PostDate = newsMasterRow.FromDate.ToString("yyyy/MM/dd HH:mm"),
                        Title = newsMasterRow.Title,
                        Body = newsMasterRow.Body,
                        Action = newsMasterRow.IsActionNull() ? null : (int?) newsMasterRow.Action,
                    });
            }

            contentResponse.AppData = new NewsViewModel
                {
                    EntryNum = newsMaster.Count(),
                    News = newsList,
                };

            return contentResponse;
        }

        public ContentResponse<NewsResultViewModel> OpenNews(int newsId)
        {
            var contentResponse = new ContentResponse<NewsResultViewModel>();

            var user = ApplicationContext.CurrentUser;

            // Container
            var newsDataContainer = ApplicationContext.NewsDataContainer;
            var userDataContainer = ApplicationContext.UserDataContainer;

            // RepositoryAdapter
            var newsMasterDataRepositoryAdapter = newsDataContainer.NewsMaster;
            var userNewsDataRepositoryAdapter = userDataContainer.UserNews;

            // 追加
            var userNews = userNewsDataRepositoryAdapter.GetDataByUserId(user.UserId);
            if (userNews.All(x => x.NewsId != newsId))
                userNewsDataRepositoryAdapter.AddNew(user.UserId, newsId, ApplicationContext.Now);

            var newsMasterRow = newsMasterDataRepositoryAdapter.GetDataByPK(newsId);
            contentResponse.AppData = new NewsResultViewModel
                {
                    HasImage = !newsMasterRow.IsImageUrlNull(),
                    ImageBase64 = ImageUtil.ToBase64(newsMasterRow.IsImageUrlNull() ? null : newsMasterRow.ImageUrl),
                };

            return contentResponse;
        }
    }
}