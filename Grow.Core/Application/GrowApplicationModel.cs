﻿using System;
using System.Reflection;
using System.Threading;
using Fango.Redis.Caching;
using Grow.Core.Application.Caching;
using Grow.Core.Application.User;
using Grow.Core.Application.Util;
using Grow.Data.DataSet.Container;
using Grow.Data.General.DataSet.Container;
using Grow.Data.History.DataSet.Container;
using Grow.Data.User.DataSet.Container;
using Grow.Data.Duty.DataSet.Container;
using Grow.Data.Guild.DataSet.Container;
using Fango.Core.Logging;
using Grow.Core.Application.Common;
using Fango.Web.Core.ApiModel;

namespace Grow.Core.Application
{
    public class GrowApplicationModel : Fango.Core.Application.ApplicationModel
    {
        // TODO 共通リソースの管理方法を要再考
        public static ApplicationResources Resources;

        public GrowUser CurrentUser { get; protected set; }
        public Version ClientVersion { get; protected set; }
        
        public DateTimeOffset Now { get; protected set; }
        public Func<ContentResponse, int> MapCodeToStatus { get; set; }

        // Grow.Data.
        private Lazy<MasterDataContainer> _masterDataContainer;
        public MasterDataContainer MasterDataContainer { get { return _masterDataContainer.Value; } }

        private Lazy<AppPaymentDataContainer> _appPaymentDataContainer;
        public AppPaymentDataContainer AppPaymentDataContainer { get { return _appPaymentDataContainer.Value; } }

        //private Lazy<LocalizeDataContainer> _localizeDataContainer;
        //public LocalizeDataContainer LocalizeDataContainer { get { return _localizeDataContainer.Value; } }

        private Lazy<NewsDataContainer> _newsDataContainer;
        public NewsDataContainer NewsDataContainer { get { return _newsDataContainer.Value; } }

        // Grow.Data.History
        private Lazy<HistoryDataContainer> _historyDataContainer;
        public HistoryDataContainer HistoryDataContainer { get { return _historyDataContainer.Value; } }

        // Grow.Data.User
        private Lazy<UserDataContainer> _userDataContainer;
        public UserDataContainer UserDataContainer { get { return _userDataContainer.Value; } }

        private Lazy<UserLabyrinthDataContainer> _userLabyrinthDataContainer;
        public UserLabyrinthDataContainer UserLabyrinthDataContainer { get { return _userLabyrinthDataContainer.Value; } }

        // Grow.Data.Duty
        private Lazy<DutyDataContainer> _dutyDataContainer;
        public DutyDataContainer DutyDataContainer { get { return _dutyDataContainer.Value; } }

        // Grow.Data.Guild
        private Lazy<GuildDataContainer> _guildDataContainer;
        public GuildDataContainer GuildDataContainer { get { return _guildDataContainer.Value; } }


        public GrowApplicationModel()
        {
            InitializeDataContainers();

            // TODO 暫定で一律 200 OK を返すようにする。要修正
            MapCodeToStatus = (contentResponse) => 200;
        }

        public GrowApplicationModel(int userId, bool isCancelHealUserStatus = false, Version version = null)
            : this()
        {
            CurrentUser = GrowUser.GetInstance(userId);
            //ClientVersion = version ?? GetClientVersion(); TODO:一旦コメントアウトしました。
            Now = GrowDateUtil.GetDateTimeOffsetNow(CurrentUser.UserId);
            UpdateUserLastLogin(CurrentUser.UserId);
        }

        private void InitializeDataContainers()
        {
            // Grow.Data.Battle

            // Grow.Data
            _masterDataContainer = new Lazy<MasterDataContainer>(() => new MasterDataContainer(), true);
            _appPaymentDataContainer = new Lazy<Data.DataSet.Container.AppPaymentDataContainer>(() => new AppPaymentDataContainer(), true);
            //_localizeDataContainer = new Lazy<LocalizeDataContainer>(() => new LocalizeDataContainer(), true);
            _newsDataContainer = new Lazy<NewsDataContainer>(() => new NewsDataContainer(), true);
            // Grow.Data.History
            _historyDataContainer = new Lazy<HistoryDataContainer>(() => new HistoryDataContainer(), true);
            // Grow.Data.User
            _userDataContainer = new Lazy<UserDataContainer>(() => new UserDataContainer(), true);
            _userLabyrinthDataContainer = new Lazy<UserLabyrinthDataContainer>(() => new UserLabyrinthDataContainer(), true);
            // Grow.Data.Duty
            _dutyDataContainer = new Lazy<DutyDataContainer>(() => new DutyDataContainer(), true);
            //Grow.Data.Guild
            _guildDataContainer = new Lazy<GuildDataContainer>(() => new GuildDataContainer(), true);
        }

        private void UpdateUserLastLogin(int userId)
        {
            try
            {
                var redis = new RedisCacheClient();
                var expire = CacheLimit.UserLastLogin;

                int value;
                var m = MethodBase.GetCurrentMethod();
                var cacheKey = string.Format("{0}-{1}", m.DeclaringType.Name + "." + m.Name, userId);
                if (redis.TryGet(cacheKey, out value))
                    return;

                redis.Add(cacheKey, 1, expire);

                // TODO System.net.Http ライブラリに依存しているため分割する
                var userAgent = System.Web.HttpContext.Current.Request.UserAgent;
                //UserDataContainer.UserLastLogin.UpdateIsOnlineAndLastLoginByPK(userId, true, 0); // TODO : Device情報取得
            }
            catch (Exception exception)
            {
                LoggerManager.DefaultLogger.WarnFormat("UserLastLogin Update Failed:{0}\n{1}\n{2}", userId, exception.Message, exception.StackTrace);
            }
        }

        private static Version GetClientVersion()
        {
            var principal = Thread.CurrentPrincipal;
            var splitedName = principal.Identity.Name.Split(',');

            Version version;
            var canParse = Version.TryParse(splitedName[1], out version);
            if (!canParse)
            {
                //LoggerManager.DefaultLogger.Error("クライアントバージョンをparse出来ませんでした。 principal.Identity.Name = " + principal.Identity.Name);
                throw new Exception("クライアントバージョンをParseできませんでした");
            }
            return version;
        }
    }

    // TODO 共通リソースの管理方法を要再考
    public class ApplicationResources
    {
        public Func<string> GetBaseUrl { get; set; }
    }
}