﻿using Grow.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grow.Core.Application.User;
using Grow.Core.InterfaceModel.User;
using Grow.Core.InterfaceModel.Gear;
using Grow.Core.Application;
using Grow.Data.User.DataSet;

namespace Grow.Core.Application.Gear
{
    public class GearUser : ModelBase
    {
        //public GearUser(int userId) : base(userId) { }

        public GearUser(GrowApplicationModel applicationContext) : base(applicationContext) { }

        public List<UserGearModel> GetGearList()
        {
            var userGearModelList = new List<UserGearModel>();
            //var skills = new List<UserGearSkillModel>();
            //Dictionary<int, List<UserGearSkillModel>> userGearSkillModelDict = this.GetGearSkillDict();
            var userGearList = ApplicationContext.UserDataContainer.UserGear.GetDataByUserId(User.UserId);
            foreach (var userGear in userGearList)
            {
                var userGearModel = new UserGearModel()
                {
                    UserGearId = userGear.UserGearId,
                    UserId = userGear.UserId,
                    GearId = userGear.GearId,
                    GearType = userGear.GearType,
                    Category = userGear.Category,
                    Property = userGear.Property,
                    Rarity = userGear.Rarity,
                    Attack = userGear.Attack,
                    Defense = userGear.Defense,
                    CutPercentage = userGear.CutPercentage,
                    Stun = userGear.Stun,
                    Point = userGear.Point
                };
                userGearModelList.Add(userGearModel);
            }
            return userGearModelList;
        }

        public long AddNew(int GearId)
        {
            var gearMasterAdapter = ApplicationContext.MasterDataContainer.GearMaster;
            long userGearId = 0;
            var gearMaster = gearMasterAdapter.GetDataByGearId(GearId);
            userGearId = ApplicationContext.UserDataContainer.UserGear.AddNew(User.UserId, gearMaster.GearId, gearMaster.GearType, gearMaster.Category,
                                                                gearMaster.Property, gearMaster.Rarity, gearMaster.Attack, gearMaster.Defense,
                                                                gearMaster.CutPercentage, gearMaster.Stun, gearMaster.Point);
            return userGearId;
        }

        public void InsertBulkUserGear(int[] GearIds)
        {
            var userGearDataTable = new UserDataSet.UserGearDataTable();
            var gearMasterAdapter = ApplicationContext.MasterDataContainer.GearMaster;
            foreach (var GearId in GearIds)
            {
                var userGearRow = userGearDataTable.NewUserGearRow();
                var GearMaster = gearMasterAdapter.GetDataByGearId(GearId);
                userGearRow.UserId = User.UserId;
                userGearRow.GearId = GearMaster.GearId;
                userGearRow.GearType = GearMaster.GearType;
                userGearRow.Category = GearMaster.Category;
                userGearRow.Property = GearMaster.Property;
                userGearRow.Rarity = GearMaster.Rarity;
                userGearRow.Attack = GearMaster.Attack;
                userGearRow.Defense = GearMaster.Defense;
                userGearRow.CutPercentage = GearMaster.CutPercentage;
                userGearRow.Stun = GearMaster.Stun;
                userGearRow.Point = GearMaster.Point;
                userGearRow.AddTime = ApplicationContext.Now;
                userGearRow.UpdtTime = ApplicationContext.Now;
                userGearDataTable.AddUserGearRow(userGearRow);
            }
            ApplicationContext.UserDataContainer.UserGear.BulkInsert(userGearDataTable);
        }

        /*
        private Dictionary<int, List<UserGearSkillModel>> GetGearSkillDict()
        {
            Dictionary<int, List<UserGearSkillModel>> userGearSkillDict = new Dictionary<int, List<UserGearSkillModel>>();
            List<UserGearSkillModel> userGearSkillModelList = new List<UserGearSkillModel>();
            UserGearSkillModel userGearSkillModel = null;
            var userGearSkillList = new UserGearSkillTableRepositoryAdapter().GetDataByUserId(Userinfo.UserId);
            foreach (var userGearSkill in userGearSkillList)
            {
                userGearSkillModel = new UserGearSkillModel()
                {
                    UserGearId = userGearSkill.UserGearId,
                    GearSkillDetailId = userGearSkill.GearSkillDetailId,
                    SkillName = userGearSkill.SkillName,
                    Attack = userGearSkill.Attack,
                    Effect = userGearSkill.Effect
                };
                var userGearId = Convert.ToInt32(userGearSkillModel.UserGearId);
                if(!userGearSkillDict.ContainsKey(userGearId))
                {
                    userGearSkillDict[userGearId] = new List<UserGearSkillModel>();
                }
                userGearSkillDict[userGearId].Add(userGearSkillModel);
            }
            return userGearSkillDict;
        }
        */
    }
}
