﻿using Grow.Core.InterfaceModel.Gear;
using Fango.Web.Core.ApiModel;

namespace Grow.Core.Application.Gear
{
    public class UserGearApiModel : GrowApiModel
    {
        public UserGearApiModel(GrowApplicationModel applicationContext)
            : base(applicationContext) { }

        /// <summary>
        /// フレンドの取得
        /// </summary>
        /// <returns></returns>
        public ContentResponse<UserGearsViewModel> GetUserGears()
        {
            var contentResponse = new ContentResponse<UserGearsViewModel>();

            //var user = ApplicationContext.CurrentUser;
            var now = ApplicationContext.Now;
            var userGearList = new GearUser(ApplicationContext).GetGearList();
            contentResponse.AppData = new UserGearsViewModel()
            {
                UserGears = userGearList
            };

            return contentResponse;
        }
    }
}