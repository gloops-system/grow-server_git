﻿using Fango.Core.Logging;
using Grow.Core.Application;
using Grow.Core.Common;
//using Edge.Core.Configuration;
using Grow.Core.Purchase;
using Grow.Core.InterfaceModel.Purchase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grow.Core.Application.Common;
using Fango.Web.Core.ApiModel;

namespace Grow.Core.Application.Purchase
{
    public class AndroidAppPaymentApiModel : GrowApiModel
    {
        public AndroidAppPaymentApiModel(GrowApplicationModel applicationContext)
            : base(applicationContext) { }

        /*** /api/purchase/receipt GET ***/

        public ContentResponse<PurchaseViewModel> ExecutePurchase(string receipt,string signature)
        {
            try
            {
                var userId = ApplicationContext.CurrentUser.UserId;
                var resultContent = new ContentResponse<PurchaseViewModel>();
                var userDataContainer = ApplicationContext.UserDataContainer;
                var appPaymentData = ApplicationContext.AppPaymentDataContainer;

                // レシート検証
                var androidPayment = new AndroidAppPayment(ApplicationContext, receipt, signature);
                var receiptModel = androidPayment.VerifyReceipt();

                var userGemRow = userDataContainer.UserGem.FindUserGem(userId);
                var gem = userGemRow == null ? 0 : userGemRow.Num;

                if (receiptModel == null)
                {
                    resultContent.AppData = new PurchaseViewModel()
                    {
                        BeforeGem = gem,
                        Gem = gem,
                    };
                    return resultContent;
                }

                var buyGem = 0;
                if (receiptModel.PurchaseState == (int)AppCode.PurchaseState.購入 &&
                    receiptModel.Status == (int)AppCode.PaymentStatus.検証_OK)
                {
                    ////セール中
                    //var saleMasterRow = ApplicationContext.AppPaymentDataContainer.SaleMaster.GetCurrent();
                    //if (saleMasterRow != null)
                    //{
                    //    var saleCoinMasterRows =
                    //        ApplicationContext.AppPaymentDataContainer.SaleCoinMaster.GetDataBySaleId((int)AppCode.TargetDevice.Android, saleMasterRow.SaleId);
                    //    buyGem = saleCoinMasterRows.First(x => x.ProductId == receiptModel.ProductId).Coin;
                    //}

                    //セールショップ対象がなかったら通常のもので代替
                    if (buyGem == 0)
                    {
                        var coinMasterRows = ApplicationContext.AppPaymentDataContainer.AndroidGemMaster.FindItems();
                        buyGem = coinMasterRows.First(x => x.ProductId == receiptModel.ProductId).Gem;
                    }
                    // Coin付与
                    userDataContainer.UserGem.IncrementUserGem(userId, buyGem);
                    // Update Status
                    appPaymentData.AndroidAppPayment.UpdateStatus(receiptModel.OrderId, (int)AppCode.PaymentStatus.コイン付与完了);
                }

                resultContent.AppData = new PurchaseViewModel()
                {
                    BeforeGem = gem,
                    Gem = gem + buyGem,
                };

                return resultContent;
            }
            catch (Exception e)
            {
                LoggerManager.DefaultLogger.ErrorFormat("Payment:{0}", e.StackTrace);
                return null;
            }
        }
    }
}
