﻿using Fango.Core.Logging;
using Grow.Core.Application;
using Grow.Core.Common;
//using Edge.Core.Purchase;
//using Edge.Core.InterfaceModel.Purchase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fango.Core.Sequence;
using Grow.Core.InterfaceModel.Purchase;
using Grow.Core.Purchase;
using Grow.Core.Application.Common;
using Grow.Core.Application.Caching;
using Fango.Web.Core.ApiModel;
//using Inazuma.Core.Caching;

namespace Grow.Core.Application.Purchase
{
    public class AppPaymentApiModel : GrowApiModel
    {
        public AppPaymentApiModel(GrowApplicationModel applicationContext) : base(applicationContext) { }

        /*** /api/purchase/startPayment POST ***/
        public ContentResponse<StartPaymentViewModel> StartPayment(string productId)
        {
            var userId = UserId;

            var appPaymentDataContainer = ApplicationContext.AppPaymentDataContainer;

            var historyId = SequenceClientManager.DefaultSequenceClient.GetNextValue(CacheKey.AppPaymentHistoryId(), 1);

            appPaymentDataContainer.AppPaymentHistory.AddNew(historyId, userId, productId, (int)AppCode.PaymentStatus.未検証);

            return new ContentResponse<StartPaymentViewModel>()
            {
                AppData = new StartPaymentViewModel()
                {
                    HistoryId = historyId,
                },
            };
        }

        /*** /api/purchase/cancelPayment POST ***/
        public ContentResponse CancelPayment(long historyId)
        {
            var appPaymentDataContainer = ApplicationContext.AppPaymentDataContainer;
            appPaymentDataContainer.AppPaymentHistory.UpdateStatusByHistoryId(historyId, (int)AppCode.PaymentStatus.ユーザーキャンセル);

            return new ContentResponse();
        }

        /*** /api/purchase/receipt POST ***/
        public ContentResponse<PurchaseViewModel> ExecutePurchase(string receipt, long historyId)
        {
            try
            {
                var userId = ApplicationContext.CurrentUser.UserId;
                var resultContent = new ContentResponse<PurchaseViewModel>();
                var userDataContainer = ApplicationContext.UserDataContainer;
                var appPaymentData = ApplicationContext.AppPaymentDataContainer;

                // レシート検証
                var appPayment = new AppPayment(ApplicationContext, receipt);
                var receiptModel = appPayment.VerifyReceipt();

                var userGemRow = userDataContainer.UserGem.FindUserGem(userId);
                var gem = userGemRow == null ? 0 : userGemRow.Num;

                if (receiptModel == null)
                {
                    resultContent.AppData = new PurchaseViewModel()
                    {
                        BeforeGem = gem,
                        Gem = gem,
                    };
                    return resultContent;
                }

                var buyGem = 0;
                if (receiptModel.ReceiptStatus == (int)AppCode.ReceiptStatus.正常 &&
                    receiptModel.Status == (int)AppCode.PaymentStatus.検証_OK)
                {
                    ////セール中
                    //var saleMasterRow = ApplicationContext.AppPaymentDataContainer.SaleMaster.GetCurrent();
                    //if (saleMasterRow != null)
                    //{
                    //    var saleCoinMasterRows =
                    //        ApplicationContext.AppPaymentDataContainer.SaleCoinMaster.GetDataBySaleId((int)AppCode.TargetDevice.iOS, saleMasterRow.SaleId);
                    //    buyGem = saleCoinMasterRows.First(x => x.ProductId == receiptModel.ProductId).Coin;
                    //}

                    //セールショップ対象がなかったら通常のもので代替
                    if(buyGem == 0)
                    {
                        var coinMasterRows = ApplicationContext.AppPaymentDataContainer.GemMaster.FindItems();
                        buyGem = coinMasterRows.First(x => x.ProductId == receiptModel.ProductId).Gem;
                    }
                    // Coin付与
                    userDataContainer.UserGem.IncrementUserGem(userId, buyGem);
                    // Update Status
                    appPaymentData.AppPayment.UpdateStatus(receiptModel.TransactionId, (int)AppCode.PaymentStatus.コイン付与完了);
                    appPaymentData.AppPaymentHistory.UpdateStatusByHistoryId(historyId, (int)AppCode.PaymentStatus.コイン付与完了);
                }

                resultContent.AppData = new PurchaseViewModel()
                {
                    BeforeGem = gem,
                    Gem = gem + buyGem,
                };

                return resultContent;
            }
            catch (Exception e)
            {
                LoggerManager.DefaultLogger.ErrorFormat("Payment:{0}", e);
                return null;
            }
        }
    }
}
