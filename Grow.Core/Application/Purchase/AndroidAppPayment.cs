﻿using System.IO;
using System.Runtime.Serialization.Json;
using System.Security.Cryptography;
using Fango.Core.Logging;
//using Inazuma.Core.Common;
//using Inazuma.Core.DomainModel.Purchase;
//using Inazuma.Core.Util;
//using Inazuma.Data.DataSet.Adapter;
//using Inazuma.Data.General.DataSet;
//using Inazuma.Data.General.DataSet.Adapter;
using System;
using System.Collections.Generic;
using System.Text;
using Grow.Core.Common;
using Grow.Core.Application;
using Grow.Core.Application.Common;
using Grow.Core.InterfaceModel.Purchase;
using Grow.Data.DataSet;
using Grow.Core.Application.Util;

namespace Grow.Core.Purchase
{

    public class AndroidAppPayment : ModelBase
    {
        private const string PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnYI/oRl/PXi3rvbxSwZ4q1ttsA2AAPL/Pq8EXisDhTAzVSUMuTfLaZwafjaDn5axae3oD1tOJLISSH7YdnK2BZ/gkO38cr6N1rjyRwdCYDs/cPsF3Sr9yzB9lbZNnHjVEu4Z/0IF40FlPWaLaU84QJuPhjdnstvalvZUq8OrKrr7bddy8dbPWBeADnXk6jEAfJKJFubyPUN2YqDV0m0R4XQalS0e4A3o2cUnz3u+XWbgbrxLi+7U8D7Cs4ilfN2AMiI2/7VkeQ5lnCXbd+qKdNERqjomdatcR7uUg6O0cUVZ858hmNQvux83nTVnmB+Xg/K/JXkcqc+ZDkGjbj1xJQIDAQAB";

        //public int UserId { get; set; }
        public string Signature { get; set; }
        public string ReceiptRawData { get; private set; }
        public string ResponseRawData { get; private set; }
        public Dictionary<string, string> ReceiptList { get; private set; }
        //public AppPaymentDataContainer AppPaymentDataContainer { get; set; }
        //public UserAssetDataContainer UserAssetDataContainer { get; set; }

        public AndroidAppPayment(GrowApplicationModel applicationContext, string receiptRawData, string signature)
            : base(applicationContext)
        {
            //this.UserId = userId;
            this.Signature = signature;
            this.ReceiptRawData = receiptRawData;
            //this.AppPaymentDataContainer = new AppPaymentDataContainer();
            //this.UserAssetDataContainer = new UserAssetDataContainer();
        }

        private string FromBase64ToString(string input)
        {
            var bytes = Convert.FromBase64String(input);
            var enc = Encoding.GetEncoding("UTF-8");
            return enc.GetString(bytes);
        }

        private AndroidReceiptModel JsonDeserialize(string jsonString)
        {
            var serializer = new DataContractJsonSerializer(typeof(AndroidReceiptModel));
            byte[] bytes = Encoding.UTF8.GetBytes(jsonString);
            var ms = new MemoryStream(bytes);
            return  (AndroidReceiptModel)serializer.ReadObject(ms);
        }

        public AppPaymentDataSet.AndroidAppPaymentRow AddReceipt(AndroidReceiptModel receipt)
        {
            var appPaymentRow = ApplicationContext.AppPaymentDataContainer.AndroidAppPayment.FindItem(receipt.OrderId);
            if (appPaymentRow == null)
            {
                ApplicationContext.AppPaymentDataContainer.AndroidAppPayment.AddNew(receipt.OrderId,
                                                          receipt.NotificationId,
                                                          receipt.UserId,
                                                          receipt.Nonce,
                                                          receipt.PackageName,
                                                          receipt.ProductId,
                                                          receipt.PurchaseTime,
                                                          receipt.PurchaseState,
                                                          receipt.Status,
                                                          receipt.Gem);
            }
            return appPaymentRow;
        }

        private void UpdateStatus(string orderId, int status)
        {
            ApplicationContext.AppPaymentDataContainer.AndroidAppPayment.UpdateStatus(orderId, status);
        }

        public AndroidReceiptModel VerifyReceipt()
        {
            LoggerManager.DefaultLogger.Info(string.Format("ReceiptRawData:{0},Signature:{1}", ReceiptRawData,Signature));
            var receipt = JsonDeserialize(ReceiptRawData);
            receipt.UserId = User.UserId;

            var gemMaster = ApplicationContext.AppPaymentDataContainer.AndroidGemMaster.FindItem(receipt.ProductId);

            // Not Exist MasterData
            if (gemMaster == null)
            {
                if (receipt.ProductId == "com.zeptolab.ctrbonus.superpower1")
                    return null;

                LoggerManager.DefaultLogger.ErrorFormat("coinMaster is NULL/ProductId:{0}", receipt.ProductId);
                throw new ArgumentException();
            }

            receipt.Gem = gemMaster.Gem;

            // レシート登録
            var appPaymentRow = AddReceipt(receipt);
            if (appPaymentRow != null && appPaymentRow.Status == (int)AppCode.PaymentStatus.コイン付与完了)
                return null;

            var paymentStatus = AppCode.PaymentStatus.未検証;
            var coin = gemMaster.Gem;

            //署名の検証
            if (VerifyDigitalSignature(this.ReceiptRawData, this.Signature, PUBLIC_KEY))
            {
                paymentStatus = AppCode.PaymentStatus.検証_OK;
            }
            else
            {
                coin = 0;
                paymentStatus = AppCode.PaymentStatus.検証_NG;
            }

            // Update Status
            UpdateStatus(receipt.OrderId, (int)paymentStatus);
            receipt.Gem = coin;
            receipt.Status =  (int)paymentStatus;
            return receipt;
        }

        private static bool VerifyDigitalSignature(string message, string signature, string publicKey)
        {
            var msgData = Encoding.UTF8.GetBytes(message);
            var sha = new SHA1Managed();
            var hashData = sha.ComputeHash(msgData);
            var rsa = CryptoUtil.CreateRSACryptoService(Convert.FromBase64String(publicKey));
            if (rsa == null)
                return false;
            var rsaDeformatter = new RSAPKCS1SignatureDeformatter(rsa);
            rsaDeformatter.SetHashAlgorithm("SHA1");
            return rsaDeformatter.VerifySignature(hashData, Convert.FromBase64String(signature));
        }
    }
}
