﻿using Grow.Core.Application;
using Grow.Core.Common;
using Grow.Core.InterfaceModel.Purchase;
using Grow.Core.Application.Common;
using Grow.Data.DataSet;
using Fango.Core.Logging;
//using Inazuma.Core.Common;
//using Inazuma.Core.DomainModel.Purchase;
//using Inazuma.Data.DataSet.Adapter;
//using Inazuma.Data.General.DataSet;
//using Inazuma.Data.General.DataSet.Adapter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Script.Serialization;

namespace Grow.Core.Purchase
{
    public class Receipt
    {
        public int status { get; set; }
        public ReceiptDetail receipt { get; set; }
    }

    public class ReceiptDetail
    {
        public string original_purchase_date_pst { get; set; }
        public string unique_identifier { get; set; }
        public long original_transaction_id { get; set; }
        public string bvrs { get; set; }
        public long transaction_id { get; set; }
        public int quantity { get; set; }
        public string product_id { get; set; }
        public int item_id { get; set; }
        public string purchase_date_ms { get; set; }
        public string purchase_date { get; set; }
        public string original_purchase_date { get; set; }
        public string purchase_date_pst { get; set; }
        public string bid { get; set; }
        public long original_purchase_date_ms { get; set; }
    }

    public class AppPayment : ModelBase
    {
        private const string APP_STORE_URL = "https://sandbox.itunes.apple.com/verifyReceipt";
        private const string APP_STORE_REL_URL = "https://buy.itunes.apple.com/verifyReceipt";
        private const string RECEIPT_DATA_KEY = "purchase-info";

        public int UserId { get; set; }
        public string ReceiptRawData { get; private set; }
        public string ResponseRawData { get; private set; }
        public Dictionary<string, string> ReceiptList { get; private set; }
        //public AppPaymentDataContainer AppPaymentDataContainer { get; set; }
        //public UserAssetDataContainer UserAssetDataContainer { get; set; }

        public AppPayment(GrowApplicationModel applicationContext, string receiptRawData)
            : base(applicationContext)
        {
            //this.UserId = userId;
            this.ReceiptRawData = receiptRawData;
            this.ReceiptList = ParseProperty(FromBase64ToString(receiptRawData));
            //this.AppPaymentDataContainer = new AppPaymentDataContainer();
            //this.UserAssetDataContainer = new UserAssetDataContainer();
        }

        private string FromBase64ToString(string input)
        {
            var bytes = Convert.FromBase64String(input);
            var enc = Encoding.GetEncoding("UTF-8");
            return enc.GetString(bytes);
        }

        private Dictionary<string, string> ParseProperty(string base64String)
        {
            var strRawList = base64String.Replace(';', ',').Split(',');
            var receiptList = new Dictionary<string, string>();
            for (int i = 0; i < strRawList.Count(); i++)
            {
                int index = strRawList[i].IndexOf("\"") + 1;
                int endIndex = strRawList[i].IndexOf("\"", index);
                if (endIndex <= 0) break;

                var key = strRawList[i].Substring(index, endIndex - index);
                index = strRawList[i].IndexOf("\"", endIndex + 1) + 1;
                endIndex = strRawList[i].IndexOf("\"", index);

                var value = strRawList[i].Substring(index, endIndex - index);
                if (!string.IsNullOrEmpty(key))
                    receiptList.Add(key, value);
            }
            return receiptList;
        }

        private AppPaymentDataSet.AppPaymentRow AddReceipt(ReceiptModel receipt)
        {
            var appPaymentRow = ApplicationContext.AppPaymentDataContainer.AppPayment.FindItem(receipt.TransactionId);
            if (appPaymentRow == null)
            {
                ApplicationContext.AppPaymentDataContainer.AppPayment.AddNew(receipt.TransactionId,
                                                          receipt.OriginalTransactionId,
                                                          receipt.UserId,
                                                          receipt.Bid,
                                                          receipt.Bvrs,
                                                          receipt.ItemId,
                                                          receipt.OriginalPurchaseDate,
                                                          receipt.PurchaseDate,
                                                          receipt.ProductId,
                                                          receipt.Quantity,
                                                          receipt.ReceiptStatus,
                                                          receipt.Status,
                                                          receipt.Gem);
            }
            return appPaymentRow;
        }

        private void UpdateStatus(long transactionId, int status)
        {
            ApplicationContext.AppPaymentDataContainer.AppPayment.UpdateStatus(transactionId, status);
        }

        private void UpdateReceiptStatus(long transactionId, int receiptStatus)
        {
            ApplicationContext.AppPaymentDataContainer.AppPayment.UpdateReceiptStatus(transactionId, receiptStatus);
        }

        private ReceiptModel ParseToReceiptModel(Dictionary<string, string> purchaseDic)
        {
            try
            {
                var receipt = new ReceiptModel()
                    {
                        TransactionId = long.Parse(purchaseDic["transaction-id"]),
                        OriginalTransactionId = long.Parse(purchaseDic["original-transaction-id"]),
                        Bid = purchaseDic["bid"],
                        Bvrs = purchaseDic["bvrs"],
                        ItemId = int.Parse(purchaseDic["item-id"]),
                        ProductId = purchaseDic["product-id"],
                        PurchaseDate = purchaseDic["purchase-date"],
                        OriginalPurchaseDate = purchaseDic["original-purchase-date"],
                        UserId = UserId,
                        Quantity = int.Parse(purchaseDic["quantity"]),
                    };

                return receipt;
            }
            catch (Exception)
            {
                LoggerManager.DefaultLogger.Error(string.Format(" Receipt Parse fail/transaction-id:{0}/original-transaction-id:{1}/bid:{2}/bvrs:{3}/ItemId:{4}/product-id:{5}/purchase-date:{6}/original-purchase-date:{7}/quantity:{8}",
                    purchaseDic["transaction-id"], purchaseDic["original-transaction-id"], purchaseDic["bid"], purchaseDic["bvrs"], purchaseDic["item-id"], purchaseDic["product-id"], purchaseDic["purchase-date"], purchaseDic["original-purchase-date"], purchaseDic["quantity"]));
                throw new AggregateException();
            }
        }

        private HttpResponseMessage PostReceipt(string receiptData)
        {
            using (var client = new HttpClient())
            {
                HttpContent content = new StringContent(@"{ ""receipt-data"":""" + receiptData + @"""}");
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return client.PostAsync(APP_STORE_REL_URL, content).Result;
            }
        }

        private HttpResponseMessage PostReceiptSandbox(string receiptData)
        {
            using (var client = new HttpClient())
            {
                HttpContent content = new StringContent(@"{ ""receipt-data"":""" + receiptData + @"""}");
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return client.PostAsync(APP_STORE_URL, content).Result;
            }
        }

        public ReceiptModel VerifyReceipt()
        {
            var rawPurchaseInfo = this.ReceiptList[RECEIPT_DATA_KEY];
            var purchaseInfo = FromBase64ToString(rawPurchaseInfo);
            var purchaseinfoList = ParseProperty(purchaseInfo);
            var receipt = ParseToReceiptModel(purchaseinfoList);
            receipt.ReceiptStatus = (int)AppCode.ReceiptStatus.初期値;

            var gemMaster = ApplicationContext.AppPaymentDataContainer.GemMaster.FindItem(receipt.ProductId);

            // Not Exist MasterData
            if (gemMaster == null)
            {
                if (receipt.ProductId == "com.zeptolab.ctrbonus.superpower1")
                    return null;

                LoggerManager.DefaultLogger.ErrorFormat("coinMaster is NULL/ProductId:{0}", receipt.ProductId);
                throw new ArgumentException();
            }

            receipt.Gem = gemMaster.Gem;

            // レシート登録
            var appPaymentRow = AddReceipt(receipt);
            if (appPaymentRow != null && appPaymentRow.Status == (int)AppCode.PaymentStatus.コイン付与完了)
                return null;

            var paymentStatus = AppCode.PaymentStatus.未検証;
            var coin = gemMaster.Gem;

            try
            {
                // 本番
                var response = PostReceipt(this.ReceiptRawData);
                this.ResponseRawData = response.Content.ReadAsStringAsync().Result;
                
                var jsSerializer = new JavaScriptSerializer();
                var deserializeResponse = jsSerializer.Deserialize<Receipt>(ResponseRawData);

                // 本番チェック
                if (deserializeResponse.status != (int) AppCode.ReceiptStatus.正常 ||
                    receipt.TransactionId != deserializeResponse.receipt.transaction_id)
                {
                    LoggerManager.DefaultLogger.Info(string.Format("ResponseRawData:{0}", ResponseRawData));
                    LoggerManager.DefaultLogger.Info(string.Format("Staus:{0}", deserializeResponse.status));

                    // Sandbox
                    response = PostReceiptSandbox(this.ReceiptRawData);
                    this.ResponseRawData = response.Content.ReadAsStringAsync().Result;
                    deserializeResponse = jsSerializer.Deserialize<Receipt>(ResponseRawData);

                    // Sandboxチェック
                    if (deserializeResponse.status != (int) AppCode.ReceiptStatus.正常 ||
                        receipt.TransactionId != deserializeResponse.receipt.transaction_id)
                    {
                        coin = 0;
                        paymentStatus = AppCode.PaymentStatus.検証_NG;
                    }
                    else
                    {
                        paymentStatus = AppCode.PaymentStatus.検証_OK;
                    }
                }
                else
                {
                    paymentStatus = AppCode.PaymentStatus.検証_OK;
                }

                // Update ReceiptStatus
                UpdateReceiptStatus(deserializeResponse.receipt.transaction_id, deserializeResponse.status);

                // Update Status
                UpdateStatus(receipt.TransactionId, (int) paymentStatus);

                return new ReceiptModel()
                    {
                        Gem = coin,
                        UserId = this.UserId,
                        TransactionId = deserializeResponse.receipt.transaction_id,
                        OriginalTransactionId = deserializeResponse.receipt.original_transaction_id,
                        Bid = deserializeResponse.receipt.bid,
                        Bvrs = deserializeResponse.receipt.bvrs,
                        ItemId = deserializeResponse.receipt.item_id,
                        ProductId = deserializeResponse.receipt.product_id,
                        Quantity = deserializeResponse.receipt.quantity,
                        PurchaseDate = deserializeResponse.receipt.purchase_date,
                        OriginalPurchaseDate = deserializeResponse.receipt.original_purchase_date,
                        ReceiptStatus = deserializeResponse.status,
                        Status = (int) paymentStatus,
                    };
            }
            catch (Exception)
            {
                LoggerManager.DefaultLogger.Error(string.Format("receipt missing /ReceiptRawData:{0}/ResponseRawData:{1}", ReceiptRawData,ResponseRawData));
                throw;
            }
        }
    }
}
