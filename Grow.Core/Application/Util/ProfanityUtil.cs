﻿using Fango.OpenSocial.OAuth;
using Fango.OpenSocial.OpenSocial;
using Fango.Redis.Caching;
using Grow.Core.Application.Caching;

namespace Grow.Core.Application.Util
{
    public class ProfanityUtil
    {
        /// <summary>
        /// NGワードチェック
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool CheckProfanity(string sessionId, string text)
        {
#if DEBUG
            if (sessionId == "Dummy")
                return true;
#endif
            var redis = new RedisCacheClient();
            var oauthSession = redis.Get<OAuthSession>(CacheKey.OAuthSession(sessionId));
            var osClient = new OpenSocialRestClientMbgaApp(oauthSession.ToDictionary());

            var result = osClient.FetchProfanity(text).Valid;

            return result.HasValue && result.Value;
        }
    }
}