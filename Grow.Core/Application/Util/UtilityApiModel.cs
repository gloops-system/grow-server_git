﻿using Grow.Core.InterfaceModel.User;
using Grow.Core.Application.Avatar;
using Grow.Core.Application.Monster;
using Grow.Core.Application.Gear;
using Grow.Core.Application.Duty;
using Fango.Web.Core.ApiModel;
using Fango.Core.Util;
using Grow.Core.InterfaceModel.Utility;
using System;

namespace Grow.Core.Application.Utility
{
    public class UtilityApiModel : GrowApiModel
    {
        public UtilityApiModel(GrowApplicationModel applicationContext)
            : base(applicationContext) { }

        /// <summary>
        /// GetServerTime
        /// </summary>
        /// <returns></returns>
        public ContentResponse<UtilityModel> GetServerTime()
        {
            var contentResponse = new ContentResponse<UtilityModel>();
            
            var utility = new UtilityModel();
            var timestamp = DateUtil.GetDateTimeOffsetNow().UtcDateTime.ToUnixTime() * 1000;
            utility.TimeStamp = timestamp;

            contentResponse.AppData = utility;

            return contentResponse;
        }
    }
}