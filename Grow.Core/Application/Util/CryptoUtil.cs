﻿using System.IO;
using System.Security.Cryptography;
using Fango.Core.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Grow.Core.Application.Util
{
    public class CryptoUtil
    {


      

        public static RSACryptoServiceProvider CreateRSACryptoService(byte[] key)
        {
            byte[] SeqOID = { 0x30, 0x0D, 0x06, 0x09, 0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 0x01, 0x01, 0x01, 0x05, 0x00 };
            byte[] seq = new byte[15];
            var mem = new MemoryStream(key);
            var binr = new BinaryReader(mem);   
            byte bt = 0;
            ushort twobytes = 0;

            try
            {

                twobytes = binr.ReadUInt16();
                if (twobytes == 0x8130)
                    binr.ReadByte();	
                else if (twobytes == 0x8230)
                    binr.ReadInt16();	
                else
                    return null;

                seq = binr.ReadBytes(15);		
                if (!CompareBytearrays(seq, SeqOID))	
                    return null;

                twobytes = binr.ReadUInt16();
                if (twobytes == 0x8103)	
                    binr.ReadByte();
                else if (twobytes == 0x8203)
                    binr.ReadInt16();
                else
                    return null;

                bt = binr.ReadByte();
                if (bt != 0x00)	
                    return null;

                twobytes = binr.ReadUInt16();
                if (twobytes == 0x8130)	
                    binr.ReadByte();	
                else if (twobytes == 0x8230)
                    binr.ReadInt16();	
                else
                    return null;

                twobytes = binr.ReadUInt16();
                byte lowbyte = 0x00;
                byte highbyte = 0x00;

                if (twobytes == 0x8102)	
                    lowbyte = binr.ReadByte();
                else if (twobytes == 0x8202)
                {
                    highbyte = binr.ReadByte();	
                    lowbyte = binr.ReadByte();
                }
                else
                    return null;
                byte[] modint = { lowbyte, highbyte, 0x00, 0x00 };  
                int modsize = BitConverter.ToInt32(modint, 0);

                byte firstbyte = binr.ReadByte();
                binr.BaseStream.Seek(-1, SeekOrigin.Current);

                if (firstbyte == 0x00)
                {	
                    binr.ReadByte();	
                    modsize -= 1;	
                }

                byte[] modulus = binr.ReadBytes(modsize);

                if (binr.ReadByte() != 0x02)
                    return null;
                int expbytes = (int)binr.ReadByte();
                byte[] exponent = binr.ReadBytes(expbytes);


                
                var RSA = new RSACryptoServiceProvider();
                RSAParameters RSAKeyInfo = new RSAParameters();
                RSAKeyInfo.Modulus = modulus;
                RSAKeyInfo.Exponent = exponent;
                RSA.ImportParameters(RSAKeyInfo);
                return RSA;
            }
            catch (Exception)
            {
                return null;
            }

            finally { binr.Close(); }

        }


        private static bool CompareBytearrays(byte[] a, byte[] b)
        {
            if (a.Length != b.Length)
                return false;
            int i = 0;
            foreach (byte c in a)
            {
                if (c != b[i])
                    return false;
                i++;
            }
            return true;
        }

        public static string ConvertSHA512String(string value)
        {
            var bytedata = Encoding.UTF8.GetBytes(value);
            var hashedData = SHA512.Create().ComputeHash(bytedata);
            var hashedString = new StringBuilder();
            foreach (var b in hashedData)
            {
                hashedString.Append(b.ToString("x2"));
            }
            return hashedString.ToString();
        }

        public static string ConvertMd5String(string value)
        {
            var bytedata = Encoding.UTF8.GetBytes(value);
            var hashedData = MD5.Create().ComputeHash(bytedata);
            var hashedString = new StringBuilder();
            foreach (var b in hashedData)
            {
                hashedString.Append(b.ToString("x2"));
            }
            return hashedString.ToString();
        }
    }
}
