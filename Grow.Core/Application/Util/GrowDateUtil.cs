﻿using System;
using System.Diagnostics;
using Fango.Core.Logging;
using Fango.Core.Util;
using Fango.Redis.Caching;

namespace Grow.Core.Application.Util
{
    // TODO : DBの時刻使いたいかも
    public class GrowDateUtil
    {
        public static DateTimeOffset GetDateTimeOffsetNow(int userId)
        {
#if DEBUG
            // デバッグ用日付変更
            try
            {
                var cacheKey = DebugDate(userId);

                var redis = new RedisCacheClient();
                DateTimeOffset changeDate;
                redis.TryGet(cacheKey, out changeDate);

                return changeDate == default(DateTimeOffset)
                           ? DateUtil.GetDateTimeOffsetNow()
                           : changeDate;
            }
            catch (Exception ex)
            {
                // 本番では通らないはずだが、念のためErrorFormat
                LoggerManager.DefaultLogger.DebugFormat("[GetDateTimeOffsetNow] {0}", ex.ToString());
            }
#endif
            // 本番環境ではFangoのDateUtil.GetTimeOffsetNow()を常に返す
            return DateUtil.GetDateTimeOffsetNow();
        }

        /// <summary>
        /// ゲーム内時刻の設定
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="dateTimeOffset"></param>
        [Conditional("DEBUG")]
        public static void CreateInGameDateTimeOffset(int userId, DateTimeOffset dateTimeOffset)
        {
            var cacheKey = DebugDate(userId);
            var redis = new RedisCacheClient();
            redis.Add(cacheKey, dateTimeOffset, TimeSpan.FromHours(2));
        }

        /// <summary>
        /// ゲーム内時刻の設定解除
        /// </summary>
        /// <param name="userId"></param>
        [Conditional("DEBUG")]
        public static void DeleteInGameDateTimeOffset(int userId)
        {
            var cacheKey = DebugDate(userId);
            var redis = new RedisCacheClient();
            redis.Remove(cacheKey);
        }

        /// <summary>
        /// デバッグ用のキー
        /// DateTimeOffset型の値を設定する
        /// このキーが存在すると、設定されている日時が入る
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private static string DebugDate(int userId)
        {
            return "DebugDate-" + userId;
        }
    }
}