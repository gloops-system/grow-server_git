﻿using System;
using System.Diagnostics;
using Fango.Core.Logging;
using Fango.Core.Util;
using Fango.Redis.Caching;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Grow.Core.Application.Util
{
    /// <summary>
    /// Utility class for battle
    /// </summary>
    public class GrowBattleRedisUtil
    {
        private static Object thisLock = new Object();
        private static RedisCacheClient redisCacheClient = new RedisCacheClient();
        private static byte BATTLE_TTL_IN_MINUTES = 20;
        private static string EXCLUDED_TARGET_USER_LIST_KEY = "ExcludedUsersBattle";
        private static string EXCLUDED_TARGET_USER_KEY = "ExcludedUsersBattle{{UserId:{0}}}";

        private GrowBattleRedisUtil()
        {
            // private constructor for utility class
        }

        private static string GetCacheKey()
        {
            return string.Format(EXCLUDED_TARGET_USER_LIST_KEY);
        }

        private static string GetCacheKey(int userId)
        {
            return string.Format(EXCLUDED_TARGET_USER_KEY, userId);
        }

        /// <summary>
        /// Add this user to a list of excluded target users for matching
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="targetUserId"></param>
        public static void PutToExcludedList(int userId, int targetUserId)
        {
            lock(thisLock)
            {
                redisCacheClient.Add<int>(GetCacheKey(targetUserId), userId, TimeSpan.FromMinutes(BATTLE_TTL_IN_MINUTES));
                redisCacheClient.AddLast<int>(GetCacheKey(), new int[] { targetUserId }, TimeSpan.FromMinutes(BATTLE_TTL_IN_MINUTES));
            }
        }

        /// <summary>
        /// Get a list of excluded target users for matching
        /// </summary>
        /// <returns></returns>
        public static List<int> GetExcludedList()
        {
            lock (thisLock)
            {
                var uids = redisCacheClient.GetList<dynamic>(GetCacheKey(), 0, -1);

                var objList = new List<dynamic>(uids);

                var resultList = new List<int>();

                // filter only existing keys
                Parallel.ForEach(objList, delegate(dynamic data)
                {
                    int value;
                    if (redisCacheClient.TryGet<int>(GetCacheKey((int)data), out value))
                    {
                        lock (resultList)
                        {
                            resultList.Add((int)data);
                        }
                    }
                });

                // update new valid list
                redisCacheClient.Remove(GetCacheKey());
                redisCacheClient.AddLast(GetCacheKey(), resultList.ToArray(), TimeSpan.FromMinutes(BATTLE_TTL_IN_MINUTES));

                return resultList;
            }
        }

        /// <summary>
        /// Remove this user from a list of excluded target users for matching
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="targetUserId"></param>
        /// <returns></returns>
        public static bool RemoveFromExcludedList(int userId, int targetUserId)
        {
            int value;
            redisCacheClient.TryGet<int>(GetCacheKey(targetUserId), out value);

            if (userId == value)
            {
                return redisCacheClient.Remove(GetCacheKey(targetUserId));
            }

            return false;
        }
    }
}
