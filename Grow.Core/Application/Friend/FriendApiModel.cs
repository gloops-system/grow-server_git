﻿using System.Collections.Generic;
using System.Linq;
using Grow.Core.Application;
using Grow.Core.Application.Common;
using Grow.Core.Application.Friend;
using Grow.Core.Application.User;
using Grow.Core.Application.Util;
using Grow.Core.InterfaceModel.Friend;
using Fango.Core.Logging;
using Fango.Web.Core.ApiModel;

namespace Grow.Core.Application.Friend
{
    public class FriendApiModel : GrowApiModel
    {
        public FriendApiModel(GrowApplicationModel applicationContext)
            : base(applicationContext) { }

        /// <summary>
        /// フレンドの取得
        /// </summary>
        /// <returns></returns>
        public ContentResponse<FriendOwnViewModel> GetFriendInfo()
        {
            var contentResponse = new ContentResponse<FriendOwnViewModel>();

            var user = ApplicationContext.CurrentUser;
            var now = ApplicationContext.Now;

            var friendList = new List<FriendModel>();
            var friendNoAnswerList = new List<FriendModel>();

            // Container
            var userDataContainer = ApplicationContext.UserDataContainer;

            // RepositoryAdapter
            var userFriendRepositoryAdapter = userDataContainer.UserFriend;
            var userLoginRepositoryAdapter = userDataContainer.UserLogin;

            // ユーザーのフレンド情報を取得
            var userFriend = userFriendRepositoryAdapter.GetDataByUserId(user.UserId);
            foreach (var userFriendRow in userFriend)
            {
                var targetUser = GrowUser.GetInstance(userFriendRow.TargetUserId);
                var userLastLogin = userLoginRepositoryAdapter.GetDataByPK(targetUser.UserId);
                friendList.Add(new FriendModel
                    {
                        UserId = targetUser.UserId,
                        //Name = targetUser.NickName,
                        //Level = targetUser.Level,
                        //Comment = targetUser.Comment,
                        Status = userFriendRow.Status,
                        LastLogin = FriendUtil.GetSpan(now, userLastLogin.LastLogin),

                        //CharacterId = targetUser.PartyLeaderCharacterId,
                        //MobageAvatar = targetUser.ThumbnailUrl ?? string.Empty,
                    });
            }

            // ユーザーにフレンド申請してきている情報を取得
            var userFriendNoAnswer = userFriendRepositoryAdapter.GetDataByTargetUserId(user.UserId)
                                                                .Where(x => x.Status == (int) AppCode.FriendType.申請中)
                                                                .ToArray();
            foreach (var userFriendRow in userFriendNoAnswer)
            {
                var targetUser = GrowUser.GetInstance(userFriendRow.UserId);
                var userLastLogin = userLoginRepositoryAdapter.GetDataByPK(targetUser.UserId);
                friendNoAnswerList.Add(new FriendModel
                    {
                        UserId = targetUser.UserId,
                        //Name = targetUser.NickName,
                        //Level = targetUser.Level,
                        //Comment = targetUser.Comment,
                        Status = userFriendRow.Status,
                        LastLogin = FriendUtil.GetSpan(now, userLastLogin.LastLogin),

                        //CharacterId = targetUser.PartyLeaderCharacterId,
                        //MobageAvatar = targetUser.ThumbnailUrl ?? string.Empty,
                    });
            }

            contentResponse.AppData = new FriendOwnViewModel
                {
                    CurrentFriendNum = friendList.Count(),
                    //MaxFriendNum = user.MaxFriendNum,
                    NoAnswerNum = friendNoAnswerList.Count(),
                    FriendList = friendList,
                    FriendListNoAnswer = friendNoAnswerList,
                };

            return contentResponse;
        }

        /// <summary>
        /// 非フレンドの取得
        /// </summary>
        /// <returns></returns>
        public ContentResponse<FriendOthersViewModel> GetOthersInfo()
        {
            var contentResponse = new ContentResponse<FriendOthersViewModel>();

            var user = ApplicationContext.CurrentUser;
            var now = ApplicationContext.Now;

            var friendList = new List<FriendModel>();
            var mobageFriendList = new List<FriendModel>();

            // Container
            var userDataContainer = ApplicationContext.UserDataContainer;

            // RepositoryAdapter
            var userFriendRepositoryAdapter = userDataContainer.UserFriend;

            // 非フレンド情報を取得
            //var nonFriendList = userFriendRepositoryAdapter.GetNonFriendList(AppCode.ShowNonFriendNum, user.UserId,
            //                                                                 user.Level, AppCode.ShowNonFriendBeforeDay,
            //                                                                 AppCode.ShowNonFriendAdjustNum);
            var nonFriendList = userFriendRepositoryAdapter.GetNonFriendList(AppCode.ShowNonFriendNum, user.UserId,
                                                                             0, AppCode.ShowNonFriendBeforeDay,
                                                                             AppCode.ShowNonFriendAdjustNum);
            foreach (var nonFriendListRow in nonFriendList)
            {
                var targetUser = GrowUser.GetInstance(nonFriendListRow.UserId);
                friendList.Add(new FriendModel
                    {
                        UserId = targetUser.UserId,
                        //Name = targetUser.NickName,
                        //Level = targetUser.Level,
                        //Comment = targetUser.Comment,
                        Status = (int) AppCode.FriendType.無関係,
                        LastLogin = FriendUtil.GetSpan(now, nonFriendListRow.Lastlogin),

                        //CharacterId = targetUser.PartyLeaderCharacterId,
                        //MobageAvatar = targetUser.ThumbnailUrl ?? string.Empty,
                    });
            }

            contentResponse.AppData = new FriendOthersViewModel
                {
                    NonFriendList = friendList,
                    MobageFriendList = null,// TODO : 実装 mobageFriendList,
                };

            return contentResponse;
        }

        /// <summary>
        /// 申請
        /// </summary>
        /// <param name="targetUserId"></param>
        /// <returns></returns>
        public ContentResponse ExecuteRequest(int targetUserId)
        {
            var contentResponse = new ContentResponse();

            var user = ApplicationContext.CurrentUser;

            // Container
            var userDataContainer = ApplicationContext.UserDataContainer;

            // RepositoryAdapter
            var userFriendRepositoryAdapter = userDataContainer.UserFriend;

            // チェック処理で問題がなければ、申請中データを作成する
            var friendManager = new FriendManager(user);
            if (friendManager.ValidateRequest(targetUserId, userFriendRepositoryAdapter, contentResponse))
                userFriendRepositoryAdapter.AddNew(user.UserId, targetUserId, (int)AppCode.FriendType.申請中, ApplicationContext.Now);

            return contentResponse;
        }

        /// <summary>
        /// 応答
        /// </summary>
        /// <param name="targetUserId"></param>
        /// <param name="answer"></param>
        /// <returns></returns>
        public ContentResponse ExecuteAnswer(int targetUserId, int answer)
        {
            var contentResponse = new ContentResponse();

            var user = ApplicationContext.CurrentUser;

            // Container
            var userDataContainer = ApplicationContext.UserDataContainer;

            // RepositoryAdapter
            var userFriendRepositoryAdapter = userDataContainer.UserFriend;

            // 申請中データを更新
            if (answer == (int) AppCode.FriendRequestAnswer.承認)
            {
                var friendManager = new FriendManager(user);
                if (friendManager.ValidateAnswer(targetUserId, userFriendRepositoryAdapter, contentResponse))
                {
                    // リクエスト元のデータのStatusをフレンドへ変更
                    userFriendRepositoryAdapter.UpdateStatusByPK(targetUserId, user.UserId, (int)AppCode.FriendType.フレンド, ApplicationContext.Now);
                    // リクエスト先のフレンドデータを作成
                    userFriendRepositoryAdapter.AddNew(user.UserId, targetUserId, (int)AppCode.FriendType.フレンド, ApplicationContext.Now, true);

                    // TODO : お知らせ
                }
            }
            else
            {
                // リクエスト元のデータを削除
                userFriendRepositoryAdapter.DeleteByPK(targetUserId, user.UserId);
            }

            return contentResponse;
        }

        /// <summary>
        /// フレンド削除
        /// </summary>
        /// <param name="targetUserId"></param>
        /// <returns></returns>
        public ContentResponse ExecuteDelete(int targetUserId)
        {
            var contentResponse = new ContentResponse();

            var user = ApplicationContext.CurrentUser;

            // Container
            var userDataContainer = ApplicationContext.UserDataContainer;

            // RepositoryAdapter
            var userFriendRepositoryAdapter = userDataContainer.UserFriend;

            // フレンド削除
            var friendManager = new FriendManager(user);
            if (friendManager.ValidateDelete(targetUserId, userFriendRepositoryAdapter, contentResponse))
            {
                // 双方のデータを削除
                userFriendRepositoryAdapter.DeleteByPK(user.UserId, targetUserId);
                userFriendRepositoryAdapter.DeleteByPK(targetUserId, user.UserId);
            }

            return contentResponse;
        }
    }
}