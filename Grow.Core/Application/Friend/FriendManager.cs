﻿using System.Linq;
using Grow.Core.Application.Common;
using Grow.Core.Application.User;
using Grow.Data.User.DataSet.Adapter.UserBase;
using Fango.Web.Core.ApiModel;

namespace Grow.Core.Application.Friend
{
    public class FriendManager
    {
        private GrowUser User { get; set; }
        public FriendManager (GrowUser user)
        {
            User = user;
        }

        /// <summary>
        /// 申請用チェック処理
        /// </summary>
        /// <param name="targetUserId"></param>
        /// <param name="userFriendRepositoryAdapter"></param>
        /// <param name="contentResponse"></param>
        /// <returns></returns>
        public bool ValidateRequest(int targetUserId, UserFriendDataRepositoryAdapter userFriendRepositoryAdapter, ContentResponse contentResponse)
        {
            var user = GrowUser.GetInstance(User.UserId);
            var targetUser = GrowUser.GetInstance(targetUserId);

            var userFriend = userFriendRepositoryAdapter.GetDataByUserId(User.UserId);
            var targetUserFriend = userFriendRepositoryAdapter.GetDataByUserId(targetUserId);

            // ユーザーの存在チェック、同一UserIdチェック
            if (!user.IsExistsUser || !targetUser.IsExistsUser || User.UserId == targetUserId)
            {
                contentResponse.Code = (int) AppCode.ApiResultCode.パラメータ不正;
                contentResponse.Message = @"ユーザー情報が存在しません"; // TODO : ローカライズ対応
                return false;
            }

            //// 自分のフレンド上限チェック
            //if (user.MaxFriendNum <= userFriend.Count())
            //{
            //    contentResponse.Code = (int) AppCode.ApiResultCode.パラメータ不正;
            //    contentResponse.Message = @"自分のフレンド上限数を超えています"; // TODO : ローカライズ対応
            //    return false;
            //}

            //// 相手のフレンド上限チェック
            //if ((targetUser.MaxFriendNum) <= targetUserFriend.Count())
            //{
            //    contentResponse.Code = (int) AppCode.ApiResultCode.パラメータ不正;
            //    contentResponse.Message = @"相手のフレンド上限数を超えています"; // TODO : ローカライズ対応
            //    return false;
            //}

            // 自分から申請済み
            if (userFriend.Any(x => x.UserId == User.UserId && x.TargetUserId == targetUserId))
            {
                contentResponse.Code = (int) AppCode.ApiResultCode.パラメータ不正;
                contentResponse.Message = @"既に申請を送信済みです"; // TODO : ローカライズ対応
                return false;
            }

            // 相手から申請されてる
            if (targetUserFriend.Any(x => x.UserId == targetUserId && x.TargetUserId == User.UserId))
            {
                contentResponse.Code = (int) AppCode.ApiResultCode.パラメータ不正;
                contentResponse.Message = @"相手からフレンド申請がきています"; // TODO : ローカライズ対応
                return false;
            }

            return true;
        }

        /// <summary>
        /// 承認用チェック処理
        /// </summary>
        /// <param name="targetUserId"></param>
        /// <param name="userFriendRepositoryAdapter"></param>
        /// <param name="contentResponse"></param>
        /// <returns></returns>
        public bool ValidateAnswer(int targetUserId, UserFriendDataRepositoryAdapter userFriendRepositoryAdapter, ContentResponse contentResponse)
        {
            var user = GrowUser.GetInstance(User.UserId);
            var targetUser = GrowUser.GetInstance(targetUserId);

            var userFriend = userFriendRepositoryAdapter.GetDataByUserId(User.UserId);
            var targetUserFriend = userFriendRepositoryAdapter.GetDataByUserId(targetUserId);

            // ユーザーの存在チェック、同一UserIdチェック
            if (!user.IsExistsUser || !targetUser.IsExistsUser || User.UserId == targetUserId)
            {
                contentResponse.Code = (int)AppCode.ApiResultCode.パラメータ不正;
                contentResponse.Message = @"ユーザー情報が存在しません"; // TODO : ローカライズ対応
                return false;
            }

            // 申請データの存在チェック
            if (userFriendRepositoryAdapter.GetDataByPK(targetUserId, User.UserId) == null)
            {
                contentResponse.Code = (int)AppCode.ApiResultCode.パラメータ不正;
                contentResponse.Message = @"リクエストデータが存在しません"; // TODO : ローカライズ対応
                return false;
            }

            //// 自分のフレンド上限チェック
            //if (user.MaxFriendNum <= userFriend.Count())
            //{
            //    contentResponse.Code = (int)AppCode.ApiResultCode.パラメータ不正;
            //    contentResponse.Message = @"自分のフレンド上限数を超えています"; // TODO : ローカライズ対応
            //    return false;
            //}

            //// 相手のフレンド上限チェック
            //if ((targetUser.MaxFriendNum) <= targetUserFriend.Count())
            //{
            //    contentResponse.Code = (int)AppCode.ApiResultCode.パラメータ不正;
            //    contentResponse.Message = @"相手のフレンド上限数を超えています"; // TODO : ローカライズ対応
            //    return false;
            //}

            return true;
        }

        /// <summary>
        /// 削除用チェック処理
        /// </summary>
        /// <param name="targetUserId"></param>
        /// <param name="userFriendRepositoryAdapter"></param>
        /// <param name="contentResponse"></param>
        /// <returns></returns>
        public bool ValidateDelete(int targetUserId, UserFriendDataRepositoryAdapter userFriendRepositoryAdapter, ContentResponse contentResponse)
        {
            var user = GrowUser.GetInstance(User.UserId);
            var targetUser = GrowUser.GetInstance(targetUserId);

            // ユーザーの存在チェック、同一UserIdチェック
            if (!user.IsExistsUser || !targetUser.IsExistsUser || User.UserId == targetUserId)
            {
                contentResponse.Code = (int)AppCode.ApiResultCode.パラメータ不正;
                contentResponse.Message = @"ユーザー情報が存在しません"; // TODO : ローカライズ対応
                return false;
            }

            // フレンドデータの存在チェック
            if (userFriendRepositoryAdapter.GetDataByPK(User.UserId, targetUserId) == null)
            {
                contentResponse.Code = (int)AppCode.ApiResultCode.パラメータ不正;
                contentResponse.Message = @"データが存在しません"; // TODO : ローカライズ対応
                return false;
            }
            if (userFriendRepositoryAdapter.GetDataByPK(targetUserId, User.UserId) == null)
            {
                contentResponse.Code = (int)AppCode.ApiResultCode.パラメータ不正;
                contentResponse.Message = @"データが存在しません"; // TODO : ローカライズ対応
                return false;
            }

            return true;
        }
    }
}