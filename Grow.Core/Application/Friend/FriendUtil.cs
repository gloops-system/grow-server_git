﻿using System;

namespace Grow.Core.Application.Friend
{
    public class FriendUtil
    {
        public static string GetSpan(DateTimeOffset now, DateTimeOffset lastLogin)
        {
            var span = now - lastLogin;

            if (span.TotalDays >= 7)
                return "1週間以上";

            if (span.TotalDays >= 3)
                return "3日以上";

            if (span.TotalDays >= 1)
                return "1日以上";

            if (span.TotalMinutes <= 30)
                return "30分以内";

            if (span.TotalMinutes <= 60)
                return "1時間以内";

            if (span.TotalMinutes <= 60 * 3)
                return "3時間以内";

            if (span.TotalMinutes <= 60 * 6)
                return "6時間以内";

            return "24時間以内";
        }
    }
}