﻿using Grow.Data.User.DataSet.Adapter.UserBase;
using Fango.Web.Core.Models;
using Fango.Web.Core.Repositories;

namespace Grow.Core.Repositories.Users
{
    public class UserLoginDataRepository : IUserLoginDataRepository
    {
        /// <summary>
        /// IUserLoginDataRepository用
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserLoginRow GetDataByUserId(int userId)
        {
            var userLoginDataRepositoryAdapter = new UserLoginDataRepositoryAdapter();
            var userLoginRow = userLoginDataRepositoryAdapter.GetDataByPK(userId);
            if (userLoginRow == null)
                return null;

            var userLoginInfo = new UserLoginRow
            {
                UUID = userLoginRow.UUID,
                Secret = userLoginRow.Secret,
            };
            return userLoginInfo;
        }

        /// <summary>
        /// InGameIdで検索
        /// </summary>
        /// <param name="inGameId"></param>
        /// <returns></returns>
        public UserLoginRow GetDataByGameId(int inGameId)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// IUserLoginDataRepository用
        /// </summary>
        /// <param name="inGameId"></param>
        /// <returns></returns>
        public int CheckInGameId(int inGameId)
        {
            var userLoginDataRepositoryAdapter = new UserLoginDataRepositoryAdapter();
            return userLoginDataRepositoryAdapter.CheckInGameId(inGameId);
        }

        /// <summary>
        /// パスワードを更新
        /// </summary>
        /// <param name="sha1Password"></param>
        /// <param name="loginInfoStatus"></param>
        /// <param name="userId"></param>
        public void UpdatePassword(byte[] sha1Password, int loginInfoStatus, int userId)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// UUID、Sercret、Statusを更新
        /// </summary>
        /// <param name="uuid"></param>
        /// <param name="status"></param>
        /// <param name="newSecret"></param>
        /// <param name="targetUserId"></param>
        /// <returns></returns>
        public int UpdateUuidAndSecretAndStatusByUserId(string uuid, int status, string newSecret, int targetUserId)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// 指定したユーザーを無効化
        /// </summary>
        /// <param name="userId"></param>
        public void VoidUser(int userId)
        {
            throw new System.NotImplementedException();
        }
    }
}