﻿using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Grow.Data.General.DataSet.InviteDataSetTableAdapters;
using Grow.Extensions.Core;

namespace Grow.Data.General.DataSet.Caching
{
    [Preload]
    internal class InviteDetailMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private Dictionary<int, InviteDataSet.InviteDetailMasterRow[]> _cacheGetDataByInviteId;
        private InviteDataSet.InviteDetailMasterRow[] _allData;

        private readonly static InviteDetailMasterCacheTableAdapter Instance = new InviteDetailMasterCacheTableAdapter();
        private InviteDetailMasterCacheTableAdapter() { }

        public static InviteDetailMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new InviteDetailMasterTableAdapter())
            {
                _allData = adapter.GetData().ToArray();
                _cacheGetDataByInviteId = _allData.GenerateCache(x => x.InviteId);
            }
        }

        public InviteDataSet.InviteDetailMasterRow[] GetDataByInviteId(int inviteId)
        {
            if (CACHE_ENABLED)
                return GetDataByFromCache(_cacheGetDataByInviteId, inviteId);

            using (var adapter = new InviteDetailMasterTableAdapter())
            {
                return adapter.GetDataByInviteId(inviteId).ToArray();
            }
        }
    }
}