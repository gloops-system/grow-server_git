﻿using System;
using System.Linq;
using Fango.Data.Caching;
using Grow.Data.General.DataSet.InviteDataSetTableAdapters;
using Grow.Extensions.Core;

namespace Grow.Data.General.DataSet.Caching
{
    [Preload]
    internal class InviteMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private InviteDataSet.InviteMasterRow[] _allData;

        private readonly static InviteMasterCacheTableAdapter Instance = new InviteMasterCacheTableAdapter();
        private InviteMasterCacheTableAdapter() { }

        public static InviteMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new InviteMasterTableAdapter())
            {
                _allData = adapter.GetData().ToArray();
            }
        }

        public InviteDataSet.InviteMasterRow[] GetCurrentData(DateTimeOffset now)
        {
            if (CACHE_ENABLED)
                return GetDataFromCache(_allData).Where(x => now.InBetween(x.FromDate, x.ToDate)).ToArray();

            using (var adapter = new InviteMasterTableAdapter())
            {
                return adapter.GetCurrentData(now).ToArray();
            }
        }
    }
}