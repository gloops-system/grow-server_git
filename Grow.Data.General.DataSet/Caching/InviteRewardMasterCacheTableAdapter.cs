﻿using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Grow.Data.General.DataSet.InviteDataSetTableAdapters;
using Grow.Extensions.Core;

namespace Grow.Data.General.DataSet.Caching
{
    [Preload]
    internal class InviteRewardMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private Dictionary<int, InviteDataSet.InviteRewardMasterRow[]> _cacheGetDataByInviteId;
        private InviteDataSet.InviteRewardMasterRow[] _allData;

        private readonly static InviteRewardMasterCacheTableAdapter Instance = new InviteRewardMasterCacheTableAdapter();
        private InviteRewardMasterCacheTableAdapter() { }

        public static InviteRewardMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new InviteRewardMasterTableAdapter())
            {
                _allData = adapter.GetData().ToArray();
                _cacheGetDataByInviteId = _allData.GenerateCache(x => x.InviteId);
            }
        }

        public InviteDataSet.InviteRewardMasterRow[] GetDataByInviteId(int inviteId)
        {
            if (CACHE_ENABLED)
                return GetDataByFromCache(_cacheGetDataByInviteId, inviteId);

            using (var adapter = new InviteRewardMasterTableAdapter())
            {
                return adapter.GetDataByInviteId(inviteId).ToArray();
            }
        }
    }
}