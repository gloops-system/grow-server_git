﻿using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Grow.Data.General.DataSet.LocalizeDataSetTableAdapters;
using Grow.Extensions.Core;

namespace Grow.Data.General.DataSet.Caching
{
    [Preload]
    internal class LocalizeTextMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static Dictionary<string, LocalizeDataSet.LocalizeTextMasterRow> _cacheGetDataByPK;
        private static LocalizeDataSet.LocalizeTextMasterRow[] _allData;

        private readonly static LocalizeTextMasterCacheTableAdapter Instance = new LocalizeTextMasterCacheTableAdapter();
        private LocalizeTextMasterCacheTableAdapter() { }

        public static LocalizeTextMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new LocalizeTextMasterTableAdapter())
            {
                _allData = adapter.GetData().ToArray();
                _cacheGetDataByPK = _allData.ToDictionary(x => GenKey(x.LocalizeTextGroupId, x.LocalizeTextId));
            }
        }

        public LocalizeDataSet.LocalizeTextMasterRow GetDataByPK(int localizeTextGroupId, int localizeTextId)
        {
            if (CACHE_ENABLED)
                return GetDataByPKFromCache(_cacheGetDataByPK, GenKey(localizeTextGroupId,localizeTextId));

            using (var adapter = new LocalizeTextMasterTableAdapter())
            {
                return adapter.GetDataByPK(localizeTextGroupId, localizeTextId).FirstOrDefault();
            }
        }

        public LocalizeDataSet.LocalizeTextMasterRow[] GetData()
        {
            if (CACHE_ENABLED)
                return GetDataFromCache(_allData).ToArray();
 
            using (var adapter = new LocalizeTextMasterTableAdapter())
            {
                return adapter.GetData().ToArray();
            }
        }
    }
}