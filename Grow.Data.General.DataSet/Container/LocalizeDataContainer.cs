﻿using System;
using Grow.Data.General.DataSet.Adapter;

namespace Grow.Data.General.DataSet.Container
{
    public class LocalizeDataContainer
    {
        private readonly Lazy<LocalizeTextMasterDataRepositoryAdapter> _localizeTextMaster;
        public LocalizeTextMasterDataRepositoryAdapter LocalizeTextMaster { get { return _localizeTextMaster.Value; } }

        public LocalizeDataContainer()
        {
            _localizeTextMaster = new Lazy<LocalizeTextMasterDataRepositoryAdapter>(() => new LocalizeTextMasterDataRepositoryAdapter(), true);
        }
    }
}
