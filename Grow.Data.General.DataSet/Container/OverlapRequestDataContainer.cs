﻿using System;
using Grow.Data.General.DataSet.Adapter;

namespace Grow.Data.General.DataSet.Container
{
    public class OverlapRequestDataContainer
    {
        private readonly Lazy<OverlapRequestDataRepositoryAdapter> _overlapRequest;
        public OverlapRequestDataRepositoryAdapter OverlapRequest { get { return _overlapRequest.Value; } }

        public OverlapRequestDataContainer()
        {
            _overlapRequest = new Lazy<OverlapRequestDataRepositoryAdapter>(() => new OverlapRequestDataRepositoryAdapter(), true);
        }
    }
}