﻿using System;
using Grow.Data.General.DataSet.Adapter;

namespace Grow.Data.General.DataSet.Container
{
    public class PaymentDataContainer
    {
        private readonly Lazy<PaymentDataRepositoryAdapter> _payment;
        public PaymentDataRepositoryAdapter Payment { get { return _payment.Value; } }

        public PaymentDataContainer()
        {
            _payment = new Lazy<PaymentDataRepositoryAdapter>(() => new PaymentDataRepositoryAdapter(), true);
        }
    }
}