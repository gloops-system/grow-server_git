﻿using System;
using Grow.Data.General.DataSet.Adapter;

namespace Grow.Data.General.DataSet.Container
{
    public class NewsDataContainer
    {
        private readonly Lazy<NewsMasterDataRepositoryAdapter> _newsMaster;
        public NewsMasterDataRepositoryAdapter NewsMaster { get { return _newsMaster.Value; } }

        public NewsDataContainer()
        {
            _newsMaster = new Lazy<NewsMasterDataRepositoryAdapter>(() => new NewsMasterDataRepositoryAdapter(), true);
        }
    }
}