﻿using System;
using Grow.Data.General.DataSet.Adapter;

namespace Grow.Data.General.DataSet.Container
{
    public class DeviceDataContainer
    {
        private readonly Lazy<UserDeviceDataRepositoryAdapter> _device;
        public UserDeviceDataRepositoryAdapter Device { get { return _device.Value; } }

        public DeviceDataContainer()
        {
            _device = new Lazy<UserDeviceDataRepositoryAdapter>(() => new UserDeviceDataRepositoryAdapter(), true);
        }
    }
}
