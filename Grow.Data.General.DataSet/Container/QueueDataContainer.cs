﻿using System;
using Grow.Data.General.DataSet.Adapter;

namespace Grow.Data.General.DataSet.Container
{
    public class QueueDataContainer
    {
        private readonly Lazy<QueueNotificationDataRepositoryAdapter> _queueNotification;
        public QueueNotificationDataRepositoryAdapter QueueNotification { get { return _queueNotification.Value; } }

        private readonly Lazy<QueueTwitterDataRepositoryAdapter> _queueTwitter;
        public QueueTwitterDataRepositoryAdapter QueueTwitter { get { return _queueTwitter.Value; } }

        private readonly Lazy<QueueFacebookDataRepositoryAdapter> _queueFacebook;
        public QueueFacebookDataRepositoryAdapter QueueFacebook { get { return _queueFacebook.Value; } }

        public QueueDataContainer()
        {
            _queueNotification = new Lazy<QueueNotificationDataRepositoryAdapter>(() => new QueueNotificationDataRepositoryAdapter(), true);
            _queueTwitter = new Lazy<QueueTwitterDataRepositoryAdapter>(() => new QueueTwitterDataRepositoryAdapter(), true);
            _queueFacebook = new Lazy<QueueFacebookDataRepositoryAdapter>(() => new QueueFacebookDataRepositoryAdapter(), true);
        }
    }
}