﻿using System;
using Grow.Data.General.DataSet.Adapter;

namespace Grow.Data.General.DataSet.Container
{
    public class InviteDataContainer
    {
        private readonly Lazy<InviteMasterDataRepositoryAdapter> _inviteMaster;
        public InviteMasterDataRepositoryAdapter InviteMaster { get { return _inviteMaster.Value; } }

        private readonly Lazy<InviteDetailMasterDataRepositoryAdapter> _inviteDetailMaster;
        public InviteDetailMasterDataRepositoryAdapter InviteDetailMaster { get { return _inviteDetailMaster.Value; } }

        private readonly Lazy<InviteRewardMasterDataRepositoryAdapter> _inviteRewardMaster;
        public InviteRewardMasterDataRepositoryAdapter InviteRewardMaster { get { return _inviteRewardMaster.Value; } }

        public InviteDataContainer()
        {
            _inviteMaster = new Lazy<InviteMasterDataRepositoryAdapter>(() => new InviteMasterDataRepositoryAdapter(), true);
            _inviteDetailMaster = new Lazy<InviteDetailMasterDataRepositoryAdapter>(() => new InviteDetailMasterDataRepositoryAdapter(), true);
            _inviteRewardMaster = new Lazy<InviteRewardMasterDataRepositoryAdapter>(() => new InviteRewardMasterDataRepositoryAdapter(), true);
        }
    }
}