﻿using System;
using System.Linq;
using Fango.Core.Data;
using Grow.Data.General.DataSet.QueueDataSetTableAdapters;

namespace Grow.Data.General.DataSet.Adapter
{
    public class QueueNotificationDataRepositoryAdapter
    {
        /// <summary>
        /// 全取得
        /// </summary>
        /// <returns></returns>
        public QueueDataSet.QueueNotificationRow[] GetData()
        {
            var rows = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var queueNotificationTableAdapter = new QueueNotificationTableAdapter())
                    {
                        return queueNotificationTableAdapter.GetData().ToArray();
                    }
                });
            return rows;
        }

        /// <summary>
        /// TopCountとDeliveryDateで取得
        /// </summary>
        /// <param name="topCount"></param>
        /// <param name="deliveryDate"></param>
        /// <returns></returns>
        public QueueDataSet.QueueNotificationRow[] GetDataByTopAndDeliveryDate(int topCount, DateTimeOffset deliveryDate)
        {
            var rows = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var queueNotificationTableAdapter = new QueueNotificationTableAdapter())
                    {
                        return queueNotificationTableAdapter.GetDataByTopAndDeliveryDate(topCount, deliveryDate).ToArray();
                    }
                });
            return rows;
        }

        /// <summary>
        /// キューの追加
        /// </summary>
        /// <param name="notificationId"></param>
        /// <param name="notificationType"></param>
        /// <param name="deliveryDate"></param>
        /// <param name="fromUser"></param>
        /// <param name="toUser"></param>
        /// <param name="message"></param>
        /// <param name="optionBadge"></param>
        /// <param name="optionSound"></param>
        /// <param name="optionCollapseKey"></param>
        /// <param name="optionStyle"></param>
        /// <param name="optionIconUrl"></param>
        /// <param name="optionExtras"></param>
        /// <param name="targetDevice"></param>
        public int AddNew(long notificationId, int notificationType, DateTimeOffset deliveryDate, int? fromUser, int? toUser, string message, int? optionBadge,
                           string optionSound, string optionCollapseKey, string optionStyle, string optionIconUrl,
                           string optionExtras, int targetDevice, DateTime now)
        {
            var insertCount = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var queueNotificationTableAdapter = new QueueNotificationTableAdapter())
                    {
                        return queueNotificationTableAdapter.AddNew(notificationId, notificationType, deliveryDate, fromUser, toUser, message,
                                                             optionBadge, optionSound, optionCollapseKey, optionStyle,
                                                             optionIconUrl, optionExtras, targetDevice, now, now);
                    }
                });
            return insertCount;
        }

        /// <summary>
        /// キューの削除
        /// </summary>
        /// <param name="notificationId"></param>
        public void DeleteByPK(long notificationId)
        {
            new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var queueNotificationTableAdapter = new QueueNotificationTableAdapter())
                    {
                        queueNotificationTableAdapter.DeleteByPK(notificationId);
                    }
                });
        }
    }
}