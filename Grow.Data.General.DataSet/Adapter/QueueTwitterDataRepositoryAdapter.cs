﻿using System;
using System.Linq;
using Fango.Core.Data;
using Grow.Data.General.DataSet.QueueDataSetTableAdapters;

namespace Grow.Data.General.DataSet.Adapter
{
    public class QueueTwitterDataRepositoryAdapter
    {
        /// <summary>
        /// 全取得
        /// </summary>
        /// <returns></returns>
        public QueueDataSet.QueueTwitterRow[] GetData()
        {
            var rows = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var queueTwitterTableAdapter = new QueueTwitterTableAdapter())
                    {
                        return queueTwitterTableAdapter.GetData().ToArray();
                    }
                });
            return rows;
        }

        /// <summary>
        /// PKで取得
        /// </summary>
        /// <param name="twitterId"></param>
        /// <returns></returns>
        public QueueDataSet.QueueTwitterRow GetDataByPK(long twitterId)
        {
            var row = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var queueTwitterTableAdapter = new QueueTwitterTableAdapter())
                {
                    return queueTwitterTableAdapter.GetDataByPK(twitterId).FirstOrDefault();
                }
            });
            return row;
        }

        /// <summary>
        /// TopCountとDeliveryDateで取得
        /// </summary>
        /// <param name="topCount"></param>
        /// <param name="deliveryDate"></param>
        /// <returns></returns>
        public QueueDataSet.QueueTwitterRow[] GetDataByTopAndDeliveryDate(int topCount, DateTimeOffset deliveryDate)
        {
            var rows = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var queueTwitterTableAdapter = new QueueTwitterTableAdapter())
                    {
                        return queueTwitterTableAdapter.GetDataByTopAndDeliveryDate(topCount, deliveryDate).ToArray();
                    }
                });
            return rows;
        }

        /// <summary>
        /// キューの追加
        /// </summary>
        /// <param name="twitterId"></param>
        /// <param name="deliveryDate"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public int AddNew(long twitterId, DateTimeOffset deliveryDate, string message, DateTime now)
        {
            var insertCount = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var queueTwitterTableAdapter = new QueueTwitterTableAdapter())
                    {
                        return queueTwitterTableAdapter.AddNew(twitterId, deliveryDate, message, now, now);
                    }
                });
            return insertCount;
        }

        /// <summary>
        /// キューの更新
        /// </summary>
        /// <param name="twitterId"></param>
        /// <param name="deliveryDate"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public int UpdateByPK(long twitterId, DateTimeOffset deliveryDate, string message, DateTime now)
        {
            var updateCount = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var queueTwitterTableAdapter = new QueueTwitterTableAdapter())
                    {
                        return queueTwitterTableAdapter.UpdateByPK(deliveryDate, message, now, twitterId);
                    }
                });
            return updateCount;
        }

        /// <summary>
        /// キューの削除
        /// </summary>
        /// <param name="twitterId"></param>
        public void DeleteByPK(long twitterId)
        {
            new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var queueTwitterTableAdapter = new QueueTwitterTableAdapter())
                    {
                        queueTwitterTableAdapter.DeleteByPK(twitterId);
                    }
                });
        }
    }
}