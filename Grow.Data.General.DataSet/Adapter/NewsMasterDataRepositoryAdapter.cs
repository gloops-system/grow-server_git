﻿using System;
using System.Linq;
using Fango.Core.Data;
using Grow.Data.General.DataSet.NewsDataSetTableAdapters;

namespace Grow.Data.General.DataSet.Adapter
{
    public class NewsMasterDataRepositoryAdapter
    {
        /// <summary>
        /// 全取得
        /// </summary>
        /// <returns></returns>
        public NewsDataSet.NewsMasterRow[] GetData()
        {
            var newsMasterRow = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var newsMasterTableAdapter = new NewsMasterTableAdapter())
                    {
                        return newsMasterTableAdapter.GetData().ToArray();
                    }
                });
            return newsMasterRow;
        }

        /// <summary>
        /// 表示期間内のお知らせを取得
        /// </summary>
        /// <returns></returns>
        public NewsDataSet.NewsMasterRow[] GetDataInSession(DateTimeOffset now)
        {
            var newsMasterRow = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var newsMasterTableAdapter = new NewsMasterTableAdapter())
                    {
                        return newsMasterTableAdapter.GetCurrentData(now).ToArray();
                    }
                });
            return newsMasterRow;
        }

        /// <summary>
        /// NewsIdで取得
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        public NewsDataSet.NewsMasterRow GetDataByPK(int newsId)
        {
            var newsMasterRow = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var newsMasterTableAdapter = new NewsMasterTableAdapter())
                    {
                        return newsMasterTableAdapter.GetDataByPK(newsId).FirstOrDefault();
                    }
                });
            return newsMasterRow;
        }

        /// <summary>
        /// 追加
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="title"></param>
        /// <param name="detail"></param>
        /// <param name="imageUrl"></param>
        /// <param name="action"></param>
        /// <param name="dispOrder"></param>
        /// <returns></returns>
        public int AddNew(int newsId, DateTimeOffset fromDate, DateTimeOffset toDate, string title, string detail, string imageUrl, int? action, int dispOrder)
        {
            var insertCount = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var newsMasterTableAdapter = new NewsMasterTableAdapter())
                    {
                        return newsMasterTableAdapter.AddNew(newsId, fromDate, toDate, title, detail, imageUrl, action, dispOrder);
                    }
                });
            return insertCount;
        }
    }
}