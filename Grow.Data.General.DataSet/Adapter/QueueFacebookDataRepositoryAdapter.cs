﻿using System;
using System.Linq;
using Fango.Core.Data;
using Grow.Data.General.DataSet.QueueDataSetTableAdapters;

namespace Grow.Data.General.DataSet.Adapter
{
    public class QueueFacebookDataRepositoryAdapter
    {
        /// <summary>
        /// 全取得
        /// </summary>
        /// <returns></returns>
        public QueueDataSet.QueueFacebookRow[] GetData()
        {
            var rows = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var queueFacebookTableAdapter = new QueueFacebookTableAdapter())
                    {
                        return queueFacebookTableAdapter.GetData().ToArray();
                    }
                });
            return rows;
        }

        /// <summary>
        /// PKで取得
        /// </summary>
        /// <param name="facebookId"></param>
        /// <returns></returns>
        public QueueDataSet.QueueFacebookRow GetDataByPK(long facebookId)
        {
            var row = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var queueFacebookTableAdapter = new QueueFacebookTableAdapter())
                {
                    return queueFacebookTableAdapter.GetDataByPK(facebookId).FirstOrDefault();
                }
            });
            return row;
        }

        /// <summary>
        /// TopCountとDeliveryDateで取得
        /// </summary>
        /// <param name="topCount"></param>
        /// <param name="deliveryDate"></param>
        /// <returns></returns>
        public QueueDataSet.QueueFacebookRow[] GetDataByTopAndDeliveryDate(int topCount, DateTimeOffset deliveryDate)
        {
            var rows = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var queueFacebookTableAdapter = new QueueFacebookTableAdapter())
                    {
                        return queueFacebookTableAdapter.GetDataByTopAndDeliveryDate(topCount, deliveryDate).ToArray();
                    }
                });
            return rows;
        }

        /// <summary>
        /// キューの追加
        /// </summary>
        /// <param name="facebookId"></param>
        /// <param name="deliveryDate"></param>
        /// <param name="message"></param>
        /// <param name="link"></param>
        /// <param name="picture"></param>
        /// <param name="name"></param>
        /// <param name="caption"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public int AddNew(long facebookId, DateTimeOffset deliveryDate, string message, string link, string picture, string name, string caption, string description, DateTime now)
        {
            var insertCount = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var queueFacebookTableAdapter = new QueueFacebookTableAdapter())
                    {
                        return queueFacebookTableAdapter.AddNew(facebookId, deliveryDate, message, link, picture, name,
                                                                caption, description, now, now);
                    }
                });
            return insertCount;
        }

        /// <summary>
        /// キューの更新
        /// </summary>
        /// <param name="facebookId"></param>
        /// <param name="deliveryDate"></param>
        /// <param name="message"></param>
        /// <param name="link"></param>
        /// <param name="picture"></param>
        /// <param name="name"></param>
        /// <param name="caption"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public int UpdateByPK(long facebookId, DateTimeOffset deliveryDate, string message, string link, string picture, string name, string caption, string description, DateTime now)
        {
            var updateCount = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var queueFacebookTableAdapter = new QueueFacebookTableAdapter())
                    {
                        return queueFacebookTableAdapter.UpdateByPK(deliveryDate, message, link, picture, name, caption,
                                                                    description, now, facebookId);
                    }
                });
            return updateCount;
        }

        /// <summary>
        /// キューの削除
        /// </summary>
        /// <param name="facebookId"></param>
        public void DeleteByPK(long facebookId)
        {
            new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var queueFacebookTableAdapter = new QueueFacebookTableAdapter())
                    {
                        queueFacebookTableAdapter.DeleteByPK(facebookId);
                    }
                });
        }
    }
}