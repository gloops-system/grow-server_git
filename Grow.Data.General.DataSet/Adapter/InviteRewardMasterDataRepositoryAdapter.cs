﻿using Fango.Core.Data;
using Grow.Data.General.DataSet.Caching;

namespace Grow.Data.General.DataSet.Adapter
{
    public class InviteRewardMasterDataRepositoryAdapter
    {
        /// <summary>
        /// InviteIdで取得
        /// </summary>
        /// <param name="inviteId"></param>
        /// <returns></returns>
        public InviteDataSet.InviteRewardMasterRow[] GetDataByInviteId(int inviteId)
        {
            var rows = new DbAccessHandler().ExecuteAction(() =>
                {
                    var inviteRewardMasterTableAdapter = InviteRewardMasterCacheTableAdapter.GetInstance();
                    return inviteRewardMasterTableAdapter.GetDataByInviteId(inviteId);
                });
            return rows;
        }
    }
}