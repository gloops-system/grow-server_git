﻿using System;
using Fango.Core.Data;
using Grow.Data.General.DataSet.Caching;

namespace Grow.Data.General.DataSet.Adapter
{
    public class InviteMasterDataRepositoryAdapter
    {
        /// <summary>
        /// 開催中の招待イベントマスタを取得
        /// </summary>
        /// <returns></returns>
        public InviteDataSet.InviteMasterRow[] GetCurrentData(DateTimeOffset now)
        {
            var rows = new DbAccessHandler().ExecuteAction(() =>
                {
                    var inviteMasterTableAdapter = InviteMasterCacheTableAdapter.GetInstance();
                    return inviteMasterTableAdapter.GetCurrentData(now);
                });
            return rows;
        }
    }
}