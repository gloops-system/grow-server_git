﻿using Fango.Core.Data;
using Grow.Data.General.DataSet.Caching;

namespace Grow.Data.General.DataSet.Adapter
{
    public class LocalizeTextMasterDataRepositoryAdapter
    {

        public LocalizeDataSet.LocalizeTextMasterRow[] GetData()
        {
            var localizeTextMasterRow = new DbAccessHandler().ExecuteAction(() =>
                {
                    var localizeTextMasterCacheTableAdapter = LocalizeTextMasterCacheTableAdapter.GetInstance();
                    return localizeTextMasterCacheTableAdapter.GetData();
                });

            return localizeTextMasterRow;
        }

        public LocalizeDataSet.LocalizeTextMasterRow GetDataByPK(int localizeTextGroupId ,int localizeTextId)
        {
            var localizeTextMasterRow = new DbAccessHandler().ExecuteAction(() =>
            {
                var localizeTextMasterCacheTableAdapter = LocalizeTextMasterCacheTableAdapter.GetInstance();
                return localizeTextMasterCacheTableAdapter.GetDataByPK(localizeTextGroupId,localizeTextId);

            });
            return localizeTextMasterRow;
        }
    }
}
