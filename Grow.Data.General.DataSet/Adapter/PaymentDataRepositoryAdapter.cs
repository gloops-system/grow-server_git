﻿using System;
using System.Linq;
using Fango.Core.Data;
using Grow.Data.General.DataSet.PaymentDataSetTableAdapters;

namespace Grow.Data.General.DataSet.Adapter
{
    public class PaymentDataRepositoryAdapter
    {
        /// <summary>
        /// 全取得
        /// </summary>
        /// <returns></returns>
        public PaymentDataSet.PaymentRow[] GetData()
        {
            var rows = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var paymentTableAdapter = new PaymentTableAdapter())
                    {
                        return paymentTableAdapter.GetData().ToArray();
                    }
                });
            return rows;
        }

        /// <summary>
        /// paymentIdで取得
        /// </summary>
        /// <param name="paymentId"></param>
        /// <returns></returns>
        public PaymentDataSet.PaymentRow GetDataByPK(string paymentId)
        {
            var row = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var paymentTableAdapter = new PaymentTableAdapter())
                    {
                        return paymentTableAdapter.GetDataByPK(paymentId).FirstOrDefault();
                    }
                });
            return row;
        }

        /// <summary>
        /// 追加
        /// </summary>
        /// <param name="paymentId"></param>
        /// <param name="userId"></param>
        /// <param name="itemId"></param>
        /// <param name="amount"></param>
        /// <param name="unitPrice"></param>
        /// <param name="device"></param>
        /// <returns></returns>
        public int AddNew(string paymentId, int userId, int itemId, int amount, int unitPrice, int device, DateTime now)
        {
            var insertCount = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var paymentTableAdapter = new PaymentTableAdapter())
                    {
                        return paymentTableAdapter.AddNew(paymentId, userId, itemId, amount, unitPrice, device, now, now);
                    }
                });
            return insertCount;
        }

        /// <summary>
        /// ステータスの更新
        /// </summary>
        /// <param name="paymentId"></param>
        /// <returns></returns>
        public int UpdateStatusAndCompDateByPK(string paymentId, DateTime now)
        {
            var updateCount = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var paymentTableAdapter = new PaymentTableAdapter())
                    {
                        {
                            return paymentTableAdapter.UpdateCompDateByPK(now, now, paymentId);
                        }
                    }
                });
            return updateCount;
        }
    }
}
