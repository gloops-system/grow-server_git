﻿using System;
using System.Linq;
using Fango.Core.Data;
using Grow.Data.General.DataSet.DeviceDataSetTableAdapters;

namespace Grow.Data.General.DataSet.Adapter
{
    public class UserDeviceDataRepositoryAdapter
    {
        public DeviceDataSet.UserDeviceRow[] GetData()
        {
            var userDeviceRow = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userDeviceTableAdapter = new UserDeviceTableAdapter())
                {
                    return userDeviceTableAdapter.GetData().ToArray();
                }
            });
            return userDeviceRow;
        }

        public int AddNew(int userId, string userAgent, string carrier, string phoneId, DateTime now)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userDeviceTableAdapter = new UserDeviceTableAdapter())
                {
                    return userDeviceTableAdapter.AddNew(userId, userAgent, carrier, phoneId, now, now);
                }
            });
            return result;
        }

        public DeviceDataSet.UserDeviceRow GetDataByUserId(int userId)
        {
            var userDeviceRow = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userDeviceTableAdapter = new UserDeviceTableAdapter())
                {
                    return userDeviceTableAdapter.GetDataByUserId(userId).FirstOrDefault();
                }
            });
            return userDeviceRow;
        }

        public DeviceDataSet.UserDeviceRow GetDataByPK(int userId , string userAgent)
        {
            var userDeviceRow = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userDeviceTableAdapter = new UserDeviceTableAdapter())
                {
                    return userDeviceTableAdapter.GetDataByPK(userId,userAgent).FirstOrDefault();
                }
            });
            return userDeviceRow;
        }

        public int UpdatePhoneIdByPK(int userId, string userAgent,string phoneId, DateTime now)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userDeviceTableAdapter = new UserDeviceTableAdapter())
                {
                    return userDeviceTableAdapter.UpdatePhoneIdByPK(phoneId,now, userId, userAgent);
                }
            });
            return result;
        }
    }
}
