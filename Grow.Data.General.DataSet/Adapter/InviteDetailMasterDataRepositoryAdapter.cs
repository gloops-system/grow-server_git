﻿using Fango.Core.Data;
using Grow.Data.General.DataSet.Caching;

namespace Grow.Data.General.DataSet.Adapter
{
    public class InviteDetailMasterDataRepositoryAdapter
    {
        /// <summary>
        /// InviteIdで取得
        /// </summary>
        /// <param name="inviteId"></param>
        /// <returns></returns>
        public InviteDataSet.InviteDetailMasterRow[] GetDataByInviteId(int inviteId)
        {
            var rows = new DbAccessHandler().ExecuteAction(() =>
                {
                    var inviteDetailMasterTableAdapter = InviteDetailMasterCacheTableAdapter.GetInstance();
                    return inviteDetailMasterTableAdapter.GetDataByInviteId(inviteId);
                });
            return rows;
        }
    }
}