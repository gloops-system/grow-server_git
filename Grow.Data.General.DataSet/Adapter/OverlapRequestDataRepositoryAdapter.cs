﻿using Fango.Core.Data;
using Grow.Data.General.DataSet.OverlapTableAdapters;

namespace Grow.Data.General.DataSet.Adapter
{
    public class OverlapRequestDataRepositoryAdapter
    {
        public bool IsOverlap(int userId)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var overlapRequestTableAdapter = new OverlapRequestTableAdapter())
                {
                    bool? isOverlap = null;
                    overlapRequestTableAdapter.AddAndCheckOverlap(userId, ref isOverlap);
                    return isOverlap;
                }
            });
            return result ?? true;
        }
    }
}
