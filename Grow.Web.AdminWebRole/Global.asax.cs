﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Fango.Web.Core.Configuration;
using Fango.Web.Core.WebApi;
//using Microsoft.WindowsAzure.ServiceRuntime;
using Sharding;
//using Inazuma.Core.ApplicationModel;

namespace Grow.Web.AdminWebRole
{
    // メモ: IIS6 または IIS7 のクラシック モードの詳細については、
    // http://go.microsoft.com/?LinkId=9394801 を参照してください

    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
#if DEBUG
            // デバッグ時のみappSettingsの内容をlocal.configファイルで上書きする
            LocalWebConfiguration.LoadSettings(HttpContext.Current.Server.MapPath("~/local.config"));
#endif
            AreaRegistration.RegisterAllAreas();

            // ApiControllerのセレクタを名前空間を使用するものに切り替える
            GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpControllerSelector), new NamespaceControllerSelector(GlobalConfiguration.Configuration));

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // Application スタート時にリソース情報を取得する
            //InazumaApplicationModel.Resources =
            //    new ApplicationResources
            //    {
            //        // BaseUrlをリクエストのUrlから取得するようにする。
            //        GetBaseUrl = () => HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority)
            //    };
        }
    }
}