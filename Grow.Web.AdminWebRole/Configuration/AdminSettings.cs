﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Fango.Core.Configuration;

namespace Grow.Web.AdminWebRole.Configuration
{
    public class AdminSettings : CoreSettings
    {
        public static string database_prefix { get { return settings("database.prefix"); } }
        public static string clay_admin_root { get { return settings("clay.admin.root"); } }
        public static string clay_project_name { get { return settings("clay.project.name"); } }

        public static string PortalNewsStorePath { get { return settings("portal.news.store.path"); } }
    }
}