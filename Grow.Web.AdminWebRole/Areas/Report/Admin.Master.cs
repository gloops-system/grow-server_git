﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Grow.Web.AdminWebRole.Configuration;

namespace Grow.Web.AdminWebRole.Areas.Report
{
    public partial class Admin : System.Web.UI.MasterPage
    {
        protected string AppPath;
        protected string page_title = "title";
        //protected string image_root = AdminSettings.image_root;

        protected void Page_Load(object sender, EventArgs e)
        {
            AppPath = Request.ApplicationPath.Length == 1 ? "" : Request.ApplicationPath;
            css_dev.Visible = Request.Url.Host.Contains("clayapp.jp");
        }

        protected void Menu_DataBound(object sender, EventArgs e)
        {
            SetExpand(tv_menu.Nodes, tv_menu.SelectedNode);
        }

        protected void SetExpand(TreeNodeCollection nodes, TreeNode selected)
        {
            foreach (TreeNode node in nodes)
            {
                if (selected != null && (selected.DataPath == node.DataPath || selected.DataPath.Contains(node.DataPath + "&")))
                    SetParentExpand(node);
                else if (node.Depth > 1)
                    node.Collapse();

                SetExpand(node.ChildNodes, selected);
            }
        }

        protected void SetParentExpand(TreeNode node)
        {
            if (node == null)
                return;

            node.Expand();
            SetParentExpand(node.Parent);
        }

        protected static string GetUserId(HttpContext context)
        {
            return context.User.Identity.Name;
        }
    }
}