﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Clay.Core.Configuration;
//using Clay.Core.Logging;
//using Clay.Data.Admin;
//using Clay.Data.Admin.Report;
using Grow.Data.DataSet;
//using Edge.Data.DataSet.AdminDataSetTableAdapters;
using Grow.Web.AdminWebRole.Areas.Report.Core;
using Grow.Web.AdminWebRole.Configuration;
using log4net;
using Fango.Core.Logging;

namespace Grow.Web.AdminWebRole.Areas.Report
{
    public partial class _default : Page
    {
         //private static readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static readonly string CustomLabelCategory = "report";
        //private CustomLabelFormatter _customLabelFormatter;

        private static readonly string ReportConnstringKey = ConfigurationManager.AppSettings[string.Format("connection.string.{0}_report", AdminSettings.database_prefix)];

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                ph_main.Visible = true;
                ph_category.Visible = false;

                //if (Request.IsQueryStringNull("snap"))
                //{
                //    if (!string.IsNullOrEmpty(Request.QueryString["from"]))
                //        daterange.From = Request.QueryString["from"];
                //    else
                //        daterange.From = DateTime.Now.AddMonths(-1).ToString("yyyy/MM/dd");
                //    if (!string.IsNullOrEmpty(Request.QueryString["to"]))
                //        daterange.To = Request.QueryString["to"];
                //    else
                //        daterange.To = DateTime.Now.ToString("yyyy/MM/dd");

                //    BindDropDownList();
                //    BindSnapshots();
                //    BindData();
                //}
                //else
                //{
                //    ph_plan_panel.Visible = false;

                //    var snapTA = new AdminCustomReportSnapshotTableAdapter();
                //    var snapDT = snapTA.GetDataByPK(Request.QueryStringAsInt("id"), DateTime.ParseExact(Request.QueryString["snap"], "yyyyMMddHHmmssfff", null));
                //    if (snapDT.Count > 0)
                //    {
                //        if (!snapDT[0].IsParamFromNull())
                //            daterange.From = snapDT[0].ParamFrom.ToString("yyyy/MM/dd");

                //        if (!snapDT[0].IsParamToNull())
                //            daterange.To = snapDT[0].ParamTo.ToString("yyyy/MM/dd");

                //        BindDropDownList();
                //        BindSnapshots();
                //        BindData(snapDT[0]);

                //        ph_create.Visible = false;
                //        b_delete.Visible = true;
                //    }
                //    else
                //        Response.Redirect(string.Format("default.aspx?id={0}", Request.QueryString["id"]));
                //}
            }
            else if (!IsPostBack && !string.IsNullOrEmpty(Request.QueryString["cat"]))
            {
                //ph_main.Visible = false;
                //ph_category.Visible = true;

                //rp_links.DataSource = new AdminCustomReportCstTableAdapter().GetDataByCategory(Request.QueryString["cat"]);
                //rp_links.DataBind();
            }
            //else if (!IsPostBack && string.IsNullOrEmpty(Request.QueryString["id"]))
            //    Response.Redirect("edit.aspx");
        }

        protected void Change_Click(object sender, EventArgs e)
        {
            //string parameters = string.Join("&", new ReportQuery("", Request.Form).InputParameters.Select(x => x.Key + "=" + x.Value).ToArray());
            //if (daterange.Visible)
            //    Response.Redirect(string.Format("default.aspx?id={0}&from={1}&to={2}&{3}", DropDownList1.SelectedValue, daterange.From, daterange.To, parameters).TrimEnd('&'));
            //else
            //    Response.Redirect(string.Format("default.aspx?id={0}&{1}", DropDownList1.SelectedValue, parameters).TrimEnd('&'));
        }

        protected void BindDropDownList()
        {
            //foreach (var acrcRow in new AdminCustomReportCategoryTableAdapter().GetData())
            //{
            //    foreach (var acrRow in new AdminCustomReportCstTableAdapter().GetDataByCategory(acrcRow.Category))
            //    {
            //        var item = new ListItem(acrRow.Title, acrRow.Id.ToString());
            //        item.Attributes["OptionGroup"] = acrcRow.Category;
            //        DropDownList1.Items.Add(item);
            //    }
            //}
            //DropDownList1.SelectedValue = Request.QueryString["id"];
        }

        protected void BindSnapshots()
        {
            ddl_snapshots.Items.Clear();
            ddl_snapshots.Items.Add(new ListItem("選択してください", ""));

            //foreach (var snap in new AdminCustomReportSnapshotTableAdapter().GetDataById(Request.QueryStringAsInt("id")))
            //{
            //    if (!snap.IsMemoNull() && !string.IsNullOrEmpty(snap.Memo))
            //    {
            //        var item = new ListItem(snap.SnapshotDate.ToString() + " " + snap.Memo, snap.SnapshotDate.ToString("yyyyMMddHHmmssfff"));
            //        item.Attributes["OptionGroup"] = snap.SnapshotDate.ToString("yyyy-MM-dd");
            //        ddl_snapshots.Items.Add(item);
            //    }
            //    else
            //    {
            //        var item = new ListItem(snap.SnapshotDate.ToString(), snap.SnapshotDate.ToString("yyyyMMddHHmmssfff"));
            //        item.Attributes["OptionGroup"] = snap.SnapshotDate.ToString("yyyy-MM-dd");
            //        ddl_snapshots.Items.Add(item);
            //    }
            //}

            if (ddl_snapshots.Items.Count > 1)
            {
                ph_load.Visible = true;

                try
                {
                    ddl_snapshots.SelectedValue = Request.QueryString["snap"];
                }
                catch
                {
                }
            }
        }

        protected void BindData()
        {
            //var aTa = new AdminCustomReportTableAdapter();
            //var aDt = aTa.GetDataById(int.Parse(Request.QueryString["id"]));

            ////キャッシュ回避（snapshot_date, updttimeの最大値を付与）
            ////if (Request.IsQueryStringNull("t"))
            ////{
            ////    var snapDt = new AdminCustomReportSnapshotTableAdapter().GetDataById(Request.QueryStringAsInt("id"));
            ////    var labelDt = new AdminCustomLabelTableAdapter().GetDataByCategoryAndId(CustomLabelCategory, Request.QueryStringAsInt("id"));
            ////    Response.Redirect(Request.RawUrl + "&t=" + new[] { aDt[0].UpdtTime.ToUnixTime(), (snapDt.Count > 0 ? snapDt.Max(x => x.SnapshotDate) : DateTime.MinValue).ToUnixTime(), (labelDt.Count > 0 ? labelDt.Max(x => x.Updttime) : DateTime.MinValue).ToUnixTime() }.Max());
            ////}

            //Header.Title += string.Format("（{0}）", aDt[0].Title);

            //string connString = ClaySettings.ConnectionString(ConfigurationManager.AppSettings["connection.string." + aDt[0].Server]);
            //SqlDataSource1.ConnectionString = connString;
            //var query = new ReportQuery(aDt[0].Query, Request.QueryString);
            //SqlDataSource1.SelectCommand = query.TransformedQuery;//実行時はクエリ内の変数宣言を使用しない
            //SqlDataSource1.SelectParameters.Clear();

            //if (aDt[0].WithDateRange)
            //{
            //    SqlDataSource1.SelectParameters.Clear();
            //    SqlDataSource1.SelectParameters.Add("from", daterange.From);
            //    SqlDataSource1.SelectParameters.Add("to", daterange.To);
            //}

            //foreach (var p in query.ReportParameters)//実行時はクエリ内の変数宣言を使用しないためSelectParametersに追加
            //    SqlDataSource1.SelectParameters.Add(p.Name, p.DbType, p.Value);

            //if (query.ReportParameters.Count > 0)
            //{
            //    ph_params.Visible = true;
            //    rp_params.DataSource = query.ReportParameters;
            //    rp_params.DataBind();

            //    if (query.InputParameters.Count == 0)
            //    {
            //        string param = string.Join("&", query.ReportParameters.Select(x => x.PrefixedName + "=" + x.Value).ToArray());
            //        Response.Redirect(aDt[0].WithDateRange
            //                              ? string.Format("default.aspx?id={0}&from={1}&to={2}&{3}",
            //                                              DropDownList1.SelectedValue, daterange.From, daterange.To,
            //                                              param)
            //                              : string.Format("default.aspx?id={0}&{1}", DropDownList1.SelectedValue, param));
            //    }

            //    //validation
            //    if (query.ReportParameters.Count > query.ReportParameters.Count(x => !string.IsNullOrEmpty(x.Value)))
            //        l_error.Text = "パラメータを指定してください";
            //    else
            //    {
            //        foreach (var p in query.ReportParameters.Where(x => !string.IsNullOrEmpty(x.Value)))
            //        {
            //            try
            //            {
            //                p.ParseValue();
            //            }
            //            catch (Exception ex)
            //            {
            //                if (string.IsNullOrEmpty(l_error.Text))
            //                    l_error.Text = p.Name + "：" + ex.Message;
            //            }
            //        }
            //    }
            //}

            DateTime start = DateTime.Now, end = DateTime.Now;
            DataView dv = null;

            //try
            //{
            //    dv = (query.ReportParameters.Count == 0 || query.ReportParameters.Any(x => x.Value != null))
            //        ? (DataView)SqlDataSource1.Select(new DataSourceSelectArguments())
            //        : new DataView();
            //    end = DateTime.Now;
            //}
            //catch (Exception ex)
            //{
            //     l_error2.Text = ex.Message;
            //}

           //_customLabelFormatter = new CustomLabelFormatter(CustomLabelCategory, Request.QueryStringAsInt("id"));

            //GridView1.DataSource = dv;
            //if (!Request.IsQueryStringNull("page"))
            //    GridView1.PageIndex = Request.QueryStringAsInt("page") - 1;
            //GridView1.DataBind();


            //l_errors.Text = string.Join("<br/>", _customLabelFormatter.ErrorMessages.Distinct().ToArray());

            //ph_plan_panel.Visible = dv != null && dv.Count > 0;

            //try
            //{
            //    var sqlparameters = new List<SqlParameter>();
            //    if (aDt[0].WithDateRange)
            //    {
            //        sqlparameters.Add(new SqlParameter("@from", daterange.From));
            //        sqlparameters.Add(new SqlParameter("@to", daterange.To));
            //    }

            //    l_query.Text = aDt[0].Query;

            //    var plan = SqlExecutionPlan.Create(connString, aDt[0].Query, query.ReportParameters.Select(p => new SqlParameter("@" + p.Name, p.ParseValue())).ToArray());
            //    l_cost.Text = plan.TotalCost.ToString();
            //    l_missing_index.Text = string.Join("<br/><br/>", plan.MissingIndexes.ConvertAll(s => Server.HtmlEncode(s).Replace("\n", "<br/>")).ToArray());

            //    ph_plan.Visible = ph_plan_detail.Visible = plan.MissingIndexes.Count > 0;
            //}
            //catch (Exception ex)
            //{
            //    l_cost.Text = ex.Message;
            //}

            //daterange.Visible = aDt[0].WithDateRange;
            //ph_chart.Visible = aDt[0].WithChart;
            //ph_snapshot.Visible = aDt[0].WithSnapshot;

            //if (daterange.Visible)
            //    event_panel.BindData(daterange.FromDate, daterange.ToDate);

            if (dv == null || !string.IsNullOrEmpty(l_error.Text))
                return;

            l_count.Text = dv.Count + "件";
            b_tsv.Visible = dv.Count > 0;

            //if (end - start > TimeSpan.FromSeconds(5))
            //    LoggerManager.DefaultLogger.Error(LogEntry.GetLogEntry(string.Format("クエリ見直し対象/{0}:{1}/実行時間:{2}/件数:{3}", aDt[0].Id, aDt[0].Title, end - start, dv.Count)));
        }

        //protected void BindData(AdminDataSet.AdminCustomReportSnapshotRow snap)
        //{
        //    var aTA = new AdminCustomReportTableAdapter();
        //    var aDT = aTA.GetDataById(int.Parse(Request.QueryString["id"]));

        //    //キャッシュ回避（snapshot_date, updttimeの最大値を付与）
        //    if (Request.IsQueryStringNull("t"))
        //    {
        //        var snapDT = new AdminCustomReportSnapshotTableAdapter().GetDataById(Request.QueryStringAsInt("id"));
        //        var labelDT = new AdminCustomLabelTableAdapter().GetDataByCategoryAndId(CustomLabelCategory, Request.QueryStringAsInt("id"));
        //        Response.Redirect(Request.RawUrl + "&t=" + new[] { aDT[0].UpdtTime.ToUnixTime(), (snapDT.Count > 0 ? snapDT.Max(x => x.SnapshotDate) : DateTime.MinValue).ToUnixTime(), (labelDT.Count > 0 ? labelDT.Max(x => x.Updttime) : DateTime.MinValue).ToUnixTime() }.Max());
        //    }

        //    Header.Title += string.Format("（{0}）", aDT[0].Title);

        //    var tableName = aDT[0].Category + "_" + aDT[0].Title;
        //    DataView dv = ReportSnapshot.LoadSnapshotData(ReportConnstringKey, tableName, snap.SnapshotDate).DefaultView;
        //    _customLabelFormatter = new CustomLabelFormatter(CustomLabelCategory, Request.QueryStringAsInt("id"));
        //    GridView1.DataSource = dv;
        //    if (!Request.IsQueryStringNull("page"))
        //        GridView1.PageIndex = Request.QueryStringAsInt("page") - 1;
        //    GridView1.DataBind();
        //    l_errors.Text = string.Join("<br/>", _customLabelFormatter.ErrorMessages.Distinct().ToArray());

        //    daterange.Visible = aDT[0].WithDateRange;
        //    ph_chart.Visible = aDT[0].WithChart;

        //    //if (!snap.IsParamFromNull() && !snap.IsParamToNull())
        //    //    event_panel.BindData(snap.param_from, snap.param_to);

        //    var query = new ReportQuery(aDT[0].Query, !snap.IsParamCustomNull() ? snap.ParamCustom : null);
        //    if (query.ReportParameters.Count > 0)
        //    {
        //        ph_params.Visible = true;
        //        b_param.Visible = false;
        //        ph_readonly.Visible = true;
        //        rp_params.DataSource = query.ReportParameters;
        //        rp_params.DataBind();
        //    }

        //    l_count.Text = dv.Count + "件";
        //    b_tsv.Visible = dv.Count > 0;
        //}

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.Header)
            //{
            //    _customLabelFormatter.FormatHeader(e.Row);
            //}
            //else if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    _customLabelFormatter.FormatDataRow(e.Row);
            //}
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //var param = (from key in Request.QueryString.AllKeys.Where(x => x != "page") where !Request.IsQueryStringNull(key) select key + "=" + Request.QueryString[key]).ToList();
            //if (e.NewPageIndex > 0)
            //    param.Add("page=" + (e.NewPageIndex + 1));

            //Response.Redirect("default.aspx?" + string.Join("&", param.ToArray()));
        }

        protected void Load_Click(object sender, EventArgs e)
        {
            //if (!string.IsNullOrEmpty(ddl_snapshots.SelectedValue) && new AdminCustomReportSnapshotTableAdapter().GetDataByPK(Request.QueryStringAsInt("id"), DateTime.ParseExact(ddl_snapshots.SelectedValue, "yyyyMMddHHmmssfff", null)).Count > 0)
            //{
            //    Response.Redirect(string.Format("default.aspx?id={0}&snap={1}", DropDownList1.SelectedValue, ddl_snapshots.SelectedValue));
            //}
            //else
            //{
            //    l_result.Text = string.IsNullOrEmpty(ddl_snapshots.SelectedValue) ? "未選択です" : "選択したスナップショットは存在しません";
            //}
        }

        protected void Create_Click(object sender, EventArgs e)
        {
            //var aTA = new AdminCustomReportTableAdapter();
            //var aDT = aTA.GetDataById(int.Parse(Request.QueryString["id"]));

            //SqlDataSource1.ConnectionString = ClaySettings.ConnectionString(ConfigurationManager.AppSettings["connection.string." + aDT[0].Server]);
            //var query = new ReportQuery(aDT[0].Query, Request.QueryString);
            //SqlDataSource1.SelectCommand = query.TransformedQuery;//実行時はクエリ内の変数宣言を使用しない
            //SqlDataSource1.SelectParameters.Clear();

            //if (aDT[0].WithDateRange)
            //{
            //    SqlDataSource1.SelectParameters.Clear();
            //    SqlDataSource1.SelectParameters.Add("from", daterange.From);
            //    SqlDataSource1.SelectParameters.Add("to", daterange.To);
            //}

            //foreach (var p in query.ReportParameters)//実行時はクエリ内の変数宣言を使用しないためSelectParametersに追加
            //    SqlDataSource1.SelectParameters.Add(p.Name, p.DbType, p.Value);

            //var dv = (DataView)SqlDataSource1.Select(new DataSourceSelectArguments());

            //var snapshot_date = DateTime.Now;
            //var acrDT = new AdminCustomReportTableAdapter().GetDataById(Request.QueryStringAsInt("id"));
            //ReportSnapshot.SnapshotStatus status = ReportSnapshot.SaveToDatabase(ReportConnstringKey, acrDT[0].Category + "_" + acrDT[0].Title, dv.Table, snapshot_date);
            //new AdminCustomReportSnapshotTableAdapter().AddNew(
            //    Request.QueryStringAsInt("id"),
            //    snapshot_date,
            //    t_name.Text,
            //    acrDT[0].WithDateRange ? daterange.FromDate : (DateTime?)null,
            //    acrDT[0].WithDateRange ? daterange.ToDate : (DateTime?)null,
            //    query.SerializedReportParameter
            //);

            //BindSnapshots();

            //ph_create.Visible = false;
            //l_result.Text = "スナップショットを作成しました";

            //if ((status & ReportSnapshot.SnapshotStatus.Renamed) != 0)
            //{
            //    l_result.Text += "<br/><b>※レポート定義が変更されているため、旧スナップショットデータはバックアップに退避されました</b>";
            //}
        }

        protected void Delete_Click(object sender, EventArgs e)
        {
            //var aTA = new AdminCustomReportTableAdapter();
            //var aDT = aTA.GetDataById(int.Parse(Request.QueryString["id"]));

            //string table_name = aDT[0].Category + "_" + aDT[0].Title;
            //DateTime snapshot_date = DateTime.ParseExact(ddl_snapshots.SelectedValue, "yyyyMMddHHmmssfff", null);

            ////new admin_custom_report_snapshotTableAdapter().DeleteByPK(Request.QueryStringAsInt("id"), snapshot_date);
            //ReportSnapshot.DeleteSnapshotData(ReportConnstringKey, table_name, snapshot_date);

            //BindSnapshots();
            //l_result.Text = "スナップショットを削除しました";

            //b_delete.Visible = false;
        }
    }
}
