﻿<%@ Page Language="C#" MasterPageFile="~/Areas/Report/Admin.master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Edge.Web.AdminWebRole.Areas.Report._default" %>
<%@ OutputCache Duration="300" VaryByParam="*" %>
<%@ Register TagPrefix="inazuma" TagName="DateRange"  Src="~/Areas/Report/Controls/DateRange.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headerContentPlaceHolder" runat="server">
<!--[if IE]>
<script src="../js/excanvas-compressed.js" type="text/javascript" ></script>
<![endif]-->
<script type="text/javascript" src="js/jquery.flot.js"></script>
<script type="text/javascript" src="js/dateformat.js"></script>
<script type="text/javascript" src="js/tooltip.js"></script>
<script type="text/javascript" src="js/table2CSV.js"></script>
<link rel="stylesheet" type="text/css" href="css/tooltip.css" />

<style>
fieldset {   
  -moz-border-radius:10px;  
  border-radius: 10px;  
  -webkit-border-radius: 10px;
  border-color:#CCCCCC;
  padding:10px;
  width:500px;
}
</style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" runat="server">

<asp:PlaceHolder ID="ph_main" runat="server">

    <div style="float:right;width:100px;text-align:right;padding-right:5px;"><a href="edit.aspx?id=<%= Request.QueryString["id"] %>">レポート編集</a></div>

    <asp:DropDownList ID="DropDownList1" runat="server">
    </asp:DropDownList>

    <inazuma:DateRange runat="server" id="daterange" />
    <asp:Button ID="b_change" runat="server" Text="変更" OnClick="Change_Click" /><br />
    
    <asp:PlaceHolder ID="ph_plan" runat="server" Visible="false">
    <div><a href="#plan" style="color:#FF0000">※インデックスが不足しています</a></div>
    </asp:PlaceHolder>
    
    <asp:PlaceHolder ID="ph_snapshot" runat="server">
    <fieldset>
    <legend>スナップショット</legend>
    <asp:PlaceHolder ID="ph_load" runat="server" Visible="false">
        <asp:DropDownList ID="ddl_snapshots" runat="server"/>
        <asp:Button ID="b_load" runat="server" Text="読込" OnClick="Load_Click"  />
        <asp:Button ID="b_delete" runat="server" Text="削除" OnClick="Delete_Click" Visible="false" OnClientClick="return confirm('本当に削除してよろしいですか？');" />
        <br />
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="ph_create" runat="server">
        メモ（任意）：<asp:TextBox ID="t_name" runat="server" />
        <asp:Button ID="b_create" runat="server" Text="作成" OnClick="Create_Click" />
        <br />
    </asp:PlaceHolder>
    <asp:Label ID="l_result" runat="server" EnableViewState="false" ForeColor="Red" />
    </fieldset>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="ph_params" runat="server" Visible="false">
        <fieldset>
        <legend>カスタムパラメータ</legend>
        <asp:Repeater ID="rp_params" runat="server">
            <HeaderTemplate><table></HeaderTemplate>
            <ItemTemplate>
                <tr>
                <td><asp:Label Text='<%# Eval("Name") %>' runat="server"/></td>
                <td>:</td>
                <td><input type="text" name='<%# Eval("PrefixedName") %>' value='<%# Eval("Value") %>' placeholder='<%# Eval("DeclareType") %>' class="custom-param"/></td>
                </tr>
            </ItemTemplate>
            <FooterTemplate></table></FooterTemplate>
        </asp:Repeater>
        <asp:Button Text="変更" ID="b_param" runat="server" OnClick="Change_Click" />&nbsp;
        <asp:Label ID="l_error" runat="server" ForeColor="Red" EnableViewState="false" />
        </fieldset>
        <asp:PlaceHolder id="ph_readonly" runat="server" Visible="false">
        <script>
            $(function () {
                $('.custom-param').attr('readonly', 'readonly');
            });
        </script>
        </asp:PlaceHolder>
    </asp:PlaceHolder>
    <p><%= DateTime.Now %>時点（5分間キャッシュしています）</p>
    <p><asp:Label ID="l_errors" runat="server" ForeColor="Red" /></p>

<script type="text/javascript">
    var options = {
        yaxis: { min: 0 },
        grid: { hoverable: true },
        legend: { position: 'nw' }
    };

    var mode;
    $(function () {
        var data = [];
        $('#<%= GridView1.ClientID %> tr').each(function () {
      if (this.className != 'pager' && this.parentNode.parentNode.id == '<%= GridView1.ClientID %>') {
          var ths = $("th", this);
          for (var i = 1; i < ths.length; i++) {
              data.push({ data: [], label: ths[i].innerHTML });
          }
          var tds = $("td", this);
          if (tds.length) {
              for (var i = 1; i < tds.length; i++) {
                  var x = convert_val(tds[0].innerHTML);
                  var y = tds[i].innerHTML.replace(/,/g, '');
                  data[i - 1].data.push([x, y]);
              }
          }
      }
  });

    if (mode == 'time') {
        options.xaxis = {
            mode: 'time',
            timeformat: '%m/%d',
            tickSize: [1, "day"]
        };
    } else if (mode == 'month') {
        options.xaxis = {
            tickSize: 1,
            tickFormatter: function (val, axis) {
                return val.toString().substr(0, 4) + "/" + val.toString().substr(4);
            }
        };
    } else {
        options.xaxis = {
            tickSize: 1,
            tickFormatter: function (val, axis) {
                return val.toString();
            }
        };
    }

    var placeholder = $("#placeholder");

    $.plot(placeholder, data, options);

    $("body").mousemove(function (event) {
        if (event.target.tagName.toLowerCase() != "canvas") {
            tooltip.hide();
        }
    });
    placeholder.bind("redraw", function (event) {
        tooltip.hide();
    });
    placeholder.bind("plothover", function (event, pos, item) {
        if (item) {
            if (mode == 'time') {
                var date = new Date(item.datapoint[0]);
                date.setHours(date.getHours() - 9);
                tooltip.show(item.series.label + "<br/>" + new DateFormat("yyyy/MM/dd").format(new Date(item.datapoint[0])) + "<br/>" + item.datapoint[1]);
            } else if (mode == 'month') {
                tooltip.show(item.series.label + "<br/>" + item.datapoint[0].toString().substr(0, 4) + "/" + item.datapoint[0].toString().substr(4) + "<br/>" + item.datapoint[1]);
            } else {
                tooltip.show(item.series.label + "<br/>" + item.datapoint[0] + "<br/>" + item.datapoint[1]);
            }
        }
    });
    placeholder.find('div.legend').css('cursor', 'pointer').attr('title', 'click to hide').click(function () {
        var legend = $(this);
        if (legend.css('opacity') == '1')
            legend.css('opacity', '0.1').attr('title', 'click to show');
        else
            legend.css('opacity', '1').attr('title', 'click to hide');
    });
});
function convert_val(dt) {
    var date = Date.parse(dt);
    if (date) {
        mode = 'time';
        return parseInt(date) + (9 * 60 * 60) * 1000; //unix-time
    } else if (dt.match(/\d{4}\/\d{2}/i)) {
        mode = 'month';
        return dt.replace('/', '');
    } else {
        mode = 'default';
        return dt;
    }
}
</script>



<asp:PlaceHolder id="ph_chart" runat="server">
<div id="placeholder" style="width:1000px;height:500px;"></div>

</asp:PlaceHolder>
    
    <asp:Button id="b_tsv" Text="TSV表示" runat="server" Visible="false"/>
    <script>$('#<%= b_tsv.ClientID %>').click(function () { $('#<%= GridView1.ClientID %>').table2CSV({ separator: '\t', title: $('#<%= DropDownList1.ClientID %> option:selected').text() }); return false; });</script>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" PageSize="200" AllowSorting="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" OnRowDataBound="GridView1_RowDataBound" OnPageIndexChanging="GridView1_PageIndexChanging">
        <RowStyle ForeColor="#000066" />
        <FooterStyle BackColor="White" ForeColor="#000066" />
        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" CssClass="pager" />
        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
    </asp:GridView>
    <asp:Label ID="l_error2" runat="server" ForeColor="Red" EnableViewState="false" />
    <asp:Label ID="l_count" runat="server" /><br/>
    <br/>
    
    <asp:PlaceHolder ID="ph_plan_panel" runat="server" Visible="false">
    <a name="plan" id="plan"></a>
    クエリ実行プラン<br />
    <table cellspacing="0" cellpadding="3" rules="all" border="1" style="background-color:White;border-color:#CCCCCC;border-width:1px;border-style:None;border-collapse:collapse;">
        <tr>
            <td style="white-space:nowrap">サブツリーの推定コスト</td>
            <td><asp:Label ID="l_cost" runat="server" /></td>
        </tr>
        <asp:PlaceHolder ID="ph_plan_detail" runat="server" Visible="false">
        <tr>
            <td>不足インデックス</td>
            <td><asp:Label ID="l_missing_index" runat="server" style="color:#008200"/></td>
        </tr>
        </asp:PlaceHolder>
        <tr>
            <td>クエリ</td>
            <td><pre><asp:Label ID="l_query" runat="server" /></pre></td>
        </tr>
    </table>
    </asp:PlaceHolder>
    
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Edge.Data.DataSet.Properties.Settings.inazuma_devConnectionString %>"
        ></asp:SqlDataSource>

</asp:PlaceHolder>

<asp:PlaceHolder ID="ph_category" runat="server">
    <asp:Repeater id="rp_links" runat="server">
        <HeaderTemplate><ul></ul></HeaderTemplate>
        <ItemTemplate><li><asp:HyperLink ID="HyperLink1" Text='<%# Eval("title") %>' NavigateUrl='<%# "?id=" + DataBinder.Eval(Container.DataItem, "id") %>' runat="server" /></li></ItemTemplate>
        <FooterTemplate></ul></FooterTemplate>
    </asp:Repeater>
</asp:PlaceHolder>

</asp:Content>
