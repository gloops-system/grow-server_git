﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
//using Clay.Data.Admin.Report;
using Grow.Data.DataSet;
//using Edge.Data.DataSet.AdminDataSetTableAdapters;

namespace Grow.Web.AdminWebRole.Areas.Report.Core
{
    public class CustomLabelFormatter
    {
        //private Dictionary<string, AdminDataSet.AdminCustomLabelRow> custom_labels;
        //private ILookup<string, AdminDataSet.AdminCustomLabelParameterRow> custom_label_parameters;

        public List<string> ErrorMessages = new List<string>();

        public CustomLabelFormatter(string category, int id)
        {
            //var labelDT = new AdminCustomLabelTableAdapter().GetDataByCategoryAndId(category, id);
            //custom_labels = labelDT.ToDictionary(x => x.TargetColumn, x => x);

            //ErrorMessages.AddRange(Enumerable.Select(labelDT.Where(x => !string.IsNullOrEmpty(x.MethodName) && !CustomLabelManager.Defined.ContainsKey(x.MethodName)), x => string.Format("[{0}] {1}は実装されていません", x.TargetColumn, x.MethodName)));

            //custom_label_parameters = new AdminCustomLabelParameterTableAdapter().GetDataByCategoryAndId(category, id)
            //    .ToLookup(x => x.TargetColumn, x => x);
        }

        public void FormatHeader(GridViewRow gvRow)
        {
            //string[] columns = gvRow.Cells.Cast<TableCell>().Select(x => x.Text).ToArray();

            //for (int i = 0; i < columns.Length; i++)
            //{
            //    AdminDataSet.AdminCustomLabelRow custom_label;
            //    if (custom_labels.TryGetValue(columns[i], out custom_label))
            //    {
            //        if (!custom_label.Visible)
            //            gvRow.Cells[i].Style["display"] = "none";

            //        if (!string.IsNullOrEmpty(custom_label.HeaderName))
            //            gvRow.Cells[i].Text = custom_label.HeaderName;
            //    }
            //}
        }

        public void FormatDataRow(GridViewRow gvRow)
        {
            //DataRow row = (DataRow)((DataRowView)gvRow.DataItem).Row;
            //DataColumn[] columns = row.Table.Columns.Cast<DataColumn>().ToArray();

            //for (int i = 0; i < row.Table.Columns.Count; i++)
            //{
            //    DataColumn col = columns[i];
            //    AdminDataSet.AdminCustomLabelRow custom_label;
            //    if (custom_labels.TryGetValue(col.ColumnName, out custom_label))
            //    {
            //        if (!custom_label.Visible)
            //            gvRow.Cells[i].Style["display"] = "none";

            //        if (!string.IsNullOrEmpty(custom_label.MethodName) && custom_label_parameters.Contains(col.ColumnName))
            //        {
            //            object[] parameters = custom_label_parameters[col.ColumnName].OrderBy(x => x.ParameterIndex)
            //                .Select(x => columns.Any(y => y.ColumnName == x.ParameterColumn) && !Convert.IsDBNull(row[x.ParameterColumn]) ? row[x.ParameterColumn] : null).ToArray();

            //            CustomLabel label;
            //            if (CustomLabelManager.Defined.TryGetValue(custom_label.MethodName, out label))
            //            {
            //                for (int j = 0; j < label.Parameters.Length; j++)
            //                {
            //                    if (parameters[j] != null)
            //                    {
            //                        try
            //                        {
            //                            parameters[j] = Convert.ChangeType(parameters[j], label.Parameters[j].ParameterType);
            //                        }
            //                        catch
            //                        {
            //                        }
            //                    }
            //                }
            //            }

            //            string[] missing = custom_label_parameters[col.ColumnName]
            //                .Where(x => !columns.Any(y => y.ColumnName == x.ParameterColumn)).Select(x => x.ParameterColumn).ToArray();
            //            if (missing.Length > 0)
            //                ErrorMessages.Add(string.Format("[{0}] クエリ定義にない{1}のカラムが指定されています", col.ColumnName, string.Join(", ", missing)));

            //            string error = null;
            //            string label_value = CustomLabelManager.GetValue(HttpContext.Current.Items, custom_label.MethodName, parameters, row[col.ColumnName], ref error);

            //            if (error != null)
            //                ErrorMessages.Add(string.Format("[{0}] {1}", col.ColumnName, error));

            //            string original_value = Convert.ToString(row[col.ColumnName]);
            //            gvRow.Cells[i].Text = label_value;
            //            if (label_value != original_value)
            //            {
            //                gvRow.Cells[i].BackColor = Color.LightYellow;
            //                gvRow.Cells[i].HorizontalAlign = HorizontalAlign.Left;
            //            }
            //        }
            //    }
            //}
        }
    }
}