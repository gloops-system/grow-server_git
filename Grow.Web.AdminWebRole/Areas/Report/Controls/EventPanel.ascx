﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EventPanel.ascx.cs" Inherits="Edge.Web.AdminWebRole.Areas.Report.Controls.EventPanel" %>
<link rel="stylesheet" href="css/calendar.aspx" type="text/css" />

<style type="text/css">
 .btn_event {
     -webkit-border-radius: 11px;
     -moz-border-radius: 11px;
     border-radius: 11px;
     background: #eee;
     border: 1px solid #ddd;
     color: #707070;
     font-size: 0.8em;
     font-weight: bold;
     width:110px;
     text-align:center;
     cursor:pointer;
 }

 ul#events {
     margin: 0;
     padding: 0;
     list-style-type: none;
 }

 ul#events li, .ev_filter {
     font-size: 0.8em;
     padding: 0 0 0 5px;
     margin:0 0 2px 0;
     position: relative;
 }
 #ev_buttons {
 	padding:10px 0 5px 0;
 	display:none;
 }
 #ev_panel {
 	display:none;
 }
 #ev_timeline {
 	background-color:#f6f6f6;
 }
</style>

<script type="text/javascript">
    var event_data = [<%= event_data %>];
    $(function () {
        var left = $('.tickLabel:last').width() + 4;
        $('#ev_timeline, #ev_buttons, #ev_filters').css('margin-left', left);
        $('#ev_timeline').css('width', 1000 - left - 9);
        bind_event();
        $("input[@type='checkbox']").click(bind_event);
        if (event_data.length)
            $('#ev_buttons').show();
    });

    function bind_event() {
        $('#events').html('');
        $.each(event_data, function (idx, elem) {
            checked = false;
            $("input[@name='ev_filter']:checked").each(function (idx, el) {
                if ($(el).val() == elem.ev_type) {
                    checked = true;
                }
            });
            if (checked)
                $('#events').append($('<li/>').css('left', elem.left).addClass('ev_' + elem.ev_type).text(elem.label));
        });
        $('#ev_panel').css('height', 20 * ($('#events').children().length + 1) + 'px');
    }
</script>

<div id="ev_buttons">
<div onclick="$('#ev_panel, #btn_event_open, #btn_event_close').toggle();" class="btn_event" id="btn_event_open">イベントを表示&raquo;</div>
<div onclick="$('#ev_panel, #btn_event_open, #btn_event_close').toggle();" class="btn_event" id="btn_event_close" style="display:none;">&laquo;イベントを非表示</div>
</div>
<div id="ev_panel">
    <div id="ev_filters">
        <asp:Repeater ID="rp_filter" runat="server">
            <ItemTemplate>
                <label class="ev_filter"><input type="checkbox" name="ev_filter" value="<%# DataBinder.Eval(Container.DataItem, "event_type") %>" checked="checked" /><%# DataBinder.Eval(Container.DataItem, "name") %></label>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    <div id="ev_timeline">
        <ul id="events">
        </ul>
    </div>
    <br style="clear:both"/>
</div>