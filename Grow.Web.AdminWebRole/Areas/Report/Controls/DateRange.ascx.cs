﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Grow.Web.AdminWebRole.Areas.Report.Controls
{
    public partial class DateRange : System.Web.UI.UserControl
    {
        protected string AppPath;

        protected void Page_Load(object sender, EventArgs e)
        {
            AppPath = Request.ApplicationPath.Length == 1 ? "" : Request.ApplicationPath;
        }

        public string From
        {
            get { return t_from.Text; }
            set { t_from.Text = value; }
        }

        public string To
        {
            get { return t_to.Text; }
            set { t_to.Text = value; }
        }

        public DateTime FromDate
        {
            get { return DateTime.Parse(From); }
        }

        public DateTime ToDate
        {
            get { return DateTime.Parse(To).AddDays(1).AddSeconds(-1); }
        }
    }
}