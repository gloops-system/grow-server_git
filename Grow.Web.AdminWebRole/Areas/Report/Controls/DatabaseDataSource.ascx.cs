﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Grow.Web.AdminWebRole.Areas.Report.Controls
{
    public partial class DatabaseDataSource : System.Web.UI.UserControl, IDataSource
    {
        public static readonly List<string> databases = new List<string>(ConfigurationManager.AppSettings.AllKeys.Where(s => s.StartsWith("connection.string.")))
               .ConvertAll(s => s.Substring("connection.string.".Length)).OrderBy(x => x).ToList();

        public event EventHandler DataSourceChanged;

        public DatabaseDataSource()
        {
        }

        public DataSourceView GetView(string viewName)
        {
            return new DatabaseDataSourceView(this, viewName);
        }

        public ICollection GetViewNames()
        {
            return databases;
        }

        protected virtual void OnDataSourceChanged(EventArgs e)
        {
            EventHandler handler = this.DataSourceChanged;
            if (handler != null)
                handler(this, e);
        }

        private class DatabaseDataSourceView : DataSourceView
        {
            public DatabaseDataSourceView(IDataSource owner, string name)
                : base(owner, name)
            {
            }
            protected override IEnumerable ExecuteSelect(DataSourceSelectArguments arguments)
            {
                return DatabaseDataSource.databases;
            }
        }
    }
}