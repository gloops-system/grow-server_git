﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DateRange.ascx.cs" Inherits="Edge.Web.AdminWebRole.Areas.Report.Controls.DateRange" %>
<script src="<%= AppPath %>/js/jquery-ui-1.7.custom.min.js" type="text/javascript"  charset="utf-8"></script>
<link type="text/css" href="<%= AppPath %>/css/smoothness/jquery-ui-1.7.custom.css" rel="stylesheet" /> 
<script type="text/javascript">
    $(function () {
        $('#<%= t_from.ClientID %>').datepicker({ dateFormat: 'yy/mm/dd' });
    $('#<%= t_to.ClientID %>').datepicker({ dateFormat: 'yy/mm/dd' });
});
</script>


期間：<asp:TextBox ID="t_from" runat="server" Width="80"/>～<asp:TextBox ID="t_to" runat="server" Width="80"/>