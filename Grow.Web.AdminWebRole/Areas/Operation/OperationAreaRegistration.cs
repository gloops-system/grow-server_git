﻿using System.Web.Mvc;

namespace Grow.Web.AdminWebRole.Areas.Operation
{
    public class OperationAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Operation";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Operation_Top",
                "Operation/",
                new { controller = "Top", action = "Index" });
            
            context.MapRoute(
                "Operation_default",
                "Operation/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
