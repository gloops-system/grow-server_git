﻿namespace Grow.Web.AdminWebRole.Areas.Sales.Models.ViewModel
{
    public class SalesViewModel
    {
        public string Date { get; set; }
        public long SalesTotal { get; set; }
        public long SalesIos { get; set; }
        public long SalesAndroid { get; set; }
        public long ForcastTotal { get; set; }
    }
}