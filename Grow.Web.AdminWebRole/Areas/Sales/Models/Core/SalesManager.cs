﻿using System;
using System.Collections.Generic;
using System.Linq;
using Grow.Data.General.DataSet.Adapter;
//using Edge.Data.General.DataSet.Adapter.Admin;
using Grow.Web.AdminWebRole.Areas.Sales.Models.ViewModel;

namespace Grow.Web.AdminWebRole.Areas.Sales.Models.Core
{
    public class SalesManager
    {
        private const string DATETIME_DAILY_FORMAT = "yyyy-MM-dd";
        private const string DATETIME_MONTHLY_FORMAT = "yyyy-MM";

        /// <summary>
        /// 統合管理画面用ダッシュボード用(日別)
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public static List<SalesViewModel> GetSalesDailyDataByDate(DateTimeOffset from, DateTimeOffset to)
        {
            var today = DateTime.Today;
            var todayOffset = new DateTimeOffset(today, new TimeSpan(9, 0, 0));
            //var adminPaymentDataRepo = new AdminPaymentDataRepository();
            //var adminGeneralDataContainer = new AdminGeneralDataContainer();

            //// 決済予測データ
            //var adminPaymentForecastRows = adminGeneralDataContainer.AdminPaymentForecast
            //                                                        .GetDataByDate(DateTime.Parse(from.ToString(DATETIME_DAILY_FORMAT)),
            //                                                                       DateTime.Parse(to.ToString(DATETIME_DAILY_FORMAT)));

            //var salesViewModels = adminPaymentDataRepo.GetDataByFromTo(from, to).Select(x =>
            //{
            //    var paymentForecastRow =
            //        adminPaymentForecastRows.FirstOrDefault(y => y.Date.ToString(DATETIME_DAILY_FORMAT) == x.Date.ToString(DATETIME_DAILY_FORMAT));
            //    return new SalesViewModel()
            //    {
            //        Date = x.Date.ToString(DATETIME_DAILY_FORMAT),
            //        SalesTotal = x.CurrencyTotal,
            //        SalesIos = x.CurrencyIosTotal,
            //        SalesAndroid = x.CurrencyAndroidTotal,
            //        ForcastTotal = paymentForecastRow == null ? 0 : paymentForecastRow.Currency
            //    };
            //}).ToList();

            //// 本日分
            //if (from <= todayOffset && todayOffset < to)
            //{
            //    var todayIosData = adminPaymentDataRepo.GetTodayIosData();
            //    var todayAndData = adminPaymentDataRepo.GetTodayAndroidData();
            //    var todayForecastRow =
            //        adminPaymentForecastRows.FirstOrDefault(y => y.Date.ToString(DATETIME_DAILY_FORMAT) == today.ToString(DATETIME_DAILY_FORMAT));
            //    var todaySaleViewModel = new SalesViewModel()
            //    {
            //        Date = today.ToString(DATETIME_DAILY_FORMAT),
            //        SalesTotal = todayIosData.CurrencyTotal + todayAndData.CurrencyTotal,
            //        SalesIos = todayIosData.CurrencyTotal,
            //        SalesAndroid = todayAndData.CurrencyTotal,
            //        ForcastTotal = todayForecastRow == null ? 0 : todayForecastRow.Currency
            //    };
            //    salesViewModels.Add(todaySaleViewModel);
            //}
            //return salesViewModels;
            return null;
        }

        /// <summary>
        /// 統合管理画面用ダッシュボード用(月別)
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public static List<SalesViewModel> GetSalesMonthlyDataByDate(DateTimeOffset from, DateTimeOffset to)
        {
            //var today = DateTime.Today;
            //var todayOffset = new DateTimeOffset(today, new TimeSpan(9, 0, 0));
            //var adminPaymentDataRepo = new AdminPaymentDataRepository();
            //var adminGeneralDataContainer = new AdminGeneralDataContainer();

            //// 決済予測データ
            //var adminPaymentForecastRows = adminGeneralDataContainer.AdminPaymentForecast
            //                                                        .GetDataByDate(DateTime.Parse(from.ToString(DATETIME_DAILY_FORMAT)),
            //                                                                       DateTime.Parse(to.ToString(DATETIME_DAILY_FORMAT)));
            //var monthlyDataList = 
            //    adminPaymentDataRepo.GetMonthlyDataDetail(from, to)
            //                        .Where(x => !x.IsDateNull())
            //                        .Select(x =>
            //                        {
            //                            var paymentForecast = 
            //                                adminPaymentForecastRows.Where(y => y.Date.ToString(DATETIME_MONTHLY_FORMAT) == x.Date.ToString(DATETIME_MONTHLY_FORMAT))
            //                                                        .Sum(y => y.Currency);
            //                            var salesViewModel = new SalesViewModel()
            //                            {
            //                                Date = x.Date.ToString(DATETIME_MONTHLY_FORMAT),
            //                                SalesTotal = x.IsCurrencyTotalNull() ? 0 : x.CurrencyTotal,
            //                                SalesIos = x.IsCurrencyIosTotalNull() ? 0 : x.CurrencyIosTotal,
            //                                SalesAndroid = x.IsCurrencyAndroidTotalNull() ? 0 : x.CurrencyAndroidTotal,
            //                                ForcastTotal = paymentForecast,
            //                            };
            //                            return salesViewModel;
            //                        }).ToList();

            //// 本日分
            //if (from <= todayOffset && todayOffset < to)
            //{
            //    var todayIosData = adminPaymentDataRepo.GetTodayIosData();
            //    var todayAndData = adminPaymentDataRepo.GetTodayAndroidData();
            //    var currentMonthRow = monthlyDataList.FirstOrDefault(x => x.Date == todayOffset.ToString(DATETIME_MONTHLY_FORMAT));

            //    // 月初時
            //    if (currentMonthRow == null)
            //    {
            //        var todayForecastRow =
            //            adminPaymentForecastRows.FirstOrDefault(y => y.Date.ToString(DATETIME_DAILY_FORMAT) == today.ToString(DATETIME_DAILY_FORMAT));
            //        var todaySaleViewModel = new SalesViewModel()
            //        {
            //            Date = today.ToString(DATETIME_DAILY_FORMAT),
            //            SalesTotal = todayIosData.CurrencyTotal + todayAndData.CurrencyTotal,
            //            SalesIos = todayIosData.CurrencyTotal,
            //            SalesAndroid = todayAndData.CurrencyTotal,
            //            ForcastTotal = todayForecastRow == null ? 0 : todayForecastRow.Currency
            //        };
            //        monthlyDataList.Add(todaySaleViewModel);
            //    }
            //    else
            //    {
            //        monthlyDataList = monthlyDataList.Select(x =>
            //                                         {
            //                                             var isCurrentMonth = x.Date == todayOffset.ToString(DATETIME_MONTHLY_FORMAT);
            //                                             return new SalesViewModel()
            //                                             {
            //                                                 Date = x.Date,
            //                                                 SalesTotal = isCurrentMonth ? x.SalesTotal + todayIosData.CurrencyTotal + todayAndData.CurrencyTotal : x.SalesTotal,
            //                                                 SalesIos = isCurrentMonth ? x.SalesIos + todayIosData.CurrencyTotal : x.SalesIos,
            //                                                 SalesAndroid = isCurrentMonth ? x.SalesAndroid + todayAndData.CurrencyTotal : x.SalesAndroid,
            //                                                 ForcastTotal = x.ForcastTotal
            //                                             };
            //                                         }).ToList();
            //    }
            //}

            //return monthlyDataList;
            return null;
        }
    }
}