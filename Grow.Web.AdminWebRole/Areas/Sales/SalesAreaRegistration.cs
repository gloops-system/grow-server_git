﻿using System.Web.Mvc;

namespace Grow.Web.AdminWebRole.Areas.Sales
{
    public class SalesAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Sales";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
/*
            context.MapRoute(
                "Sales_Top",
                "Sales/",
                new { controller = "Top", action = "Index" });
*/
            context.MapRoute(
                "Sales_default",
                "Sales/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
