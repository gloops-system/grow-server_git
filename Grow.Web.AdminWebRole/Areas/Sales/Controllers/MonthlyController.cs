﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Grow.Web.AdminWebRole.Areas.Sales.Models.Core;
using Grow.Web.AdminWebRole.Areas.Sales.Models.ViewModel;
using Fango.Core.Logging;
using System.Collections.Generic;

namespace Grow.Web.AdminWebRole.Areas.Sales.Controllers
{
    public class MonthlyController : ApiController
    {
        //
        // GET: Api/Sales/Monthly/

        /// <summary>
        /// 統合管理画面用API（決済月別）
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        [System.Web.Http.HttpGet]
        public HttpResponseMessage Index(string from, string to)
        {
            var response = new List<SalesViewModel>();
            var statusCode = HttpStatusCode.OK;
            try
            {
                DateTimeOffset fromParam;
                DateTimeOffset toParam;
                if (!DateTimeOffset.TryParse(from, out fromParam) || !DateTimeOffset.TryParse(to, out toParam) ||
                    fromParam.Hour > 0 || toParam.Hour > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new SalesViewModel());
                }
                response = SalesManager.GetSalesMonthlyDataByDate(fromParam, toParam);
            }
            catch (Exception e)
            {
                statusCode = HttpStatusCode.InternalServerError;
                response.Add(new SalesViewModel());
                LoggerManager.DefaultLogger.ErrorFormat(string.Format("{0}\n{1}", e.Message, e.StackTrace));
            }
            return Request.CreateResponse(statusCode, response);
        }
    }
}
