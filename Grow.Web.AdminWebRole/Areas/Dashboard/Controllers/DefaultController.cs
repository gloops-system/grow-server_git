﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Fango.Core.Util;
using Grow.Data.Admin.DataSet.Adapter.Payment;
using Grow.Data.Admin.DataSet.Adapter;

namespace Grow.Web.AdminWebRole.Areas.Dashboard.Controllers
{
    public class GridData
    {
        public string 日付 { get; set; }
        public string 売上 { get; set; }
        public string iOS売上 { get; set; }
        public string Android売上 { get; set; }
        public string DAU { get; set; }
        public string iOSDAU { get; set; }
        public string AndroidDAU { get; set; }
        public string PayedUser { get; set; }
        public string ショップ消費コイン { get; set; }
        public string ショップ消費コインUU { get; set; }
        public string 視察消費コイン { get; set; }
        public string 視察消費コインUU { get; set; }
        public string ガチャ消費コイン { get; set; }
        public string ガチャ消費コインUU { get; set; }
        public string 決済予測 { get; set; }
        public int 達成率 { get; set; }
    }

    public class MonthlyGridData
    {
        public string 日付 { get; set; }
        public string 売上 { get; set; }
        public string PayedUser { get; set; }
        public string 決済予測 { get; set; }
        public int 達成率 { get; set; }
    }
    public class DefaultController : Controller
    {
        //
        // GET: /Dashboard/Default/

        public ActionResult Index()
        {
            const string format = "yyyy-MM-dd";
            const string separateFormat = "{0:N0}";

            var fromParam = Request.Params["from"];
            var toParam   = Request.Params["to"];

            DateTimeOffset from;
            DateTimeOffset to;

            var now = DateUtil.GetDateTimeOffsetNow();

            if (!DateTimeOffset.TryParse(fromParam, out from))
            {
                var year = now.Year;
                var month = now.Month;
                from = DateTimeOffset.Parse(year + "-" + month + "-01");
            }

            if (!DateTimeOffset.TryParse(toParam, out to))
            {
                to = now.Date;
            }

            to = to.AddDays(1);

            var dataRepository = new AdminPaymentDataRepository();
            var adminGeneralDataContainer = new AdminPaymentDataContainer();
            var dataFromRepository = dataRepository.GetDataByFromTo(from, to);          
            var allPaymentForecastRows = adminGeneralDataContainer.AdminPaymentForecast.AdminPaymentForecastDataRows();
            var adminUserCountRows =
                new AdminUserCountDataRepositoryAdapter().GetDataByDateRange(DateTime.Parse(from.ToString()),
                                                                             DateTime.Parse(to.ToString()));

            //データの初期化
            var data = new List<GridData>();
            var diffDays = (to - from).Days;

            for (int i = 0; i < diffDays ; i++)
            {
                data.Add(new GridData
                    {
                        日付 = from.AddDays(i).ToString(format),
                        PayedUser = "-",
                        売上 = "-",
                        iOS売上 = "-",
                        Android売上 = "-",
                        DAU = "-",
                        iOSDAU = "-",
                        AndroidDAU = "-",
                        ショップ消費コイン = "-",
                        ショップ消費コインUU = "-",
                        視察消費コイン = "-",
                        視察消費コインUU = "-",
                        ガチャ消費コイン = "-",
                        ガチャ消費コインUU = "-",
                        決済予測 = "-",
                        達成率 = 0
                    });
            }

            //データの置き換え
            dataFromRepository.ToList().ForEach(row =>
            {
                var dateString = row.Date.ToString(format);
                var targetToRemove = data.FirstOrDefault(x => x.日付 == dateString);
                if (targetToRemove!=null)
                {
                    data.Remove(targetToRemove);
                }
                var paymentForecastRow = allPaymentForecastRows.FirstOrDefault(x => x.Date == row.Date);
                var achievementRate = paymentForecastRow == null || paymentForecastRow.Currency == 0 ? 0 : Math.Floor((double)row.CurrencyTotal * 100 / (double)paymentForecastRow.Currency);

                var activeUsers = adminUserCountRows.FirstOrDefault(y => y.Date.ToString(format) == row.Date.ToString(format));

                var grid = new GridData
                {
                    日付 = dateString,
                    売上 = string.Format(separateFormat, row.CurrencyTotal),
                    iOS売上 = string.Format(separateFormat, row.CurrencyIosTotal),
                    Android売上 = string.Format(separateFormat, row.CurrencyAndroidTotal),
                    DAU = activeUsers == null ? "-" : activeUsers.ActiveUserCount.ToString("#,0"),
                    iOSDAU = activeUsers == null ? "-" : activeUsers.ActiveUserIosCount.ToString("#,0"),
                    AndroidDAU = activeUsers == null ? "-" : activeUsers.ActiveUserAndroidCount.ToString("#,0"),
                    PayedUser = string.Format(separateFormat, row.CurrencyUser),
                    ショップ消費コイン = string.Format(separateFormat, row.ShopBuyUseCoin),
                    ショップ消費コインUU = string.Format(separateFormat, row.ShopBuyUser),
                    視察消費コイン = string.Format(separateFormat, row.ScoutUseCoin),
                    視察消費コインUU = string.Format(separateFormat, row.ScoutUsedCoinUser),
                    ガチャ消費コイン = string.Format(separateFormat, row.GachaUseCoin),
                    ガチャ消費コインUU = string.Format(separateFormat, row.GachaUsedCoinUser),
                    決済予測 = paymentForecastRow == null ? "-" : paymentForecastRow.Currency.ToString("#,0"),
                    達成率 = (int) achievementRate
                };
                data.Add(grid);
            });

            ViewBag.From = from.Date.ToString(format);
            ViewBag.To   = to.Date.AddDays(-1).ToString(format);

            //今日のデータ
            var todayString = now.Date.ToString("yyyy-MM-dd");

            if (data.Any(x => x.日付 == todayString))
            {
                var targetToRemove = data.FirstOrDefault(x => x.日付 == todayString);

                if (targetToRemove!=null)
                {
                    data.Remove(targetToRemove);
                }

                var todayIosData = dataRepository.GetTodayIosData();
                var todayArdData = dataRepository.GetTodayAndroidData();
                var currentPaymentForecastRow = allPaymentForecastRows.FirstOrDefault(x => x.Date == now.Date);
                data.Add(new GridData
                    {
                        日付 = todayIosData.Date.ToString("yyyy-MM-dd"),
                        売上 = string.Format(separateFormat, todayIosData.CurrencyTotal + todayArdData.CurrencyTotal),
                        PayedUser = string.Format(separateFormat, todayIosData.CurrencyUser + todayArdData.CurrencyUser),
                        決済予測 = currentPaymentForecastRow == null ? "-" : currentPaymentForecastRow.Currency.ToString("#,0"),
                        達成率 = (int)(currentPaymentForecastRow == null || currentPaymentForecastRow.Currency == 0 ? 0 :
                            Math.Floor(((double)todayIosData.CurrencyTotal + (double)todayArdData.CurrencyTotal) * 100 / (double)currentPaymentForecastRow.Currency))
                    });
            }
            ViewBag.data = data;


            //月別データ
            var monthlyData = dataRepository.GetMonthlyData();
            var monthlyDataList = monthlyData.Select(row =>
                {
                    var forecast = allPaymentForecastRows.Where(x => DateTime.Parse(row.Date.ToString()) <= x.Date &&
                                                                     x.Date < DateTime.Parse(row.Date.AddMonths(1).ToString()))
                                                         .Sum(x => x.Currency);
                    var achievementRate = forecast == 0 ? 0 : Math.Floor((double)row.CurrencyTotal * 100 / (double)forecast);
                    var m = new MonthlyGridData()
                    {
                        PayedUser = string.Format(separateFormat, row.CurrencyUser),
                        日付 = row.Date.ToString("yyyy-MM"),
                        売上 = string.Format(separateFormat, row.CurrencyTotal),
                        決済予測 = forecast.ToString("#,0"),
                        達成率 = (int)achievementRate
                    };
                    return m;
                }).ToList();

            //今月のデータ
            var currentIosMonthlyData = dataRepository.GetCurrentIosMonthlyData();
            var currentArdMonthlyData = dataRepository.GetCurrentAndroidMonthlyData();
            var currentForecast = allPaymentForecastRows.Where(x => DateTime.Parse(currentIosMonthlyData.Date.ToString()) <= x.Date &&
                                                                    x.Date.CompareTo(now.Date) <= 0)
                                                        .Sum(x => x.Currency);
            var currentAchievementRate = currentForecast == 0
                                             ? 0
                                             //: Math.Floor(((double)currentIosMonthlyData.CurrencyTotal * 100 + (double)currentArdMonthlyData.CurrencyTotal * 100) / (double)currentForecast);
                                             : Math.Floor(((double)(currentIosMonthlyData.CurrencyTotal + currentArdMonthlyData.CurrencyTotal) * 100) / (double)currentForecast);
            monthlyDataList.Add(new MonthlyGridData
                {
                    PayedUser = string.Format(separateFormat, currentIosMonthlyData.CurrencyUser + currentArdMonthlyData.CurrencyUser),
                    日付 = currentIosMonthlyData.Date.ToString("yyyy-MM"),
                    売上 = string.Format(separateFormat, currentIosMonthlyData.CurrencyTotal + currentArdMonthlyData.CurrencyTotal),
                    決済予測 = currentForecast.ToString("#,0"),
                    達成率 = (int)currentAchievementRate
                });
            ViewBag.TotalCurrentPaymentForecast = allPaymentForecastRows.Where(x => DateTime.Parse(currentIosMonthlyData.Date.ToString()) <= x.Date &&
                                                                                    x.Date.CompareTo(now.Date.AddMonths(1)) < 0)
                                                                        .Sum(x => x.Currency);

            ViewBag.MonthlyData = monthlyDataList;

            return View();
        }

    }
}
