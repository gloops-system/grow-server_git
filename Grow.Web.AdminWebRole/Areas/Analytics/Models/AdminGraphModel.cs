﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Fango.Core.Util;

namespace Grow.Web.AdminWebRole.Areas.Analytics.Models
{
    /// <summary>
    /// 複数日付データ同時表示を行うためのグラフデータ格納用ベースモデル
    /// </summary>
    public class AdminGraphModel
    {
        public DateTimeOffset TargetDate { get; set; }

        /// <summary>
        /// グラフの開始日時
        /// </summary>
        public DateTimeOffset FromDate
        {
            get
            {
                return new DateTimeOffset(DateUtil.GetDateTimeOffsetNow().Date, DateUtil.Offset);
            }
        }

        /// <summary>
        /// グラフの終了日時
        /// </summary>
        public DateTimeOffset ToDate
        {
            get { return FromDate.AddDays(1); }
        }

        public string ToJsStringByData(IEnumerable<AdminGraphPoint> graphData)
        {
            return string.Join(", ", graphData.Select(r => r.ToString()));
        }
    }

    /// <summary>
    /// グラフ位置データ格納用モデル
    /// </summary>
    public class AdminGraphPoint
    {
        private const string DateFormat = "yyyy-MM-dd";

        private const string KeyFormatString = "[new Date(\"{0}\"),{1}]";

        public DateTimeOffset Date { get; set; }
        public int Value { get; set; }
        public bool IsNextDay { get; set; }

        public AdminGraphPoint(DateTimeOffset date, int value, bool isNextDay = false)
        {
            Date = date;
            Value = value;
            IsNextDay = isNextDay;
        }

        public override string ToString()
        {
            var now = DateUtil.GetDateTimeOffsetNow();

            var displayDate = string.Format(
                KeyFormatString,
                string.Format("{0} {1} +09:00", now.AddDays(IsNextDay ? 1 : 0).ToString(DateFormat), Date.ToString("HH:mm:ss")), Value);

            return displayDate;
        }
    }
}