﻿using System.Web;
using System.Web.Mvc;

namespace Grow.Web.AdminWebRole
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}