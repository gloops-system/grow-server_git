﻿using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using Fango.Web.Core.Handlers;

namespace Grow.Web.AdminWebRole
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // 運用系API
            config.Routes.MapHttpRoute(
                name: "OperationApi",
                routeTemplate: "api/operation/{controller}/{action}",
                defaults: new { action = UrlParameter.Optional }
            );

            // 統合管理画面用API
            config.Routes.MapHttpRoute(
                name: "SalesApi",
                routeTemplate: "api/sales/{controller}/{action}",
                defaults: new { action = UrlParameter.Optional }
            );

            // クライアント用API
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{group}/{controller}/{action}",
                defaults: new { action = UrlParameter.Optional },
                constraints: null,
                handler: new ClientApiHandler(config, null)
            );

            // メディアタイプをjsonのみとし、xmlには対応しない
            var appXmlType = config.Formatters.XmlFormatter.SupportedMediaTypes.FirstOrDefault(t => t.MediaType == "application/xml");
            config.Formatters.XmlFormatter.SupportedMediaTypes.Remove(appXmlType);

            //config.Filters.Add(new ApiExceptionFilterAttribute());

            //gzip圧縮
            //referd: http://stackoverflow.com/questions/10443588/mvc4-webapi-compress-get-method-response
            //config.MessageHandlers.Add(new CompressHandler());


            // IQueryable または IQueryable<T> 戻り値の型を持つアクションのクエリのサポートを有効にするには、次のコード行のコメントを解除してください。
            // 予期しないクエリまたは悪意のあるクエリの処理を避けるには、QueryableAttribute の検証設定を使用して受信するクエリを検証してください。
            // 詳細については、http://go.microsoft.com/fwlink/?LinkId=279712 を参照してください。
            //config.EnableQuerySupport();

            // アプリケーションでのトレースを無効にするには、以下のコード行をコメント アウトするか、削除してください
            // 詳細については、http://www.asp.net/web-api を参照してください
            config.EnableSystemDiagnosticsTracing();
        }
    }
}
