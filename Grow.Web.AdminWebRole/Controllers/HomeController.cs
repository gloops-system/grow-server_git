﻿using System.Web.Mvc;

namespace Grow.Web.AdminWebRole.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
