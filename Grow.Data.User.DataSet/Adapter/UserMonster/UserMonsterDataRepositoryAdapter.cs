﻿using Fango.Core.Data;
using Fango.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grow.Data.User.DataSet.UserDataSetTableAdapters;
using Grow.Data.User.DataSet.Properties;
using Microsoft.ApplicationBlocks.Data;

namespace Grow.Data.User.DataSet.Adapter.UserMonster
{
    public class UserMonsterDataRepositoryAdapter
    {
        public UserDataSet.UserMonsterRow GetDataByPK(long userMonsterId)
        {
            var dataRow = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userMonsterTableAdapter = new UserMonsterTableAdapter())
                {
                    return userMonsterTableAdapter.GetDataByPK(userMonsterId).FirstOrDefault();
                }
            });
            return dataRow;
        }
        public UserDataSet.UserMonsterRow[] GetDataByUserId(int UserId)
        {
            var dataRows = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userMonsterTableAdapter = new UserMonsterTableAdapter())
                {
                    return userMonsterTableAdapter.GetMonstersByUserId(UserId).ToArray();
                }
            });
            return dataRows;
        }

        public long AddNew(int userId, int monsterId, byte status, byte life, int hp, int lastHp, int hpRecoveryAmount,
                           int stunResistance, int stunRecoveryStand, int stunRecoveryDown, int attack, int defense)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userMonsterTableAdapter = new UserMonsterTableAdapter())
                {
                    var now = DateUtil.GetDateTimeOffsetNow();
                    return userMonsterTableAdapter.AddNew(userId, monsterId, status, 1, 0, life, hp, lastHp, now,
                                                         hpRecoveryAmount,stunResistance,stunRecoveryStand,stunRecoveryDown,
                                                         attack, defense, 0, now, now);

                }
            });
            return Convert.ToInt64(result);
        }

        public void BulkInsert(UserDataSet.UserMonsterDataTable userMonsterDataTable)
        {
            var userConnStr = Settings.Default.edge_user_devConnectionString;
            new DbAccessHandler().ExecuteAction(() => SqlHelper.BulkInsert(userConnStr, userMonsterDataTable));
        }

        public int UpdateUserMonsterStatus(long UserMonsterId, byte Status)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userMonsterTableAdapter = new UserMonsterTableAdapter())
                {
                    return userMonsterTableAdapter.UpdateUserMonsterStatus(Status, UserMonsterId);

                }
            });
            return result;
        }

        public int DeleteByUserMonster(long UserMonsterId)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userMonsterTableAdapter = new UserMonsterTableAdapter())
                {
                    return userMonsterTableAdapter.DeleteByPK(UserMonsterId);

                }
            });
            return result;
        }

        public int PullBackUserMonster(long UserMonsterId)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userMonsterTableAdapter = new UserMonsterTableAdapter())
                {
                    var now = DateUtil.GetDateTimeOffsetNow();
                    return userMonsterTableAdapter.PullBackUserMonster(now, now, UserMonsterId);
                }
            });
            return result;
        }

        public int RecoverUserMonster(long UserMonsterId, int Hp)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userMonsterTableAdapter = new UserMonsterTableAdapter())
                {
                    var now = DateUtil.GetDateTimeOffsetNow();
                    return userMonsterTableAdapter.RecoverUserMonster(Hp, now, now, UserMonsterId);
                }
            });
            return result;
        }
    }
}
