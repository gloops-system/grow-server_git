﻿using Grow.Data.User.DataSet.UserLabyrinthDataSetTableAdapters;
using Fango.Core.Data;
using Fango.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Data.User.DataSet.Adapter.UserMonster
{
    public class UserMeetDispatchMonsterDataRepositoryAdapter 
    {
        public UserLabyrinthDataSet.UserMeetDispatchMonsterRow[] GetMeetMonsterList(int targetUserId)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new UserMeetDispatchMonsterTableAdapter())
                {
                    return adapter.GetPlayerDispatchedMonster(targetUserId).ToArray();
                }
            });
            return result;
        }        
    }
}
