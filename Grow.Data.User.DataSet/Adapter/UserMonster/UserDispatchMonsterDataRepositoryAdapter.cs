﻿using Grow.Data.User.DataSet.UserLabyrinthDataSetTableAdapters;
using Fango.Core.Data;
using Fango.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Data.User.DataSet.Adapter.UserMonster
{
    public class UserDispatchMonsterDataRepositoryAdapter
    {
        public void AddNew(int userId, int targetUserId, long userMonsterId, int targetPosition)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userDispatchMonsterTableAdapter = new UserDispatchMonsterTableAdapter())
                {
                    var now = DateUtil.GetDateTimeOffsetNow();
                    userDispatchMonsterTableAdapter.AddNew(userId, userMonsterId, targetUserId, targetPosition, now, now);
                }
            });
        }


        public UserLabyrinthDataSet.UserDispatchMonsterRow GetDispatchMonster(int targetUserId, long userMonsterId)
        {
            var dataRow = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userDispatchMonsterTableAdapter = new UserDispatchMonsterTableAdapter())
                {
                    return userDispatchMonsterTableAdapter.GetDataByTargetUserIdAndUserMonsterId(targetUserId, userMonsterId).FirstOrDefault();
                }
            });
            return dataRow;
        }

        public List<UserLabyrinthDataSet.UserDispatchMonsterRow> GetMeetMonsterList(int targetUserId)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userDispatchMonsterTableAdapter = new UserDispatchMonsterTableAdapter())
                {
                    return userDispatchMonsterTableAdapter.GetDataByTargetUserId(targetUserId).ToList<UserLabyrinthDataSet.UserDispatchMonsterRow>();
                }
            });
            return result;
        }        

        public int DeleteByUserMonsterId(long UserMonsterId)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new UserDispatchMonsterTableAdapter())
                {
                    return adapter.DeleteDataByPK(UserMonsterId);

                }
            });
            return result;
        }
    }
}
