﻿using Grow.Data.User.DataSet.UserDataSetTableAdapters;
using Fango.Core.Data;
using Fango.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Data.User.DataSet.Adapter.UserAvatar
{
    public class UserAvatarDataRepositoryAdapter
    {
        public UserDataSet.UserAvatarRow[] GetDataByUserId(int UserId)
        {
            var dataRows = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userAvatarTableAdapter = new UserAvatarTableAdapter())
                {
                    return userAvatarTableAdapter.GetAvatarsByUserId(UserId).ToArray();
                }
            });
            return dataRows;
        }

        public UserDataSet.UserAvatarRow GetDataByUserAvatarId(long userAvatarId)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userAvatarTableAdapter = new UserAvatarTableAdapter())
                {
                    return userAvatarTableAdapter.GetDataByPK(userAvatarId).FirstOrDefault();
                }
            });
            return result;
        }

        public long AddNew(int userId, int avatarId, int hp, int sp, int attack, int defense, int score)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userAvatarTableAdapter = new UserAvatarTableAdapter())
                {
                    var now = DateUtil.GetDateTimeOffsetNow();
                    return userAvatarTableAdapter.AddNew(userId, avatarId, 0, 1, hp, sp, attack, defense, score, now, now);

                }
            });
            return Convert.ToInt64(result);
        }

        public int UpdateAvatar(UserDataSet.UserAvatarRow userAvatar)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userAvatarTableAdapter = new UserAvatarTableAdapter())
                {
                    var now = DateUtil.GetDateTimeOffsetNow();
                    return userAvatarTableAdapter.UpdateAvatarByPK(userAvatar.Exp,
                                                               userAvatar.Level,
                                                               userAvatar.Hp,
                                                               userAvatar.Sp,
                                                               userAvatar.Attack,
                                                               userAvatar.Defense,
                                                               userAvatar.Score,
                                                               now,
                                                               userAvatar.UserAvatarId);
                }
            });
            return result;
        }
    }
}
