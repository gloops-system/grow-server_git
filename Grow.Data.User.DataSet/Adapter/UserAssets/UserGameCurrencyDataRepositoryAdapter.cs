﻿using System;
using System.Linq;
using Fango.Core.Data;
using Grow.Data.User.DataSet.UserDataSetTableAdapters;

namespace Grow.Data.User.DataSet.Adapter.UserAssets
{
    public class UserGameCurrencyDataRepositoryAdapter
    {
        /// <summary>
        /// 全取得
        /// </summary>
        /// <returns></returns>
        public UserDataSet.UserGameCurrencyRow[] GetData()
        {
            var userGameCurrencyRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userGameCurrencyTableAdapter = new UserGameCurrencyTableAdapter())
                    {
                        return userGameCurrencyTableAdapter.GetData().ToArray();
                    }
                });
            return userGameCurrencyRows;
        }

        /// <summary>
        /// PKで取得
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserDataSet.UserGameCurrencyRow GetDataByPK(int userId)
        {
            var userGameCurrencyRow = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userGameCurrencyTableAdapter = new UserGameCurrencyTableAdapter())
                    {
                        return userGameCurrencyTableAdapter.GetDataByPK(userId).FirstOrDefault();
                    }
                });
            return userGameCurrencyRow;
        }

        /// <summary>
        /// Increment
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="num"></param>
        /// <param name="now"></param>
        /// <returns></returns>
        public int IncrementUserGameCurrency(int userId, int num, DateTimeOffset now)
        {
            var cnt = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userGameCurrencyTableAdapter = new UserGameCurrencyTableAdapter())
                    {
                        userGameCurrencyTableAdapter.IncrementUserGameCurrency(userId, num, now);
                        return 1;
                    }
                });
            return cnt;
        }

        /// <summary>
        /// Decrement
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="num"></param>
        /// <param name="now"></param>
        /// <returns></returns>
        public int DecrementUserGameCurrency(int userId, int num, DateTimeOffset now)
        {
            var cnt = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userGameCurrencyTableAdapter = new UserGameCurrencyTableAdapter())
                    {
                        userGameCurrencyTableAdapter.DecrementUserGameCurrency(userId, num, now);
                        return 1;
                    }
                });
            return cnt;
        }
    }
}