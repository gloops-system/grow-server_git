﻿using System;
using System.Linq;
using Fango.Core.Data;
using Grow.Data.User.DataSet.UserDataSetTableAdapters;

namespace Grow.Data.User.DataSet.Adapter.UserAssets
{
    public class UserMaterialDataRepositoryAdapter
    {
        /// <summary>
        /// 全取得
        /// </summary>
        /// <returns></returns>
        public UserDataSet.UserMaterialRow[] GetData()
        {
            var userMaterialRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userMaterialTableAdapter = new UserMaterialTableAdapter())
                    {
                        return userMaterialTableAdapter.GetData().ToArray();
                    }
                });
            return userMaterialRows;
        }

        /// <summary>
        /// UserIdで取得
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserDataSet.UserMaterialRow[] GetDataByUserId(int userId)
        {
            var userMaterialRow = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userMaterialTableAdapter = new UserMaterialTableAdapter())
                    {
                        return userMaterialTableAdapter.GetDataByUserId(userId).ToArray();
                    }
                });
            return userMaterialRow;
        }

        /// <summary>
        /// UserIdで取得(DataTable版)
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserDataSet.UserMaterialDataTable GetDataTableByUserId(int userId)
        {
            var userMaterialDataTable = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userMaterialTableAdapter = new UserMaterialTableAdapter())
                    {
                        return userMaterialTableAdapter.GetDataByUserId(userId);
                    }
                });
            return userMaterialDataTable;
        }

        // TODO : 増減させる処理はひとつで良いのではないか
        
        /// <summary>
        /// 素材を増やす
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="materialId"></param>
        /// <param name="num"></param>
        /// <returns></returns>
        public int IncrementUserMaterial(int userId, int materialId, int num, DateTime now)
        {
            var current = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userMaterialTableAdapter = new UserMaterialTableAdapter())
                    {
                        userMaterialTableAdapter.IncrementUserMaterial(userId, materialId, num, now);
                        return 1;
                    }
                });
            return current;
        }

        /// <summary>
        /// 素材を減らす
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="materialId"></param>
        /// <param name="num"></param>
        /// <returns></returns>
        public int DecrementUserMaterial(int userId, int materialId, int num, DateTime now)
        {
            var current = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userMaterialTableAdapter = new UserMaterialTableAdapter())
                    {
                        userMaterialTableAdapter.DecrementUserMaterial(userId, materialId, num, now);
                        return 1;
                    }
                });
            return current;
        }

        /// <summary>
        /// DataTableで一括更新
        /// </summary>
        /// <param name="userMaterialDataTable"></param>
        public int UpdateNumByDataTable(UserDataSet.UserMaterialDataTable userMaterialDataTable)
        {
            var updateCount = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userMaterialTableAdapter = new UserMaterialTableAdapter())
                    {
                        return userMaterialTableAdapter.Update(userMaterialDataTable);
                    }
                });
            return updateCount;
        }
    }
}