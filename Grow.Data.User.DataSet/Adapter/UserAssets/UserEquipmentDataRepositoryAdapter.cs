﻿using System.Linq;
using Fango.Core.Data;
using Fango.Core.Util;
using System;
using Grow.Data.User.DataSet.UserLabyrinthDataSetTableAdapters;
using Grow.Data.User.DataSet.Properties;
using Microsoft.ApplicationBlocks.Data;

namespace Grow.Data.User.DataSet.Adapter.UserAssets
{
    public class UserEquipmentDataRepositoryAdapter
    {
        public UserLabyrinthDataSet.UserEquipmentRow[] GetDataByUserId(int userId)
        {
            var results = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new UserEquipmentTableAdapter())
                {
                    return adapter.GetDataByUserId(userId).ToArray();
                }
            }
            );

            return results;
        }

        public void DeleteDataByUserId(int userId)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new UserEquipmentTableAdapter())
                {
                    adapter.DeleteDataByUserId(userId);
                }
            }
            );
        }

        public void BulkInsert(UserLabyrinthDataSet.UserEquipmentDataTable userEquipmentDataTable)
        {
            var userConnStr = Settings.Default.edge_user_devConnectionString;
            new DbAccessHandler().ExecuteAction(() => SqlHelper.BulkInsert(userConnStr, userEquipmentDataTable));
        }
    }
}
