﻿using System;
using System.Linq;
using Fango.Core.Data;
using Grow.Data.User.DataSet.UserDataSetTableAdapters;

namespace Grow.Data.User.DataSet.Adapter.UserAssets
{
    public class UserNewsDataRepositoryAdapter
    {
        /// <summary>
        /// 全取得
        /// </summary>
        /// <returns></returns>
        public UserDataSet.UserNewsRow[] GetData()
        {
            var userNewsRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userNewsTableAdapter = new UserNewsTableAdapter())
                    {
                        return userNewsTableAdapter.GetData().ToArray();
                    }
                });
            return userNewsRows;
        }

        /// <summary>
        /// UserIdで取得
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserDataSet.UserNewsRow[] GetDataByUserId(int userId)
        {
            var userNewsRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userNewsTableAdapter = new UserNewsTableAdapter())
                    {
                        return userNewsTableAdapter.GetDataByUserId(userId).ToArray();
                    }
                });
            return userNewsRows;
        }

        /// <summary>
        /// 追加
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="newsId"></param>
        /// <param name="now"></param>
        /// <returns></returns>
        public int AddNew(int userId, int newsId, DateTimeOffset now)
        {
            var insertCount = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userNewsTableAdapter = new UserNewsTableAdapter())
                    {
                        return userNewsTableAdapter.AddNew(userId, newsId, now, now);
                    }
                });
            return insertCount;
        }
    }
}