﻿using System;
using System.Linq;
using Fango.Core.Data;
using Grow.Data.User.DataSet.Properties;
using Grow.Data.User.DataSet.UserDataSetTableAdapters;
using Microsoft.ApplicationBlocks.Data;

namespace Grow.Data.User.DataSet.Adapter.UserAssets
{
    public class UserGiftDataRepositoryAdapter
    {
        /// <summary>
        /// 全取得
        /// </summary>
        /// <returns></returns>
        public UserDataSet.UserGiftRow[] GetData()
        {
            var userGiftRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userGiftTableAdapter = new UserGiftTableAdapter())
                    {
                        return userGiftTableAdapter.GetData().ToArray();
                    }
                });
            return userGiftRows;
        }

        /// <summary>
        /// PKで取得
        /// </summary>
        /// <param name="userGiftId"></param>
        /// <returns></returns>
        public UserDataSet.UserGiftRow GetDataByPK(long userGiftId)
        {
            var userGiftRow = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userGiftTableAdapter = new UserGiftTableAdapter())
                    {
                        return userGiftTableAdapter.GetDataByPK(userGiftId).FirstOrDefault();
                    }
                });
            return userGiftRow;
        }

        /// <summary>
        /// UserIdで取得
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserDataSet.UserGiftRow[] GetDataByUserId(int userId)
        {
            var userGiftRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userGiftTableAdapter = new UserGiftTableAdapter())
                    {
                        return userGiftTableAdapter.GetDataByUserId(userId).ToArray();
                    }
                });
            return userGiftRows;
        }

        /// <summary>
        /// 追加(取得時)
        /// </summary>
        /// <param name="userGiftId"></param>
        /// <param name="userId"></param>
        /// <param name="giftType"></param>
        /// <param name="category"></param>
        /// <param name="id"></param>
        /// <param name="num"></param>
        /// <returns></returns>
        public int AddNew(long userGiftId, int userId, int giftType, int category, int id, int num, DateTime now)
        {
            var insertCount = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userGiftTableAdapter = new UserGiftTableAdapter())
                    {
                        return userGiftTableAdapter.AddNew(userGiftId, userId, giftType, category, id, num, null, now, now);
                    }
                });
            return insertCount;
        }

        /// <summary>
        /// 更新(開封時)
        /// </summary>
        /// <param name="userGiftId"></param>
        /// <param name="getDate"></param>
        /// <returns></returns>
        public int UpdateByPK(long userGiftId, DateTimeOffset getDate, DateTime now)
        {
            var updateCount = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userGiftTableAdapter = new UserGiftTableAdapter())
                    {
                        return userGiftTableAdapter.UpdateByPK(now, userGiftId);
                    }
                });
            return updateCount;
        }

        /// <summary>
        /// BulkInsert
        /// </summary>
        /// <param name="userGiftDataTable"></param>
        public void BulkInsert(UserDataSet.UserGiftDataTable userGiftDataTable)
        {
            var userConnStr = Settings.Default.edge_user_devConnectionString;
            new DbAccessHandler().ExecuteAction(() => SqlHelper.BulkInsert(userConnStr, userGiftDataTable));
        }
    }
}