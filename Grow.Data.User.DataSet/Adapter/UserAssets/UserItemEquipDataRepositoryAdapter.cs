﻿using System;
using System.Linq;
using Fango.Core.Data;
using Grow.Data.User.DataSet.Properties;
using Grow.Data.User.DataSet.UserDataSetTableAdapters;
using Microsoft.ApplicationBlocks.Data;

namespace Grow.Data.User.DataSet.Adapter.UserAssets
{
    public class UserItemEquipDataRepositoryAdapter
    {
        /// <summary>
        /// 全取得
        /// </summary>
        /// <returns></returns>
        public UserDataSet.UserItemEquipRow[] GetData()
        {
            var userItemEquipRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userItemEquipTableAdapter = new UserItemEquipTableAdapter())
                    {
                        return userItemEquipTableAdapter.GetData().ToArray();
                    }
                });
            return userItemEquipRows;
        }

        /// <summary>
        /// PKで取得
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserDataSet.UserItemEquipRow GetDataByPK(int userId, int itemId)
        {
            var userItemEquipRow = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userItemEquipTableAdapter = new UserItemEquipTableAdapter())
                    {
                        return userItemEquipTableAdapter.GetDataByPK(userId, itemId).FirstOrDefault();
                    }
                });
            return userItemEquipRow;
        }

        /// <summary>
        /// UserIdで取得
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserDataSet.UserItemEquipDataTable GetDataByUserId(int userId)
        {
            var userItemEquipRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userItemEquipTableAdapter = new UserItemEquipTableAdapter())
                    {
                        return userItemEquipTableAdapter.GetDataByUserId(userId);
                    }
                });
            return userItemEquipRows;
        }

        /// <summary>
        /// 追加
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="itemId"></param>
        /// <param name="num"></param>
        /// <param name="dispOrder"></param>
        /// <returns></returns>
        public int AddNew(int userId, int itemId, int num, int dispOrder, DateTime now)
        {
            var insertCount = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userItemEquipTableAdapter = new UserItemEquipTableAdapter())
                    {
                        return userItemEquipTableAdapter.AddNew(userId, itemId, num, dispOrder, now, now);
                    }
                });
            return insertCount;
        }

        /// <summary>
        /// アイテム設定
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="itemId"></param>
        /// <param name="num"></param>
        /// <param name="dispOrder"></param>
        /// <returns></returns>
        public int SetItem(int userId, int itemId, int num, int dispOrder, DateTime now)
        {
            var updateCount = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userItemEquipTableAdapter = new UserItemEquipTableAdapter())
                    {
                        userItemEquipTableAdapter.SetItem(userId, itemId, num, dispOrder, now);
                        return 1;
                    }
                });
            return updateCount;
        }

        /// <summary>
        /// アイテム減算
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="itemId"></param>
        /// <param name="decrimentNum"></param>
        public void DecrementUserItem(int userId, int itemId, int decrimentNum, DateTime now)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userItemEquipTableAdapter = new UserItemEquipTableAdapter())
                {
                    userItemEquipTableAdapter.DecrementUserItem(userId, itemId, decrimentNum, now);
                }
            });
        }

        /// <summary>
        /// BulkInsert
        /// </summary>
        /// <param name="userItemEquipDataTable"></param>
        public void BulkInsert(UserDataSet.UserItemEquipDataTable userItemEquipDataTable)
        {
            var userConnStr = Settings.Default.edge_user_devConnectionString;
            new DbAccessHandler().ExecuteAction(() => SqlHelper.BulkInsert(userConnStr, userItemEquipDataTable));
        }



        /// <summary>
        /// DataTableで一括更新
        /// </summary>
        /// <param name="userItemEquipDataTable"></param>
        /// <returns></returns>
        public int UpdateDataByDataTable(UserDataSet.UserItemEquipDataTable userItemEquipDataTable)
        {
            var updateCount = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userItemEquipTableAdapter = new UserItemEquipTableAdapter())
                {
                    return userItemEquipTableAdapter.Update(userItemEquipDataTable);
                }
            });
            return updateCount;
        }
    }
}