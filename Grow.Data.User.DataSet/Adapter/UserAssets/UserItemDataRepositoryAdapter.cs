﻿using System;
using System.Linq;
using Fango.Core.Data;
using Grow.Data.User.DataSet.UserDataSetTableAdapters;

namespace Grow.Data.User.DataSet.Adapter.UserAssets
{
    public class UserItemDataRepositoryAdapter
    {
        /// <summary>
        /// 全取得
        /// </summary>
        /// <returns></returns>
        public UserDataSet.UserItemRow[] GetData()
        {
            var userItemRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userItemTableAdapter = new UserItemTableAdapter())
                    {
                        return userItemTableAdapter.GetData().ToArray();
                    }
                });
            return userItemRows;
        }

        /// <summary>
        /// PKで取得
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="itemId"></param>
        /// <returns></returns>
        public UserDataSet.UserItemRow GetDataByPK(int userId, int itemId)
        {
            var userItemRow = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userItemTableAdapter = new UserItemTableAdapter())
                    {
                        return userItemTableAdapter.GetDataByPK(userId, itemId).FirstOrDefault();
                    }
                });
            return userItemRow;
        }

        /// <summary>
        /// UserIdで取得
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserDataSet.UserItemRow[] GetDataByUserId(int userId)
        {
            var userItemRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userItemTableAdapter = new UserItemTableAdapter())
                    {
                        return userItemTableAdapter.GetDataByUserId(userId).ToArray();
                    }
                });
            return userItemRows;
        }

        /// <summary>
        /// アイテムを増やす
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="itemId"></param>
        /// <param name="num"></param>
        /// <param name="now"></param>
        /// <returns></returns>
        public int IncrementUserItem(int userId, int itemId, int num, DateTimeOffset now)
        {
            var current = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userItemTableAdapter = new UserItemTableAdapter())
                    {
                        userItemTableAdapter.IncrementUserItem(userId, itemId, num, now);
                        return 1;
                    }
                });
            return current;
        }

        /// <summary>
        /// アイテムを減らす
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="itemId"></param>
        /// <param name="num"></param>
        /// <param name="now"></param>
        /// <returns></returns>
        public int DecrementUserItem(int userId, int itemId, int num, DateTimeOffset now)
        {
            var current = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userItemTableAdapter = new UserItemTableAdapter())
                    {
                        userItemTableAdapter.DecrementUserItem(userId, itemId, num, now);
                        return 1;
                    }
                });
            return current;
        }
    }
}