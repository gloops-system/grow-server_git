﻿using Grow.Data.User.DataSet.UserDataSetTableAdapters;
using Fango.Core.Data;
using Fango.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Data.User.DataSet.Adapter.UserAssets
{
    public class UserNotificationDataRepositoryAdapter
    {
        public UserDataSet.UserNotificationRow[] GetDataByUserIdAndStatus(int userId, bool status)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new UserNotificationTableAdapter())
                {
                    return adapter.GetDataByUserIdAndStatus(userId, status).ToArray();
                }
            });
            return result;
        }        

        public long AddNew(int userId, string message, short category, long threadId)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new UserNotificationTableAdapter())
                {
                    var now = DateUtil.GetDateTimeOffsetNow();
                    return adapter.AddNew(userId, message, category, threadId, false, now, now);
                }
            });
            return Convert.ToInt64(result);
        }

        public void UpdateLastReadNotification(long lastUserNotificationId)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new UserNotificationTableAdapter())
                {
                    var now = DateUtil.GetDateTimeOffsetNow();
                    adapter.UpdateLastReadNotification(now, lastUserNotificationId);
                }
            });            
        }
    }
}
