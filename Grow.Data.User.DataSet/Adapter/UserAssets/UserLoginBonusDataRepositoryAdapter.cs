﻿using System;
using System.Linq;
using Fango.Core.Data;
using Grow.Data.User.DataSet.UserDataSetTableAdapters;

namespace Grow.Data.User.DataSet.Adapter.UserAssets
{
    public class UserLoginBonusDataRepositoryAdapter
    {
        /// <summary>
        /// UserIdで取得
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserDataSet.UserLoginBonusRow[] GetDataByUserId(int userId)
        {
            var userLoginBonusRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userLoginBonusTableAdapter = new UserLoginBonusTableAdapter())
                    {
                        return userLoginBonusTableAdapter.GetDataByUserId(userId).ToArray();
                    }
                });
            return userLoginBonusRows;
        }

        /// <summary>
        /// 更新か追加
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="loginBonusId"></param>
        /// <param name="loginNum"></param>
        /// <param name="sheetId"></param>
        public void UpdateOrAddUserLoginBonus(int userId, int loginBonusId, int loginNum, int sheetId, DateTime now)
        {
            new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userLoginBonusTableAdapter = new UserLoginBonusTableAdapter())
                    {
                        userLoginBonusTableAdapter.UpdateOrAddUserLoginBonus(userId, loginBonusId, loginNum,
                                                                             sheetId, now);
                    }
                });
        }

        /// <summary>
        /// 削除
        /// </summary>
        /// <param name="userId"></param>
        public void DeleteByPK(int userId)
        {
            new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userLoginBonusTableAdapter = new UserLoginBonusTableAdapter())
                    {
                        userLoginBonusTableAdapter.DeleteByPK(userId);
                    }
                });
        }
    }
}