﻿using System;
using System.Linq;
using Fango.Core.Data;
using Grow.Data.User.DataSet.UserDataSetTableAdapters;

namespace Grow.Data.User.DataSet.Adapter.UserAssets
{
    public class UserShopCounterDataRepositoryAdapter
    {
        public UserDataSet.UserShopCounterRow GetDataByPK(int userId, int shopItemId)
        {
            var userMaterialRow = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userMaterialTableAdapter = new UserShopCounterTableAdapter())
                    {
                        return userMaterialTableAdapter.GetDataByPK(userId, shopItemId).FirstOrDefault();
                    }
                });
            return userMaterialRow;
        }

        public UserDataSet.UserShopCounterDataTable GetDataByUserId(int userId)
        {
            var userMaterialDataTable = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userMaterialTableAdapter = new UserShopCounterTableAdapter())
                {
                    return userMaterialTableAdapter.GetDataByUserId(userId);
                }
            });
            return userMaterialDataTable;
        }

        public void UpdateOrAddUserShopCounter(int userId, int shopItemId, int buyNum, DateTime now)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userMaterialTableAdapter = new UserShopCounterTableAdapter())
                {
                    userMaterialTableAdapter.UpdateOrAddUserShopCounter(userId, shopItemId, buyNum, now);
                }
            });
        }
    }
}