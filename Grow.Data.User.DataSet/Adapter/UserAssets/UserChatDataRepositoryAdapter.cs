﻿using System;
using System.Linq;
using Fango.Core.Data;
using Grow.Data.User.DataSet.UserDataSetTableAdapters;

namespace Grow.Data.User.DataSet.Adapter.UserAssets
{
    public class UserChatDataRepositoryAdapter
    {
        public UserDataSet.UserChatRow GetDataByPK(int userId)
        {
            var userMaterialRow = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userMaterialTableAdapter = new UserChatTableAdapter())
                    {
                        return userMaterialTableAdapter.GetDataByPK(userId).FirstOrDefault();
                    }
                });
            return userMaterialRow;
        }

        public int GetActiveUserCountByChatId(int chatId, DateTime now)
        {
            var userCount = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userMaterialTableAdapter = new UserChatTableAdapter())
                {
                    var halfHour = now.AddMinutes(-30);
                    return userMaterialTableAdapter.GetActiveUserCountByChatId(chatId, halfHour);
                }
            });
            return (int)userCount;
        }

        public void UpdateChatIdByUserId(int userId, int chatId, DateTime now)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userMaterialTableAdapter = new UserChatTableAdapter())
                {
                    userMaterialTableAdapter.UpdateChatIdByUserId(userId, chatId, now);
                }
            });
        }

        public void AddNew(int userId, int chatId, DateTime now)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userMaterialTableAdapter = new UserChatTableAdapter())
                {
                    return userMaterialTableAdapter.AddNew(userId, chatId, now);
                }
            });
        }
    }
}