﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fango.Core.Data;
using Grow.Data.User.DataSet.Properties;
using Grow.Data.User.DataSet.UserDataSetTableAdapters;
using Microsoft.ApplicationBlocks.Data;

namespace Grow.Data.User.DataSet.Adapter.UserCharacter
{
    public class UserCharacterDataRepositoryAdapter
    {
        /*
        /// <summary>
        /// 全取得
        /// </summary>
        /// <returns></returns>
        public UserDataSet.UserCharacterRow[] GetData()
        {
            var userCharacterRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userCharacterTableAdapter = new UserCharacterTableAdapter())
                    {
                        return userCharacterTableAdapter.GetData().ToArray();
                    }
                });
            return userCharacterRows;
        }

        /// <summary>
        /// UserIdとUserCharacterIdで取得
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="userCharacterId"></param>
        /// <returns></returns>
        public UserDataSet.UserCharacterRow GetDataByPK(int userId, long userCharacterId)
        {
            var userCharacterRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userCharacterTableAdapter = new UserCharacterTableAdapter())
                    {
                        return userCharacterTableAdapter.GetDataByPK(userId, userCharacterId).FirstOrDefault();
                    }
                });
            return userCharacterRows;
        }

        /// <summary>
        /// UserIdで取得
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserDataSet.UserCharacterDataTable GetDataByUserId(int userId)
        {
            var userCharacterDataTable = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userCharacterTableAdapter = new UserCharacterTableAdapter())
                    {
                        return userCharacterTableAdapter.GetDataByUserId(userId);
                    }
                });
            return userCharacterDataTable;
        }

        /// <summary>
        /// 追加
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="userCharacterId"></param>
        /// <param name="characterId"></param>
        /// <param name="level"></param>
        /// <param name="exp"></param>
        /// <param name="skillLevel"></param>
        /// <param name="skillExp"></param>
        /// <param name="hp"></param>
        /// <param name="attack"></param>
        /// <param name="defence"></param>
        /// <param name="speed"></param>
        /// <param name="now"></param>
        public void AddNew(int userId, long userCharacterId, int characterId, int level, int exp, int skillLevel,
                           int skillExp,int hp, int attack, int defence, int speed, DateTimeOffset now)
        {
            new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userCharacterTableAdapter = new UserCharacterTableAdapter())
                    {
                        userCharacterTableAdapter.AddNew(userId, userCharacterId, characterId, level, exp, skillLevel,
                                                         skillExp, hp, attack, defence, speed, now, now);
                    }
                });
        }

        /// <summary>
        /// 削除
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="userCharacterId"></param>
        public void DeleteDataByPK(int userId, long userCharacterId)
        {
            new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userCharacterTableAdapter = new UserCharacterTableAdapter())
                    {
                        return userCharacterTableAdapter.DeleteDataByPK(userId, userCharacterId);
                    }
                });
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="userCharacterId"></param>
        /// <param name="afterSkillExp"></param>
        public void UpdateLevelUpByPK(int userId, long userCharacterId, int afterSkillExp, DateTime now)
        {
            new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userCharacterTableAdapter = new UserCharacterTableAdapter())
                    {
                        return userCharacterTableAdapter.UpdateSkillExpByPK(afterSkillExp, now, userId, userCharacterId);
                    }
                });
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="userCharacterId"></param>
        /// <param name="afterLevel"></param>
        /// <param name="afterExp"></param>
        /// <param name="addHp"></param>
        /// <param name="addAttack"></param>
        /// <param name="addDefense"></param>
        /// <param name="addSpeed"></param>
        public void UpdateLevelUpByPK(int userId, long userCharacterId, int afterLevel, int afterExp, int addHp,
                                      int addAttack, int addDefense, int addSpeed, DateTime now)
        {
            new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userCharacterTableAdapter = new UserCharacterTableAdapter())
                    {
                        return userCharacterTableAdapter.UpdateLevelUpByPK(afterLevel, afterExp, addHp, addAttack,
                                                                           addDefense, addSpeed, now, userId,
                                                                           userCharacterId);
                    }
                });
        }

        /// <summary>
        /// スキル合成
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="baseUserCharacterId"></param>
        /// <param name="afterSkillLevel"></param>
        /// <param name="afterSkillExp"></param>
        /// <param name="subUserCharacterList"></param>
        /// <param name="now"></param>
        /// <returns></returns>
        public int? MixSkill(int userId, long baseUserCharacterId, int afterSkillLevel, int afterSkillExp, List<long> subUserCharacterList, DateTimeOffset now)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userCharacterTableAdapter = new UserCharacterTableAdapter())
                    {
                        return userCharacterTableAdapter.MixSkill(userId, baseUserCharacterId, afterSkillLevel,
                                                                  afterSkillExp, subUserCharacterList, now);
                    }
                });
            return result;
        }

        /// <summary>
        /// BulkInsert
        /// </summary>
        /// <param name="userCharacterDataTable"></param>
        public void BulkInsert(UserDataSet.UserCharacterDataTable userCharacterDataTable)
        {
            var userConnStr = Settings.Default.edge_user_devConnectionString;
            new DbAccessHandler().ExecuteAction(() => SqlHelper.BulkInsert(userConnStr, userCharacterDataTable));
        }

        /// <summary>
        /// DataTableで一括更新
        /// </summary>
        /// <param name="userCharacterDataTable"></param>
        /// <returns></returns>
        public int UpdateDataByDataTable(UserDataSet.UserCharacterDataTable userCharacterDataTable)
        {
            var updateCount = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userCharacterTableAdapter = new UserCharacterTableAdapter())
                {
                    return userCharacterTableAdapter.Update(userCharacterDataTable);
                }
            });
            return updateCount;
        }
        */
    }
}