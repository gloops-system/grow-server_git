﻿using System;
using System.Linq;
using Fango.Core.Data;
using Grow.Data.User.DataSet.Properties;
using Grow.Data.User.DataSet.UserDataSetTableAdapters;
using Microsoft.ApplicationBlocks.Data;

namespace Grow.Data.User.DataSet.Adapter.UserCharacter
{
    public class UserBookDataRepositoryAdapter
    {
        /// <summary>
        /// 全取得
        /// </summary>
        /// <returns></returns>
        public UserDataSet.UserBookRow[] GetData()
        {
            var userBookRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userBookTableAdapter = new UserBookTableAdapter())
                    {
                        return userBookTableAdapter.GetData().ToArray();
                    }
                });
            return userBookRows;
        }

        /// <summary>
        /// PKで取得
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="characterId"></param>
        /// <returns></returns>
        public UserDataSet.UserBookRow GetDataByPK(int userId, int characterId)
        {
            var userBookRow = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userBookTableAdapter = new UserBookTableAdapter())
                    {
                        return userBookTableAdapter.GetDataByPK(userId, characterId).FirstOrDefault();
                    }
                });
            return userBookRow;
        }

        /// <summary>
        /// UserIdで取得
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserDataSet.UserBookRow[] GetDataByUserId(int userId)
        {
            var userBookRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userBookTableAdapter = new UserBookTableAdapter())
                    {
                        return userBookTableAdapter.GetDataByUserId(userId).ToArray();
                    }
                });
            return userBookRows;
        }

        /// <summary>
        /// 追加
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="characterId"></param>
        public void AddNew(int userId, int characterId, DateTime now)
        {
            new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userBookTableAdapter = new UserBookTableAdapter())
                    {
                        userBookTableAdapter.AddNew(userId, characterId, now, now, now);
                    }
                });
        }

        /// <summary>
        /// BulkInsert
        /// </summary>
        /// <param name="userBookDataTable"></param>
        public void BulkInsert(UserDataSet.UserBookDataTable userBookDataTable)
        {
            var userConnStr = Settings.Default.edge_user_devConnectionString;
            new DbAccessHandler().ExecuteAction(() => SqlHelper.BulkInsert(userConnStr, userBookDataTable));
        }
    }
}