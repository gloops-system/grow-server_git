﻿using System;
using System.Linq;
using Fango.Core.Data;
using Fango.Data.DataSet.Serialization;
using Fango.Redis.Caching;
using Grow.Data.User.DataSet.UserDataSetTableAdapters;

namespace Grow.Data.User.DataSet.Adapter.UserBase
{
    public class UserBaseDataRepositoryAdapter
    {
        public static string GetCacheKey(int userId)
        {
            return string.Format("UserBase:{{UserId:{0}}}", userId);
        }

        private static readonly TimeSpan OneMinutes = TimeSpan.FromMinutes(1);

        /// <summary>
        /// 全取得
        /// </summary>
        /// <returns></returns>
        public UserDataSet.UserBaseRow[] GetData()
        {
            var userBaseRow = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userBaseTableAdapter = new UserBaseTableAdapter())
                {
                    return userBaseTableAdapter.GetData().ToArray();
                }
            });

            return userBaseRow;
        }

        /// <summary>
        /// PKで取得
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="useCache"></param>
        /// <returns></returns>
        public UserDataSet.UserBaseRow GetDataByPK(int userId, bool useCache = true)
        {
            UserDataSet.UserBaseRow userBaseRow = null;
            //TODO: Comment now for error when get data from cache
            //if (useCache)
            //    userBaseRow = GetByPKFromCache(userId);

            if (userBaseRow != null)
                return userBaseRow;

            userBaseRow = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userBaseTableAdapter = new UserBaseTableAdapter())
                    {
                        return userBaseTableAdapter.GetDataByPK(userId).FirstOrDefault();
                    }
                });

            PutToCache(userId, userBaseRow);

            return userBaseRow;
        }

        /// <summary>
        /// InviteCodeで取得
        /// </summary>
        /// <param name="inviteCode"></param>
        /// <returns></returns>
        public UserDataSet.UserBaseRow GetDataByInviteCode(string inviteCode)
        {
            var userBaseRow = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userBaseTableAdapter = new UserBaseTableAdapter())
                    {
                        return userBaseTableAdapter.GetDataByInviteCode(inviteCode).FirstOrDefault();
                    }
                });

            return userBaseRow;
        }

        /// <summary>
        /// キャッシュに登録
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="cacheItem"></param>
        private void PutToCache(int userId, UserDataSet.UserBaseRow cacheItem)
        {
            if (cacheItem == null) return;

            var redisCacheClient = new RedisCacheClient();
            var serializer = new DataRowJsonSerializer<UserDataSet.UserBaseDataTable, UserDataSet.UserBaseRow>();
            redisCacheClient.Add(GetCacheKey(userId), serializer.Serialize(cacheItem), OneMinutes);
        }

        /// <summary>
        /// キャッシュから取得
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private UserDataSet.UserBaseRow GetByPKFromCache(int userId)
        {
            var redisCacheClient = new RedisCacheClient();
            var userBaseRow = redisCacheClient.Get(GetCacheKey(userId));
            var serializer = new DataRowJsonSerializer<UserDataSet.UserBaseDataTable, UserDataSet.UserBaseRow>();
            return userBaseRow == null ? null : serializer.Deserialize(userBaseRow);
        }

        /// <summary>
        /// 追加
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="nickName"></param>
        /// <param name="tutorialStatus"></param>
        /// <param name="inviteCode"></param>
        /// <param name="useCache"></param>
        public void AddNew(int userId, string nickName, string facebookId, int tutorialStatus, string inviteCode, DateTimeOffset now, bool useCache = true)
        {
            new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userBaseTableAdapter = new UserBaseTableAdapter())
                    {
                        userBaseTableAdapter.AddNew(userId, nickName, facebookId, tutorialStatus, inviteCode, now, now);
                    }
                });

            if (useCache)
                ResetUserBaseCache(userId);
        }

        /// <summary>
        /// キャッシュを削除
        /// </summary>
        /// <param name="userId"></param>
        private static void ResetUserBaseCache(int userId)
        {
            var redisCacheClient = new RedisCacheClient();
            redisCacheClient.Remove(GetCacheKey(userId));
        }
    }
}