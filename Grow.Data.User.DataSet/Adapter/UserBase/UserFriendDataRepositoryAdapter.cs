﻿using System;
using System.Linq;
using Fango.Core.Data;
using Grow.Data.User.DataSet.UserDataSetTableAdapters;

namespace Grow.Data.User.DataSet.Adapter.UserBase
{
    public class UserFriendDataRepositoryAdapter
    {
        /// <summary>
        /// 全取得
        /// </summary>
        /// <returns></returns>
        public UserDataSet.UserFriendRow[] GetData()
        {
            var userFriendRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userFriendTableAdapter = new UserFriendTableAdapter())
                    {
                        return userFriendTableAdapter.GetData().ToArray();
                    }
                });
            return userFriendRows;
        }

        /// <summary>
        /// PKで取得
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="targetUserId"></param>
        /// <returns></returns>
        public UserDataSet.UserFriendRow GetDataByPK(int userId, int targetUserId)
        {
            var userFriendRow = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userFriendTableAdapter = new UserFriendTableAdapter())
                    {
                        return userFriendTableAdapter.GetDataByPK(userId, targetUserId).FirstOrDefault();
                    }
                });
            return userFriendRow;
        }

        /// <summary>
        /// UserIdで取得
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserDataSet.UserFriendRow[] GetDataByUserId(int userId)
        {
            var userFriendRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userFriendTableAdapter = new UserFriendTableAdapter())
                    {
                        return userFriendTableAdapter.GetDataByUserId(userId).ToArray();
                    }
                });
            return userFriendRows;
        }

        /// <summary>
        /// TargetUserId
        /// </summary>
        /// <param name="targetUserId"></param>
        /// <returns></returns>
        public UserDataSet.UserFriendRow[] GetDataByTargetUserId(int targetUserId)
        {
            var userFriendRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userFriendTableAdapter = new UserFriendTableAdapter())
                    {
                        return userFriendTableAdapter.GetDataByTargetUserId(targetUserId).ToArray();
                    }
                });
            return userFriendRows;
        }

        /// <summary>
        /// 非フレンドを取得
        /// </summary>
        /// <param name="top"></param>
        /// <param name="userId"></param>
        /// <param name="levelTo"></param>
        /// <param name="adjustDay"></param>
        /// <param name="adjustNum"></param>
        /// <returns></returns>
        public UserDataSet.GetNonFriendListRow[] GetNonFriendList(int top, int userId, int levelTo,
                                                                  int adjustDay, int adjustNum)
        {
            var nonFriendListRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var getNonFriendListTableAdapter = new GetNonFriendListTableAdapter())
                    {
                        return getNonFriendListTableAdapter.GetData(top, userId, levelTo, adjustDay, adjustNum).ToArray();
                    }
                });
            return nonFriendListRows;
        }

        /// <summary>
        /// 追加
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="targetUserId"></param>
        /// <param name="status"></param>
        /// <param name="now"></param>
        /// <param name="isApproval"></param>
        /// <returns></returns>
        public int AddNew(int userId, int targetUserId, int status, DateTimeOffset now, bool isApproval = false)
        {
            var insertCount = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userFriendTableAdapter = new UserFriendTableAdapter())
                    {
                        var acceptDate = isApproval ? (DateTimeOffset?)now : null;
                        return userFriendTableAdapter.AddNew(userId, targetUserId, status, now, acceptDate, now, now);
                    }
                });
            return insertCount;
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="targetUserId"></param>
        /// <param name="status"></param>
        /// <param name="now"></param>
        /// <returns></returns>
        public int UpdateStatusByPK(int userId, int targetUserId, int status, DateTimeOffset now)
        {
            var updateCount = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userFriendTableAdapter = new UserFriendTableAdapter())
                    {
                        return userFriendTableAdapter.UpdateStatusByPK(status, now, now, userId, targetUserId);
                    }
                });
            return updateCount;
        }

        /// <summary>
        /// 削除
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="targetUserId"></param>
        public void DeleteByPK(int userId, int targetUserId)
        {
            new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userFriendTableAdapter = new UserFriendTableAdapter())
                    {
                        userFriendTableAdapter.DeleteByPK(userId, targetUserId);
                    }
                });
        }
    }
}
