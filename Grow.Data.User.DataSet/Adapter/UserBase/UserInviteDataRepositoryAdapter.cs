﻿using System;
using System.Linq;
using Fango.Core.Data;
using Grow.Data.User.DataSet.UserDataSetTableAdapters;

namespace Grow.Data.User.DataSet.Adapter.UserBase
{
    public class UserInviteDataRepositoryAdapter
    {
        /// <summary>
        /// 全取得
        /// </summary>
        /// <returns></returns>
        public UserDataSet.UserInviteRow[] GetData()
        {
            var userInviteRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userInviteTableAdapter = new UserInviteTableAdapter())
                    {
                        return userInviteTableAdapter.GetData().ToArray();
                    }
                });
            return userInviteRows;
        }

        /// <summary>
        /// PKで取得
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="inputUserId"></param>
        /// <returns></returns>
        public UserDataSet.UserInviteRow GetDataByPK(int userId, int inputUserId)
        {
            var userInviteRow = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userInviteTableAdapter = new UserInviteTableAdapter())
                    {
                        return userInviteTableAdapter.GetDataByPK(userId, inputUserId).FirstOrDefault();
                    }
                });
            return userInviteRow;
        }

        /// <summary>
        /// UserIdで取得
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserDataSet.UserInviteRow[] GetDataByUserId(int userId)
        {
            var userInviteRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userInviteTableAdapter = new UserInviteTableAdapter())
                    {
                        return userInviteTableAdapter.GetDataByUserId(userId).ToArray();
                    }
                });
            return userInviteRows;
        }

        /// <summary>
        /// 追加
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="targetUserId"></param>
        /// <param name="inviteId"></param>
        /// <returns></returns>
        public int AddNew(int userId, int targetUserId, int inviteId, DateTime now)
        {
            var insertCount = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userInviteTableAdapter = new UserInviteTableAdapter())
                    {
                        return userInviteTableAdapter.AddNew(userId, targetUserId, inviteId, now, now);
                    }
                });
            return insertCount;
        }
    }
}