﻿using System;
using System.Linq;
using Fango.Core.Data;
using Grow.Data.User.DataSet.UserDataSetTableAdapters;

namespace Grow.Data.User.DataSet.Adapter.UserBase
{
    public class UserStatusDataRepositoryAdapter
    {
        /// <summary>
        /// PKで取得
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserDataSet.UserStatusRow GetDataByPK(int userId)
        {
            var userStatusRow = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userStatusTableAdapter = new UserStatusTableAdapter())
                    {
                        return userStatusTableAdapter.GetDataByPK(userId).FirstOrDefault();
                    }
                });
            return userStatusRow;
        }

        /// <summary>
        /// 追加
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="level"></param>
        /// <param name="exp"></param>
        /// <param name="dp"></param>
        /// <param name="maxDp"></param>
        /// <param name="gil"></param>
        /// <param name="maxCharacterNum"></param>
        /// <param name="maxCostPoint"></param>
        /// <param name="maxItemNum"></param>
        /// <param name="maxItemEquipNum"></param>
        /// <param name="maxFriendNum"></param>
        /// <param name="deckSummary"></param>
        /// <param name="partyId"></param>
        /// <param name="comment"></param>
        /// <param name="now"></param>
        public void AddNew(int userId, short level, int exp, long userAvatarId, long userGearId, int gold, int life, int stamina, int hp,
                        int sp, int attack, int defense, int cutPercentage, int criticalPercentage, int stunValue, int stunResistance,
                        int stunRecoveryStand, int stunRecoveryDown, int scoreUp, int lastQuestAreaId, int lastLabyrinthAreaId,
                        short maxGearBoxNum, short maxFriendsNum, short maxMonsterBoxNum, short maxItemBoxNum, short maxGearBagNum, 
                        short maxUsableMonstersNum, short maxItemBagNum, DateTimeOffset now)
        {
            new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userStatusTableAdapter = new UserStatusTableAdapter())
                    {
                        userStatusTableAdapter.AddNew(userId, level, exp, userAvatarId, userGearId, gold, life, now, life, stamina, now, stamina, hp,
                                                        sp, attack, defense, cutPercentage, criticalPercentage, stunValue, stunResistance,
                                                        stunRecoveryStand, stunRecoveryDown, scoreUp, lastQuestAreaId, lastLabyrinthAreaId,
                                                        maxGearBoxNum, maxFriendsNum, maxMonsterBoxNum, maxItemBoxNum, maxGearBagNum, maxUsableMonstersNum, maxItemBagNum,
                                                        now, now);
                    }
                });
        }


        /// <summary>
        /// ユーザーアップデートの基底メソッド
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="level"></param>
        /// <param name="exp"></param>
        /// <param name="maxDp"></param>
        /// <param name="gil"></param>
        /// <param name="maxCharacterNum"></param>
        /// <param name="maxCostPoint"></param>
        /// <param name="maxItemNum"></param>
        /// <param name="maxItemEquipNum"></param>
        /// <param name="maxFriendNum"></param>
        /// <param name="deckSummary"></param>
        /// <param name="partyId"></param>
        /// <param name="comment"></param>
        /// <param name="now"></param>
        public void UpdateByPK(int userId, short level, int exp, long userAvatarId, long userGearId, int gold, int hp, int sp,
                                int attack, int defense, int cutPercentage, int criticalPercentage, int stunValue, int stunResistance,
                                int stunRecoveryStand, int stunRecoveryDown, int scoreUp, int lastQuestAreaId, int lastLabyrinthAreaId,
                                short maxGearBoxNum, short maxFriendsNum, short maxMonsterBoxNum, short maxItemBoxNum, short maxGearBagNum,
                                short maxUsableMonstersNum, short maxItemBagNum, DateTimeOffset now)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userStatusTableAdapter = new UserStatusTableAdapter())
                {
                    userStatusTableAdapter.UpdateByPK(level, exp, userAvatarId, userGearId, gold, hp, sp, attack, defense, cutPercentage, criticalPercentage,
                                                      stunValue, stunResistance, stunRecoveryStand, stunRecoveryDown, scoreUp, maxGearBoxNum, maxFriendsNum, maxMonsterBoxNum, 
                                                      maxItemBoxNum, maxGearBagNum, maxUsableMonstersNum, maxItemBagNum, now, lastQuestAreaId, lastLabyrinthAreaId, userId);                                                      
                }
            });
        }

        public void UpdateAvatarBonus(int userId, short hpBonus, short spBonus, short attackBonus, short defenseBonus, short scoreUpBonus, DateTimeOffset now)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userStatusTableAdapter = new UserStatusTableAdapter())
                {
                    userStatusTableAdapter.UpdateAvatarBonus(hpBonus, spBonus, attackBonus, defenseBonus, scoreUpBonus, now, userId);
                }
            });
        }

        /// <summary>
        /// 経験値の更新
        /// </summary>
        /// <param name="addExp"></param>
        /// <param name="userId"></param>
        public void UpdateExpByPK(int addExp, int userId, DateTime now)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userStatusTableAdapter = new UserStatusTableAdapter())
                {
                    userStatusTableAdapter.UpdateExpByPK(addExp,now, userId);
                }
            });
        }

        /// <summary>
        /// Gilの更新
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="gil"></param>
        public void IncrementGoldByPK(int userId, int gold, DateTimeOffset now)
        {
            new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userStatusTableAdapter = new UserStatusTableAdapter())
                    {
                        userStatusTableAdapter.IncrementGoldByPK(gold, now, userId);
                    }
                });
        }

        public void DecrementGoldByPK(int userId, int gold, DateTimeOffset now)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userStatusTableAdapter = new UserStatusTableAdapter())
                {
                    userStatusTableAdapter.DecrementGoldByPK(gold, now, userId);
                }
            });
        }

        public void UpdateGoldByPK(int userId, int gold, DateTimeOffset now)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userStatusTableAdapter = new UserStatusTableAdapter())
                {
                    userStatusTableAdapter.UpdateGoldByPK(gold, now, userId);
                }
            });
        }

        public void UpdateHealDataByPk(int userId, int addLifeHealPoint, DateTimeOffset lifeLastHealTime, int addStaminaHealPoint, DateTimeOffset staminaLastHealTime, DateTimeOffset now)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userStatusTableAdapter = new UserStatusTableAdapter())
                {
                    userStatusTableAdapter.UpdateHealDataByPK(userId, addLifeHealPoint, lifeLastHealTime,
                                                              addStaminaHealPoint, staminaLastHealTime, now);
                }
            });
        }

        public void UpdateLifeByPK(int userId, int addLife)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userStatusTableAdapter = new UserStatusTableAdapter())
                {
                    userStatusTableAdapter.UpdateLifeByPK(addLife, userId);
                }
            });
        }

        public void UpdateStaminaByPK(int userId, int addStamina)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userStatusTableAdapter = new UserStatusTableAdapter())
                {
                    userStatusTableAdapter.UpdateStaminaByPK(addStamina, userId);
                }
            });
        }

        public void RecoveryStaminaByPK(int userId, int stamina, DateTimeOffset now)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userStatusTableAdapter = new UserStatusTableAdapter())
                {
                    userStatusTableAdapter.RecoveryStamina(stamina, now, userId);
                }
            });
        }
    }
}