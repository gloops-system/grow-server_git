﻿using System;
using System.Linq;
using Fango.Core.Data;
using Grow.Data.User.DataSet.UserDataSetTableAdapters;

namespace Grow.Data.User.DataSet.Adapter.UserBase
{
    public class UserLoginDataRepositoryAdapter
    {
        /// <summary>
        /// 追加
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="uniqueId"></param>
        /// <param name="passwordSha1"></param>
        /// <param name="uuid"></param>
        /// <param name="secret"></param>
        /// <param name="isOnline"></param>
        /// <param name="lastLogin"></param>
        /// <param name="device"></param>
        /// <param name="status"></param>
        /// <param name="now"></param>
        /// <returns></returns>
        public void AddNew(int userId, int uniqueId, byte[] passwordSha1, string uuid, string secret, bool isOnline,
                          DateTimeOffset lastLogin, int device, int status, DateTimeOffset now)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userLoginTableAdapter = new UserLoginTableAdapter())
                {
                    return userLoginTableAdapter.AddNew(userId, uniqueId, passwordSha1, uuid, secret, isOnline,
                                                        lastLogin, device, status, now, now);
                }
            });
        }

        /// <summary>
        /// PKで取得
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserDataSet.UserLoginRow GetDataByPK(int userId)
        {
            var userLoginRow = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userLoginTableAdapter = new UserLoginTableAdapter())
                    {
                        return userLoginTableAdapter.GetDataByPK(userId).FirstOrDefault();
                    }
                });
            return userLoginRow;
        }

        /// <summary>
        /// UniqueIdで取得
        /// </summary>
        /// <param name="uniqueId"></param>
        /// <returns></returns>
        public UserDataSet.UserLoginRow[] GetDataByUniqueId(int uniqueId)
        {
            var userLoginRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userLoginTableAdapter = new UserLoginTableAdapter())
                    {
                        return userLoginTableAdapter.GetDataByUniqueId(uniqueId).ToArray();
                    }
                });
            return userLoginRows;
        }

        /// <summary>
        /// PasswordとStatusの更新
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="passwordSha1"></param>
        /// <param name="status"></param>
        public void UpdatePasswordAndStatus(int userId, byte[] passwordSha1, int status)
        {
            new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userLoginTableAdapter = new UserLoginTableAdapter())
                    {
                        userLoginTableAdapter.UpdatePasswordAndStatus(passwordSha1, status, userId);
                    }
                });
        }

        public int CheckInGameId(int inGameId)
        {
            int? num = 0;
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var tableAdapter = new UserLoginTableAdapter())
                {
                    num++;
                }
            });

            return (int)num;
        }
    }
}