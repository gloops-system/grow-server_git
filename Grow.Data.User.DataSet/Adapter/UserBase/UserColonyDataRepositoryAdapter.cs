﻿using System;
using System.Linq;
using Fango.Core.Data;
using Grow.Data.User.DataSet.UserDataSetTableAdapters;
using System.Collections.Generic;

namespace Grow.Data.User.DataSet.Adapter.UserBase
{
    public class UserColonyDataRepositoryAdapter
    {
        /// <summary>
        /// PKで取得
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserDataSet.UserColonyRow GetDataByPK(int userId)
        {
            var userStatusRow = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userColonyTableAdapter = new UserColonyTableAdapter())
                    {
                        return userColonyTableAdapter.GetDataByPK(userId).FirstOrDefault();
                    }
                });

            return userStatusRow;
        }

        /// <summary>
        /// Get colony data
        /// </summary>
        /// <param name="userIds">List of excluded target users</param>
        /// <returns></returns>
        public List<UserDataSet.UserColonyRow> GetDataExcludeFromList(int offLineTimeInMinutes, List<int> userIds)
        {
            var userColonyRow = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userColonyTableAdapter = new UserColonyTableAdapter())
                {
                    var stringUserIds = string.Join(",", userIds.ToArray());
                    var result = userColonyTableAdapter.GetDataExcludeFromList(offLineTimeInMinutes, stringUserIds);
                    return result.ToList();
                }
            });

            return userColonyRow;
        }

        /// <summary>
        /// 追加
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="level"></param>
        /// <param name="gold"></param>
        /// <param name="plasma"></param>
        /// <param name="gem"></param>
        /// <param name="now"></param>
        public void AddNew(int userId, short level, int gold, int plasma, DateTimeOffset now)
        {
            new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userColonyTableAdapter = new UserColonyTableAdapter())
                    {
                        userColonyTableAdapter.AddNew(userId, level, gold, plasma, now, now, now, "", 0);
                    }
                });
        }


        /// <summary>
        /// ユーザーアップデートの基底メソッド
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="level"></param>
        /// <param name="gold"></param>
        /// <param name="plasma"></param>
        /// <param name="gem"></param>
        /// <param name="colonyData"></param>
        /// <param name="colonyHistoryIndex"></param>
        /// <param name="now"></param>
        public void UpdateByPK(int userId, short level, int gold, int plasma, string colonyData, long colonyHistoryIndex, DateTimeOffset now)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userColonyTableAdapter = new UserColonyTableAdapter())
                {
                    userColonyTableAdapter.UpdateByPK(userId, level, gold, plasma, colonyData, colonyHistoryIndex, now, now);
                }
            });
        }
    }
}