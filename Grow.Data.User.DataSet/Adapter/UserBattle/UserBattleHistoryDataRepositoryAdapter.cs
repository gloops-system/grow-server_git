﻿using System;
using System.Linq;
using Fango.Core.Data;
using System.Collections.Generic;
using Grow.Data.User.DataSet.BattleHistoryTableAdapters;

namespace Grow.Data.User.DataSet.Adapter.UserBattle
{
    public class UserBattleHistoryDataRepositoryAdapter
    {
        /// <summary>
        /// Get battle history by id
        /// </summary>
        /// <param name="battleHistoryId"></param>
        /// <returns></returns>
        public BattleHistory.UserBattleHistoryRow GetDataByPK(int battleHistoryId)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userBattleHistoryTableAdapter = new UserBattleHistoryTableAdapter())
                {
                    return userBattleHistoryTableAdapter.GetDataByPK(battleHistoryId).FirstOrDefault();
                }
            });

            return result;
        }

        /// <summary>
        /// Add new battle
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="userGold"></param>
        /// <param name="userPlasma"></param>
        /// <param name="targetUserId"></param>
        /// <param name="targetGold"></param>
        /// <param name="targetPlasma"></param>
        /// <param name="battleStatus"></param>
        /// <param name="battleResult"></param>
        /// <param name="now"></param>
        /// <returns></returns>
        public int AddNew(int userId, int userGold, int userPlasma, int targetUserId, int targetGold, int targetPlasma, byte battleStatus, byte battleResult, DateTimeOffset now)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var userBattleHistoryTableAdapter = new UserBattleHistoryTableAdapter())
                    {
                        return userBattleHistoryTableAdapter.AddNew(userId, 0, 0, targetUserId, targetGold, targetPlasma, 0, 0, battleStatus, battleResult, "", now, now);
                    }
                });

            return Convert.ToInt32(result);
        }

        /// <summary>
        /// Update battle result
        /// </summary>
        /// <param name="battleHistoryId"></param>
        /// <param name="userId"></param>
        /// <param name="targetUserId"></param>
        /// <param name="targetLootedGold"></param>
        /// <param name="targetLootedPlasma"></param>
        /// <param name="battleStatus"></param>
        /// <param name="battleResult"></param>
        /// <param name="battleReplayData"></param>
        /// <param name="now"></param>
        public void UpdateBattle(int battleHistoryId, int userId, int targetUserId, int targetLootedGold, int targetLootedPlasma, byte battleStatus, byte battleResult, string battleReplayData, DateTimeOffset now)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userBattleHistoryTableAdapter = new UserBattleHistoryTableAdapter())
                {
                    return userBattleHistoryTableAdapter.UpdateBattle(targetLootedGold, targetLootedPlasma, battleStatus, battleResult, battleReplayData, now, battleHistoryId, userId, targetUserId);
                }
            });
        }

    }
}