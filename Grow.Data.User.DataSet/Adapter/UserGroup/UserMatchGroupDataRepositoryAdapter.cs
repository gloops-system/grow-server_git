﻿using Fango.Core.Util;
using Fango.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grow.Data.User.DataSet.UserLabyrinthDataSetTableAdapters;

namespace Grow.Data.User.DataSet.Adapter.UserGroup
{
    public class UserMatchGroupDataRepositoryAdapter 
    {

        public UserLabyrinthDataSet.UserMatchGroupRow GetUserGroupByUserId(int userId)
        {
            var dataRow = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new UserMatchGroupTableAdapter())
                {
                    return adapter.GetDataByPK(userId).FirstOrDefault();
                }
            });
            return dataRow;
        }

        public int IncreaseUserPoint(int userId, int deltaPoint)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new UserMatchGroupTableAdapter())
                {
                    var now = DateUtil.GetDateTimeOffsetNow();
                    return adapter.IncreaseUserPoint(deltaPoint, now, userId);
                }
            });
            return result;
        }

        public void UpdateUserPoint(int userId, int point)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new UserMatchGroupTableAdapter())
                {
                    var now = DateUtil.GetDateTimeOffsetNow();
                    adapter.UpdateUserPoint(point, now, userId);
                }
            });
        }

        public void AddNew(int userId, int matchGroupId)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new UserMatchGroupTableAdapter())
                {
                    adapter.AddNew(userId, matchGroupId, 0);

                }
            });
        }
    }
}
