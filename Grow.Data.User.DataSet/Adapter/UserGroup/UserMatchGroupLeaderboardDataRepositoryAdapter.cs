﻿using Grow.Data.User.DataSet.UserLabyrinthDataSetTableAdapters;
using Fango.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Data.User.DataSet.Adapter.UserGroup
{
    public class UserMatchGroupLeaderboardDataRepositoryAdapter 
    {
        public UserLabyrinthDataSet.UserMatchGroupLeaderboardRow[] GetGlobalLeaderboard(int number)
        {
            var dataTable = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new UserMatchGroupLeaderboardTableAdapter())
                {
                    return adapter.GetGlobalLeaderboard(number).ToArray();
                }
            });
            return dataTable;
        }

        public UserLabyrinthDataSet.UserMatchGroupLeaderboardRow[] GetRivalsLeaderboard(int UserId)
        {
            var dataTable = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new UserMatchGroupLeaderboardTableAdapter())
                {
                    return adapter.GetRivalsLeaderboard(UserId).ToArray();
                }
            });
            return dataTable;
        }

    }
}
