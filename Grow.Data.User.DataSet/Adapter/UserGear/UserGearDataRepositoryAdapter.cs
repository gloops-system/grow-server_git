﻿using Grow.Data.User.DataSet.Properties;
using Grow.Data.User.DataSet.UserDataSetTableAdapters;
using Fango.Core.Data;
using Fango.Core.Util;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Data.User.DataSet.Adapter.UserGear
{
    public class UserGearDataRepositoryAdapter 
    {
        public UserDataSet.UserGearRow[] GetDataByUserId(int userId)
        {
            var dataTable = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new UserGearTableAdapter())
                {
                    return adapter.GetDataByUserId(userId).ToArray();
                }
            });
            return dataTable;
        }

        public long AddNew(int userId, int gearId, short gearType, short category, short property, short rarity, int attack,
                           int defense, int cutPercentage, int stun, int point)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var userGearTableAdapter = new UserGearTableAdapter())
                {
                    var now = DateUtil.GetDateTimeOffsetNow();
                    return userGearTableAdapter.AddNew(userId, gearId, gearType, category, property, rarity, attack,
                                                       defense, cutPercentage, stun, point, now, now);

                }
            });
            return Convert.ToInt64(result);
        }

        public void BulkInsert(UserDataSet.UserGearDataTable userGearDataTable)
        {
            var userConnStr = Settings.Default.edge_user_devConnectionString;
            new DbAccessHandler().ExecuteAction(() => SqlHelper.BulkInsert(userConnStr, userGearDataTable));
        }
    }
}
