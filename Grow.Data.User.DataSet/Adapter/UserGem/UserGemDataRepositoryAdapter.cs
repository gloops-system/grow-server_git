﻿using System.Linq;
using Fango.Core.Data;
using Fango.Core.Util;
using Grow.Data.User.DataSet.UserDataSetTableAdapters;
using System;

namespace Grow.Data.User.DataSet.Adapter.UserGem
{
    public class UserGemDataRepositoryAdapter
    {
        public UserDataSet.UserGemRow FindUserGem(int userId)
        {
            var userCoin = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var UserGemTableAdapter = new UserGemTableAdapter())
                {
                    return UserGemTableAdapter.GetDataByPK(userId).FirstOrDefault();
                }
            }
            );

            return userCoin;
        }

        public int AddNew(int userId, int coin)
        {
            var insertCount = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var UserGemTableAdapter = new UserGemTableAdapter())
                {
                    var now = DateUtil.GetDateTimeOffsetNow();
                    return UserGemTableAdapter.AddNew(userId, coin, now, now);
                }
            }
            );

            return Convert.ToInt32(insertCount); ;
        }

        public int GetGemSum()
        {
            var sum = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var UserGemTableAdapter = new UserGemTableAdapter())
                {
                    return UserGemTableAdapter.GetGemSum();
                }
            });

            return (sum != null) ? Convert.ToInt32(sum) : 0;
        }

        public int IncrementUserGem(int userId, int coin)
        {
            var updateCount = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var UserGemTableAdapter = new UserGemTableAdapter())
                {
                    return UserGemTableAdapter.IncrementUserGem(coin, DateUtil.GetDateTimeOffsetNow(), userId);
                }
            }
            );

            return updateCount;
        }

        public int DecrementUserGem(int userId, int coin)
        {
            var updateCount = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var UserGemTableAdapter = new UserGemTableAdapter())
                {
                    return UserGemTableAdapter.DecrementUserGem(coin, DateUtil.GetDateTimeOffsetNow(), userId);
                }
            }
            );

            return updateCount;
        }
    }
}
