﻿using Fango.Core.Util;
using Fango.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grow.Data.User.DataSet.UserLabyrinthDataSetTableAdapters;
using Grow.Data.User.DataSet.Properties;
using System.Data.SqlClient;
using System.Data;

namespace Grow.Data.User.DataSet.Adapter.MatchGroup
{
    public class MatchGroupDataRepositoryAdapter 
    {
        public List<UserLabyrinthDataSet.MatchGroupRow> GetListGroupPaging(int pageSize, long lastGroupId)
        {
            var dataRows = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new MatchGroupTableAdapter())
                {
                    return adapter.GetDataPaging(pageSize, lastGroupId).ToList<UserLabyrinthDataSet.MatchGroupRow>();
                }
            });
            return dataRows;
        }

        public UserLabyrinthDataSet.MatchGroupRow GetLastestGroup()
        {
            var dataRow = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new MatchGroupTableAdapter())
                {
                    return adapter.GetLastestGroup().FirstOrDefault();
                }
            });
            return dataRow;
        }

        public long AddNew(int userNum, int averageStrength)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new MatchGroupTableAdapter())
                {
                    return adapter.AddNew(userNum, averageStrength);

                }
            });
            return Convert.ToInt64(result);
        }

        public void UpdateUserNum(long matchGroupId, int userNum)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new MatchGroupTableAdapter())
                {
                    adapter.UpdateUserNum(userNum, matchGroupId);

                }
            });            
        }

        public void CalculateUserStrength()
        {
            var userConnStr = Settings.Default.edge_user_devConnectionString;
            using (var con = new SqlConnection(userConnStr))
            {
                con.Open();
                var cmd = new SqlCommand("CalculateUsersStrength", con)
                {
                    CommandTimeout = 600,
                    CommandType = CommandType.StoredProcedure,
                };
                cmd.ExecuteNonQuery();
            }
        }

        public void DoMatchingGroup()
        {
            var userConnStr = Settings.Default.edge_user_devConnectionString;
            using (var con = new SqlConnection(userConnStr))
            {
                con.Open();
                var cmd = new SqlCommand("RunMatchingGroup", con)
                {
                    CommandTimeout = 600,
                    CommandType = CommandType.StoredProcedure,
                };
                cmd.ExecuteNonQuery();
            }
        }
    }
}
