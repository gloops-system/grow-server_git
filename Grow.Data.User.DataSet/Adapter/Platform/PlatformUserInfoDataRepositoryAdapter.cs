﻿using System;
using System.Linq;
using Fango.Core.Data;
using Grow.Data.User.DataSet.PlatformDataSetTableAdapters;

namespace Grow.Data.User.DataSet.Adapter.Platform
{
    public class PlatformUserInfoDataRepositoryAdapter
    {
        // TODO Inazuma.Core.Caching.CacheKey とどちらで管理するか要検討
        public static readonly TimeSpan CacheTimeout = TimeSpan.FromMinutes(1);

        public static string GetCacheKey(int userId)
        {
            return string.Format("PlatformUserInfo:{{UserId:{0}}}", userId);
        }

        public PlatformDataSet.PlatformUserInfoRow[] GetData()
        {
            var platformUserInfoRow = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var platformUserInfoTableAdapter = new PlatformUserInfoTableAdapter())
                {
                    return platformUserInfoTableAdapter.GetData().ToArray();
                }
            });
            return platformUserInfoRow;
        }


        public PlatformDataSet.PlatformUserInfoRow GetDataByPK(int userId, bool useCache = true)
        {
            //Func<PlatformDataSet.PlatformUserInfoRow> getCacheFunction =
            //    useCache
            //    ? () => GetByPKFromCache(userId)
            //    : (Func<PlatformDataSet.PlatformUserInfoRow>)null
            //    ;

            //Action<PlatformDataSet.PlatformUserInfoRow> modifyCacheAction =
            //    useCache
            //    ? (cacheItem) => PutToCache(userId, cacheItem)
            //    : (Action<PlatformDataSet.PlatformUserInfoRow>)null
            //    ;

            var test = new PlatformUserInfoTableAdapter().GetDataByPK(userId).FirstOrDefault();

            var userBaseRow = new DbAccessHandler().ExecuteAction(
                dataAccessFunction: () =>
                {
                    using (var platformUserInfoTableAdapter = new PlatformUserInfoTableAdapter())
                    {
                        return platformUserInfoTableAdapter.GetDataByPK(userId).FirstOrDefault();
                    }
                }
                );

            return userBaseRow;
        }

        //private PlatformDataSet.PlatformUserInfoRow GetByPKFromCache(int userId)
        //{
        //    using (var sharedCacheClient = CacheClientFactory.CreateSharedCacheClient())
        //    {
        //        var platformUserInfoJson = sharedCacheClient.Get<byte[]>(GetCacheKey(userId));

        //        if (platformUserInfoJson == null)
        //        {
        //            return null;
        //        }

        //        var serializer = new DataRowJsonSerializer<PlatformDataSet.PlatformUserInfoDataTable, PlatformDataSet.PlatformUserInfoRow>();

        //        var platformUserInfo = serializer.Deserialize(platformUserInfoJson);

        //        return platformUserInfo;
        //    }
        //}

        //private void PutToCache(int userId, PlatformDataSet.PlatformUserInfoRow cacheItem)
        //{
        //    if (cacheItem == null) return;

        //    using (var sharedCacheClient = CacheClientFactory.CreateSharedCacheClient())
        //    {
        //        var serializer = new DataRowJsonSerializer<PlatformDataSet.PlatformUserInfoDataTable, PlatformDataSet.PlatformUserInfoRow>();

        //        var userBase = serializer.Serialize(cacheItem);

        //        sharedCacheClient.Put(GetCacheKey(userId), userBase, CacheTimeout);
        //    }
        //}

        public void AddNew(int userId, string nickName, string profileUrl, string thumbnailUrl, string gender, DateTime birthday, string bloodType, bool isVerified,string prefecture, int? grade, DateTime now)
        {
            new DbAccessHandler().ExecuteAction(
                dataAccessAction: () =>
                {
                    using (var platformUserInfoTableAdapter = new PlatformUserInfoTableAdapter())
                    {
                        platformUserInfoTableAdapter.AddNew(userId,
                                                            nickName,
                                                            profileUrl,
                                                            thumbnailUrl,
                                                            gender,
                                                            birthday,
                                                            bloodType,
                                                            now,
                                                            now,
                                                            isVerified,
                                                            now,
                                                            prefecture,
                                                            grade,
                                                            now,
                                                            now);
                    }
                }
             );
        }

        public int UpdateByPK(int userId, string nickName, string profileUrl, string thumbnailUrl, string gender, DateTime birthday, string bloodType, bool isVerified, DateTimeOffset? isVerifiedDate, string prefecture, int? grade, DateTime now)
        {
            var updateCount = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var platformUserInfoTableAdapter = new PlatformUserInfoTableAdapter())
                {
                    return platformUserInfoTableAdapter.UpdateByPK(nickName,
                                                            profileUrl,
                                                            thumbnailUrl,
                                                            gender,
                                                            birthday,
                                                            bloodType,
                                                            now,
                                                            now,
                                                            isVerified,
                                                            isVerifiedDate,
                                                            prefecture,
                                                            grade,
                                                            now,
                                                            userId);
                }
            }
            );

            return updateCount;
        }
    }
}
