﻿using System;
using Grow.Data.User.DataSet.Adapter.UserGroup;
using Grow.Data.User.DataSet.Adapter.UserMonster;
using Grow.Data.User.DataSet.Adapter.MatchGroup;
using Grow.Data.User.DataSet.Adapter.UserAssets;


namespace Grow.Data.User.DataSet.Container
{
    public class UserLabyrinthDataContainer
    {
        private readonly Lazy<UserMatchGroupDataRepositoryAdapter> _userMatchGroup;
        public UserMatchGroupDataRepositoryAdapter UserMatchGroup { get { return _userMatchGroup.Value; } }

        private readonly Lazy<UserMatchGroupLeaderboardDataRepositoryAdapter> _userMatchGroupLeaderboard;
        public UserMatchGroupLeaderboardDataRepositoryAdapter UserMatchGroupLeaderboard { get { return _userMatchGroupLeaderboard.Value; } }

        private readonly Lazy<UserDispatchMonsterDataRepositoryAdapter> _userDispatchMonster;
        public UserDispatchMonsterDataRepositoryAdapter UserDispatchMonster { get { return _userDispatchMonster.Value; } }

        private readonly Lazy<UserMeetDispatchMonsterDataRepositoryAdapter> _userMeetDispatchMonster;
        public UserMeetDispatchMonsterDataRepositoryAdapter UserMeetDispatchMonster { get { return _userMeetDispatchMonster.Value; } }

        private readonly Lazy<MatchGroupDataRepositoryAdapter> _matchGroup;
        public MatchGroupDataRepositoryAdapter MatchGroup { get { return _matchGroup.Value; } }

        private readonly Lazy<UserEquipmentDataRepositoryAdapter> _userEquipment;
        public UserEquipmentDataRepositoryAdapter UserEquipment { get { return _userEquipment.Value; } }

        public UserLabyrinthDataContainer()
        {
            // User Group
            _userMatchGroup = new Lazy<UserMatchGroupDataRepositoryAdapter>(() => new UserMatchGroupDataRepositoryAdapter(), true);
            _userMatchGroupLeaderboard = new Lazy<UserMatchGroupLeaderboardDataRepositoryAdapter>(() => new UserMatchGroupLeaderboardDataRepositoryAdapter(), true);
            
            // User Monster Dispatch
            _userDispatchMonster = new Lazy<UserDispatchMonsterDataRepositoryAdapter>(() => new UserDispatchMonsterDataRepositoryAdapter(), true);
            _userMeetDispatchMonster = new Lazy<UserMeetDispatchMonsterDataRepositoryAdapter>(() => new UserMeetDispatchMonsterDataRepositoryAdapter(), true);

            _matchGroup = new Lazy<MatchGroupDataRepositoryAdapter>(() => new MatchGroupDataRepositoryAdapter(), true);

            _userEquipment = new Lazy<UserEquipmentDataRepositoryAdapter>(() => new UserEquipmentDataRepositoryAdapter(), true);
        }
    }
}