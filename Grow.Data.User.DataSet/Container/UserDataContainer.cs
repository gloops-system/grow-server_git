﻿using System;
using Grow.Data.User.DataSet.Adapter.UserAssets;
using Grow.Data.User.DataSet.Adapter.UserBase;
using Grow.Data.User.DataSet.Adapter.UserMonster;
using Grow.Data.User.DataSet.Adapter.UserAvatar;
using Grow.Data.User.DataSet.Adapter.UserGear;
using Grow.Data.User.DataSet.Adapter.UserGem;
using Grow.Data.User.DataSet.Adapter.UserBattle;


namespace Grow.Data.User.DataSet.Container
{
    public class UserDataContainer
    {
        private readonly Lazy<UserBaseDataRepositoryAdapter> _userBase;
        public UserBaseDataRepositoryAdapter UserBase { get { return _userBase.Value; } }

        private readonly Lazy<UserFriendDataRepositoryAdapter> _userFriend;
        public UserFriendDataRepositoryAdapter UserFriend { get { return _userFriend.Value; } }

        private readonly Lazy<UserLoginDataRepositoryAdapter> _userLogin;
        public UserLoginDataRepositoryAdapter UserLogin { get { return _userLogin.Value; } }

        private readonly Lazy<UserStatusDataRepositoryAdapter> _userStatus;
        public UserStatusDataRepositoryAdapter UserStatus { get { return _userStatus.Value; } }

        private readonly Lazy<UserColonyDataRepositoryAdapter> _userColony;
        public UserColonyDataRepositoryAdapter UserColony { get { return _userColony.Value; } }

        private readonly Lazy<UserNewsDataRepositoryAdapter> _userNews;
        public UserNewsDataRepositoryAdapter UserNews { get { return _userNews.Value; } }

        private readonly Lazy<UserAvatarDataRepositoryAdapter> _useAvatar;
        public UserAvatarDataRepositoryAdapter UserAvatar { get { return _useAvatar.Value; } }

        private readonly Lazy<UserMonsterDataRepositoryAdapter> _userMonster;
        public UserMonsterDataRepositoryAdapter UserMonster { get { return _userMonster.Value; } }

        private readonly Lazy<UserGearDataRepositoryAdapter> _userGear;
        public UserGearDataRepositoryAdapter UserGear { get { return _userGear.Value; } }

        private readonly Lazy<UserGemDataRepositoryAdapter> _userGem;
        public UserGemDataRepositoryAdapter UserGem { get { return _userGem.Value; } }

        private readonly Lazy<UserItemDataRepositoryAdapter> _userItem;
        public UserItemDataRepositoryAdapter UserItem { get { return _userItem.Value; } }

        private readonly Lazy<UserNotificationDataRepositoryAdapter> _userNotification;
        public UserNotificationDataRepositoryAdapter UserNotification { get { return _userNotification.Value; } }

        private readonly Lazy<UserGameCurrencyDataRepositoryAdapter> _userGameCurrency;
        public UserGameCurrencyDataRepositoryAdapter UserGameCurrency { get { return _userGameCurrency.Value; } }

        private readonly Lazy<UserBattleHistoryDataRepositoryAdapter> _userBattleHistory;
        public UserBattleHistoryDataRepositoryAdapter UserBattleHistory { get { return _userBattleHistory.Value; } }

        public UserDataContainer()
        {
            // UserBase
            _userBase = new Lazy<UserBaseDataRepositoryAdapter>(() => new UserBaseDataRepositoryAdapter(), true);
            _userFriend = new Lazy<UserFriendDataRepositoryAdapter>(() => new UserFriendDataRepositoryAdapter(), true);
            _userLogin = new Lazy<UserLoginDataRepositoryAdapter>(() => new UserLoginDataRepositoryAdapter(), true);
            _userStatus = new Lazy<UserStatusDataRepositoryAdapter>(() => new UserStatusDataRepositoryAdapter(), true);
            _userColony = new Lazy<UserColonyDataRepositoryAdapter>(() => new UserColonyDataRepositoryAdapter(), true);

            // UserAssets
            _userItem = new Lazy<UserItemDataRepositoryAdapter>(() => new UserItemDataRepositoryAdapter(), true);
            _userNotification = new Lazy<UserNotificationDataRepositoryAdapter>(() => new UserNotificationDataRepositoryAdapter(), true);
            _userNews = new Lazy<UserNewsDataRepositoryAdapter>(() => new UserNewsDataRepositoryAdapter(), true);
            _userGameCurrency = new Lazy<UserGameCurrencyDataRepositoryAdapter>(() => new UserGameCurrencyDataRepositoryAdapter(), true);

            // User Avatar
            _useAvatar = new Lazy<UserAvatarDataRepositoryAdapter>(() => new UserAvatarDataRepositoryAdapter(), true);

            // User Monster
            _userMonster = new Lazy<UserMonsterDataRepositoryAdapter>(() => new UserMonsterDataRepositoryAdapter(), true);

            // User Gear
            _userGear = new Lazy<UserGearDataRepositoryAdapter>(() => new UserGearDataRepositoryAdapter(), true);
            // User Gem
            _userGem = new Lazy<UserGemDataRepositoryAdapter>(() => new UserGemDataRepositoryAdapter(), true);

            // UserBattle
            _userBattleHistory = new Lazy<UserBattleHistoryDataRepositoryAdapter>(() => new UserBattleHistoryDataRepositoryAdapter(), true);
        }
    }
}