﻿using System;
using Grow.Data.User.DataSet.Adapter.Platform;

namespace Grow.Data.User.DataSet.Container
{
    public class PlatformDataContainer
    {
        private readonly Lazy<PlatformUserInfoDataRepositoryAdapter> _platformUserInfo;
        public PlatformUserInfoDataRepositoryAdapter PlatformUserInfo { get { return _platformUserInfo.Value; } }

        public PlatformDataContainer()
        {
            _platformUserInfo = new Lazy<PlatformUserInfoDataRepositoryAdapter>(() => new PlatformUserInfoDataRepositoryAdapter(), true);
        }
    }
}
