﻿using System;
using Grow.Data.DataSet.Adapter;
using Grow.Data.DataSet.Adapter.Master;

namespace Grow.Data.DataSet.Container
{
    public class MasterDataContainer
    {
        private readonly Lazy<AvatarMasterDataRepositoryAdapter> _avatarMaster;
        public AvatarMasterDataRepositoryAdapter AvatarMaster { get { return _avatarMaster.Value; } }

        private readonly Lazy<AvatarLevelMasterDataRepositoryAdapter> _avatarLevelMaster;
        public AvatarLevelMasterDataRepositoryAdapter AvatarLevelMaster { get { return _avatarLevelMaster.Value; } }

        private readonly Lazy<AvatarBonusMasterDataRepositoryAdapter> _avatarBonusMaster;
        public AvatarBonusMasterDataRepositoryAdapter AvatarBonusMaster { get { return _avatarBonusMaster.Value; } }

        private readonly Lazy<MonsterMasterDataRepositoryAdapter> _monsterMaster;
        public MonsterMasterDataRepositoryAdapter MonsterMaster { get { return _monsterMaster.Value; } }

        private readonly Lazy<GearMasterDataRepositoryAdapter> _gearMaster;
        public GearMasterDataRepositoryAdapter GearMaster { get { return _gearMaster.Value; } }

        private readonly Lazy<ItemMasterDataRepositoryAdapter> _itemMaster;
        public ItemMasterDataRepositoryAdapter ItemMaster { get { return _itemMaster.Value; } }

        private readonly Lazy<UserLevelMasterDataRepositoryAdapter> _userLevelMaster;
        public UserLevelMasterDataRepositoryAdapter UserLevelMaster { get { return _userLevelMaster.Value; } }        

        public MasterDataContainer()
        {
            _avatarMaster = new Lazy<AvatarMasterDataRepositoryAdapter>(() => new AvatarMasterDataRepositoryAdapter(), true);
            _avatarLevelMaster = new Lazy<AvatarLevelMasterDataRepositoryAdapter>(() => new AvatarLevelMasterDataRepositoryAdapter(), true);
            _avatarBonusMaster = new Lazy<AvatarBonusMasterDataRepositoryAdapter>(() => new AvatarBonusMasterDataRepositoryAdapter(), true);
            _monsterMaster = new Lazy<MonsterMasterDataRepositoryAdapter>(() => new MonsterMasterDataRepositoryAdapter(), true);
            _gearMaster = new Lazy<GearMasterDataRepositoryAdapter>(() => new GearMasterDataRepositoryAdapter(), true);
            _itemMaster = new Lazy<ItemMasterDataRepositoryAdapter>(() => new ItemMasterDataRepositoryAdapter(), true);
            _userLevelMaster = new Lazy<UserLevelMasterDataRepositoryAdapter>(() => new UserLevelMasterDataRepositoryAdapter(), true);
        }
    }
}