﻿using System;
using Grow.Data.DataSet.Adapter.LoginBonus;

namespace Grow.Data.DataSet.Container
{
    public class LoginBonusDataContainer
    {
        private readonly Lazy<LoginBonusMasterDataRepositoryAdapter> _loginBonusMaster;
        public LoginBonusMasterDataRepositoryAdapter LoginBonusMaster { get { return _loginBonusMaster.Value; } }

        private readonly Lazy<LoginBonusModeMasterDataRepositoryAdapter> _loginBonusModeMaster;
        public LoginBonusModeMasterDataRepositoryAdapter LoginBonusModeMaster { get { return _loginBonusModeMaster.Value; } }

        private readonly Lazy<LoginBonusRankMasterDataRepositoryAdapter> _loginBonusRankMaster;
        public LoginBonusRankMasterDataRepositoryAdapter LoginBonusRankMaster { get { return _loginBonusRankMaster.Value; } }

        private readonly Lazy<LoginBonusSheetMasterDataRepositoryAdapter> _loginBonusSheetMaster;
        public LoginBonusSheetMasterDataRepositoryAdapter LoginBonusSheetMaster { get { return _loginBonusSheetMaster.Value; } }

        public LoginBonusDataContainer()
        {
            _loginBonusMaster = new Lazy<LoginBonusMasterDataRepositoryAdapter>(() => new LoginBonusMasterDataRepositoryAdapter(), true);
            _loginBonusModeMaster = new Lazy<LoginBonusModeMasterDataRepositoryAdapter>(() => new LoginBonusModeMasterDataRepositoryAdapter(), true);
            _loginBonusRankMaster = new Lazy<LoginBonusRankMasterDataRepositoryAdapter>(() => new LoginBonusRankMasterDataRepositoryAdapter(), true);
            _loginBonusSheetMaster = new Lazy<LoginBonusSheetMasterDataRepositoryAdapter>(() => new LoginBonusSheetMasterDataRepositoryAdapter(), true);
        }
    }
}