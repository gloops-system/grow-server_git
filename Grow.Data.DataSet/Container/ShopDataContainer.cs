﻿using System;
using Grow.Data.DataSet.Adapter.Shop;

namespace Grow.Data.DataSet.Container
{
    public class ShopDataContainer
    {
        private readonly Lazy<ShopMasterDataRepositoryAdapter> _shopMaster;
        public ShopMasterDataRepositoryAdapter ShopMaster { get { return _shopMaster.Value; } }

        private readonly Lazy<ShopItemMasterDataRepositoryAdapter> _shopItemMaster;
        public ShopItemMasterDataRepositoryAdapter ShopItemMaster { get { return _shopItemMaster.Value; } }

        public ShopDataContainer()
        {
            _shopMaster = new Lazy<ShopMasterDataRepositoryAdapter>(() => new ShopMasterDataRepositoryAdapter(), true);
            _shopItemMaster = new Lazy<ShopItemMasterDataRepositoryAdapter>(() => new ShopItemMasterDataRepositoryAdapter(), true);
        }
    }
}
