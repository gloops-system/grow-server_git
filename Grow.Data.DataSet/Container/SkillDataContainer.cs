﻿using System;
using Grow.Data.DataSet.Adapter.Skill;

namespace Grow.Data.DataSet.Container
{
    public class SkillDataContainer
    {
        private readonly Lazy<SkillMasterDataRepositoryAdapter> _skillMaster;
        public SkillMasterDataRepositoryAdapter SkillMaster { get { return _skillMaster.Value; } }

        public SkillDataContainer()
        {
            _skillMaster = new Lazy<SkillMasterDataRepositoryAdapter>(() => new SkillMasterDataRepositoryAdapter(), true);
        }
    }
}
