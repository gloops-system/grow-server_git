﻿using System;
using Grow.Data.DataSet.Adapter.Airship;

namespace Grow.Data.DataSet.Container
{
    public class AirshipDataContainer
    {
        private readonly Lazy<AirshipMasterDataRepositoryAdapter> _airshipMaster;
        public AirshipMasterDataRepositoryAdapter AirShipMaster { get { return _airshipMaster.Value; } }

        private readonly Lazy<AirshipFloorMasterDataRepositoryAdapter> _airshipFloorMaster;
        public AirshipFloorMasterDataRepositoryAdapter AirShipFloorMaster { get { return _airshipFloorMaster.Value; } }

        private readonly Lazy<AirshipNpcMasterDataRepositoryAdapter> _airshipNpcMaster;
        public AirshipNpcMasterDataRepositoryAdapter AirshipNpcMaster { get { return _airshipNpcMaster.Value; } }

        private readonly Lazy<AirshipNpcActionMasterDataRepositoryAdapter> _airshipNpcActionMaster;
        public AirshipNpcActionMasterDataRepositoryAdapter AirshipNpcActionMaster { get { return _airshipNpcActionMaster.Value; } }

        public AirshipDataContainer()
        {
            _airshipMaster = new Lazy<AirshipMasterDataRepositoryAdapter>(() => new AirshipMasterDataRepositoryAdapter(), true);
            _airshipFloorMaster = new Lazy<AirshipFloorMasterDataRepositoryAdapter>(() => new AirshipFloorMasterDataRepositoryAdapter(), true);
            _airshipNpcMaster = new Lazy<AirshipNpcMasterDataRepositoryAdapter>(() => new AirshipNpcMasterDataRepositoryAdapter(), true);
            _airshipNpcActionMaster = new Lazy<AirshipNpcActionMasterDataRepositoryAdapter>(() => new AirshipNpcActionMasterDataRepositoryAdapter(), true);
        }
    }
}