﻿using System;
using Grow.Data.DataSet.Adapter.Skill;

namespace Grow.Data.DataSet.Container
{
    public class SphereDataContainer
    {
        private readonly Lazy<SphereGridMasterDataRepositoryAdapter> _sphereGridMaster;
        public SphereGridMasterDataRepositoryAdapter SphereGridMaster { get { return _sphereGridMaster.Value; } }

        private readonly Lazy<SphereGridEffectMasterDataRepositoryAdapter> _sphereGridEffectMaster;
        public SphereGridEffectMasterDataRepositoryAdapter SphereGridEffectMaster { get { return _sphereGridEffectMaster.Value; } }

        private readonly Lazy<SphereGridPositionMasterDataRepositoryAdapter> _sphereGridPositionMaster;
        public SphereGridPositionMasterDataRepositoryAdapter SphereGridPositionMaster { get { return _sphereGridPositionMaster.Value; } }

        private readonly Lazy<SphereGridWayMasterDataRepositoryAdapter> _sphereGridWayMaster;
        public SphereGridWayMasterDataRepositoryAdapter SphereGridWayMaster { get { return _sphereGridWayMaster.Value; } }

        public SphereDataContainer()
        {
            _sphereGridMaster         = new Lazy<SphereGridMasterDataRepositoryAdapter>(() => new SphereGridMasterDataRepositoryAdapter(), true);
            _sphereGridEffectMaster   = new Lazy<SphereGridEffectMasterDataRepositoryAdapter>(() => new SphereGridEffectMasterDataRepositoryAdapter(), true);
            _sphereGridPositionMaster = new Lazy<SphereGridPositionMasterDataRepositoryAdapter>(() => new SphereGridPositionMasterDataRepositoryAdapter(), true);
            _sphereGridWayMaster      = new Lazy<SphereGridWayMasterDataRepositoryAdapter>(() => new SphereGridWayMasterDataRepositoryAdapter(), true);
        }
    }
}
