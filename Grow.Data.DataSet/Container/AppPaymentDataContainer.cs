﻿using Grow.Data.DataSet.Adapter;
using Grow.Data.DataSet.Adapter.Purchase;
using System;
//using Inazuma.Data.General.DataSet.Adapter.Sale;

namespace Grow.Data.DataSet.Container
{
    public class AppPaymentDataContainer
    {
        private Lazy<AppPaymentDataRepositoryAdapter> _appPayment;
        public AppPaymentDataRepositoryAdapter AppPayment { get { return _appPayment.Value; } }

        private Lazy<AppPaymentHistoryDataRepositoryAdapter> _appPaymentHistory;
        public AppPaymentHistoryDataRepositoryAdapter AppPaymentHistory { get { return _appPaymentHistory.Value; } }

        private Lazy<AndroidAppPaymentDataRepositoryAdapter> _androidAppPayment;
        public AndroidAppPaymentDataRepositoryAdapter AndroidAppPayment { get { return _androidAppPayment.Value; } }

        private Lazy<GemMasterDataRepositoryAdapter> _gemMaster;
        public GemMasterDataRepositoryAdapter GemMaster { get { return _gemMaster.Value; } }

        private Lazy<AndroidGemMasterDataRepositoryAdapter> _androidgemMaster;
        public AndroidGemMasterDataRepositoryAdapter AndroidGemMaster { get { return _androidgemMaster.Value; } }

        //private Lazy<SaleMasterDataRepositoryAdapter> _saleMaster;
        //public SaleMasterDataRepositoryAdapter SaleMaster { get { return _saleMaster.Value; } }

        //private Lazy<SaleCoinMasterDataRepositoryAdapter> _saleCoinMaster;
        //public SaleCoinMasterDataRepositoryAdapter SaleCoinMaster { get { return _saleCoinMaster.Value; } }

        public AppPaymentDataContainer()
        {
            _appPayment = new Lazy<AppPaymentDataRepositoryAdapter>(() => new AppPaymentDataRepositoryAdapter(), true);
            _appPaymentHistory = new Lazy<AppPaymentHistoryDataRepositoryAdapter>(() => new AppPaymentHistoryDataRepositoryAdapter(), true);
            _androidAppPayment = new Lazy<AndroidAppPaymentDataRepositoryAdapter>(() => new AndroidAppPaymentDataRepositoryAdapter(), true);
            _gemMaster = new Lazy<GemMasterDataRepositoryAdapter>(() => new GemMasterDataRepositoryAdapter(), true);
            _androidgemMaster = new Lazy<AndroidGemMasterDataRepositoryAdapter>(() => new AndroidGemMasterDataRepositoryAdapter(), true);
            //_saleMaster = new Lazy<SaleMasterDataRepositoryAdapter>(() => new SaleMasterDataRepositoryAdapter(), true);
            //_saleCoinMaster = new Lazy<SaleCoinMasterDataRepositoryAdapter>(() => new SaleCoinMasterDataRepositoryAdapter(), true);
        }
    }
}
