﻿using System;
using Grow.Data.DataSet.Adapter.Shop;

namespace Grow.Data.DataSet.Container
{
    public class GameCurrencyDataContainer
    {
        private readonly Lazy<GameCurrencyMasterDataRepositoryAdapter> _gameCurrencyMaster;
        public GameCurrencyMasterDataRepositoryAdapter GameCurrencyMater { get { return _gameCurrencyMaster.Value; } }

        public GameCurrencyDataContainer()
        {
            _gameCurrencyMaster = new Lazy<GameCurrencyMasterDataRepositoryAdapter>(() => new GameCurrencyMasterDataRepositoryAdapter(), true);
        }
    }
}
