﻿using System;
using Grow.Data.DataSet.Adapter.Enemy;

namespace Grow.Data.DataSet.Container
{
    public class EnemyDataContainer
    {
        private readonly Lazy<EnemyMasterDataRepositoryAdapter> _enemyMaster;
        public EnemyMasterDataRepositoryAdapter EnemyMaster { get { return _enemyMaster.Value; } }

        private readonly Lazy<EnemyEquipSkillMasterDataRepositoryAdapter> _enemyEquipSkillMaster;
        public EnemyEquipSkillMasterDataRepositoryAdapter EnemyEquipSkill { get { return _enemyEquipSkillMaster.Value; } }

        private readonly Lazy<EnemyFigureMasterDataRepositoryAdapter> _enemyFigureMaster;
        public EnemyFigureMasterDataRepositoryAdapter EnemyFigureMaster { get { return _enemyFigureMaster.Value; } }

        public EnemyDataContainer()
        {
            _enemyMaster =           new Lazy<EnemyMasterDataRepositoryAdapter>(() => new EnemyMasterDataRepositoryAdapter(), true);
            _enemyEquipSkillMaster = new Lazy<EnemyEquipSkillMasterDataRepositoryAdapter>(() => new EnemyEquipSkillMasterDataRepositoryAdapter(), true);
            _enemyFigureMaster =     new Lazy<EnemyFigureMasterDataRepositoryAdapter>(() => new EnemyFigureMasterDataRepositoryAdapter(), true);
        }
    }
}
