﻿using Grow.Data.DataSet.Caching;
using Fango.Core.Data;

namespace Grow.Data.DataSet.Adapter.Master
{
    public class AvatarLevelMasterDataRepositoryAdapter
    {
        public MasterDataSet.AvatarLevelMasterRow[] GetDataByAvatarId(int avatarId)
        {
            var dataRow = new DbAccessHandler().ExecuteAction(() =>
            {                             
                var adapter = AvatarLevelMasterCacheTableAdapter.GetInstance();
                var avatarLevelMasters = adapter.GetDataByAvatarId(avatarId);
                return avatarLevelMasters;
            });
            return dataRow;            
        }        
    }
}
