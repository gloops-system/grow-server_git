﻿using Grow.Data.DataSet.Caching;
using Fango.Core.Data;

namespace Grow.Data.DataSet.Adapter.Master
{
    public class AvatarMasterDataRepositoryAdapter
    {
        public MasterDataSet.AvatarMasterRow GetDataByAvatarId(int AvatarId)
        {
            var dataRow = new DbAccessHandler().ExecuteAction(() =>
            {                             
                var adapter = AvatarMasterCacheTableAdapter.GetInstance();
                var avatarMaster = adapter.GetDataByAvatarId(AvatarId);
                return avatarMaster;
            });
            return dataRow;            
        }        
    }
}
