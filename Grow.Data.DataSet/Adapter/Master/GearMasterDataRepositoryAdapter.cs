﻿using Grow.Data.DataSet.Caching;
using Fango.Core.Data;

namespace Grow.Data.DataSet.Adapter.Master
{
    public class GearMasterDataRepositoryAdapter
    {
        public MasterDataSet.GearMasterRow GetDataByGearId(int GearId)
        {
            var dataRow = new DbAccessHandler().ExecuteAction(() =>
            {                             
                var adapter = GearMasterCacheTableAdapter.GetInstance();
                var gearMaster = adapter.GetDataByGearId(GearId);
                return gearMaster;
            });
            return dataRow;            
        }        
    }
}
