﻿using Fango.Core.Data;
using Grow.Data.DataSet;
using Grow.Data.DataSet.Caching;

namespace Grow.Data.DataSet.Adapter
{
    public class GemMasterDataRepositoryAdapter
    {
        public MasterDataSet.GemMasterRow[] FindItems()
        {
            var GemMasterRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    var gemMasterCacheTableAdapter = GemMasterCacheTableAdapter.GetInstance();
                    return gemMasterCacheTableAdapter.GetData();
                });
            return GemMasterRows;
        }
        public MasterDataSet.GemMasterRow FindItem(string productId)
        {
            var GemMasterRow = new DbAccessHandler().ExecuteAction(() =>
            {
                var GemMasterTableAdapter = GemMasterCacheTableAdapter.GetInstance();
                return GemMasterTableAdapter.GetDataByPk(productId);
            });
            return GemMasterRow;
        }
    }
}
