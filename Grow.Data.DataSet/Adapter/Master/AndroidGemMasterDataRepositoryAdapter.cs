﻿using Grow.Data.DataSet.Caching;
using Fango.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Data.DataSet.Adapter
{
    public class AndroidGemMasterDataRepositoryAdapter
    {
        public MasterDataSet.AndroidGemMasterRow[] FindItems()
        {
            var coinMasterRows = new DbAccessHandler().ExecuteAction(() =>
            {
                var coinMasterCacheTableAdapter = AndroidGemMasterCacheTableAdapter.GetInstance();
                return coinMasterCacheTableAdapter.GetData();
            });
            return coinMasterRows;
        }
        public MasterDataSet.AndroidGemMasterRow FindItem(string productId)
        {
            var coinMasterRow = new DbAccessHandler().ExecuteAction(() =>
            {
                var coinMasterTableAdapter = AndroidGemMasterCacheTableAdapter.GetInstance();
                return coinMasterTableAdapter.GetDataByPk(productId);
            });
            return coinMasterRow;
        }
    }
}
