﻿using Grow.Data.DataSet.Caching;
using Fango.Core.Data;

namespace Grow.Data.DataSet.Adapter.Master
{
    public class ItemMasterDataRepositoryAdapter
    {
        public MasterDataSet.ItemMasterRow GetDataByItemId(int ItemId)
        {
            var dataRow = new DbAccessHandler().ExecuteAction(() =>
            {                             
                var adapter = ItemMasterCacheTableAdapter.GetInstance();
                var itemMaster = adapter.GetDataByItemId(ItemId);
                return itemMaster;
            });
            return dataRow;
        }        
    }
}
