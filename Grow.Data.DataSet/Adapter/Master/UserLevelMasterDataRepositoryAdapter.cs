﻿using Fango.Core.Data;
using Grow.Data.DataSet.Caching;

namespace Grow.Data.DataSet.Adapter.Master
{
    public class UserLevelMasterDataRepositoryAdapter
    {
        /// <summary>
        /// 全取得
        /// </summary>
        /// <returns></returns>
        public MasterDataSet.UserLevelMasterRow[] GetData()
        {
            var userLevelMasterRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    var userLevelMasterTableAdapter = UserLevelMasterCacheTableAdapter.GetInstance();
                    return userLevelMasterTableAdapter.GetData();
                });
            return userLevelMasterRows;
        }

        /// <summary>
        /// PKで取得
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public MasterDataSet.UserLevelMasterRow GetDataByPK(short level)
        {
            var userLevelMasterRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    var userLevelMasterTableAdapter = UserLevelMasterCacheTableAdapter.GetInstance();
                    return userLevelMasterTableAdapter.GetDataByPK(level);
                });
            return userLevelMasterRows;
        }
    }
}