﻿using Grow.Data.DataSet.Caching;
using Fango.Core.Data;

namespace Grow.Data.DataSet.Adapter.Master
{
    public class AvatarBonusMasterDataRepositoryAdapter
    {
        public MasterDataSet.AvatarBonusMasterRow GetDataByAvatarId(int AvatarId)
        {
            var dataRow = new DbAccessHandler().ExecuteAction(() =>
            {                             
                var adapter = AvatarBonusMasterCacheTableAdapter.GetInstance();
                var avatarBonusMaster = adapter.GetDataByAvatarId(AvatarId);
                return avatarBonusMaster;
            });
            return dataRow;            
        }        
    }
}
