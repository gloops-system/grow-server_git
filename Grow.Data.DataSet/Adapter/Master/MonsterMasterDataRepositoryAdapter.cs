﻿using Grow.Data.DataSet.Caching;
using Fango.Core.Data;

namespace Grow.Data.DataSet.Adapter.Master
{
    public class MonsterMasterDataRepositoryAdapter
    {
        public MasterDataSet.MonsterMasterRow GetDataByMonsterId(int MonsterId)
        {
            var dataRow = new DbAccessHandler().ExecuteAction(() =>
            {                             
                var adapter = MonsterMasterCacheTableAdapter.GetInstance();
                var monsterMaster = adapter.GetDataByMonsterId(MonsterId);
                return monsterMaster;
            });
            return dataRow;            
        }        
    }
}
