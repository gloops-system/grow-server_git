﻿using Fango.Core.Data;
using Grow.Data.DataSet.Caching;

namespace Grow.Data.DataSet.Adapter.Skill
{
    public class SphereGridEffectMasterDataRepositoryAdapter
    {
        public SphereDataSet.SphereGridEffectMasterRow GetDataByPK(int gridId)
        {
            var row = new DbAccessHandler().ExecuteAction(() =>
            {
                var tableAdapter = SphereGridEffectMasterCacheTableAdapter.GetInstance();
                return tableAdapter.GetDataByPK(gridId);
            });
            return row;
        }
    }
}
