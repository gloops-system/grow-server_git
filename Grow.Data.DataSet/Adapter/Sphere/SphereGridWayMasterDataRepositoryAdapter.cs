﻿using Fango.Core.Data;
using Grow.Data.DataSet.Caching;

namespace Grow.Data.DataSet.Adapter.Skill
{
    public class SphereGridWayMasterDataRepositoryAdapter
    {
        public SphereDataSet.SphereGridWayMasterRow[] GetDataByGrid1(int grid1)
        {
            var rows = new DbAccessHandler().ExecuteAction(() =>
            {
                var tableAdapter = SphereGridWayMasterCacheTableAdapter.GetInstance();
                return tableAdapter.GetDataByGrid1(grid1);
            });
            return rows;
        }
    }
}
