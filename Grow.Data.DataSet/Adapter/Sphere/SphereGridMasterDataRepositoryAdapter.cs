﻿using Fango.Core.Data;
using Grow.Data.DataSet.Caching;

namespace Grow.Data.DataSet.Adapter.Skill
{
    public class SphereGridMasterDataRepositoryAdapter
    {
        public SphereDataSet.SphereGridMasterRow GetDataByPK(int gridId)
        {
            var row = new DbAccessHandler().ExecuteAction(() =>
            {
                var tableAdapter = SphereGridMasterCacheTableAdapter.GetInstance();
                return tableAdapter.GetDataByPK(gridId);
            });
            return row;
        }

        public SphereDataSet.SphereGridMasterRow[] GetDataByJobId(int jobId)
        {
            var rows = new DbAccessHandler().ExecuteAction(() =>
            {
                var tableAdapter = SphereGridMasterCacheTableAdapter.GetInstance();
                return tableAdapter.GetDataByJobId(jobId);
            });
            return rows;
        }
    }
}
