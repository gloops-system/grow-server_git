﻿using Fango.Core.Data;
using Grow.Data.DataSet.Caching;

namespace Grow.Data.DataSet.Adapter.Skill
{
    public class SphereGridPositionMasterDataRepositoryAdapter
    {
        public SphereDataSet.SphereGridPositionMasterRow[] GetData()
        {
            var rows = new DbAccessHandler().ExecuteAction(() =>
            {
                var tableAdapter = SphereGridPositionMasterCacheTableAdapter.GetInstance();
                return tableAdapter.GetData();
            });
            return rows;
        }

        public SphereDataSet.SphereGridPositionMasterRow GetDataByPK(int gridPosition)
        {
            var row = new DbAccessHandler().ExecuteAction(() =>
            {
                var tableAdapter = SphereGridPositionMasterCacheTableAdapter.GetInstance();
                return tableAdapter.GetDataByPK(gridPosition);
            });
            return row;
        }
    }
}
