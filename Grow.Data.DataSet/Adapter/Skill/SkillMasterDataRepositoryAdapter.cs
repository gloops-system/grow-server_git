﻿using Fango.Core.Data;
using Grow.Data.DataSet.Caching;

namespace Grow.Data.DataSet.Adapter.Skill
{
    public class SkillMasterDataRepositoryAdapter
    {
        public SkillDataSet.SkillMasterRow GetDataByPK(int skillId)
        {
            var row = new DbAccessHandler().ExecuteAction(() =>
            {
                var tableAdapter = SkillMasterCacheTableAdapter.GetInstance();
                return tableAdapter.GetDataByPK(skillId);
            });
            return row;
        }
    }
}
