﻿using System;
using System.Collections.Generic;
using Fango.Core.Data;
using Fango.Core.Caching;
using Fango.Core.Util;
using Grow.Data.DataSet.AppPaymentDataSetTableAdapters;
using System.Linq;
using Fango.Redis.Caching;

namespace Grow.Data.DataSet.Adapter.Purchase
{
    public class AppPaymentDataRepositoryAdapter
    {
        public AppPaymentDataSet.AppPaymentRow FindItem(long transactionId)
        {
            var appPaymentRow = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var appPaymentTableAdapter = new AppPaymentTableAdapter())
                    {
                        return appPaymentTableAdapter.GetDataByPK(transactionId).FirstOrDefault();
                    }
                });
            return appPaymentRow;
        }

        public void UpdateCoin(long transactionId, int coin)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var appPaymentTableAdapter = new AppPaymentTableAdapter())
                {
                    return appPaymentTableAdapter.UpdateGemByPK(coin, transactionId);
                }
            });
        }

        public void UpdateReceiptStatus(long transactionId, int receiptStatus)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var appPaymentTableAdapter = new AppPaymentTableAdapter())
                {
                    return appPaymentTableAdapter.UpdateReceiptStatusByPK(receiptStatus, transactionId);
                }
            });
        }

        public void UpdateStatus(long transactionId, int status)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var appPaymentTableAdapter = new AppPaymentTableAdapter())
                {
                    return appPaymentTableAdapter.UpdateStatusByPK(status, transactionId);
                }
            });
        }

        public void AddNew(long transactionId,
                           long originalTransactionId,
                           int userId,
                           string bid,
                           string bvrs,
                           int itemId,
                           string originalPurchaseDate,
                           string purchaseDate,
                           string productId,
                           int quantity,
                           int receiptStatus,
                           int status,
                           int coin
            )
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var appPaymentTableAdapter = new AppPaymentTableAdapter())
                {
                    var now = DateUtil.GetDateTimeOffsetNow();
                    return appPaymentTableAdapter.AddNew(transactionId,
                                                         originalTransactionId,
                                                         userId,
                                                         bid,
                                                         bvrs,
                                                         itemId,
                                                         originalPurchaseDate,
                                                         purchaseDate,
                                                         productId,
                                                         quantity,
                                                         receiptStatus,
                                                         status,
                                                         coin,
                                                         now,
                                                         now);
                }
            });
        }
        
        /// <summary>
        /// 課金日時指定とステータス
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="status"></param>
        /// <returns>UserId, PurchaseDate, ProductId, Quantity, Status, Coin</returns>
        public List<AppPaymentDataSet.AppPaymentRow> GetDataByFromTo(DateTimeOffset from, DateTimeOffset to, int status)
        {
            var list = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var appPaymentTableAdapter = new AppPaymentTableAdapter())
                    {
                        var data = appPaymentTableAdapter.GetDataByFromTo(from, to, status);
                        return data;
                    }
                });
            return list.ToList();
        }

        public int GetCountByProductId(string productId, DateTimeOffset from, DateTimeOffset to)
        {
            var count = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var appPaymentTableAdapter = new AppPaymentTableAdapter())
                {
                    return appPaymentTableAdapter.GetCountByProductIdAndAddTime(productId, from, to);
                }
            });
            return count != null ? (int)count : 0;
        }

        public AdminGeneralHourlySummaryRow[] GetDateData(DateTimeOffset date)
        {
            string cacheKey = "HourlyCoinBuyData_" + date.ToString("yyyy-MM-dd");

            //キャッシュの確認
            var cacheClient = CacheClientFactory();
            var cacheData = cacheClient.Get<AdminGeneralHourlySummaryRow[]>(cacheKey);
            if (cacheData != null)
            {
                return cacheData;
            }

            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var appPaymentTableAdapter = new AppPaymentHourlyTableAdapter())
                {
                    var hourlyArray = appPaymentTableAdapter.GetDateData(date).Select(x=> new AdminGeneralHourlySummaryRow
                        {
                            hour = x.hour,
                            gem = x.gem,
                        }).ToArray();

                    //5分間キャッシュにストア
                    cacheClient.Add(cacheKey, hourlyArray, TimeSpan.FromMinutes(5));
                    return hourlyArray;
                }
            });
            return result;
        }

        public AppPaymentDataSet.AppPaymentRow[] GetDataByUserId(int userId)
        {
            var list = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var appPaymentTableAdapter = new AppPaymentTableAdapter())
                {
                    return appPaymentTableAdapter.GetDataByUserId(userId).ToArray();
                }
            });
            return list;
        }

        private RedisCacheClient CacheClientFactory()
        {
            var cacheClient = new RedisCacheClient();
            return cacheClient;
        }
    }

    public class AdminGeneralHourlySummaryRow
    {
        public int hour;
        public int gem;
    }
}