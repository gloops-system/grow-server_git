﻿using System;
using System.Collections.Generic;
using Fango.Core.Data;
using Fango.Core.Caching;
using Fango.Core.Util;
using Grow.Data.DataSet.AppPaymentDataSetTableAdapters;
using System.Linq;
using Fango.Redis.Caching;

namespace Grow.Data.DataSet.Adapter.Purchase
{
    public class AndroidAppPaymentDataRepositoryAdapter
    {
        public AppPaymentDataSet.AndroidAppPaymentRow FindItem(string orderId)
        {
            var appPaymentRow = new DbAccessHandler().ExecuteAction(() =>
                {
                    using (var appPaymentTableAdapter = new AndroidAppPaymentTableAdapter())
                    {
                        return appPaymentTableAdapter.GetDataByPK(orderId).FirstOrDefault();
                    }
                });
            return appPaymentRow;
        }

        public void UpdateCoin(string orderId, int coin)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var appPaymentTableAdapter = new AndroidAppPaymentTableAdapter ())
                {
                    return appPaymentTableAdapter.UpdateGemByPK(coin, orderId);
                }
            });
        }

        public void UpdateStatus(string orderId, int status)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var appPaymentTableAdapter = new AndroidAppPaymentTableAdapter())
                {
                    return appPaymentTableAdapter.UpdateStatusByPK(status, orderId);
                }
            });
        }

        public void AddNew(string orderId, string notificationId, int userId, string nonce, string packageName, string productId, long purchaseTime, int purchaseState, int status, int coin)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var appPaymentTableAdapter = new AndroidAppPaymentTableAdapter())
                {
                    var now = DateUtil.GetDateTimeOffsetNow();
                    return appPaymentTableAdapter.AddNew(orderId,
                                                         notificationId,
                                                         userId,
                                                         nonce,
                                                         packageName,
                                                         productId,
                                                         purchaseTime,
                                                         purchaseState,
                                                         status,
                                                         coin,
                                                         now,
                                                         now);
                }
            });
        }

        public AdminGeneralHourlySummaryRow[] GetDateData(DateTimeOffset date)
        {
            string cacheKey = "HourlyCoinBuyDataAndroid_" + date.ToString("yyyy-MM-dd");

            //キャッシュの確認
            var cacheClient = CacheClientFactory();
            var cacheData = cacheClient.Get<AdminGeneralHourlySummaryRow[]>(cacheKey);
            if (cacheData != null)
            {
                return cacheData;
            }

            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var appPaymentTableAdapter = new AndroidAppPaymentHourlyTableAdapter())
                {
                    var hourlyArray = appPaymentTableAdapter.GetDateData(date).Select(x => new AdminGeneralHourlySummaryRow
                    {
                        hour = x.hour,
                        gem = x.gem,
                    }).ToArray();

                    //5分間キャッシュにストア
                    cacheClient.Add(cacheKey, hourlyArray, TimeSpan.FromMinutes(5));
                    return hourlyArray;
                }
            });
            return result;
        }

        /// <summary>
        /// 課金日時指定とステータス
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="status"></param>
        /// <returns>UserId, PurchaseDate, ProductId, Quantity, Status, Coin</returns>
        public List<AppPaymentDataSet.AndroidAppPaymentRow> GetDataByFromTo(DateTimeOffset from, DateTimeOffset to, int status)
        {
            var list = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var appPaymentTableAdapter = new AndroidAppPaymentTableAdapter())
                {
                    var data = appPaymentTableAdapter.GetDataByFromTo(from, to, status);
                    return data;
                }
            });
            return list.ToList();
        }

        public AppPaymentDataSet.AndroidAppPaymentRow[] GetDataByUserId(int userId)
        {
            var list = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var appPaymentTableAdapter = new AndroidAppPaymentTableAdapter())
                {
                    return appPaymentTableAdapter.GetDataByUserId(userId).ToArray();
                }
            });
            return list;
        }
        private RedisCacheClient CacheClientFactory()
        {
            var cacheClient = new RedisCacheClient();
            return cacheClient;
        }
    }
}