﻿using System;
using System.Collections.Generic;
using Fango.Core.Data;
using Fango.Core.Caching;
using Fango.Core.Util;
using System.Linq;
using Fango.Redis.Caching;
using Grow.Data.DataSet.AppPaymentDataSetTableAdapters;

namespace Grow.Data.DataSet.Adapter.Purchase
{
    public class AppPaymentHistoryDataRepositoryAdapter
    {
        public void AddNew(long historyId, int userId, string productId, int status)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var appPaymentHistoryTableAdapter = new AppPaymentHistoryTableAdapter())
                {
                    var now = DateUtil.GetDateTimeOffsetNow();
                    return appPaymentHistoryTableAdapter.AddNew(historyId, userId, productId, status, now, now);
                }
            });
        }

        public void UpdateStatusByHistoryId(long historyId, int status)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var appPaymentHistoryTableAdapter = new AppPaymentHistoryTableAdapter())
                {
                    var now = DateUtil.GetDateTimeOffsetNow();
                    return appPaymentHistoryTableAdapter.UpdateStatusByPK(historyId, status, now);
                }
            });
        }

        public AppPaymentDataSet.AppPaymentHistoryRow[] GetDataByUserIdAndProductIdAndStatus(int userId, int status)
        {
            return new DbAccessHandler().ExecuteAction(() =>
            {
                using (var appPaymentHistoryTableAdapter = new AppPaymentHistoryTableAdapter())
                {
                    var now = DateUtil.GetDateTimeOffsetNow();
                    return appPaymentHistoryTableAdapter.GetDataByUserIdAndStatus(userId, status).ToArray();
                }
            });
        }

        public AppPaymentDataSet.AppPaymentHistoryRow[] GetDataByUserId(int userId)
        {
            return new DbAccessHandler().ExecuteAction(() =>
            {
                using (var appPaymentHistoryTableAdapter = new AppPaymentHistoryTableAdapter())
                {
                    return appPaymentHistoryTableAdapter.GetDataByUserId(userId).ToArray();
                }
            });
        }
    }
}
