﻿using Fango.Core.Data;
using Grow.Data.DataSet.Caching;

namespace Grow.Data.DataSet.Adapter.Airship
{
    public class AirshipNpcMasterDataRepositoryAdapter
    {
        /// <summary>
        /// 全取得
        /// </summary>
        /// <returns></returns>
        public AirshipDataSet.AirshipNpcMasterRow[] GetData()
        {
            var airshipNpcMasterRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    var airshipNpcMasterTableAdapter = AirshipNpcMasterCacheTableAdapter.GetInstance();
                    return airshipNpcMasterTableAdapter.GetData();
                });
            return airshipNpcMasterRows;
        }

        /// <summary>
        /// AirshipIdで取得
        /// </summary>
        /// <param name="airshipId"></param>
        /// <returns></returns>
        public AirshipDataSet.AirshipNpcMasterRow[] GetDataByAirshipId(int airshipId)
        {
            var airshipNpcMasterRow = new DbAccessHandler().ExecuteAction(() =>
                {
                    var airshipNpcMasterTableAdapter = AirshipNpcMasterCacheTableAdapter.GetInstance();
                    return airshipNpcMasterTableAdapter.GetDataByAirshipId(airshipId);
                });
            return airshipNpcMasterRow;
        }
    }
}