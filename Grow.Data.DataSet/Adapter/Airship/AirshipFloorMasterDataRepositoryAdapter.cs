﻿using Fango.Core.Data;
using Grow.Data.DataSet.Caching;

namespace Grow.Data.DataSet.Adapter.Airship
{
    public class AirshipFloorMasterDataRepositoryAdapter
    {
        /// <summary>
        /// 全取得
        /// </summary>
        /// <returns></returns>
        public AirshipDataSet.AirshipFloorMasterRow[] GetData()
        {
            var airshipFloorMasterRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    var airshipFloorMasterTableAdapter = AirshipFloorMasterCacheTableAdapter.GetInstance();
                    return airshipFloorMasterTableAdapter.GetData();
                });
            return airshipFloorMasterRows;
        }

        /// <summary>
        /// AirshipIdで取得
        /// </summary>
        /// <param name="airshipId"></param>
        /// <returns></returns>
        public AirshipDataSet.AirshipFloorMasterRow[] GetDataByAirshipId(int airshipId)
        {
            var airshipFloorMasterRow = new DbAccessHandler().ExecuteAction(() =>
                {
                    var airshipFloorMasterTableAdapter = AirshipFloorMasterCacheTableAdapter.GetInstance();
                    return airshipFloorMasterTableAdapter.GetDataByAirshipId(airshipId);
                });
            return airshipFloorMasterRow;
        }
    }
}