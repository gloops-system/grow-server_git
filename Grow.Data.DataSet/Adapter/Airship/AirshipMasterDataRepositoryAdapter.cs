﻿using Fango.Core.Data;
using Grow.Data.DataSet.Caching;

namespace Grow.Data.DataSet.Adapter.Airship
{
    public class AirshipMasterDataRepositoryAdapter
    {
        /// <summary>
        /// 全取得
        /// </summary>
        /// <returns></returns>
        public AirshipDataSet.AirshipMasterRow[] GetData()
        {
            var airshipMasterRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    var airshipMasterTableAdapter = AirshipMasterCacheTableAdapter.GetInstance();
                    return airshipMasterTableAdapter.GetData();
                });
            return airshipMasterRows;
        }

        /// <summary>
        /// PKで取得
        /// </summary>
        /// <param name="airshipId"></param>
        /// <returns></returns>
        public AirshipDataSet.AirshipMasterRow GetDataByPK(int airshipId)
        {
            var airshipMasterRow = new DbAccessHandler().ExecuteAction(() =>
                {
                    var airshipMasterTableAdapter = AirshipMasterCacheTableAdapter.GetInstance();
                    return airshipMasterTableAdapter.GetDataByPK(airshipId);
                });
            return airshipMasterRow;
        }
    }
}