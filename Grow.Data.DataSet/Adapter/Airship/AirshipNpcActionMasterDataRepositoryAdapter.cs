﻿using Fango.Core.Data;
using Grow.Data.DataSet.Caching;

namespace Grow.Data.DataSet.Adapter.Airship
{
    public class AirshipNpcActionMasterDataRepositoryAdapter
    {
        /// <summary>
        /// 全取得
        /// </summary>
        /// <returns></returns>
        public AirshipDataSet.AirshipNpcActionMasterRow[] GetData()
        {
            var airshipNpcActionMasterRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    var airshipNpcActionMasterTableAdapter = AirshipNpcActionMasterCacheTableAdapter.GetInstance();
                    return airshipNpcActionMasterTableAdapter.GetData();
                });
            return airshipNpcActionMasterRows;
        }

        /// <summary>
        /// NpcTypeで取得
        /// </summary>
        /// <param name="npcType"></param>
        /// <returns></returns>
        public AirshipDataSet.AirshipNpcActionMasterRow[] GetDataByNpcType(int npcType)
        {
            var airshipNpcActionMasterRow = new DbAccessHandler().ExecuteAction(() =>
                {
                    var airshipNpcActionMasterTableAdapter = AirshipNpcActionMasterCacheTableAdapter.GetInstance();
                    return airshipNpcActionMasterTableAdapter.GetDataByNpcType(npcType);
                });
            return airshipNpcActionMasterRow;
        }
    }
}