﻿using Fango.Core.Data;
using Grow.Data.DataSet.Caching;

namespace Grow.Data.DataSet.Adapter.LoginBonus
{
    public class LoginBonusModeMasterDataRepositoryAdapter
    {
        /// <summary>
        /// ModeIdで取得
        /// </summary>
        /// <param name="modeId"></param>
        /// <returns></returns>
        public LoginBonusDataSet.LoginBonusModeMasterRow[] GetDataByModeId(int modeId)
        {
            var loginBonusModeMasterRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    var loginBonusModeMasterTableAdapter = LoginBonusModeMasterCacheTableAdapter.GetInstance();
                    return loginBonusModeMasterTableAdapter.GetDataByModeId(modeId);
                });
            return loginBonusModeMasterRows;
        }
    }
}