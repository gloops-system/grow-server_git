﻿using Fango.Core.Data;
using Grow.Data.DataSet.Caching;

namespace Grow.Data.DataSet.Adapter.LoginBonus
{
    public class LoginBonusRankMasterDataRepositoryAdapter
    {
        /// <summary>
        /// Modeで取得
        /// </summary>
        /// <param name="mode"></param>
        /// <returns></returns>
        public LoginBonusDataSet.LoginBonusRankMasterRow[] GetDataByMode(int mode)
        {
            var loginBonusRankMasterRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    var loginBonusRankMasterTableAdapter = LoginBonusRankMasterCacheTableAdapter.GetInstance();
                    return loginBonusRankMasterTableAdapter.GetDataByMode(mode);
                });
            return loginBonusRankMasterRows;
        }
    }
}