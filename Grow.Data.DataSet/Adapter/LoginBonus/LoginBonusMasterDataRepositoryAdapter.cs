﻿using System;
using Fango.Core.Data;
using Grow.Data.DataSet.Caching;

namespace Grow.Data.DataSet.Adapter.LoginBonus
{
    public class LoginBonusMasterDataRepositoryAdapter
    {
        /// <summary>
        /// 開催中のログインボーナスを取得
        /// </summary>
        /// <returns></returns>
        public LoginBonusDataSet.LoginBonusMasterRow[] GetCurrentData(DateTime now)
        {
            var loginBonusMasterRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    var loginBonusMasterTableAdapter = LoginBonusMasterCacheTableAdapter.GetInstance();
                    return loginBonusMasterTableAdapter.GetCurrentData(now);
                });
            return loginBonusMasterRows;
        }
    }
}