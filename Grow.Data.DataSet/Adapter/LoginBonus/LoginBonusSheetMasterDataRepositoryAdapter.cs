﻿using Fango.Core.Data;
using Grow.Data.DataSet.Caching;

namespace Grow.Data.DataSet.Adapter.LoginBonus
{
    public class LoginBonusSheetMasterDataRepositoryAdapter
    {
        /// <summary>
        /// SheetIdで取得
        /// </summary>
        /// <param name="sheetId"></param>
        /// <returns></returns>
        public LoginBonusDataSet.LoginBonusSheetMasterRow[] GetDataBySheetId(int sheetId)
        {
            var loginBonusSheetMasterRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    var loginBonusSheetMasterTableAdapter = LoginBonusSheetMasterCacheTableAdapter.GetInstance();
                    return loginBonusSheetMasterTableAdapter.GetDataBySheetId(sheetId);
                });
            return loginBonusSheetMasterRows;
        }
    }
}