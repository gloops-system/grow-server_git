﻿using Fango.Core.Data;
using Grow.Data.DataSet.Caching;

namespace Grow.Data.DataSet.Adapter.Shop
{
    public class GameCurrencyMasterDataRepositoryAdapter
    {
        /// <summary>
        /// 全件取得
        /// </summary>
        /// <returns></returns>
        public GameCurrencyDataSet.GameCurrencyMasterRow[] GetData()
        {
            var gameCurrencyMasterRows = new DbAccessHandler().ExecuteAction(() =>
            {
                var gameCurrencyMasterCacheTableAdapter = GameCurrencyMasterCacheTableAdapter.GetInstance();
                return gameCurrencyMasterCacheTableAdapter.GetData();
            });
            return gameCurrencyMasterRows;
            
        }

        /// <summary>
        /// PKで取得
        /// </summary>
        /// <param name="shopId"></param>
        /// <returns></returns>
        public GameCurrencyDataSet.GameCurrencyMasterRow GetDataByPK(int id)
        {
            var gameCurrencyMasterRow = new DbAccessHandler().ExecuteAction(() =>
                {
                    var gameCurrencyMasterCacheTableAdapter = GameCurrencyMasterCacheTableAdapter.GetInstance();
                    return gameCurrencyMasterCacheTableAdapter.GetDataByPK(id);
                });
            return gameCurrencyMasterRow;
        }
    }
}
