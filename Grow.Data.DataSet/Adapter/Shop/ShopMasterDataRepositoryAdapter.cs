﻿using Fango.Core.Data;
using Grow.Data.DataSet.Caching;

namespace Grow.Data.DataSet.Adapter.Shop
{
    public class ShopMasterDataRepositoryAdapter
    {
        /// <summary>
        /// PKで取得
        /// </summary>
        /// <param name="shopId"></param>
        /// <returns></returns>
        public ShopDataSet.ShopMasterRow GetDataByPK(int shopId)
        {
            var shopMasterRow = new DbAccessHandler().ExecuteAction(() =>
                {
                    var shopMasterTableAdapter = ShopMasterCacheTableAdapter.GetInstance();
                    return shopMasterTableAdapter.GetDataByPK(shopId);
                });
            return shopMasterRow;
        }
    }
}
