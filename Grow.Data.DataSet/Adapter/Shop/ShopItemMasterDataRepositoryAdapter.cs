﻿using System;
using Fango.Core.Data;
using Grow.Data.DataSet.Caching;

namespace Grow.Data.DataSet.Adapter.Shop
{
    public class ShopItemMasterDataRepositoryAdapter
    {
        /// <summary>
        /// ShopIdで取得
        /// </summary>
        /// <param name="shopId"></param>
        /// <param name="now"></param>
        /// <returns></returns>
        public ShopDataSet.ShopItemMasterRow[] GetCurrentDataByShopId(int shopId, DateTimeOffset now)
        {
            var shopItemMasterRows = new DbAccessHandler().ExecuteAction(() =>
                {
                    var shopItemMasterTableAdapter = ShopItemMasterCacheTableAdapter.GetInstance();
                    return shopItemMasterTableAdapter.GetCurrentDataByShopId(shopId, now);
                });
            return shopItemMasterRows;
        }

        /// <summary>
        /// PKで取得
        /// </summary>
        /// <param name="shopItemId"></param>
        /// <returns></returns>
        public ShopDataSet.ShopItemMasterRow GetDataByPK(int shopItemId)
        {
            var shopItemMasterRow = new DbAccessHandler().ExecuteAction(() =>
                {
                    var shopItemMasterTableAdapter = ShopItemMasterCacheTableAdapter.GetInstance();
                    return shopItemMasterTableAdapter.GetDataByPK(shopItemId);
                });
            return shopItemMasterRow;
        }
    }
}