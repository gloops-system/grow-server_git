﻿using Fango.Core.Data;
using Grow.Data.DataSet.Caching;

namespace Grow.Data.DataSet.Adapter.Enemy
{
    public class EnemyEquipSkillMasterDataRepositoryAdapter
    {
        public EnemyDataSet.EnemyEquipSkillMasterRow[] GetDataByMobType(int mobType)
        {
            var rows = new DbAccessHandler().ExecuteAction(() =>
                {
                    var cacheTableAadapter = EnemyEquipSkillMasterCacheTableAdapter.GetInstance();
                    return cacheTableAadapter.GetDataByMobType(mobType);
                });
            return rows;
        }
    }
}