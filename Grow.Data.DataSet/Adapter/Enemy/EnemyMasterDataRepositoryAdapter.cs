﻿using Fango.Core.Data;
using Grow.Data.DataSet.Caching;

namespace Grow.Data.DataSet.Adapter.Enemy
{
    public class EnemyMasterDataRepositoryAdapter
    {
        public EnemyDataSet.EnemyMasterRow GetDataByPK(int enemyId)
        {
            var row = new DbAccessHandler().ExecuteAction(() =>
                {
                    var cacheTableAadapter = EnemyMasterCacheTableAdapter.GetInstance();
                    return cacheTableAadapter.GetDataByPK(enemyId);
                });
            return row;
        }
    }
}