﻿using Fango.Core.Data;
using Grow.Data.DataSet.Caching;

namespace Grow.Data.DataSet.Adapter.Enemy
{
    public class EnemyFigureMasterDataRepositoryAdapter
    {
        public EnemyDataSet.EnemyFigureMasterRow[] GetDataByMobType(int mobType)
        {
            var rows = new DbAccessHandler().ExecuteAction(() =>
                {
                    var cacheTableAadapter = EnemyFigureMasterCacheTableAdapter.GetInstance();
                    return cacheTableAadapter.GetDataByMobType(mobType);
                });
            return rows;
        }
    }
}