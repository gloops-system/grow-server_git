﻿using Fango.Data.Caching;
using Fango.Core.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grow.Extensions.Core;
using Grow.Data.DataSet.MasterDataSetTableAdapters;

namespace Grow.Data.DataSet.Caching
{
    [Preload]
    public class AvatarMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static Dictionary<int, MasterDataSet.AvatarMasterRow> cacheGetDataByPK;
        private MasterDataSet.AvatarMasterRow[] allData;

        private static readonly AvatarMasterCacheTableAdapter Instance = new AvatarMasterCacheTableAdapter();
        private AvatarMasterCacheTableAdapter() 
        {
            Console.WriteLine("Initialize single tone");
        }

        public static AvatarMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new AvatarMasterTableAdapter())
            {
                allData = adapter.GetData().ToArray();
            }
            cacheGetDataByPK = allData.ToDictionary(x => x.AvatarId);
        }

        public MasterDataSet.AvatarMasterRow GetDataByAvatarId(int id)
        {
            if (CACHE_ENABLED)
                return GetDataByPKFromCache(cacheGetDataByPK, id);

            using (var adapter = new AvatarMasterTableAdapter())
            {
                return adapter.GetDataByPK(id).FirstOrDefault();
            }
        }

        public MasterDataSet.AvatarMasterRow[] GetAllAvatars()
        {
            if (CACHE_ENABLED)
                return GetDataFromCache(allData).ToArray();

            using (var adapter = new AvatarMasterTableAdapter())
            {
                return adapter.GetData().ToArray();
            }
        }
    }
}
