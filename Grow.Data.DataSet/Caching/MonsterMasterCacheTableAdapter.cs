﻿using Fango.Data.Caching;
using Fango.Core.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grow.Extensions.Core;
using Grow.Data.DataSet.MasterDataSetTableAdapters;

namespace Grow.Data.DataSet.Caching
{
    [Preload]
    public class MonsterMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static Dictionary<int, MasterDataSet.MonsterMasterRow> cacheGetDataByPK;
        private MasterDataSet.MonsterMasterRow[] allData;

        private static readonly MonsterMasterCacheTableAdapter Instance = new MonsterMasterCacheTableAdapter();
        private MonsterMasterCacheTableAdapter() 
        {
            Console.WriteLine("Initialize single tone");
        }

        public static MonsterMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new MonsterMasterTableAdapter())
            {
                allData = adapter.GetData().ToArray();
            }
            cacheGetDataByPK = allData.ToDictionary(x => x.MonsterId);
        }

        public MasterDataSet.MonsterMasterRow GetDataByMonsterId(int id)
        {
            if (CACHE_ENABLED)
                return GetDataByPKFromCache(cacheGetDataByPK, id);

            using (var adapter = new MonsterMasterTableAdapter())
            {
                return adapter.GetDataByPK(id).FirstOrDefault();
            }
        }

        public MasterDataSet.MonsterMasterRow[] GetAllMonsters()
        {
            if (CACHE_ENABLED)
                return GetDataFromCache(allData).ToArray();

            using (var adapter = new MonsterMasterTableAdapter())
            {
                return adapter.GetData().ToArray();
            }
        }
    }
}
