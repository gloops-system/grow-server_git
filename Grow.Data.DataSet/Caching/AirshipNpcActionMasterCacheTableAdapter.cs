﻿using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Grow.Data.DataSet.AirshipDataSetTableAdapters;
using Grow.Extensions.Core;

namespace Grow.Data.DataSet.Caching
{
    [Preload]
    internal class AirshipNpcActionMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static Dictionary<int, AirshipDataSet.AirshipNpcActionMasterRow[]> _cacheGetDataByNpcType;
        private AirshipDataSet.AirshipNpcActionMasterRow[] _allData;

        private readonly static AirshipNpcActionMasterCacheTableAdapter Instance = new AirshipNpcActionMasterCacheTableAdapter();
        private AirshipNpcActionMasterCacheTableAdapter() { }

        public static AirshipNpcActionMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new AirshipNpcActionMasterTableAdapter())
            {
                _allData = adapter.GetData().ToArray();
                _cacheGetDataByNpcType = _allData.GenerateCache(x => x.NpcType);
            }
        }

        public AirshipDataSet.AirshipNpcActionMasterRow[] GetData()
        {
            if (CACHE_ENABLED)
                return GetDataFromCache(_allData).ToArray();

            using (var adapter = new AirshipNpcActionMasterTableAdapter())
            {
                return adapter.GetData().ToArray();
            }
        }

        public AirshipDataSet.AirshipNpcActionMasterRow[] GetDataByNpcType(int npcType)
        {
            if (CACHE_ENABLED)
                return GetDataByFromCache(_cacheGetDataByNpcType, npcType);

            using (var adapter = new AirshipNpcActionMasterTableAdapter())
            {
                return adapter.GetDataByNpcType(npcType).ToArray();
            }
        }
    }
}