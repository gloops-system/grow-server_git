﻿using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Grow.Data.DataSet.GameCurrencyDataSetTableAdapters;
using Grow.Extensions.Core;

namespace Grow.Data.DataSet.Caching
{
    [Preload]
    internal class GameCurrencyMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static GameCurrencyDataSet.GameCurrencyMasterRow[] _allData;
        private static Dictionary<int, GameCurrencyDataSet.GameCurrencyMasterRow> _cacheGetDataByPK;

        private readonly static GameCurrencyMasterCacheTableAdapter Instance = new GameCurrencyMasterCacheTableAdapter();
        private GameCurrencyMasterCacheTableAdapter() { }

        public static GameCurrencyMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new GameCurrencyMasterTableAdapter())
            {
                _allData = adapter.GetData().ToArray();
                _cacheGetDataByPK = _allData.ToDictionary(x => x.Id);
            }
        }

        public GameCurrencyDataSet.GameCurrencyMasterRow[] GetData()
        {
            if (CACHE_ENABLED)
                return GetDataFromCache(_allData).ToArray();

            using (var adapter = new GameCurrencyMasterTableAdapter())
            {
                return adapter.GetData().ToArray();
            }
        }

        public GameCurrencyDataSet.GameCurrencyMasterRow GetDataByPK(int id)
        {
            if (CACHE_ENABLED)
                return GetDataByPKFromCache(_cacheGetDataByPK, id);

            using (var adapter = new GameCurrencyMasterTableAdapter())
            {
                return adapter.GetDataByPK(id).FirstOrDefault();
            }
        }
    }
}