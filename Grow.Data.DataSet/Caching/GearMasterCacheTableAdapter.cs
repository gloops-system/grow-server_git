﻿using Fango.Data.Caching;
using Fango.Core.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grow.Extensions.Core;
using Grow.Data.DataSet.MasterDataSetTableAdapters;

namespace Grow.Data.DataSet.Caching
{
    [Preload]
    public class GearMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static Dictionary<int, MasterDataSet.GearMasterRow> cacheGetDataByPK;
        private MasterDataSet.GearMasterRow[] allData;

        private static readonly GearMasterCacheTableAdapter Instance = new GearMasterCacheTableAdapter();
        private GearMasterCacheTableAdapter() 
        {
            Console.WriteLine("Initialize single tone");
        }

        public static GearMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new GearMasterTableAdapter())
            {
                allData = adapter.GetData().ToArray();
            }
            cacheGetDataByPK = allData.ToDictionary(x => x.GearId);
        }

        public MasterDataSet.GearMasterRow GetDataByGearId(int id)
        {
            if (CACHE_ENABLED)
                return GetDataByPKFromCache(cacheGetDataByPK, id);

            using (var adapter = new GearMasterTableAdapter())
            {
                return adapter.GetDataByPK(id).FirstOrDefault();
            }
        }

        public MasterDataSet.GearMasterRow[] GetAllGears()
        {
            if (CACHE_ENABLED)
                return GetDataFromCache(allData).ToArray();

            using (var adapter = new GearMasterTableAdapter())
            {
                return adapter.GetData().ToArray();
            }
        }
    }
}
