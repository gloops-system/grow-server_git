﻿using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Grow.Data.DataSet.SphereDataSetTableAdapters;
using Grow.Extensions.Core;

namespace Grow.Data.DataSet.Caching
{
    [Preload]
    internal class SphereGridPositionMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private readonly static SphereGridPositionMasterCacheTableAdapter Instance = new SphereGridPositionMasterCacheTableAdapter();
        private SphereGridPositionMasterCacheTableAdapter() { }

        private static SphereDataSet.SphereGridPositionMasterRow[] _cachedGetData;
        private static Dictionary<int, SphereDataSet.SphereGridPositionMasterRow> _cacheGetDataByPk;

        public static SphereGridPositionMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new SphereGridPositionMasterTableAdapter())
            {
                _cachedGetData = adapter.GetData().ToArray();
                _cacheGetDataByPk = _cachedGetData.ToDictionary(x => x.GridPosition);
            }
        }

        public SphereDataSet.SphereGridPositionMasterRow[] GetData()
        {
            if (CACHE_ENABLED)
                return _cachedGetData;

            using (var adapter = new SphereGridPositionMasterTableAdapter())
            {
                return adapter.GetData().ToArray();
            }
        }

        public SphereDataSet.SphereGridPositionMasterRow GetDataByPK(int gridId)
        {
            if (CACHE_ENABLED)
                return GetDataByPKFromCache(_cacheGetDataByPk, gridId);

            using (var adapter = new SphereGridPositionMasterTableAdapter())
            {
                return adapter.GetDataByPK(gridId).FirstOrDefault();
            }
        }
    }
}