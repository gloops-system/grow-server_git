﻿using System;
using System.Linq;
using Fango.Data.Caching;
using Grow.Data.DataSet.LoginBonusDataSetTableAdapters;
using Grow.Extensions.Core;

namespace Grow.Data.DataSet.Caching
{
    [Preload]
    internal class LoginBonusMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private LoginBonusDataSet.LoginBonusMasterRow[] _allData;

        private readonly static LoginBonusMasterCacheTableAdapter Instance = new LoginBonusMasterCacheTableAdapter();
        private LoginBonusMasterCacheTableAdapter() { }

        public static LoginBonusMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new LoginBonusMasterTableAdapter())
            {
                _allData = adapter.GetData().ToArray();
            }
        }

        public LoginBonusDataSet.LoginBonusMasterRow[] GetCurrentData(DateTimeOffset now)
        {
            if (CACHE_ENABLED)
                return GetDataFromCache(_allData).Where(x => now.InBetween(x.FromDate, x.ToDate)).ToArray();

            using (var adapter = new LoginBonusMasterTableAdapter())
            {
                return adapter.GetCurrentData(now).ToArray();
            }
        }
    }
}