﻿using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Grow.Data.DataSet.EnemyDataSetTableAdapters;
using Grow.Extensions.Core;

namespace Grow.Data.DataSet.Caching
{
    [Preload]
    internal class EnemyEquipSkillMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static readonly EnemyEquipSkillMasterCacheTableAdapter Instance = new EnemyEquipSkillMasterCacheTableAdapter();
        private EnemyEquipSkillMasterCacheTableAdapter() { }

        private static Dictionary<int, EnemyDataSet.EnemyEquipSkillMasterRow[]> _cacheGetDataByMobType;

        public static EnemyEquipSkillMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new EnemyEquipSkillMasterTableAdapter())
            {
                _cacheGetDataByMobType = adapter.GetData().GenerateCache(x => x.MobType);
            }
        }

        public EnemyDataSet.EnemyEquipSkillMasterRow[] GetDataByMobType(int mobType)
        {
            if (CACHE_ENABLED)
                return GetDataByFromCache(_cacheGetDataByMobType, mobType);

            using (var adapter = new EnemyEquipSkillMasterTableAdapter())
            {
                return adapter.GetDataByMobType(mobType).ToArray();
            }
        }
    }
}