﻿using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Grow.Data.DataSet.LoginBonusDataSetTableAdapters;
using Grow.Extensions.Core;

namespace Grow.Data.DataSet.Caching
{
    [Preload]
    internal class LoginBonusModeMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static Dictionary<int, LoginBonusDataSet.LoginBonusModeMasterRow[]> _cacheGetDataByModeId; 
        private LoginBonusDataSet.LoginBonusModeMasterRow[] _allData;

        private readonly static LoginBonusModeMasterCacheTableAdapter Instance = new LoginBonusModeMasterCacheTableAdapter();
        private LoginBonusModeMasterCacheTableAdapter() { }

        public static LoginBonusModeMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new LoginBonusModeMasterTableAdapter())
            {
                _allData = adapter.GetData().ToArray();
                _cacheGetDataByModeId = _allData.GenerateCache(x => x.ModeId);
            }
        }

        public LoginBonusDataSet.LoginBonusModeMasterRow[] GetDataByModeId(int modeId)
        {
            if (CACHE_ENABLED)
                return GetDataByFromCache(_cacheGetDataByModeId, modeId);

            using (var adapter = new LoginBonusModeMasterTableAdapter())
            {
                return adapter.GetDataByModeId(modeId).ToArray();
            }
        }
    }
}