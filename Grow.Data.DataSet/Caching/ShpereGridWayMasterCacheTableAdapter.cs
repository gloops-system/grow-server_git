﻿using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Grow.Data.DataSet.SphereDataSetTableAdapters;
using Grow.Extensions.Core;

namespace Grow.Data.DataSet.Caching
{
    [Preload]
    internal class SphereGridWayMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private readonly static SphereGridWayMasterCacheTableAdapter Instance = new SphereGridWayMasterCacheTableAdapter();
        private SphereGridWayMasterCacheTableAdapter() { }

        private static Dictionary<int, SphereDataSet.SphereGridWayMasterRow[]> _cacheGetDataByGrid1;

        public static SphereGridWayMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new SphereGridWayMasterTableAdapter())
            {
                _cacheGetDataByGrid1 = adapter.GetData().GenerateCache(x => x.JobId);
            }
        }

        public SphereDataSet.SphereGridWayMasterRow[] GetDataByGrid1(int gridId1)
        {
            if (CACHE_ENABLED)
                return GetDataByFromCache(_cacheGetDataByGrid1, gridId1);

            using (var adapter = new SphereGridWayMasterTableAdapter())
            {
                return adapter.GetDataByGrid1(gridId1).ToArray();
            }
        }
    }
}