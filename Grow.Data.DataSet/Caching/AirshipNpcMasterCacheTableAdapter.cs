﻿using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Grow.Data.DataSet.AirshipDataSetTableAdapters;
using Grow.Extensions.Core;

namespace Grow.Data.DataSet.Caching
{
    [Preload]
    internal class AirshipNpcMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static Dictionary<int, AirshipDataSet.AirshipNpcMasterRow[]> _cacheGetDataByAirshipId;
        private AirshipDataSet.AirshipNpcMasterRow[] _allData;

        private readonly static AirshipNpcMasterCacheTableAdapter Instance = new AirshipNpcMasterCacheTableAdapter();
        private AirshipNpcMasterCacheTableAdapter() { }

        public static AirshipNpcMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new AirshipNpcMasterTableAdapter())
            {
                _allData = adapter.GetData().ToArray();
                _cacheGetDataByAirshipId = _allData.GenerateCache(x => x.AirshipId);
            }
        }

        public AirshipDataSet.AirshipNpcMasterRow[] GetData()
        {
            if (CACHE_ENABLED)
                return GetDataFromCache(_allData).ToArray();

            using (var adapter = new AirshipNpcMasterTableAdapter())
            {
                return adapter.GetData().ToArray();
            }
        }

        public AirshipDataSet.AirshipNpcMasterRow[] GetDataByAirshipId(int airshipId)
        {
            if (CACHE_ENABLED)
                return GetDataByFromCache(_cacheGetDataByAirshipId, airshipId);

            using (var adapter = new AirshipNpcMasterTableAdapter())
            {
                return adapter.GetDataByAirshipId(airshipId).ToArray();
            }
        }
    }
}