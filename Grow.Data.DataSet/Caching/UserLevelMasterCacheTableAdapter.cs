﻿using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Grow.Data.DataSet.MasterDataSetTableAdapters;
using Grow.Extensions.Core;

namespace Grow.Data.DataSet.Caching
{
    [Preload]
    internal class UserLevelMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static Dictionary<short, MasterDataSet.UserLevelMasterRow> _cacheGetDataByPk;
        private static MasterDataSet.UserLevelMasterRow[] _allData;

        private readonly static UserLevelMasterCacheTableAdapter Instance = new UserLevelMasterCacheTableAdapter();
        private UserLevelMasterCacheTableAdapter() { }

        public static UserLevelMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new UserLevelMasterTableAdapter())
            {
                _allData = adapter.GetData().ToArray();
                _cacheGetDataByPk = _allData.ToDictionary(x => x.Level);
            }
        }

        public MasterDataSet.UserLevelMasterRow[] GetData()
        {
            if (CACHE_ENABLED)
                return GetDataFromCache(_allData).ToArray();

            using (var adapter = new UserLevelMasterTableAdapter())
            {
                return adapter.GetData().ToArray();
            }
        }

        public MasterDataSet.UserLevelMasterRow GetDataByPK(short level)
        {
            if (CACHE_ENABLED)
                return GetDataByPKFromCache(_cacheGetDataByPk, level);

            using (var adapter = new UserLevelMasterTableAdapter())
            {
                return adapter.GetDataByPK(level).FirstOrDefault();
            }
        }
    }
}