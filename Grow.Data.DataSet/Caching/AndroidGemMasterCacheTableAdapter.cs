﻿using Fango.Data.Caching;
using Grow.Extensions.Core;
using Grow.Data.DataSet.MasterDataSetTableAdapters;
using Grow.Data.DataSet;
using System.Collections.Generic;
using System.Linq;

namespace Grow.Data.DataSet.Caching
{
    [Preload]
    internal class AndroidGemMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static Dictionary<string, MasterDataSet.AndroidGemMasterRow> _cacheGetDataByPk;
        private MasterDataSet.AndroidGemMasterRow[] _allData;

        private readonly static AndroidGemMasterCacheTableAdapter Instance = new AndroidGemMasterCacheTableAdapter();
        private AndroidGemMasterCacheTableAdapter() { }
        public static AndroidGemMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        private IEnumerable<MasterDataSet.AndroidGemMasterRow> Data()
        {
            using (var adapter = new AndroidGemMasterTableAdapter())
            {
                return adapter.GetData();
            }
        }

        protected override void InitializeCache()
        {
            using (var adapter = new AndroidGemMasterTableAdapter())
            {
                _allData = adapter.GetData().ToArray();
            }

            _cacheGetDataByPk = _allData.ToDictionary(x => x.ProductId);
        }

        public MasterDataSet.AndroidGemMasterRow[] GetData()
        {
            if (CACHE_ENABLED)
                return GetDataFromCache(_allData).ToArray();

            using (var adapter = new AndroidGemMasterTableAdapter())
            {
                return adapter.GetData().ToArray();
            }
        }

        public MasterDataSet.AndroidGemMasterRow GetDataByPk(string productId)
        {
            if (CACHE_ENABLED)
                return GetDataByPKFromCache(_cacheGetDataByPk, productId);

            using (var adapter = new AndroidGemMasterTableAdapter())
            {
                return adapter.GetDataByPK(productId).FirstOrDefault();
            }
        }
    }
}
