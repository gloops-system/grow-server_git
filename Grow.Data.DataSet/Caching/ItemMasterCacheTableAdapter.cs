﻿using Fango.Data.Caching;
using Fango.Core.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grow.Extensions.Core;
using Grow.Data.DataSet.MasterDataSetTableAdapters;

namespace Grow.Data.DataSet.Caching
{
    [Preload]
    public class ItemMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static Dictionary<int, MasterDataSet.ItemMasterRow> cacheGetDataByPK;
        private MasterDataSet.ItemMasterRow[] allData;

        private static readonly ItemMasterCacheTableAdapter Instance = new ItemMasterCacheTableAdapter();
        private ItemMasterCacheTableAdapter() 
        {
            Console.WriteLine("Initialize single tone");
        }

        public static ItemMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new ItemMasterTableAdapter())
            {
                allData = adapter.GetData().ToArray();
            }
            cacheGetDataByPK = allData.ToDictionary(x => x.ItemId);
        }

        public MasterDataSet.ItemMasterRow GetDataByItemId(int id)
        {
            if (CACHE_ENABLED)
                return GetDataByPKFromCache(cacheGetDataByPK, id);

            using (var adapter = new ItemMasterTableAdapter())
            {
                return adapter.GetDataByPK(id).FirstOrDefault();
            }
        }

        public MasterDataSet.ItemMasterRow[] GetAllItems()
        {
            if (CACHE_ENABLED)
                return GetDataFromCache(allData).ToArray();

            using (var adapter = new ItemMasterTableAdapter())
            {
                return adapter.GetData().ToArray();
            }
        }
    }
}
