﻿using Fango.Data.Caching;
using Grow.Extensions.Core;
using Grow.Data.DataSet.MasterDataSetTableAdapters;
using Grow.Data.DataSet;
using System.Collections.Generic;
using System.Linq;

namespace Grow.Data.DataSet.Caching
{
    [Preload]
    internal class GemMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static Dictionary<string, MasterDataSet.GemMasterRow> _cacheGetDataByPk;
        private MasterDataSet.GemMasterRow[] _allData;

        private readonly static GemMasterCacheTableAdapter Instance = new GemMasterCacheTableAdapter();
        private GemMasterCacheTableAdapter() { }
        public static GemMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new GemMasterTableAdapter())
            {
                _allData = adapter.GetData().ToArray();
            }

            _cacheGetDataByPk = _allData.ToDictionary(x => x.ProductId);
        }

        public MasterDataSet.GemMasterRow[] GetData()
        {
            if (CACHE_ENABLED)
                return GetDataFromCache(_allData).ToArray();

            using (var adapter = new GemMasterTableAdapter())
            {
                return adapter.GetData().ToArray();
            }
        }

        public MasterDataSet.GemMasterRow GetDataByPk(string productId)
        {
            if (CACHE_ENABLED)
                return GetDataByPKFromCache(_cacheGetDataByPk, productId);

            using (var adapter = new GemMasterTableAdapter())
            {
                return adapter.GetDataByPK(productId).FirstOrDefault();
            }
        }
    }
}
