﻿using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Grow.Data.DataSet.LoginBonusDataSetTableAdapters;
using Grow.Extensions.Core;

namespace Grow.Data.DataSet.Caching
{
    [Preload]
    internal class LoginBonusRankMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static Dictionary<int, LoginBonusDataSet.LoginBonusRankMasterRow[]> _cacheGetDataByMode;
        private LoginBonusDataSet.LoginBonusRankMasterRow[] _allData;

        private readonly static LoginBonusRankMasterCacheTableAdapter Instance = new LoginBonusRankMasterCacheTableAdapter();
        private LoginBonusRankMasterCacheTableAdapter() { }

        public static LoginBonusRankMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new LoginBonusRankMasterTableAdapter())
            {
                _allData = adapter.GetData().ToArray();
                _cacheGetDataByMode = _allData.GenerateCache(x => x.Mode);
            }
        }

        public LoginBonusDataSet.LoginBonusRankMasterRow[] GetDataByMode(int mode)
        {
            if (CACHE_ENABLED)
                return GetDataByFromCache(_cacheGetDataByMode, mode);

            using (var adapter = new LoginBonusRankMasterTableAdapter())
            {
                return adapter.GetDataByMode(mode).ToArray();
            }
        }
    }
}