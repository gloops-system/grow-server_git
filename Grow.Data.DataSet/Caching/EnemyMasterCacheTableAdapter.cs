﻿using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Grow.Data.DataSet.EnemyDataSetTableAdapters;
using Grow.Extensions.Core;

namespace Grow.Data.DataSet.Caching
{
    [Preload]
    internal class EnemyMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static readonly EnemyMasterCacheTableAdapter Instance = new EnemyMasterCacheTableAdapter();
        private EnemyMasterCacheTableAdapter() { }

        private static Dictionary<int, EnemyDataSet.EnemyMasterRow> _cacheGetDataByPK;

        public static EnemyMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new EnemyMasterTableAdapter())
            {
                _cacheGetDataByPK = adapter.GetData().ToDictionary(x => x.EnemyId);
            }
        }

        public EnemyDataSet.EnemyMasterRow GetDataByPK(int enemyId)
        {
            if (CACHE_ENABLED)
                return GetDataByPKFromCache(_cacheGetDataByPK, enemyId);

            using (var adapter = new EnemyMasterTableAdapter())
            {
                return adapter.GetDataByPK(enemyId).FirstOrDefault();
            }
        }
    }
}