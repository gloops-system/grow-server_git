﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Grow.Data.DataSet.ShopDataSetTableAdapters;
using Grow.Extensions.Core;

namespace Grow.Data.DataSet.Caching
{
    [Preload]
    internal class ShopItemMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static Dictionary<int, ShopDataSet.ShopItemMasterRow> _cacheGetDataByPK;
        private static Dictionary<int, ShopDataSet.ShopItemMasterRow[]> _cacheGetCurrentDataByShopId;

        private readonly static ShopItemMasterCacheTableAdapter Instance = new ShopItemMasterCacheTableAdapter();
        private ShopItemMasterCacheTableAdapter() { }

        public static ShopItemMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new ShopItemMasterTableAdapter())
            {
                var allData = adapter.GetData().ToArray();
                _cacheGetCurrentDataByShopId = allData.GenerateCache(x => x.ShopId);
                _cacheGetDataByPK = allData.ToDictionary(x => x.ShopItemId);
            }
        }

        public ShopDataSet.ShopItemMasterRow[] GetCurrentDataByShopId(int shopId, DateTimeOffset now)
        {
            if (CACHE_ENABLED)
                return GetDataByFromCache(_cacheGetCurrentDataByShopId, shopId)
                    .Where(x => now.InBetween(x.FromDate, x.ToDate))
                    .ToArray();

            using (var adapter = new ShopItemMasterTableAdapter())
            {
                return adapter.GetCurrentDataByShopId(shopId, now).ToArray();
            }
        }

        public ShopDataSet.ShopItemMasterRow GetDataByPK(int shopItemId)
        {
            if (CACHE_ENABLED)
                return GetDataByPKFromCache(_cacheGetDataByPK, shopItemId);

            using (var adapter = new ShopItemMasterTableAdapter())
            {
                return adapter.GetDataByPK(shopItemId).FirstOrDefault();
            }
        }
    }
}