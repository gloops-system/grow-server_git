﻿using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Grow.Data.DataSet.SphereDataSetTableAdapters;
using Grow.Extensions.Core;

namespace Grow.Data.DataSet.Caching
{
    [Preload]
    internal class SphereGridMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private readonly static SphereGridMasterCacheTableAdapter Instance = new SphereGridMasterCacheTableAdapter();
        private SphereGridMasterCacheTableAdapter() { }

        private static Dictionary<int, SphereDataSet.SphereGridMasterRow> _cacheGetDataByPk;
        private static Dictionary<int, SphereDataSet.SphereGridMasterRow[]> _cacheGetDataByJobId;

        public static SphereGridMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new SphereGridMasterTableAdapter())
            {
                var allData = adapter.GetData();
                _cacheGetDataByPk = allData.ToDictionary(x => x.GridId);
                _cacheGetDataByJobId = allData.GenerateCache(x => x.JobId);
            }
        }

        public SphereDataSet.SphereGridMasterRow GetDataByPK(int gridId)
        {
            if (CACHE_ENABLED)
                return GetDataByPKFromCache(_cacheGetDataByPk, gridId);

            using (var adapter = new SphereGridMasterTableAdapter())
            {
                return adapter.GetDataByPK(gridId).FirstOrDefault();
            }
        }

        public SphereDataSet.SphereGridMasterRow[] GetDataByJobId(int jobId)
        {
            if (CACHE_ENABLED)
                return GetDataByFromCache(_cacheGetDataByJobId, jobId);

            using (var adapter = new SphereGridMasterTableAdapter())
            {
                return adapter.GetDataByJobId(jobId).ToArray();
            }
        }
    }
}