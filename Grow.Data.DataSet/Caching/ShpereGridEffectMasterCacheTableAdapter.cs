﻿using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Grow.Data.DataSet.SphereDataSetTableAdapters;
using Grow.Extensions.Core;

namespace Grow.Data.DataSet.Caching
{
    [Preload]
    internal class SphereGridEffectMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private readonly static SphereGridEffectMasterCacheTableAdapter Instance = new SphereGridEffectMasterCacheTableAdapter();
        private SphereGridEffectMasterCacheTableAdapter() { }

        private static Dictionary<long, SphereDataSet.SphereGridEffectMasterRow> _cacheGetDataByPk;

        public static SphereGridEffectMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new SphereGridEffectMasterTableAdapter())
            {
                _cacheGetDataByPk = adapter.GetData().ToDictionary(x => x.EffectId);
            }
        }

        public SphereDataSet.SphereGridEffectMasterRow GetDataByPK(int effectId)
        {
            if (CACHE_ENABLED)
                return GetDataByPKFromCache(_cacheGetDataByPk, effectId);

            using (var adapter = new SphereGridEffectMasterTableAdapter())
            {
                return adapter.GetDataByPK(effectId).FirstOrDefault();
            }
        }
    }
}