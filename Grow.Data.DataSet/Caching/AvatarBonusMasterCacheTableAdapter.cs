﻿using Fango.Data.Caching;
using Fango.Core.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grow.Extensions.Core;
using Grow.Data.DataSet.MasterDataSetTableAdapters;

namespace Grow.Data.DataSet.Caching
{
    [Preload]
    public class AvatarBonusMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static Dictionary<int, MasterDataSet.AvatarBonusMasterRow> cacheGetDataByPK;
        private MasterDataSet.AvatarBonusMasterRow[] allData;

        private static readonly AvatarBonusMasterCacheTableAdapter Instance = new AvatarBonusMasterCacheTableAdapter();
        private AvatarBonusMasterCacheTableAdapter() 
        {
            Console.WriteLine("Initialize single tone");
        }

        public static AvatarBonusMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new AvatarBonusMasterTableAdapter())
            {
                allData = adapter.GetData().ToArray();
            }
            cacheGetDataByPK = allData.ToDictionary(x => x.AvatarId);
        }

        public MasterDataSet.AvatarBonusMasterRow GetDataByAvatarId(int id)
        {
            if (CACHE_ENABLED)
                return GetDataByPKFromCache(cacheGetDataByPK, id);

            using (var adapter = new AvatarBonusMasterTableAdapter())
            {
                return adapter.GetDataByPK(id).FirstOrDefault();
            }
        }

        public MasterDataSet.AvatarBonusMasterRow[] GetAllAvatars()
        {
            if (CACHE_ENABLED)
                return GetDataFromCache(allData).ToArray();

            using (var adapter = new AvatarBonusMasterTableAdapter())
            {
                return adapter.GetData().ToArray();
            }
        }
    }
}
