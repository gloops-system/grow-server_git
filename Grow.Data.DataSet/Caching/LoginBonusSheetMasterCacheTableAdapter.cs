﻿using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Grow.Data.DataSet.LoginBonusDataSetTableAdapters;
using Grow.Extensions.Core;

namespace Grow.Data.DataSet.Caching
{
    [Preload]
    internal class LoginBonusSheetMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static Dictionary<int, LoginBonusDataSet.LoginBonusSheetMasterRow[]> _cacheGetDataBySheetId;
        private LoginBonusDataSet.LoginBonusSheetMasterRow[] _allData;

        private readonly static LoginBonusSheetMasterCacheTableAdapter Instance = new LoginBonusSheetMasterCacheTableAdapter();
        private LoginBonusSheetMasterCacheTableAdapter() { }

        public static LoginBonusSheetMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new LoginBonusSheetMasterTableAdapter())
            {
                _allData = adapter.GetData().ToArray();
                _cacheGetDataBySheetId = _allData.GenerateCache(x => x.SheetId);
            }
        }

        public LoginBonusDataSet.LoginBonusSheetMasterRow[] GetDataBySheetId(int sheetId)
        {
            if (CACHE_ENABLED)
                return GetDataByFromCache(_cacheGetDataBySheetId, sheetId);

            using (var adapter = new LoginBonusSheetMasterTableAdapter())
            {
                return adapter.GetDataBySheetId(sheetId).ToArray();
            }
        }
    }
}