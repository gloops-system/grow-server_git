﻿using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Grow.Data.DataSet.ShopDataSetTableAdapters;
using Grow.Extensions.Core;

namespace Grow.Data.DataSet.Caching
{
    [Preload]
    internal class ShopMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static Dictionary<int, ShopDataSet.ShopMasterRow> _cacheGetDataByPK;
        private readonly static ShopMasterCacheTableAdapter Instance = new ShopMasterCacheTableAdapter();

        private ShopMasterCacheTableAdapter() { }
        public static ShopMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new ShopMasterTableAdapter())
            {
                _cacheGetDataByPK = adapter.GetData().ToDictionary(x => x.ShopId);
            }
        }

        public ShopDataSet.ShopMasterRow GetDataByPK(int shopId)
        {
            if (CACHE_ENABLED)
                return GetDataByPKFromCache(_cacheGetDataByPK, shopId);

            using (var adapter = new ShopMasterTableAdapter())
            {
                return adapter.GetDataByPK(shopId).FirstOrDefault();
            }
        }
    }
}