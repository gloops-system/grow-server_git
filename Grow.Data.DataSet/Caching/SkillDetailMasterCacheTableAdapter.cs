﻿using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Messiah.Data.DataSet.SkillDataSetTableAdapters;
using Messiah.Extensions.Core;

namespace Messiah.Data.DataSet.Caching
{
    [Preload]
    internal class SkillDetailMasterCacheTableAdapter : MessiahRefreshableCacheTableAdapter
    {
        private static Dictionary<int, SkillDataSet.SkillDetailMasterRow[]> _cacheGetDataBySkillId;
        private static SkillDataSet.SkillDetailMasterRow[] _allData;

        private readonly static SkillDetailMasterCacheTableAdapter Instance = new SkillDetailMasterCacheTableAdapter();
        private SkillDetailMasterCacheTableAdapter() { }

        public static SkillDetailMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new SkillDetailMasterTableAdapter())
            {
                _allData = adapter.GetData().ToArray();
                _cacheGetDataBySkillId = _allData.GenerateCache(x => x.SkillId);
            }
        }

        public SkillDataSet.SkillDetailMasterRow[] GetDataBySkillId(int skillId)
        {
            if (CACHE_ENABLED)
                return GetDataByFromCache(_cacheGetDataBySkillId, skillId);

            using (var adapter = new SkillDetailMasterTableAdapter())
            {
                return adapter.GetDataBySkillId(skillId).ToArray();
            }
        }

        public SkillDataSet.SkillDetailMasterRow[] GetData()
        {
            if (CACHE_ENABLED)
                return GetDataFromCache(_allData).ToArray();

            using (var adapter = new SkillDetailMasterTableAdapter())
            {
                return adapter.GetData().ToArray();
            }
        }
    }
}