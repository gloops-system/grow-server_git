﻿using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Messiah.Data.DataSet.SkillDataSetTableAdapters;
using Messiah.Extensions.Core;

namespace Messiah.Data.DataSet.Caching
{
    [Preload]
    internal class SkillSetMasterCacheTableAdapter : MessiahRefreshableCacheTableAdapter
    {
        private readonly static SkillSetMasterCacheTableAdapter Instance = new SkillSetMasterCacheTableAdapter();
        private SkillSetMasterCacheTableAdapter() { }

        private static Dictionary<int, SkillDataSet.SkillSetMasterRow[]> _cacheGetDataBySkillSetId;

        public static SkillSetMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new SkillSetMasterTableAdapter())
            {
                _cacheGetDataBySkillSetId = adapter.GetData().GenerateCache(x => x.SkillSetId);
            }
        }

        public SkillDataSet.SkillSetMasterRow[] GetDataBySkillSetId(int skillSetId)
        {
            if (CACHE_ENABLED)
                return GetDataByFromCache(_cacheGetDataBySkillSetId, skillSetId);

            using (var adapter = new SkillSetMasterTableAdapter())
            {
                return adapter.GetDataBySkillSetId(skillSetId).ToArray();
            }
        }
    }
}