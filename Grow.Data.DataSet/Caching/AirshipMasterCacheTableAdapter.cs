﻿using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Grow.Data.DataSet.AirshipDataSetTableAdapters;
using Grow.Extensions.Core;

namespace Grow.Data.DataSet.Caching
{
    [Preload]
    internal class AirshipMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static Dictionary<int, AirshipDataSet.AirshipMasterRow> _cacheGetDataByPK;
        private AirshipDataSet.AirshipMasterRow[] _allData;

        private readonly static AirshipMasterCacheTableAdapter Instance = new AirshipMasterCacheTableAdapter();
        private AirshipMasterCacheTableAdapter () { }

        public static AirshipMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new AirshipMasterTableAdapter())
            {
                _allData = adapter.GetData().ToArray();
                _cacheGetDataByPK = _allData.ToDictionary(x => x.AirshipId);
            }
        }

        public AirshipDataSet.AirshipMasterRow[] GetData()
        {
            if (CACHE_ENABLED)
                return GetDataFromCache(_allData).ToArray();

            using (var adapter = new AirshipMasterTableAdapter())
            {
                return adapter.GetData().ToArray();
            }
        }

        public AirshipDataSet.AirshipMasterRow GetDataByPK(int airshipId)
        {
            if (CACHE_ENABLED)
                return GetDataByPKFromCache(_cacheGetDataByPK, airshipId);

            using (var adapter = new AirshipMasterTableAdapter())
            {
                return adapter.GetDataByPK(airshipId).FirstOrDefault();
            }
        }
    }
}