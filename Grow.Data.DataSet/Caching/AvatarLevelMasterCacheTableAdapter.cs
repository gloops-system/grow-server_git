﻿using Fango.Data.Caching;
using Fango.Core.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grow.Extensions.Core;
using Grow.Data.DataSet.MasterDataSetTableAdapters;

namespace Grow.Data.DataSet.Caching
{
    [Preload]
    public class AvatarLevelMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static Dictionary<int, MasterDataSet.AvatarLevelMasterRow[]> cacheGetDataByAvatarId;
        private MasterDataSet.AvatarLevelMasterRow[] allData;

        private static readonly AvatarLevelMasterCacheTableAdapter Instance = new AvatarLevelMasterCacheTableAdapter();
        private AvatarLevelMasterCacheTableAdapter()
        {
            Console.WriteLine("Initialize single tone");
        }

        public static AvatarLevelMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new AvatarLevelMasterTableAdapter())
            {
                allData = adapter.GetData().ToArray();
            }
            cacheGetDataByAvatarId = allData.GroupBy(x => x.AvatarId).ToDictionary(x => x.FirstOrDefault().AvatarId, x => x.ToArray());
        }

        public MasterDataSet.AvatarLevelMasterRow[] GetDataByAvatarId(int id)
        {
            if (CACHE_ENABLED)
                return GetDataByFromCache(cacheGetDataByAvatarId, id);

            using (var adapter = new AvatarLevelMasterTableAdapter())
            {
                return adapter.GetDataByPK(id).ToArray();
            }
        }

        public MasterDataSet.AvatarLevelMasterRow[] GetAllData()
        {
            if (CACHE_ENABLED)
                return GetDataFromCache(allData).ToArray();

            using (var adapter = new AvatarLevelMasterTableAdapter())
            {
                return adapter.GetData().ToArray();
            }
        }
    }
}
