﻿using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Grow.Data.DataSet.SkillDataSetTableAdapters;
using Grow.Extensions.Core;

namespace Grow.Data.DataSet.Caching
{
    [Preload]
    internal class SkillMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private readonly static SkillMasterCacheTableAdapter Instance = new SkillMasterCacheTableAdapter();
        private SkillMasterCacheTableAdapter() { }

        private static Dictionary<int, SkillDataSet.SkillMasterRow> _cacheGetDataByPk;

        public static SkillMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new SkillMasterTableAdapter())
            {
                _cacheGetDataByPk = adapter.GetData().ToDictionary(x => x.SkillId);
            }
        }

        public SkillDataSet.SkillMasterRow GetDataByPK(int skillId)
        {
            if (CACHE_ENABLED)
                return GetDataByPKFromCache(_cacheGetDataByPk, skillId);

            using (var adapter = new SkillMasterTableAdapter())
            {
                return adapter.GetDataByPK(skillId).FirstOrDefault();
            }
        }
    }
}