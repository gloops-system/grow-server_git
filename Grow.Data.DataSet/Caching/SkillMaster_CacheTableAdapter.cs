﻿using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Messiah.Data.DataSet.SkillDataSetTableAdapters;
using Messiah.Extensions.Core;

namespace Messiah.Data.DataSet.Caching
{
    [Preload]
    internal class SkillMaster_CacheTableAdapter : MessiahRefreshableCacheTableAdapter
    {
        private readonly static SkillMaster_CacheTableAdapter Instance = new SkillMaster_CacheTableAdapter();
        private SkillMaster_CacheTableAdapter() { }

        private static Dictionary<int, SkillDataSet.SkillMaster_Row> _cacheGetDataByPk;

        public static SkillMaster_CacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new SkillMaster_TableAdapter())
            {
                _cacheGetDataByPk = adapter.GetData().ToDictionary(x => x.SkillId);
            }
        }

        public SkillDataSet.SkillMaster_Row GetDataByPK(int skillId)
        {
            if (CACHE_ENABLED)
                return GetDataByPKFromCache(_cacheGetDataByPk, skillId);

            using (var adapter = new SkillMaster_TableAdapter())
            {
                return adapter.GetDataByPK(skillId).FirstOrDefault();
            }
        }
    }
}