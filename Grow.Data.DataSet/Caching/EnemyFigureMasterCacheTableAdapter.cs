﻿using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Grow.Data.DataSet.EnemyDataSetTableAdapters;
using Grow.Extensions.Core;

namespace Grow.Data.DataSet.Caching
{
    [Preload]
    internal class EnemyFigureMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static readonly EnemyFigureMasterCacheTableAdapter Instance = new EnemyFigureMasterCacheTableAdapter();
        private EnemyFigureMasterCacheTableAdapter() { }

        private static Dictionary<int, EnemyDataSet.EnemyFigureMasterRow[]> _cacheGetDataByMobType;

        public static EnemyFigureMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new EnemyFigureMasterTableAdapter())
            {
                _cacheGetDataByMobType = adapter.GetData().GenerateCache(x => x.MobType);
            }
        }

        public EnemyDataSet.EnemyFigureMasterRow[] GetDataByMobType(int mobType)
        {
            if (CACHE_ENABLED)
                return GetDataByFromCache(_cacheGetDataByMobType, mobType);

            using (var adapter = new EnemyFigureMasterTableAdapter())
            {
                return adapter.GetDataByMobType(mobType).ToArray();
            }
        }
    }
}