﻿using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Grow.Data.DataSet.AirshipDataSetTableAdapters;
using Grow.Extensions.Core;

namespace Grow.Data.DataSet.Caching
{
    [Preload]
    internal class AirshipFloorMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static Dictionary<int, AirshipDataSet.AirshipFloorMasterRow[]> _cacheGetDataByAirshipId;
        private AirshipDataSet.AirshipFloorMasterRow[] _allData;

        private static readonly AirshipFloorMasterCacheTableAdapter Instance = new AirshipFloorMasterCacheTableAdapter();
        private AirshipFloorMasterCacheTableAdapter() { }

        public static AirshipFloorMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new AirshipFloorMasterTableAdapter())
            {
                _allData = adapter.GetData().ToArray();
                _cacheGetDataByAirshipId = _allData.GenerateCache(x => x.AirshipId);
            }
        }

        public AirshipDataSet.AirshipFloorMasterRow[] GetData()
        {
            if (CACHE_ENABLED)
                return GetDataFromCache(_allData).ToArray();

            using (var adapter = new AirshipFloorMasterTableAdapter())
            {
                return adapter.GetData().ToArray();
            }
        }

        public AirshipDataSet.AirshipFloorMasterRow[] GetDataByAirshipId(int airshipId)
        {
            if (CACHE_ENABLED)
                return GetDataByFromCache(_cacheGetDataByAirshipId, airshipId);

            using (var adapter = new AirshipFloorMasterTableAdapter())
            {
                return adapter.GetDataByAirshipId(airshipId).ToArray();
            }
        }
    }
}