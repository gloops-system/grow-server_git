﻿namespace Sharding
{
    public class Shard
    {
        public int Id { get; set; }
        public string ConnectionString { get; set; }
    }
}