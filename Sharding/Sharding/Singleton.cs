﻿using System;

namespace Sharding
{
    public class Singleton<T>
    {
        private static  Lazy<T> _singleton = null;

        public static void Initialize(Func<T> func)
        {
            if (_singleton != null)
                throw new InvalidOperationException();
            _singleton = new Lazy<T>(func);
        }

        public static T Instance
        {
            get { return _singleton.Value; }
        }

        public static T GetInstance()
        {
            return _singleton.Value;
        }
    }
}
