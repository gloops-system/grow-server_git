﻿using System;
using System.Linq;
using ShardingSection;

namespace Sharding
{
    public class ShardingManager : Singleton<Manager>
    {

    }

    public class Manager
    {
        public Client Client { get; private set; }

        private Client _tempClient;
        private readonly object _tempClientLock = new object();

        public void Add(Shard shard)
        {
            lock (_tempClientLock)
            {
                if (_tempClient == null) _tempClient = new Client();
                _tempClient.ShardList.Add(shard);
            }
        }

        public void Add(Range range)
        {
            lock (_tempClientLock)
            {
                if (_tempClient == null) _tempClient = new Client();
                _tempClient.RangeList.Add(range);
            }
        }

        public bool Commit()
        {
            lock (_tempClientLock)
            {
                // get exclusive lock 
                // sort range
                _tempClient.RangeList = _tempClient.RangeList.OrderBy(_ => _.Start).ToArray();

                // check range and more
                Client = _tempClient;
                _tempClient = null;
            }
            return true;
        }

        public Client LoadFromSection(string sectionName)
        {
            lock (_tempClientLock)
            {
                var settings = System.Configuration.ConfigurationManager.GetSection(sectionName) as ShardingSettings;
                if (settings == null)
                    throw new InvalidOperationException(string.Format("section not found: {0}", sectionName));
                foreach (ShardElement s in settings.Shard)
                    Add(new Shard {Id = s.Id, ConnectionString = s.ConnectionString});

                foreach (RangeElement s in settings.Range)
                {
                    var shard = _tempClient.ShardList.First(_ => _.Id == s.Shard);
                    Add(new Range {Id = s.Id, Start = s.Start, End = s.End, Shard = shard});
                }
                Commit();
            }
            return Client;
        }

        // TODO load from database
        public Client Load(string connectionString)
        {
            throw new NotImplementedException();
        }
    }
}