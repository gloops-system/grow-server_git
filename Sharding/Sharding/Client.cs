﻿using System.Collections.Generic;
using System.Linq;

namespace Sharding
{
    public class Client
    {
        internal IList<Shard> ShardList;
        internal IList<Range> RangeList;

        // should be singletone
        public Client()
        {
            ShardList = new List<Shard>();
            RangeList = new List<Range>();
        }

        public IReadOnlyList<Shard> Shards
        {
            get { return (IReadOnlyList<Shard>)ShardList; }
        }

        public IReadOnlyList<Range> Ranges
        {
            get { return (IReadOnlyList<Range>)RangeList; }
        }

        public string GetConnectionString(long shardKey)
        {
            var range = RangeList.First(_ => _.Start <= shardKey && _.End > shardKey);

            // TODO default shard
            return range.Shard.ConnectionString;
        }
    }
}