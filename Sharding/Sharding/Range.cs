﻿namespace Sharding
{
    // [Start, End] = Start ≤ x < End
    public class Range
    {
        public int Id { get; set; }
        public long Start { get; set; }
        public long End { get; set; }
        public Shard Shard { get; set; }
    }
}
