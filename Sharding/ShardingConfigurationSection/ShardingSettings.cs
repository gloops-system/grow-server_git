﻿using System;
using System.Configuration;

namespace ShardingSection
{
    public class ShardingSettings : ConfigurationSection
    {
        [ConfigurationProperty("shard", IsDefaultCollection = false)]
        public Shard Shard
        {
            get { return (Shard) this["shard"]; }
        }

        [ConfigurationProperty("range", IsDefaultCollection = false)]
        public Range Range
        {
            get { return (Range)this["range"]; }
        }
    }

    public class Shard : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new ShardElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ShardElement)element).Id;
        }
    }

    public class Range : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new RangeElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((RangeElement)element).Id;
        }
    }
 
 
    public class ShardElement : ConfigurationElement
    {
        [ConfigurationProperty("id", IsRequired = true, IsKey = true)]
        [IntegerValidator(ExcludeRange = false, MinValue = 0, MaxValue = Int32.MaxValue)]
        public int Id
        {
            get { return (int) this["id"]; }
            set { this["id"] = value; }
        }

        [ConfigurationProperty("connectionString", IsRequired = true)]
        public string ConnectionString
        {
            get { return (string) this["connectionString"]; }
            set { this["connectionString"] = value; }
        }
    }

    public class RangeElement : ConfigurationElement
    {
        [ConfigurationProperty("id", IsRequired = true, IsKey = true)]
        [IntegerValidator(ExcludeRange = false, MinValue = 0, MaxValue = Int32.MaxValue)]
        public int Id
        {
            get { return (int) this["id"]; }
            set { this["id"] = value; }
        }

        [ConfigurationProperty("start", IsRequired = true)]
        [LongValidator(ExcludeRange = false, MinValue = Int64.MinValue, MaxValue = Int64.MaxValue)]
        public long Start
        {
            get { return (long) this["start"]; }
            set { this["start"] = value; }
        }

        [ConfigurationProperty("end", IsRequired = true)]
        [LongValidator(ExcludeRange = false, MinValue = Int64.MinValue, MaxValue = Int64.MaxValue)]
        public long End
        {
            get { return (long) this["end"]; }
            set { this["end"] = value; }
        }

        [ConfigurationProperty("shard", IsRequired = true)]
        [IntegerValidator(ExcludeRange = false, MinValue = 0, MaxValue = Int32.MaxValue)]
        public int Shard
        {
            get { return (int) this["shard"]; }
            set { this["shard"] = value; }
        }
    }
}



/*
  <ShardingSection>
 <shard>
      <add id="0" connectionString="c0"/>
      <add id="1" connectionString="c1"/>
      <add id="2" connectionString="c2"/>
 </shard>
 <range>
      <add id="0" start="0" end="100" shard="0"/>
      <add id="1" start="100" end="200" shard="1"/>
      <add id="2" start="300" end="400" shard="2"/>
 </range>
  </ShardingSection>
*/