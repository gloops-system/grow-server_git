﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UnitTestSharding
{
    class LocalKeyComparer<T> : IEqualityComparer<T>
    {
        private readonly Func<T, object> _keySelector;

        public LocalKeyComparer(Func<T, object> keySelector)
        {
            _keySelector = keySelector;
        }

        public bool Equals(T l, T r)
        {
            return _keySelector(l).Equals(_keySelector(r));
        }

        public int GetHashCode(T obj)
        {
            return _keySelector(obj).GetHashCode();
        }
    }

    public  class Comparer<T> : IComparer<T>
    {
        private readonly Func<T, T, int> _comparer;

        public Comparer(Func<T, T, int> comparer)
        {
            if (comparer == null)
                throw new ArgumentNullException("comparer");
            _comparer = comparer;
        }

        public int Compare(T x, T y)
        {
            return _comparer(x, y);
        }
    }

    internal static class Extensions
    {
        public static bool SequenceEqual<T>(this IEnumerable<T> list, IEnumerable<T> second, Func<T, object> keySelector)
        {
            return list.SequenceEqual(second, new LocalKeyComparer<T>(keySelector));
        }
    }
}
