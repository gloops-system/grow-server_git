﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sharding;

namespace UnitTestSharding
{
    [TestClass]
    public class ShardingTest
    {
        private static readonly Shard[] _shards = new[]
            {
                new Shard {Id = 0, ConnectionString = "c0"}, new Shard {Id = 1, ConnectionString = "c1"},
                new Shard {Id = 2, ConnectionString = "c2"}
            };

        private static readonly Range[] _ranges = new[]
            {
                new Range {Id = 1, Start = 0, End = 100, Shard = _shards[0]},
                new Range {Id = 2, Start = 100, End = 200, Shard = _shards[1]},
                new Range {Id = 3, Start = 200, End = 300, Shard = _shards[2]}
            };

        // TODO Range が重複したとき -> Exception
        // TODO 複数のRangeが同一のShardを指す -> Ok
        // TODO Rangeを外れたshard keyが指定されたとき -> Exception

        [TestMethod]
        public void TestShard()
        {
            var manager = new Manager();
            foreach (var shard in _shards)
                manager.Add(shard);
            manager.Commit();

            var client = manager.Client;
            var s = client.Shards;
            Assert.AreEqual(s.Count, _shards.Length);
            Assert.IsTrue(s.SequenceEqual(_shards, _ => _.Id));
        }

        [TestMethod]
        public void TestRange()
        {
            var manager = new Manager();
            foreach (var shard in _shards)
                manager.Add(shard);

            foreach (var range in _ranges)
                manager.Add(range);

            manager.Commit();

            var client = manager.Client;
            var r = client.Ranges;
            Assert.AreEqual(r.Count, _ranges.Length);
            Assert.IsTrue(r.SequenceEqual(_ranges, _ => _.Id));
        }


        [TestMethod]
        public void TestGetConnectionString()
        {
            var manager = new Manager();
            foreach (var shard in _shards)
                manager.Add(shard);

            foreach (var range in _ranges)
                manager.Add(range);

            manager.Commit();

            var client = manager.Client;
            var cs = client.GetConnectionString(100);
            Assert.AreEqual(cs, _shards[1].ConnectionString);
        }

        [TestMethod]
        public void TestSingletonManager()
        {
            ShardingManager.Initialize(() =>
                {
                    var manager = new Manager();
                    foreach (var shard in _shards)
                        manager.Add(shard);

                    foreach (var range in _ranges)
                        manager.Add(range);

                    manager.Commit();
                    return manager;
                });

            var client = ShardingManager.Instance.Client;
            var cs = client.GetConnectionString(100);
            Assert.AreEqual(cs, _shards[1].ConnectionString);
        }
    }
}
