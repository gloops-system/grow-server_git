﻿using System;
using Sharding;

namespace ShardingExample
{
    class Program
    {
        static void Main()
        {
            ShardingManager.Initialize(() =>
                {
                    var manager = new Manager();
                    manager.LoadFromSection("ShardingSection");
                    return manager;
                });

            var client = ShardingManager.GetInstance().Client;
            var numbers = new[] {0L,1,100,101,200,300};
            foreach (var n in numbers)
            {
                Console.WriteLine("{0}:{1}", n, client.GetConnectionString(n));
            }
        }
    }
}
