Import-Module Servermanager
# IISのIPアドレス及びドメインによる接続制限機能をインストールする
Add-WindowsFeature web-ip-security
# gloopsのIPアドレスをアクセス許可対象に加える
Add-WebConfiguration -Filter /system.webserver/security/ipsecurity -PSPath 'IIS:\' -Value @{ipAddress='158.205.102.41';subnetMask='255.255.255.255';allowed=$true}
Add-WebConfiguration -Filter /system.webserver/security/ipsecurity -PSPath 'IIS:\' -Value @{ipAddress='124.33.202.146';subnetMask='255.255.255.255';allowed=$true}
Add-WebConfiguration -Filter /system.webserver/security/ipsecurity -PSPath 'IIS:\' -Value @{ipAddress='158.205.105.187';subnetMask='255.255.255.255';allowed=$true}
# Edge
Add-WebConfiguration -Filter /system.webserver/security/ipsecurity -PSPath 'IIS:\' -Value @{ipAddress='10.0.0.0';subnetMask='255.0.0.0';allowed=$true}
# 許可したIPアドレス以外のリクエストを拒否するように設定する
Set-WebConfigurationProperty -Filter /system.webserver/security/ipsecurity -Name allowUnlisted -Value $false
