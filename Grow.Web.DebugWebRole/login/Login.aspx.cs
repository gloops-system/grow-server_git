﻿using System;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Grow.Web.DebugWebRole.login
{
    public partial class Login : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
        }

        protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
        {
            var username = Login1.UserName;
            var password = Login1.Password;
            if (Membership.ValidateUser(username, password))
            {
                FormsAuthentication.RedirectFromLoginPage(username, false);
            }
        }
    }
}