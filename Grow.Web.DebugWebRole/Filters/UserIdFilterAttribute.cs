﻿using System.Web.Mvc;
using System.Web.Routing;
using Fango.Core.Util;
using Grow.Core.Application.Util;
using Grow.Web.DebugWebRole.Core;

namespace Grow.Web.DebugWebRole.Filters
{
    public class UserIdFilterAttribute : FilterAttribute, IActionFilter
    {
        private bool IsGet { get; set; }
        public UserIdFilterAttribute(bool isGet)
        {
            IsGet = isGet;
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var userId = UserUtil.GetUserId(filterContext.HttpContext.Request);
            if (IsGet && userId == 0)
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary(
                        new { action = "Index", controller = "UserId" }));
                return;
            }

            filterContext.Controller.ViewBag.UserId = userId;
            filterContext.Controller.ViewBag.DebugDateTime = DateUtil.GetDateTimeOffsetNow();
        }

        public void OnActionExecuted(ActionExecutedContext filterContext) { }
    }
}