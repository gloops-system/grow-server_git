﻿using System;
using System.Web;

namespace Grow.Web.DebugWebRole.Core
{
    public static class UserUtil
    {
        public const string CookieName = "UserId";
        public const string SessionId = "Dummy";
        public static int GetUserId(HttpRequestBase request)
        {
            var myCookie = request.Cookies[CookieName] ?? new HttpCookie(CookieName);
            return Convert.ToInt32(myCookie.Value);
        }
    }
}