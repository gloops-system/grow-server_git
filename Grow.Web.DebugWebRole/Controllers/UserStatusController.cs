﻿using System.Linq;
using System.Web.Mvc;
using Fango.Core.Util;
using Grow.Core.Application.User;
using Grow.Core.Application.Util;
using Grow.Data.User.DataSet;
using Grow.Data.User.DataSet.UserDataSetTableAdapters;
using Grow.Web.DebugWebRole.Models.ViewModel;

namespace Grow.Web.DebugWebRole.Controllers
{
    public class UserStatusController : DebugControllerBase
    {
        [HttpGet]
        public ActionResult Show()
        {
            var userStatusRow = new UserStatusTableAdapter().GetDataByPK(UserId).First();

            if (!string.IsNullOrEmpty((string)TempData["msg"]))
                ViewBag.Message = TempData["msg"];

            return View(new UserStatusViewModel(userStatusRow));
        }

        [HttpGet]
        public ActionResult Edit()
        {
            var userStatusRow = new UserStatusTableAdapter().GetDataByPK(UserId).First();

            return View(new UserStatusViewModel(userStatusRow));
        }

        [HttpPost]
        public ActionResult Edit(UserStatusViewModel userStatusViewModel)
        {
            var userStatusTA = new UserStatusTableAdapter();
            var userStatusRow = userStatusTA.GetDataByPK(UserId).First();
            userStatusRow.Level = userStatusViewModel.Level;
            userStatusRow.Exp = userStatusViewModel.Exp;
            userStatusRow.Gold = userStatusViewModel.Gold;
            userStatusRow.Stamina = userStatusViewModel.Stamina;
            userStatusRow.MaxStamina = userStatusViewModel.MaxStamina;
            userStatusRow.Hp = userStatusViewModel.Hp;
            userStatusRow.Sp = userStatusViewModel.Sp;
            userStatusRow.Attack = userStatusViewModel.Attack;
            userStatusRow.Defense = userStatusViewModel.Defense;
            userStatusRow.CutPercentage = userStatusViewModel.CutPercentage;
            userStatusRow.CriticalPercentage = userStatusViewModel.CriticalPercentage;
            userStatusRow.StunValue = userStatusViewModel.StunValue;
            userStatusRow.StunResistance = userStatusViewModel.StunResistance;
            userStatusRow.StunRecoveryStand = userStatusViewModel.StunRecoveryStand;
            userStatusRow.StunRecoveryDown = userStatusViewModel.StunRecoveryDown;
            userStatusRow.ScoreUp = userStatusViewModel.ScoreUp;
            userStatusRow.LastQuestAreaId = userStatusViewModel.LastQuestAreaId;
            userStatusRow.LastLabyrinthAreaId = userStatusViewModel.LastLabyrinthAreaId;
            userStatusRow.MaxGearBoxNum = userStatusViewModel.MaxGearBoxNum;
            userStatusRow.MaxFriendsNum = userStatusViewModel.MaxFriendsNum;
            userStatusRow.MaxMonsterBoxNum = userStatusViewModel.MaxMonsterBoxNum;
            userStatusRow.MaxItemBoxNum = userStatusViewModel.MaxItemBoxNum;
            userStatusRow.MaxGearBagNum = userStatusViewModel.MaxGearBagNum;
            userStatusRow.MaxUsableMonstersNum = userStatusViewModel.MaxUsableMonstersNum;
            userStatusRow.MaxItemBagNum = userStatusViewModel.MaxItemBagNum;
            //userStatusTA.Update(userStatusRow);
            userStatusTA.UpdateByPK(userStatusRow.Level, userStatusRow.Exp, userStatusRow.UserAvatarId, userStatusRow.UserGearId, userStatusRow.Gold, userStatusRow.Hp, userStatusRow.Sp, userStatusRow.Attack, userStatusRow.Defense, userStatusRow.CutPercentage, userStatusRow.CriticalPercentage, userStatusRow.StunValue, userStatusRow.StunResistance, userStatusRow.StunRecoveryStand, userStatusRow.StunRecoveryDown, userStatusRow.ScoreUp,
                                    userStatusRow.MaxGearBoxNum, userStatusRow.MaxFriendsNum, userStatusRow.MaxMonsterBoxNum, userStatusRow.MaxItemBoxNum, userStatusRow.MaxGearBagNum, userStatusRow.MaxUsableMonstersNum, userStatusRow.MaxItemBagNum, userStatusRow.UpdtTime, userStatusRow.LastQuestAreaId, userStatusRow.LastLabyrinthAreaId, userStatusRow.UserId);

            TempData["msg"] = "更新しました。";

            return RedirectToAction("Show");
        }

        [HttpGet]
        public ActionResult EditIngot()
        {
            if (!string.IsNullOrEmpty((string)TempData["msg"]))
                ViewBag.Message = TempData["msg"];

            var messiahUser = GrowUser.GetInstance(UserId);
            //var useViewModel = new UserViewModel() { UserId = UserId, Ingot = messiahUser.GameCurrency };
            var useViewModel = new UserViewModel() { UserId = UserId, Ingot = 0 };
            return View(useViewModel);
        }

        [HttpPost]
        public ActionResult EditIngot(UserViewModel userViewModel)
        {
            var ta = new UserGameCurrencyTableAdapter();
            var row = ta.GetDataByPK(UserId).FirstOrDefault();

            if (row == null)
            {
                var now = DateUtil.GetDateTimeOffsetNow();
                var dt = new UserDataSet.UserGameCurrencyDataTable();
                var newRow = dt.NewUserGameCurrencyRow();
                newRow.UserId = UserId;
                newRow.Num = userViewModel.Ingot;
                newRow.AddTime = now;
                newRow.UpdtTime = now;
                dt.Rows.Add(newRow);
                ta.Update(dt);
            }
            else
            {
                row.Num = userViewModel.Ingot;
                ta.Update(row);
            }

            TempData["msg"] = "更新しました。";

            return RedirectToAction("EditIngot");
        }
    }
}