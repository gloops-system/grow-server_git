﻿using System;
using System.Web;
using System.Web.Mvc;
using Grow.Web.DebugWebRole.Core;
using Grow.Web.DebugWebRole.Filters;
using Grow.Web.DebugWebRole.Models.ViewModel;

namespace Grow.Web.DebugWebRole.Controllers
{
    [UserIdFilter(false)]
    public class UserIdController : DebugControllerBase
    {
        //
        // GET: /UserId/
        [HttpGet]
        public ActionResult Index()
        {
            int userId = UserId;
            var user = new UserViewModel() { UserId = userId };

            return View(user);
        }

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                int userId = Convert.ToInt32(collection[UserUtil.CookieName]);
                SetUserId(userId);
                return RedirectToRoute("Root");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }

        private void SetUserId(int userId)
        {
            var cookie = Request.Cookies[UserUtil.CookieName] ?? new HttpCookie(UserUtil.CookieName);
            cookie.Value = userId.ToString();
            Response.Cookies.Add(cookie);
        }
    }
}