﻿using System.Web.Mvc;
using Grow.Core.Application.User;

namespace Grow.Web.DebugWebRole.Controllers
{
    public class UserController : DebugControllerBase
    {
        //
        // GET: /UserBase/

        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Title = "ユーザー作成";
            return View();
        }

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {

            var nick = collection["Nick"];
            if (string.IsNullOrEmpty(nick))
                return Index();

            ViewBag.Title = "ユーザー作成結果";
            //var user = EdgeUser.GetInstance(UserId);
            //user.CreateInitialUserData(nick);

            return View();
        }

        [HttpPost]
        public ActionResult Destroy()
        {
            ViewBag.Title = "ユーザー削除結果";
            var user = GrowUser.GetInstance(UserId);
            var view = View();
            try
            {
                user.DeleteUser();
                view.ViewBag.Message = "削除しました";
            }
            catch
            {
                view.ViewBag.Message = "失敗しました";
            }

            return view;
        }
    }
}
