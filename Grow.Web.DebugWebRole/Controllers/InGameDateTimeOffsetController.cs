﻿using System.Web.Mvc;
using Fango.Core.Util;
using Grow.Core.Application.Util;
using Grow.Web.DebugWebRole.Models.ViewModel;

namespace Grow.Web.DebugWebRole.Controllers
{
    public class InGameDateTimeOffsetController : DebugControllerBase
    {
        [HttpGet]
        public ActionResult Index()
        {
            if (!string.IsNullOrEmpty((string)TempData["message"]))
                ViewBag.Message = TempData["message"];

            var dateTimeOffset = DateUtil.GetDateTimeOffsetNow();
            var userViewModel = new UserViewModel() { UserId = UserId, DateTimeOffset = dateTimeOffset };

            return View(userViewModel);
        }

        [HttpPost]
        public ActionResult Edit(UserViewModel userViewModel)
        {
            var dateTime = userViewModel.DateTimeOffset;
            GrowDateUtil.CreateInGameDateTimeOffset(UserId, dateTime.DateTime);
            TempData["message"] = "更新しました。";

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Destroy()
        {
            GrowDateUtil.DeleteInGameDateTimeOffset(UserId);
            TempData["message"] = "初期化しました。";

            return RedirectToAction("Index");
        }
    }
}