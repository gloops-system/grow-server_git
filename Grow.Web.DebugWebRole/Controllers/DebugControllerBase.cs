﻿using System;
using System.Web.Mvc;
using Fango.Core.Util;
using Grow.Core.Application.Util;
using Grow.Web.DebugWebRole.Core;
using Grow.Web.DebugWebRole.Filters;

namespace Grow.Web.DebugWebRole.Controllers
{
    [UserIdFilter(true)]
    public class DebugControllerBase : Controller
    {
        public int UserId { get { return UserUtil.GetUserId(Request); } }
        public DateTimeOffset DebugDateTime { get { return DateUtil.GetDateTimeOffsetNow(); } }
    }
}