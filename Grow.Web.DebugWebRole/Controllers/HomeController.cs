﻿using System.Web.Mvc;
using Grow.Web.DebugWebRole.Filters;

namespace Grow.Web.DebugWebRole.Controllers
{
    [UserIdFilter(false)]
    public class HomeController : DebugControllerBase
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            return View();
        }
    }
}