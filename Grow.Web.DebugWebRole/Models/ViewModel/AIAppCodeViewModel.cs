﻿using System;
using System.Collections.Generic;
using System.Linq;
using Grow.Core.Application.Common;

namespace Grow.Web.DebugWebRole.Models.ViewModel
{
    public class AIAppCodeViewModel
    {
        public IDictionary<string, int> Target { get; private set; }
        public IDictionary<string, int> Parameter { get; private set; }
        public IDictionary<string, int> Condition { get; private set; }
        public IDictionary<string, int> Action { get; private set; }
        public IDictionary<string, int> ActionDetailAttack { get; private set; }
        public IDictionary<string, int> ActionDetailSkill { get; private set; }
        public IDictionary<string, int> ActionDetailAnimation { get; private set; }

        public AIAppCodeViewModel()
        {
            Target = EnumToDictionary<AppCode.AITarget, int>();
            Parameter = EnumToDictionary<AppCode.AIParamater, int>();
            Condition = EnumToDictionary<AppCode.AICondition, int>();
            Action = EnumToDictionary<AppCode.AIAction, int>();
            ActionDetailAttack = EnumToDictionary<AppCode.AIActionDetailAttack, int>();
            ActionDetailSkill = EnumToDictionary<AppCode.AIActionDetailSkill, int>();
            ActionDetailAnimation = EnumToDictionary<AppCode.AIActionDetailAnimation, int>();
        }

        public static IDictionary<string, TValue> EnumToDictionary<TEnum, TValue>()
            where TEnum : struct, IComparable, IFormattable, IConvertible
            where TValue : struct
        {
            var etyp = typeof(TEnum);
            if (!etyp.IsEnum)
                throw new Exception("TEnum is not enum.");
            var vtyp = typeof(TValue);
            if (!(vtyp == Enum.GetUnderlyingType(etyp)))
                throw new Exception("TValue is not underlying type of TEnum.");

            return Enum.GetValues(etyp).Cast<TValue>().ToDictionary(val => Enum.GetName(etyp, val));
        }
    }
}