﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Grow.Data.User.DataSet;

namespace Grow.Web.DebugWebRole.Models.ViewModel
{
    public class UserStatusViewModel
    {
        [Key]
        public int UserId { get; set; }

        [Required]
        [Range(1, short.MaxValue)]
        public short Level { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        public int Exp { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int Gold { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        public int Stamina { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int MaxStamina { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int Hp { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int Sp { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int Attack { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int Defense { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int CutPercentage { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int CriticalPercentage { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int StunValue { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int StunResistance { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int StunRecoveryStand { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int StunRecoveryDown { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int ScoreUp { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int HpBonus { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int SpBonus { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int AttackBonus { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int DefenseBonus { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int ScoreUpBonus { get; set; }

        [Required]
        [Range(1, short.MaxValue)]
        public short MaxGearBoxNum { get; set; }

        [Required]
        [Range(1, short.MaxValue)]
        public short MaxFriendsNum { get; set; }

        [Required]
        [Range(1, short.MaxValue)]
        public short MaxMonsterBoxNum { get; set; }

        [Required]
        [Range(1, short.MaxValue)]
        public short MaxItemBoxNum { get; set; }

        [Required]
        [Range(1, short.MaxValue)]
        public short MaxGearBagNum { get; set; }

        [Required]
        [Range(1, short.MaxValue)]
        public short MaxUsableMonstersNum { get; set; }

        [Required]
        [Range(1, short.MaxValue)]
        public short MaxItemBagNum { get; set; }

        [Required]
        [Range(1, short.MaxValue)]
        public int LastQuestAreaId { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int LastLabyrinthAreaId { get; set; }
     

        public UserStatusViewModel() { }

        public UserStatusViewModel(UserDataSet.UserStatusRow row)
        {
            UserId = row.UserId;
            Level = row.Level;
            Exp = row.Exp;
            Gold = row.Gold;
            Stamina = row.Stamina;
            MaxStamina = row.MaxStamina;
            Hp = row.Hp;
            Sp = row.Sp;
            Attack = row.Attack;
            Defense = row.Defense;
            CutPercentage = row.CutPercentage;
            CriticalPercentage = row.CriticalPercentage;
            StunValue = row.StunValue;
            StunResistance = row.StunResistance;
            StunRecoveryStand = row.StunRecoveryStand;
            StunRecoveryDown = row.StunRecoveryDown;
            ScoreUp = row.ScoreUp;
            LastQuestAreaId = row.LastQuestAreaId;
            LastLabyrinthAreaId = row.LastLabyrinthAreaId;
            MaxGearBoxNum = row.MaxGearBoxNum;
            MaxFriendsNum = row.MaxFriendsNum;
            MaxMonsterBoxNum = row.MaxMonsterBoxNum;
            MaxItemBoxNum = row.MaxItemBoxNum;
            MaxGearBagNum = row.MaxGearBagNum;
            MaxUsableMonstersNum = row.MaxUsableMonstersNum;
            MaxItemBagNum = row.MaxItemBagNum;
        }
    }
}