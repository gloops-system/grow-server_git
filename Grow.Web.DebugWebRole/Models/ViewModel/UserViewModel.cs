﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Grow.Web.DebugWebRole.Models.ViewModel
{
    public class UserViewModel
    {
        [Range(0, int.MaxValue)]
        public int UserId { get; set; }
        public string Nick { get; set; }
        [Range(0, int.MaxValue)]
        public int Dp { get; set; }
        [Range(1, int.MaxValue)]
        public int MaxDp { get; set; }

        [Range(0, int.MaxValue)]
        public int Ingot { get; set; }

        [Required(ErrorMessage = @"入力が必要です。")]
        public DateTimeOffset DateTimeOffset { get; set; }
    }
}