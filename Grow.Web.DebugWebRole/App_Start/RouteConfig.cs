﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Grow.Web.DebugWebRole
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Normal",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "AI",
                url: "{controller}/{action}/{aiId}",
                defaults: new { controller = "Home", action = "Index", aiId = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "AIDetail",
                url: "{controller}/{action}/{aiId}/{id}",
                defaults: new { controller = "Home", action = "Index", aiId = UrlParameter.Optional, id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Root",
                url: "",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}