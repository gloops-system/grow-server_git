﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fango.Core.Data;
using Fango.Data.DataSet.Serialization;
using Fango.Redis.Caching;
using Grow.Data.Guild.DataSet.GuildDataSetTableAdapters;

namespace Grow.Data.Guild.DataSet.Adapter
{
    public class GuildDataRepositoryAdapter
    {
        public int CreateGuild(int FounderId, string Name, string Symbol, string Introduction, int RecruitType, int RequiredTrophy, DateTimeOffset now)
        {
            var returnValue = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var GuildTableAdapter = new GuildTableAdapter())
                {
                    return GuildTableAdapter.CreateGuild(FounderId, Name, Symbol, Introduction, RecruitType, RequiredTrophy, now, now);
                }
            });
            return Convert.ToInt16(returnValue);

        }

        public int UpdateGuild(string Introduction, int RecruitType, int RequiredTrophy, DateTimeOffset now, int GuildId)
        {
            var returnValue = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var GuildTableAdapter = new GuildTableAdapter())
                {
                    return GuildTableAdapter.UpdateGuild(Introduction, RecruitType, RequiredTrophy, now, GuildId);
                }
            });
            return Convert.ToInt16(returnValue);

        }

        public int DisbandGuild(int GuildId)
        {
            var returnValue = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var GuildTableAdapter = new GuildTableAdapter())
                {
                    return GuildTableAdapter.DisbandGuild(GuildId);
                }
            });
            return Convert.ToInt16(returnValue);

        }

        public int LeaveGuild(int UserId)
        {
            var returnValue = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var GuildTableAdapter = new GuildTableAdapter())
                {
                    return GuildTableAdapter.LeaveGuild(UserId);
                }
            });
            return Convert.ToInt16(returnValue);

        }

        public  GuildDataSet.GuildRow GetDataByGuildId(int GuildId)
        {
             var dataRow = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var GuildTableAdapter = new GuildTableAdapter())
                {
                    return GuildTableAdapter.GetDataByGuildId(GuildId).FirstOrDefault();
                }
            });

             return dataRow;
        }
    }
}
