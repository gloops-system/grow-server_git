﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grow.Data.Guild.DataSet.Adapter;

namespace Grow.Data.Guild.DataSet.Container
{
    public class GuildDataContainer
    {
        private readonly Lazy<GuildDataRepositoryAdapter> _guildInfo;
        public GuildDataRepositoryAdapter GuildInfo { get { return _guildInfo.Value; } }

        public GuildDataContainer()
        {
            _guildInfo = new Lazy<GuildDataRepositoryAdapter>(() => new GuildDataRepositoryAdapter(), true);
        }
    }
}
