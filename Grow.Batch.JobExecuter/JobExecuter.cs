﻿using Fango.Batch;
using Fango.Core.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;

namespace Grow.Batch.JobExecuter
{
    public class JobExecuter
    {
        static void Main(string[] args)
        {
            if (args == null || args.Length < 1 || !args[0].Contains(','))
            {
                LoggerManager.DefaultLogger.Error("[異常終了] [USAGE] <fully_qualified_name>,<assembly_name>");
                return;
            }

            var asmArray = args[0].Split(',');
            var type = asmArray[0];
            var asm = asmArray[1];
            if (asmArray.Length > 2)
                Fango.Core.Configuration.LocalConfiguration.LoadSettings(asmArray[2]);

            var typeName = new Stack<string>(type.Split('.')).Pop();
            var start = DateTime.Now;

            try
            {
                var argument = args.ToList();
                var job = LoadJob(type, asm);
                job.ShouldStop = new Func<bool>(() => false);
                var returnCode = job.ExecuteJob(argument.GetRange(1, argument.Count - 1).ToArray());

                if (returnCode == 0)
                    LoggerManager.DefaultLogger.InfoFormat("[正常終了] [{0}] {1}", typeName, DateTime.Now - start);
                else
                    LoggerManager.DefaultLogger.InfoFormat("[異常終了] [{0}] ReturnCode:{1}/{2}",
                                                           typeName, returnCode, DateTime.Now - start);
            }
            catch (WebException wex)
            {
                LoggerManager.DefaultLogger.Warn(string.Format("[異常終了] [{0}] {1}", typeName, DateTime.Now - start), wex);
            }
            catch (Exception ex)
            {
                LoggerManager.DefaultLogger.Error(string.Format("[異常終了] [{0}] {1}", typeName, DateTime.Now - start), ex);
            }
        }

        private static IJob LoadJob(string type, string asm)
        {
            var assembly = Assembly.Load(asm);
            var t = assembly.GetType(type);
            return (IJob)Activator.CreateInstance(t);
        }
    }
}
