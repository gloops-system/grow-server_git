﻿using Fango.Core.Util;
using Fango.Core.Data;
using System;
using System.Linq;
using Grow.Data.Duty.DataSet.DutyDataSetTableAdapters;

namespace Grow.Data.Duty.DataSet.Adapter.Duty
{
    public class FeverTimeDataRepositoryAdapter 
    {
        public DutyDataSet.FeverTimeRow GetActiveFeverByEventId(int eventId, DateTimeOffset now)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new FeverTimeTableAdapter())
                {
                    return adapter.GetActiveFeverByEventId(eventId, now).FirstOrDefault();
                }
            });
            return result;
        }

        public void AddNew(DateTimeOffset startTime, DateTimeOffset endTime, int eventId, int lessStamina, int attackIncrease, int criticalHit, int obtainableGold, int moreMana,
                        int moreTreasure, int dropMoreGold, int dropMoreGear, int dropMoreWeapon, int dropMoreShield, int dropMoreAccessory, int chanceCapturingMonster, int scoreIncrease, DateTimeOffset now)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new FeverTimeTableAdapter())
                {
                    adapter.AddNew(startTime, endTime, eventId, true, lessStamina, attackIncrease, criticalHit, obtainableGold, moreMana, moreTreasure, dropMoreGold,
                                   dropMoreGear, dropMoreWeapon, dropMoreShield, dropMoreAccessory, chanceCapturingMonster, scoreIncrease, now, now);
                }
            });            
        }
        
        public void DeleteAllData()
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new FeverTimeTableAdapter())
                {
                    adapter.DeleteAllData();
                }
            });
        }
    }
}
