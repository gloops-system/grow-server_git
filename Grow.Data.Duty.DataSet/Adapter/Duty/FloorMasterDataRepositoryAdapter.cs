﻿using Grow.Data.Duty.DataSet.Caching;
using Grow.Data.Duty.DataSet.DutyDataSetTableAdapters;
using Fango.Core.Data;
using Fango.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Data.Duty.DataSet.Adapter.Duty
{
    public class FloorMasterDataRepositoryAdapter 
    {
        public DutyDataSet.FloorMasterRow[] GetDataByAreaId(int areaId)
        {
            var dataRows = new DbAccessHandler().ExecuteAction(() =>
            {                             
                var adapter = FloorMasterCacheTableAdapter.GetInstance();
                return adapter.GetDataByAreaId(areaId);
            });
            return dataRows;
        }
    }
}
