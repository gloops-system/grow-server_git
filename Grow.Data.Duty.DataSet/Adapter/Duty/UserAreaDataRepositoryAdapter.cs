﻿using Fango.Core.Util;
using Fango.Core.Data;
using System;
using System.Linq;
using Grow.Data.Duty.DataSet.DutyDataSetTableAdapters;

namespace Grow.Data.Duty.DataSet.Adapter.Duty
{
    public class UserAreaDataRepositoryAdapter 
    {
        public DutyDataSet.UserAreaRow GetDataByUserIdAndAreaId(int userId, int areaId)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new UserAreaTableAdapter())
                {
                    return adapter.GetDataByUserIdAndAreaId(userId, areaId).FirstOrDefault();
                }
            });
            return result;
        }

        public DutyDataSet.UserAreaRow[] GetDataByUserIdAndEventId(int userId, int eventId)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new UserAreaTableAdapter())
                {
                    return adapter.GetDataByUserIdAndEventId(userId, eventId).ToArray();
                }
            });
            return result;
        }

        public long AddNew(int userId, int areaId, int stageId, int eventId, int point, short status)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new UserAreaTableAdapter())
                {
                    var now = DateUtil.GetDateTimeOffsetNow();
                    return adapter.AddNew(userId, areaId, stageId, eventId, point, status, now, now, now);

                }
            });
            return Convert.ToInt64(result);
        }

        public long UpdateByKey(long userAreaId, int point, short status, DateTimeOffset clearDate)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new UserAreaTableAdapter())
                {
                    var now = DateUtil.GetDateTimeOffsetNow();
                    return adapter.UpdateDataByKey(point, status, now, now, userAreaId);
                }
            });
            return Convert.ToInt64(result);
        }

        public void DeleteByUserIdAndEventId(int userId, int areaId)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new UserAreaTableAdapter())
                {
                    adapter.DeleteByUserIdAndEventId(userId, areaId);
                }
            });
        }

        public void DeleteAllDataByEventId(int eventId)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new UserAreaTableAdapter())
                {
                    adapter.DeleteAllDataByEventId(eventId);
                }
            });
        }

        public int GetLabyrinthUserTotalPoint(int userId)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new LabyrinthUserTotalPointTableAdapter())
                {
                    return adapter.GetUserTotalPoint(userId).FirstOrDefault().TotalPoint;
                }
            });
            return result;
        }
    }
}
