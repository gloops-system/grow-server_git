﻿using Fango.Core.Util;
using Fango.Core.Data;
using System;
using System.Linq;
using Grow.Data.Duty.DataSet.DutyDataSetTableAdapters;

namespace Grow.Data.Duty.DataSet.Adapter.Duty
{
    public class UserFloorDataRepositoryAdapter 
    {
        public DutyDataSet.UserFloorRow[] GetDataByUserIdAndAreaId(int userId, int areaId)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new UserFloorTableAdapter())
                {
                    return adapter.GetDataByUserIdAndAreaId(userId, areaId).ToArray();
                }
            });
            return result;
        }

        public void UpdateUserFloorData(int userId, int floorId, int areaId, int stageId, int eventId, int mapId, int point, float spentTime, int rankScore, int rankTime, int rankMonster, short status, DateTimeOffset now)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new UserFloorTableAdapter())
                {
                    adapter.UpdateUserFloorData(userId, floorId, areaId, stageId, eventId, mapId, point, spentTime, rankScore, rankTime, rankMonster, status, now);
                }
            });            
        }

        public void DeleteByUserIdAndAreaId(int userId, int areaId)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new UserFloorTableAdapter())
                {
                    adapter.DeleteByUserIdAndAreaId(userId, areaId);
                }
            });
        }

        public void DeleteByUserIdAndEventId(int userId, int eventId)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new UserFloorTableAdapter())
                {
                    adapter.DeleteByUserIdAndEventId(userId, eventId);
                }
            });
        }

        public void DeleteAllDataByEventId(int eventId)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new UserFloorTableAdapter())
                {
                    adapter.DeleteAllDataByEventId(eventId);
                }
            });
        }
    }
}
