﻿using Fango.Core.Util;
using Fango.Core.Data;
using System;
using System.Linq;
using Grow.Data.Duty.DataSet.DutyDataSetTableAdapters;
using Microsoft.ApplicationBlocks.Data;
using Grow.Data.Duty.DataSet.Properties;

namespace Grow.Data.Duty.DataSet.Adapter.Duty
{
    public class UserAreaHistoryDataRepositoryAdapter 
    {
        public int CountUserIdAndAreaId(int userId, int areaId, short status)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new UserAreaHistoryTableAdapter())
                {
                    return adapter.CountByUserIdAreaIdAndStatus(userId, areaId, status).Value;
                }
            });
            return result;
        }

        public void BulkInsert(DutyDataSet.UserAreaHistoryDataTable userAreaHistoryDataTable)
        {
            var dutyConnStr = Settings.Default.edge_duty_devConnectionString;
            new DbAccessHandler().ExecuteAction(() => SqlHelper.BulkInsert(dutyConnStr, userAreaHistoryDataTable));
        }

        public void DeleteAllDataByEventId(int eventId)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new UserAreaHistoryTableAdapter())
                {
                    adapter.DeleteAllDataByEventId(eventId);
                }
            });
        }
    }
}
