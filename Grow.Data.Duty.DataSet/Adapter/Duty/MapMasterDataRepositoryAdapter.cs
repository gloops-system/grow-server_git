﻿using Grow.Data.Duty.DataSet.DutyDataSetTableAdapters;
using Fango.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Data.Duty.DataSet.Adapter.Duty
{
    public class MapMasterDataRepositoryAdapter 
    {        
        public DutyDataSet.MapMasterRow[] GetEventMaps(int eventId)
        {
            var dataRows = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new MapMasterTableAdapter())
                {
                    return adapter.GetMapByEventId(eventId).ToArray();
                }
            });
            return dataRows;
        }
    }
}
