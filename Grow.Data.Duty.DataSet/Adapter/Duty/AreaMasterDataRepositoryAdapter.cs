﻿using Grow.Data.Duty.DataSet.Caching;
using Grow.Data.Duty.DataSet.DutyDataSetTableAdapters;
using Fango.Core.Data;
using Fango.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grow.Data.Duty.DataSet.Adapter.Duty
{
    public class AreaMasterDataRepositoryAdapter 
    {
        public DutyDataSet.AreaMasterRow GetDataByAreaId(int areaId)
        {
            var dataRow = new DbAccessHandler().ExecuteAction(() =>
            {                             
                var adapter = AreaMasterCacheTableAdapter.GetInstance();
                var areaMaster = adapter.GetDataByAreaId(areaId);
                return areaMaster;
            });
            return dataRow;
        }

        public DutyDataSet.AreaMasterRow[] GetDataByStageIdAndEventId(int stageId, int eventId)
        {
            var allArea = GetAllArea();
            var areaMasters =
                from a in allArea
                where a.StageId == stageId && a.EventId == eventId
                select a;
            return areaMasters.ToArray();
        }

        public DutyDataSet.AreaMasterRow[] GetDataByEventId(int eventId)
        {
            var allArea = GetAllArea();
            var areaMasters =
                from a in allArea
                where a.EventId == eventId
                select a;
            return areaMasters.ToArray();
        }

        public DutyDataSet.AreaMasterRow[] GetAllArea()
        {
            var adapter = AreaMasterCacheTableAdapter.GetInstance();
            return adapter.GetAllArea();
        }

        public DutyDataSet.AreaMasterRow GetNextAreaId(int areaId)
        {
            var areaMaster = GetDataByAreaId(areaId);
            if (areaMaster != null)
            {
                return GetNextAreaId(areaMaster);
            }
            else
            {
                return null;
            }
        }

        public DutyDataSet.AreaMasterRow GetNextAreaId(DutyDataSet.AreaMasterRow areaMaster)
        {
            DutyDataSet.AreaMasterRow areaResult = null;
            var areaMasters = GetDataByStageIdAndEventId(areaMaster.StageId, areaMaster.EventId);
            foreach (var row in areaMasters)
            {
                if (row.AreaIndex > areaMaster.AreaIndex)
                {
                    areaResult = row;
                    break;
                }
            }
            return areaResult;
        }
    }
}
