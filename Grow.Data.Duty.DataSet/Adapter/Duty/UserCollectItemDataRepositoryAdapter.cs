﻿using Fango.Core.Util;
using Fango.Core.Data;
using System;
using System.Linq;
using Grow.Data.Duty.DataSet.DutyDataSetTableAdapters;
using Microsoft.ApplicationBlocks.Data;
using Grow.Data.Duty.DataSet.Properties;

namespace Grow.Data.Duty.DataSet.Adapter.Duty
{
    public class UserCollectItemDataRepositoryAdapter 
    {
        public DutyDataSet.UserCollectItemRow[] GetDataByUserIdAndAreaId(int userId, int areaId)
        {
            var result = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new UserCollectItemTableAdapter())
                {
                    return adapter.GetDataByUserIdAndAreaId(userId, areaId).ToArray();
                }
            });
            return result;
        }

        public void BulkInsert(DutyDataSet.UserCollectItemDataTable userCollectItemDataTable)
        {
            var dutyConnStr = Settings.Default.edge_duty_devConnectionString;
            new DbAccessHandler().ExecuteAction(() => SqlHelper.BulkInsert(dutyConnStr, userCollectItemDataTable));
        }

        public void DeleteDataByUserIdAndAreaId(int userId, int areaId)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new UserCollectItemTableAdapter())
                {
                    adapter.DeleteDataByUserIdAndAreaId(userId, areaId);
                }
            });
        }

        public void DeleteDataByUserIdAreaIdAndFloorId(int userId, int areaId, int floorId)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new UserCollectItemTableAdapter())
                {
                    adapter.DeleteDataByUserIdAreaIdAndFloorId(userId, areaId, floorId);
                }
            });
        }
    }
}
