﻿using Fango.Core.Util;
using Fango.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grow.Data.Duty.DataSet.DutyDataSetTableAdapters;

namespace Grow.Data.Duty.DataSet.Adapter.Duty
{
    public class MatchGroupFloorMapDataRepositoryAdapter 
    {
        public DutyDataSet.MatchGroupFloorMapRow[] GetDataByMatchGroupId(long matchGroupId)
        {
            var dataRows = new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new MatchGroupFloorMapTableAdapter())
                {
                    return adapter.GetDataByPK(matchGroupId).ToArray();
                }
            });
            return dataRows;
        }

        public void AddNew(long matchGroupId, int floorId, int mapId, DateTimeOffset now)
        {
            new DbAccessHandler().ExecuteAction(() =>
            {
                using (var adapter = new MatchGroupFloorMapTableAdapter())
                {
                    adapter.AddNew(matchGroupId, floorId, mapId, now);
                }
            });
        }
    }
}
