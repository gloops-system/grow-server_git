﻿using Fango.Data.Caching;
using System;
using System.Collections.Generic;
using System.Linq;
using Grow.Extensions.Core;
using Grow.Data.Duty.DataSet.DutyDataSetTableAdapters;

namespace Grow.Data.Duty.DataSet.Caching
{
    [Preload]
    public class FloorMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static Dictionary<int, DutyDataSet.FloorMasterRow[]> _cacheGetDataByAreaId;
        private DutyDataSet.FloorMasterRow[] _allData;

        private static readonly FloorMasterCacheTableAdapter Instance = new FloorMasterCacheTableAdapter();
        private FloorMasterCacheTableAdapter()
        {
            Console.WriteLine("Initialize single tone");
        }

        public static FloorMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new FloorMasterTableAdapter())
            {
                _allData = adapter.GetData().ToArray();
            }
            _cacheGetDataByAreaId = _allData.GenerateCache(x => x.AreaId);
        }

        public DutyDataSet.FloorMasterRow[] GetDataByAreaId(int id)
        {
            if (CACHE_ENABLED)
                return GetDataByFromCache(_cacheGetDataByAreaId, id);

            using (var adapter = new FloorMasterTableAdapter())
            {
                return adapter.GetDataByAreaId(id).ToArray();
            }
        }

        public DutyDataSet.FloorMasterRow[] GetAllData()
        {
            if (CACHE_ENABLED)
                return GetDataFromCache(_allData).ToArray();

            using (var adapter = new FloorMasterTableAdapter())
            {
                return adapter.GetData().ToArray();
            }
        }
    }
}