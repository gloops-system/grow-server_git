﻿using System.Collections.Generic;
using System.Linq;
using Fango.Data.Caching;
using Grow.Data.Duty.DataSet.DutyDataSetTableAdapters;
using Grow.Extensions.Core;

namespace Grow.Data.Duty.DataSet.Caching
{
    [Preload]
    internal class AreaMasterCacheTableAdapter : EdgeRefreshableCacheTableAdapter
    {
        private static Dictionary<int, DutyDataSet.AreaMasterRow> _cacheGetDataByPK;
        private DutyDataSet.AreaMasterRow[] _allData;

        private static readonly AreaMasterCacheTableAdapter Instance = new AreaMasterCacheTableAdapter();
        private AreaMasterCacheTableAdapter() { }

        public static AreaMasterCacheTableAdapter GetInstance()
        {
            return Instance;
        }

        protected override void InitializeCache()
        {
            using (var adapter = new AreaMasterTableAdapter())
            {
                _allData = adapter.GetData().ToArray();
            }
            _cacheGetDataByPK = _allData.ToDictionary(x => x.AreaId);
        }

        public DutyDataSet.AreaMasterRow GetDataByAreaId(int id)
        {
            if (CACHE_ENABLED)
                return GetDataByPKFromCache(_cacheGetDataByPK, id);

            using (var adapter = new AreaMasterTableAdapter())
            {
                return adapter.GetDataByAreaId(id).FirstOrDefault();
            }
        }

        public DutyDataSet.AreaMasterRow[] GetAllArea()
        {
            if (CACHE_ENABLED)
                return GetDataFromCache(_allData).ToArray();

            using (var adapter = new AreaMasterTableAdapter())
            {
                return adapter.GetData().ToArray();
            }
        }
    }
}