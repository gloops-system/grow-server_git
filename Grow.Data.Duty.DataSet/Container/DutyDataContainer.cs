﻿using System;
using Grow.Data.Duty.DataSet.Adapter.Duty;

namespace Grow.Data.Duty.DataSet.Container
{
    public class DutyDataContainer
    {
        private readonly Lazy<UserAreaDataRepositoryAdapter> _userArea;
        public UserAreaDataRepositoryAdapter UserArea { get { return _userArea.Value; } }

        private readonly Lazy<UserFloorDataRepositoryAdapter> _userFloor;
        public UserFloorDataRepositoryAdapter UserFloor { get { return _userFloor.Value; } }

        private readonly Lazy<UserAreaHistoryDataRepositoryAdapter> _userAreaHistory;
        public UserAreaHistoryDataRepositoryAdapter UserAreaHistory { get { return _userAreaHistory.Value; } }

        private readonly Lazy<UserCollectItemDataRepositoryAdapter> _userCollectItem;
        public UserCollectItemDataRepositoryAdapter UserCollectItem { get { return _userCollectItem.Value; } }

        private readonly Lazy<MatchGroupFloorMapDataRepositoryAdapter> _matchGroupFloorMap;
        public MatchGroupFloorMapDataRepositoryAdapter MatchGroupFloorMap { get { return _matchGroupFloorMap.Value; } }

        private readonly Lazy<FeverTimeDataRepositoryAdapter> _feverTime;
        public FeverTimeDataRepositoryAdapter FeverTime { get { return _feverTime.Value; } }

        private readonly Lazy<AreaMasterDataRepositoryAdapter> _areaMaster;
        public AreaMasterDataRepositoryAdapter AreaMaster { get { return _areaMaster.Value; } }

        private readonly Lazy<FloorMasterDataRepositoryAdapter> _floorMaster;
        public FloorMasterDataRepositoryAdapter FloorMaster { get { return _floorMaster.Value; } }

        private readonly Lazy<MapMasterDataRepositoryAdapter> _mapMaster;
        public MapMasterDataRepositoryAdapter MapMaster { get { return _mapMaster.Value; } }

        public DutyDataContainer()
        {
            _userArea = new Lazy<UserAreaDataRepositoryAdapter>(() => new UserAreaDataRepositoryAdapter(), true);
            _userFloor = new Lazy<UserFloorDataRepositoryAdapter>(() => new UserFloorDataRepositoryAdapter(), true);
            _userAreaHistory = new Lazy<UserAreaHistoryDataRepositoryAdapter>(() => new UserAreaHistoryDataRepositoryAdapter(), true);
            _userCollectItem = new Lazy<UserCollectItemDataRepositoryAdapter>(() => new UserCollectItemDataRepositoryAdapter(), true);
            _matchGroupFloorMap = new Lazy<MatchGroupFloorMapDataRepositoryAdapter>(() => new MatchGroupFloorMapDataRepositoryAdapter(), true);
            _feverTime = new Lazy<FeverTimeDataRepositoryAdapter>(() => new FeverTimeDataRepositoryAdapter(), true);
            _areaMaster = new Lazy<AreaMasterDataRepositoryAdapter>(() => new AreaMasterDataRepositoryAdapter(), true);
            _floorMaster = new Lazy<FloorMasterDataRepositoryAdapter>(() => new FloorMasterDataRepositoryAdapter(), true);
            _mapMaster = new Lazy<MapMasterDataRepositoryAdapter>(() => new MapMasterDataRepositoryAdapter(), true);
        }
    }
}
